<?php 

    class Admin_user{
        private $_isLogged;
        private $_tblModule;
        private $_rowUserData;
        private $_tblAccessModuleId;
        
        function __construct( $numUserId = NULL ){
            $this->_isLogged = false;
            $this->_tblModule = array();
            $this->_rowUserData = array();
            $this->_tblAccessModuleId = array();
            if( is_numeric($numUserId) ){
                $this->_setData($numUserId);
            }
        }
        
        function changePasswd(){
            //$strPasswordBcrypt = password_hash( $strPassword, PASSWORD_BCRYPT );
        }
        
        /**
         * Kapott usernév és jelszó alapján megkeresi a beléptethető usert, majd lekéri az adatait
         * @global type $objDb
         * @param type $strUsername
         * @param type $strPassword
         */
        function login($strUsername, $strPassword){
            global $objDb;
            //adatok mysqli_real_escape_string
            $strQuery = "
                SELECT
                     su.id
                    ,su.passwd
                FROM
                    sys_user AS su
                WHERE
                    su.delete_date IS NULL
                    AND su.is_active = 1
                    AND su.login = '".$strUsername."'
            ";
            $rowData = $objDb->getRow($strQuery);
            if( 
                !empty($rowData) && is_numeric($rowData['id']) 
                && strlen($rowData['passwd']) == 60 && password_verify($strPassword, $rowData['passwd']) == true 
            ){    
                $this->_setData($rowData['id']); 
                $this->_isLogged = true;
            }
        }
        
        /**
         * Lekéri a paraméterül kapott user id-hoz tartozó user adatait
         * @global type $objDb
         * @param type $numUserId
         */
        private function _setData($numUserId){
            global $objDb;
            //adatok mysqli_real_escape_string
            $strQuery = "
                SELECT
                     su.id
                    ,su.name
                    ,su.login
                    ,su.email
                    ,su.user_group_id
                    ,sug.name AS user_group_name
                FROM
                    sys_user AS su
                JOIN
                    sys_user_group AS sug ON(sug.id = su.user_group_id )
                WHERE
                    su.delete_date IS NULL
                    AND sug.delete_date IS NULL
                    AND su.is_active = 1
                    AND su.id = {$numUserId}
            ";
            $rowData = $objDb->getRow($strQuery);
            if( 
                !empty($rowData) && is_numeric($rowData['id']) 
            ){    
                $this->_rowUserData = $rowData;
                $this->_rowUserData['is_super_user'] = ($rowData['user_group_id'] == 0 ? true : false);
                $this->_setAccessModule(); 
            }
        }
        
        function getNumUserId(){
            return $this->_rowUserData['id'];
        }
        
        function getRowUserData(){
            return $this->_rowUserData;        
        }
        
        function isSuperUser(){
            return $this->_rowUserData['is_super_user'];
        }
        
        function isLogged(){
            return $this->_isLogged;
        }
        
        /**
         * visszaadja az összes modult, amihez a user-nek joga van, vagy ezek közül csak a paraméterül kapott id-nak megfelelőt
         * @param type $numModuleId
         * @return type
         */
        function getTblModule( $numModuleId = null ){
            if( is_numeric($numModuleId) ){
                foreach( $this->_tblModule as $tblModule ){
                    if( isset($tblModule['submodule']) && !empty($tblModule['submodule']) ){
                        foreach( $tblModule['submodule'] as $tblSubModule ){
                            if( $tblSubModule['module']['id'] == $numModuleId ){
                                return $tblSubModule['module'];
                            }
                        }
                    }else if( $tblModule['module']['id'] == $numModuleId ){
                        return $tblModule['module'];
                    }
                }
                return false; //nincs joga a user-nek a megadott modulhoz
            }
            return $this->_tblModule;
        }
        
        /**
         * visszaadja, hogy a kérdéses modulhoz van-e joga a felhasználónak
         * @param type $numModuleId
         * @param $rowEvent - szerkesztésre, törlésre vagy új hozzáadására van-e joga az adott modulon belül
         * @return type
         */
        function isAccessModule( $numModuleId, $rowEvent = null ){
                        
            if( !isset($rowEvent) || (!empty($rowEvent) && $rowEvent['do'] == 'list') ){
                return array_key_exists($numModuleId, $this->_tblAccessModuleId);
                
            } else if (!empty($rowEvent) && $rowEvent['do'] == 'new' && $this->_tblAccessModuleId[$numModuleId]['is_new_allowed'] == 1) {
                return TRUE;
                
            } else if (!empty($rowEvent) && $rowEvent['do'] == 'edit' && is_numeric($rowEvent['id']) && $this->_tblAccessModuleId[$numModuleId]['is_edit_allowed'] == 1) {
                return TRUE;
                
            } else if (!empty($rowEvent) && $rowEvent['do'] == 'del' && is_numeric($rowEvent['id']) && $this->_tblAccessModuleId[$numModuleId]['is_delete_allowed'] == 1) {
                return TRUE;
                
            } 
            return FALSE;
        }
        
        
        /**
         * beállítja a bejelentkezett user jogosultságaihoz tartozó modulokat (mijelenjen meg neki bejelentkezés után)
         * @global type $objDb
         */
        private function _setAccessModule(){
            global $objDb;
            $strQuery = "
                SELECT 
                     mg.id AS module_group_id
                    ,concat(mg.name, (CASE WHEN mg.is_active = 0 THEN ' <i class=''far fa-eye fa-eye-slash''></i>' ELSE '' END)) AS module_group_name
                    ,mg.priority AS module_group_priority
                    ,m.id AS module_id
                    ,concat(m.name, (CASE WHEN m.is_active = 0 THEN ' <i class=''far fa-eye fa-eye-slash''></i>' ELSE '' END)) AS module_name
                    ,m.priority AS module_priority
                    ,".($this->_rowUserData['is_super_user'] === true ? "m.is_new_allowed" : "ua.is_new_allowed")." AS is_new_allowed
                    ,".($this->_rowUserData['is_super_user'] === true ? "m.is_edit_allowed" : "ua.is_edit_allowed")." AS is_edit_allowed
                    ,".($this->_rowUserData['is_super_user'] === true ? "m.is_delete_allowed" : "ua.is_delete_allowed")." AS is_delete_allowed                    
                    ,mg.icon_class AS module_group_icon_class
                    ,m.icon_class AS module_icon_class
                    ,m.url
                FROM 
                    sys_module AS m
                LEFT JOIN	
                    sys_module_group AS mg ON(m.module_group_id = mg.id AND mg.delete_date IS NULL)
                ".($this->_rowUserData['is_super_user'] === true ? "" : 
                    "JOIN sys_user_access AS ua ON(m.id = ua.module_id AND ua.delete_date IS NULL)"
                )."
                WHERE
                    m.delete_date IS NULL
                    AND ".($this->_rowUserData['is_super_user'] === true ? "TRUE" : "(mg.id IS NULL OR mg.is_active = 1)")."
                    AND ".($this->_rowUserData['is_super_user'] === true ? "TRUE" : "m.is_active = 1")."
                    ".($this->_rowUserData['is_super_user'] === true ? "" : "AND ua.user_id = {$this->_rowUserData['id']}")."
                ORDER BY
                     m.priority
                    ,mg.priority
            ";
            //print $strQuery;
            $tblData = $objDb->getAll($strQuery);
            $tblModuleTmp = array();
            foreach( $tblData as $numIdx => $rowData ){
                $this->_tblAccessModuleId[$rowData['module_id']]['is_new_allowed'] = $rowData['is_new_allowed'];
                $this->_tblAccessModuleId[$rowData['module_id']]['is_edit_allowed'] = $rowData['is_edit_allowed'];
                $this->_tblAccessModuleId[$rowData['module_id']]['is_delete_allowed'] = $rowData['is_delete_allowed'];
                
                $tblModuleTmp[$numIdx]['group']['id'] = $rowData['module_group_id'];
                $tblModuleTmp[$numIdx]['group']['name'] = $rowData['module_group_name'];
                $tblModuleTmp[$numIdx]['group']['icon_class'] = $rowData['module_group_icon_class'];
                
                $tblModuleTmp[$numIdx]['module']['id'] = $rowData['module_id'];
                $tblModuleTmp[$numIdx]['module']['name'] = $rowData['module_name'];
                $tblModuleTmp[$numIdx]['module']['icon_class'] = $rowData['module_icon_class'];
                $tblModuleTmp[$numIdx]['module']['url'] = $rowData['url'];
                $tblModuleTmp[$numIdx]['module']['is_new_allowed'] = $rowData['is_new_allowed'];
                $tblModuleTmp[$numIdx]['module']['is_edit_allowed'] = $rowData['is_edit_allowed'];
                $tblModuleTmp[$numIdx]['module']['is_delete_allowed'] = $rowData['is_delete_allowed'];
            }
            foreach( $tblModuleTmp as $numIdx => $tblModule ){
                if( !is_numeric($tblModule['group']['id']) ){
                    $this->_tblModule[$tblModule['module']['id'].'_m']['module'] = $tblModule['module'];
                }else{
                    $this->_tblModule[$tblModule['group']['id'].'_g']['group'] = $tblModule['group'];
                    $this->_tblModule[$tblModule['group']['id'].'_g']['submodule'][$tblModule['module']['id'].'_m']['module'] = $tblModule['module'];
                }
            }
            //print_r($this->_tblModule);
           
            
        }
        
        /**
         * Kijelentkezéskor törli a session értékét
         */
        function logout(){
            $this->_isLogged = false;
            unset($_SESSION['admin_user']);
            session_destroy();
        }
    }
