<?php 
    require_once "ModulePage.php";

    class Module{
        private $_strUrl;
        private $_rowData;
                
        function __construct( $strUrl, $isLoggedUser = true ){
            $this->_rowData = array();

            if( !empty($strUrl) ){
                $this->_strUrl = $strUrl;
                $this->_setData( $isLoggedUser );
            }
        }
        
        public function getRowData ()
        {
            return $this->_rowData;
        }
        
        private function _setData( $isLoggedUser ){
            global $objDb;
            $objUser =& $_SESSION['admin_user'];
            
            //általános modul adatok
            $strQuery = "
                SELECT 
                     m.id AS module_id
                    ,m.name AS module_name
                    ,m.is_new_allowed 
                    ,m.is_edit_allowed
                    ,m.is_delete_allowed
                    ,m.icon_class AS module_icon_class
                    ,m.url AS url
                    ,m.module_group_id
                    ,m.type AS module_type
                    ,mg.name AS module_group_name
                    ,mg.icon_class AS module_group_icon_class
                FROM 
                    sys_module AS m
                LEFT JOIN
                    sys_module_group AS mg ON(mg.id = m.module_group_id)
                WHERE 
                    m.url = '{$this->_strUrl}'
                    AND m.is_active = 1
                    AND m.delete_date IS NULL
                    AND (
                        mg.id IS NULL 
                        OR
                        (
                            mg.is_active = 1
                            AND mg.delete_date IS NULL
                        )
                    )
            ";
            $this->_rowData = $objDb->getRow($strQuery);
            
            if( $isLoggedUser === true ){
                //A bejelentkezett user jogainak megfelelő modul adatokkal felülírjuk
                $tblUserModule = $objUser->getTblModule( $this->_rowData['module_id'] );
                if( empty($tblUserModule) ){
                    return false;
                }else{
                    $this->_rowData['is_new_allowed'] = $tblUserModule['is_new_allowed'];
                    $this->_rowData['is_edit_allowed'] = $tblUserModule['is_edit_allowed'];
                    $this->_rowData['is_delete_allowed'] = $tblUserModule['is_delete_allowed'];
                    $this->_rowData['module_icon_class'] = $tblUserModule['icon_class'];
                    $this->_rowData['url'] = $tblUserModule['url'];
                }
            }
            $this->_rowData['file_name'] = str_replace("-", "_", ucfirst($this->_rowData['url']));
            
            return true;
        }
        
//        public function getRowData(){
//            return $this->_rowData;
//        }
        
        /**
         * visszaadja az összes érvényes modul url-t
         * @global type $objDb
         * @return array
         */
        public static function getRowUrl(){
            global $objDb;
            
            $rowUrl = $objDb->getCol("
                SELECT
                    m.url
                FROM 
                    sys_module AS m
                LEFT JOIN
                    sys_module_group AS mg ON(mg.id = m.module_group_id)
                WHERE 
                    m.url IS NOT NULL
                    AND m.url != ''
                    AND m.is_active = 1
                    AND m.delete_date IS NULL
                    AND (
                        mg.id IS NULL 
                        OR
                        (
                            mg.is_active = 1
                            AND mg.delete_date IS NULL
                        )
                    )
                ORDER BY
                     m.priority
                    ,mg.priority
            ");
            return $rowUrl;
        }
        
        /**
         * visszaadja az összes létező modul adatát (függetlenül a bejelentkezett user jogosultságaitól)
         * @return array
         */
        public static function getAllModuleData ()
        {
            $tblAllModuleData = array();
            $rowModuleUrl = Module::getRowUrl();
            //példányosítja az összes létező url-hez tartozó modult, a modul alap adataival (lehet-e új hozzáadás, szerkesztés, törlés) 
            //és nem veszi figyelembe a bejelentkezett user jogait
            foreach ($rowModuleUrl as $strUrl) {
                $objModule = new Module($strUrl, false);
                $rowData = $objModule->getRowData();
                $tblAllModuleData[$rowData['module_id']] = $rowData;
            }
            return $tblAllModuleData;
        }
        
        public function run(){
            global $objSmarty, $CONF;
            $strTplPagePath = "";
            $strTplPage = "";
            unset($_SESSION['module_data']);
            
            $strFileName = str_replace("-", "_", ucfirst($this->_rowData['url']));
            
            //Ha létezik a funkcióhoz tartozó php fáj a page/module mappában, azt használja
            if( file_exists ($CONF['admin_sys_dir']."page".DIRECTORY_SEPARATOR."module".DIRECTORY_SEPARATOR.$strFileName.".php") ){
                $strClassName = $strFileName;
                require_once $CONF['admin_sys_dir']."page".DIRECTORY_SEPARATOR."module".DIRECTORY_SEPARATOR.$strClassName.".php";          
                $objModulePage = new $strClassName($this->_rowData['module_id']);
                $strTplPage = $objModulePage->getRowEvent()['do'];
                
                $objSmarty->assign("tblListData", $objModulePage->getTblData()); //adatbázisból vett lista nézetben szereplő értékek
                $objSmarty->assign("tblList", $objModulePage->getTblListData()); //listára kerülő adatok + kereshető mezők
                
            }

            $objSmarty->assign("rowModuleData", $this->_rowData); //modul adatai (név, url, főmenü, stb.)
            $_SESSION['module_data'] = $this->_rowData;
            
            //ha szerkesztésre vagy új hozzáadására kattintott, akkor megkeressük a modul url-jének megfelelő tpl fájlt
            if ( 
                !empty($strTplPage) && in_array($strTplPage, array('new', 'edit')) 
                && file_exists ($CONF['admin_tpl_dir']."page".DIRECTORY_SEPARATOR."module".DIRECTORY_SEPARATOR.$strFileName.".tpl")
            ) 
            {
                $objSmarty->assign("strTplPage", $strTplPage);
                $strTplPagePath = "page".DIRECTORY_SEPARATOR."module".DIRECTORY_SEPARATOR.$strFileName.".tpl";
            
            } else if ( !empty($this->_rowData['module_type']) ){
                //module lista típusának megfelelő tpl-et húzza be (pl. grid vagy list)    
                $strTplPagePath = "page".DIRECTORY_SEPARATOR."Module_".$this->_rowData['module_type'].".tpl";
            }
            
            $objSmarty->display($strTplPagePath);
        }
    }
        