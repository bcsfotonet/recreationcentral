<?php 
    require_once "function.php";
    require_once 'Pagination.php';

    class ModulePage {
        protected $_objUser;
        protected $_tblData;
        protected $_tblListData;
        protected $_tblLoggedUserData;
        protected $_numModuleId;
        protected $_rowEvent;
        protected $_rowMessage;
        protected $_objPagination;
        
        function __construct( $numModuleId ){
            $this->_numModuleId = $numModuleId;
            $this->_tblData = array();
            $this->_tblListData = array();
            $this->_tblLoggedUserData = array();
            $this->_rowEvent = array();
            $this->_rowMessage = array("type" => null, "msg" => null);
            $this->_objPagination = new Pagination();
            $this->isAccessUser();
            
        }
        
        /**
         * Vizsgálja, hogy csak bejelentkezés és jogosultság esetén lehessen megnyitni a modulokat
         * @return boolean
         */
        private function isAccessUser(){
            if( isset($_SESSION['admin_user']) ){
                $this->_objUser =& $_SESSION['admin_user'];
            }
            $this->setRowEvent();

            //be van jelentkezve és van joga a modulhoz (vagy új felvételhez, szerkesztéshez, törléshez)
            if( is_object($this->_objUser) && $this->_objUser->isLogged() === true && $this->_objUser->isAccessModule( $this->_numModuleId, $this->_rowEvent ) ){
                $this->_tblLoggedUserData = $this->_objUser->getRowUserData();
                $this->_tblUserModule = $this->_objUser->getTblModule($this->_numModuleId);
                
            }else{
                //ha valami hiba miatt nem létezik az objUser, kijelentkeztetjük
                if( !is_object($this->_objUser) || $this->_objUser->isLogged() !== true ){
                    unset($_SESSION['admin_user']);
                }
                //ha nincs belépve vagy nincs joga a modulhoz, akkor főoldalra dobjuk (ha nincs belépve, ez megegyezik a loginnal)
                $this->setHeaderLocation();
                return false;
            }
            
            return true;
        }
        
        /**
         * Ha valamihez nincs joga visszadobjuk arra az oldalra, amit még láthat
         * @param string $strUrl
         */
        protected function setHeaderLocation ($strUrl = "")
        {
            global $CONF;
            header("Location: {$CONF['admin_base_url']}{$strUrl}");
            exit;
        }
        
        /**
         * url alapján visszaadja, hogy új felvételére (edit és nincs id), szerkesztésre (edit és van id) vagy törlésre kattintott-e (del és van id)
         * ha $rowUrl csak egy elemű, akkor lista nézet
         * @global type $rowUrl
         */
        private function setRowEvent ()
        {
            global $rowUrl;
            $rowEvent = array();
            
            if (isset($rowUrl[1]) && in_array($rowUrl[1], array('edit', 'del')) && isset($rowUrl[2]) && is_numeric($rowUrl[2])) {
                $rowEvent['do'] = $rowUrl[1];
                $rowEvent['id'] = $rowUrl[2];
            } elseif (isset($rowUrl[1]) && $rowUrl[1] == 'edit') {
                $rowEvent['do'] = 'new';
            } elseif (!isset($rowUrl[1]) || (isset($rowUrl[1]) && is_numeric($rowUrl[1]))) {
                $rowEvent['do'] = 'list';
                $rowEvent['page'] = (isset($rowUrl[1]) && is_numeric($rowUrl[1])) ? $rowUrl[1] : 1;
            }
//             print_r($rowEvent);
            $this->_rowEvent = $rowEvent;
        }
        
        public function getRowEvent ()
        {
            return $this->_rowEvent;
        }
        
        
        private function getData ()
        {            
            return true;
        }
        
        /**
         * Gyerek osztályban kap értéket a funkciónak megfelelően (minden lista elemet lekérdez)
         * @return array
         */
        public function getTblData ()
        {
            return $this->_tblData;
        }
        
        public function getTblListData () 
        {
            return $this->_tblListData;
        }
        
        public function getTblLoggedUserData ()
        {
            return $this->_tblLoggedUserData;
        }
        
        /**
         * Törli a sort
         * @global type $objDb
         * @param integer $numRowId
         * @param string $strTable
         */
        protected static function removeMainTableData ($numRowId, $strTable)
        {
            global $objDb, $objUser;
            $rowMessage = array('type' => 'error', 'msg' => null);
            
            $dbres = $objDb->query("
                UPDATE
                    {$strTable}
                SET
                     delete_date = now()
                    ,delete_user_id = {$objUser->getNumUserId()}
                WHERE
                    id = {$numRowId}
                    AND delete_date IS NULL
            ");
            if ($dbres === true) {
                $rowMessage = array('type' => 'ok', 'msg' => 'Az adatok törlése sikeresen megtörtént.');
            } else {
                $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt az adatok törlése során.');
            }
            
            return $rowMessage;
        }
        
        /**
         * Modulhoz tartozó fájlfeltöltések kezelésére
         * @param array $tblFileData
         * @param array $tblPostData
         * @param integer $numId
         * @return boolean
         */
        protected static function saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize = null)
        {
            global $objDb, $CONF, $objUser;
            
            $rowColumn = explode("_", $rowTable['column']);
            $strLangDir = (in_array(end($rowColumn), array("en", "ro")) ? end($rowColumn).DIRECTORY_SEPARATOR : "");
            
            if (
                (isset($tblPostData[$rowTable['column'].'_delete']) && $tblPostData[$rowTable['column'].'_delete'] == "true")
                || (
                    isset($tblFileData['name'][$rowTable['column']]) && isset($tblFileData['tmp_name'][$rowTable['column']])
                    && !empty($tblFileData['name'][$rowTable['column']]) && !empty($tblFileData['tmp_name'][$rowTable['column']])
                )
            ) {
                $strQueryUpdate = "
                    UPDATE
                        ".$rowTable['table']."
                    SET
                        ".$rowTable['column']." = NULL
                    WHERE
                        id = {$numId}
                ";
                $dbres = $objDb->query($strQueryUpdate);
                if ($dbres === FALSE) {
                    return FALSE;
                } else {
                    //TODO törlést rekurzív függvénnyel megoldani és kivinni a function.php-ba
                    $rowDelFile = glob($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR.$strLangDir.'*'); //fájlnevek lekérése
                    foreach($rowDelFile as $strFile){
                        if (is_dir($strFile)) {
//                            var_dump($strFile);
                            $rowDir = explode(DIRECTORY_SEPARATOR, $strFile);
                            if( $rowTable['column']=='image' && in_array(end($rowDir), array('big', 'small', 'original')) ){
                            
                                $rowDelFile2 = glob($strFile.DIRECTORY_SEPARATOR.$strLangDir.'*'); //fájlnevek lekérése
                                foreach($rowDelFile2 as $strFile2){
                                    if (is_file($strFile2)) {
                                        unlink($strFile2);
                                    }
                                }
                                rmdir($strFile);
                            }
                        } elseif (is_file($strFile)) {
                            if($rowTable['column']=='filename'){
                                unlink($strFile);
                            }
                        }
                    }
                }
            }
            
            if(
                isset($tblFileData['name'][$rowTable['column']]) && isset($tblFileData['tmp_name'][$rowTable['column']])
                && !empty($tblFileData['name'][$rowTable['column']]) && !empty($tblFileData['tmp_name'][$rowTable['column']])
            ){
                $strNewFileName = createUrl(pathinfo($tblFileData['name'][$rowTable['column']])['filename'], 64)."-".date("ymdHis");
                if (!empty($tblImgSize)) {
                    $strSaveDir = $CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."original".DIRECTORY_SEPARATOR.$strLangDir;
                } else {
                    $strSaveDir = $CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR.$strLangDir;
                }
                
                $returnSaveFile = saveFile (
                     $strSaveDir
                    ,$tblFileData['name'][$rowTable['column']]
                    ,$tblFileData['tmp_name'][$rowTable['column']]
                    ,$strNewFileName
                );
                
                if ($returnSaveFile === FALSE) {
                    return FALSE;
                } else { //ha rendben volt a mentés, visszaadja a mentett nevet kiterjesztéssel együtt
                    
                    if (!empty($tblImgSize)) {
                        //big és small mappák létrehozása, ha nem léteznek még
                        if (!file_exists($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."big".$strLangDir) && !is_dir($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."big".$strLangDir)) {
                            mkdir($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."big".$strLangDir, 0777, true);
                        }
                        if (!file_exists($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."small".$strLangDir) && !is_dir($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."small")) {
                            mkdir($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."small".$strLangDir, 0777, true);
                        }
                        
                        //A big könyvtárba kerül az átméretezett kép a háttérképhez képest középre helyezve
                        $bigDst=$CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."big".DIRECTORY_SEPARATOR.$strLangDir.$returnSaveFile;
                        $bigWidth = $tblImgSize['big']['width'];
                        $bigHeight = $tblImgSize['big']['height'];
                        bgImgDisplayer(
                            $CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."original".DIRECTORY_SEPARATOR.$strLangDir.$returnSaveFile
                            ,$bigDst
                            ,$bigWidth
                            ,$bigHeight
                            ,255,255,255
                        );
                        
                         //A small könyvtárba kerül a kép
                        $bigDst=$CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."small".DIRECTORY_SEPARATOR.$strLangDir.$returnSaveFile;
                        $bigWidth = $tblImgSize['small']['width'];
                        $bigHeight = $tblImgSize['small']['height'];
                        bgImgDisplayer(
                            $CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."original".DIRECTORY_SEPARATOR.$strLangDir.$returnSaveFile
                            ,$bigDst
                            ,$bigWidth
                            ,$bigHeight
                            ,255,255,255
                        );
                    }
                    $strQueryUpdate = "
                        UPDATE
                            ".$rowTable['table']."
                        SET
                             ".$rowTable['column']." = '{$returnSaveFile}'
                            ,modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                        WHERE
                            id = {$numId}
                    ";
                    $dbres = $objDb->query($strQueryUpdate);
                    if ($dbres === FALSE) {
                        return FALSE;
                    }
                }
            }
            
            return TRUE;
        }
        
        protected static function getValidateData ($strTable)
        {
            global $objDb, $MYSQL;
            $tblValidate = array();
            
            $tblValidateData = $objDb->getAll("
                SELECT
                      COLUMN_NAME
                    , COLUMN_DEFAULT
                    , IS_NULLABLE
                    , DATA_TYPE
                    , CHARACTER_MAXIMUM_LENGTH
                    , NUMERIC_PRECISION
                    , COLUMN_TYPE
                    , (CASE WHEN IS_NULLABLE = 'NO' AND COLUMN_DEFAULT IS NULL THEN 1 ELSE 0 END) AS is_fill
                    , COALESCE(CHARACTER_MAXIMUM_LENGTH, NUMERIC_PRECISION) AS maxlength
                    , (CASE WHEN CHARACTER_MAXIMUM_LENGTH IS NOT NULL THEN 'text' WHEN NUMERIC_PRECISION IS NOT NULL THEN 'number' ELSE null END) AS input_type
                FROM
                    information_schema.columns
                WHERE
                    table_schema = '".$MYSQL['dbName']."'
                    AND table_name = '".$strTable."'
            ");
            if ($tblValidateData !== FALSE) {
                foreach ($tblValidateData as $rowValidate) {
                    $tblValidate[$rowValidate['COLUMN_NAME']]['is_fill'] = $rowValidate['is_fill'];
                    $tblValidate[$rowValidate['COLUMN_NAME']]['maxlength'] = $rowValidate['maxlength'];
                    $tblValidate[$rowValidate['COLUMN_NAME']]['type'] = $rowValidate['DATA_TYPE'];
                    $tblValidate[$rowValidate['COLUMN_NAME']]['input_type'] = $rowValidate['input_type'];
                }
            }
            return $tblValidate;
        }
        
//         /**
//          * Fájlok megfelelő mappába történő mentésére szolgál
//          * @param String $strSavePath - Fájl mentési helye 
//          * @param String $strFileName - Fájl eredeti teljes neve (kiterjesztéssel együtt) $_FILES['name']
//          * @param String $strTmpPath - Fájl ideiglenes helye $_FILES['tmp_name']
//          * @param String $strNewFileName - Fájl új neve, amivel menteni szeretnénk, ha eltér az eredetitől (kiterjesztés nélkül)
//          * @return boolean - sikeres / nem sikeres fájlmozgatás
//          */
//         protected function saveFile ($strSavePath, $strFileName, $strTmpPath, $strNewFileName = null)
//         {
// //             var_dump($strSavePath);
//             if (!file_exists($strSavePath) && !is_dir($strSavePath)) {
//                 mkdir($strSavePath);
//             }
            
//             $rowPathInfo = pathinfo($strFileName);
//             $strExt = $rowPathInfo['extension']; //fájl kiterjesztése
//             $strSaveName = (!empty($strNewFileName) ? $strNewFileName.".".$strExt : $strFileName);
            
//             $strSaveFilePath = $strSavePath.$strSaveName;
//             var_dump($strSaveFilePath);
            
//             return move_uploaded_file($strTmpPath, $strSaveFilePath);
//         }
        
    }
    