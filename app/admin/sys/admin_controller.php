<?php

    require_once "vendor".DIRECTORY_SEPARATOR."smarty-2.6.31".DIRECTORY_SEPARATOR."libs".DIRECTORY_SEPARATOR."Smarty.class.php";
    require_once "DB.php";
    require_once "function.php";
    require_once "Admin_user.php";
    require_once "Module.php";
    
    session_name($CONF['project_name']."_ADMSID");
    session_start();
    
    $objDb = new DB();
    $objDb->connect();
        
    $rowUrl = array();
    $strPage = 'Login';
    $strTplPagePath = 'page'.DIRECTORY_SEPARATOR.'Login';
    
    $objSmarty = new Smarty; 
    $objSmarty->template_dir = $CONF['admin_tpl_dir'];
    $objSmarty->compile_dir = $CONF['admin_smarty_dir']."templates_c".DIRECTORY_SEPARATOR;
    $objSmarty->config_dir = $CONF['admin_smarty_dir']."configs".DIRECTORY_SEPARATOR;
    $objSmarty->cache_dir = $CONF['admin_smarty_dir']."cache".DIRECTORY_SEPARATOR;
    
    $objSmarty->caching = false;
    $objSmarty->compile_check = false;
    $objSmarty->force_compile = true;
    
    $objSmarty->plugins_dir[] = "vendor".DIRECTORY_SEPARATOR."smarty-2.6.30".DIRECTORY_SEPARATOR."libs".DIRECTORY_SEPARATOR;
    $objSmarty->plugins_dir[] = $CONF['admin_smarty_dir']."functions".DIRECTORY_SEPARATOR;
    
    $objSmarty->assign("CONF", $CONF);
    
    //$objSmarty->setPage();
    //$objSmarty->setFilters();
    
    if( !empty($_REQUEST['p']) ){
        $rowUrl = explode('/', $_REQUEST['p']);
    }
    
    if( !empty($rowUrl[0]) ){
        $strPage = ucfirst($rowUrl['0']);
    }
    
    
    if( isset($_SESSION['admin_user']) ){
        $objUser =& $_SESSION['admin_user'];
        $objSmarty->assign("tblLoggedUserData", $objUser->getRowUserData());
        $objSmarty->assign("tblModule", $objUser->getTblModule());
    }    
    
    if( 
        isset($objUser) 
        && is_file($CONF['admin_sys_dir']."page".DIRECTORY_SEPARATOR.$strPage.".php") 
        && is_file($CONF['admin_tpl_dir']."page".DIRECTORY_SEPARATOR.$strPage.".tpl") 
    ){ //létezik azonos nevű php és tpl is
        $strTplPagePath = "page".DIRECTORY_SEPARATOR.$strPage.".tpl";
        require_once "page".DIRECTORY_SEPARATOR.$strPage.".php";
        
    }elseif( isset($objUser) && is_file($CONF['admin_sys_dir']."page".DIRECTORY_SEPARATOR.$strPage.".php") ){ 
        //csak azonos nevű php létezik pl. logout
        require_once "page".DIRECTORY_SEPARATOR.$strPage.".php";
        
    }elseif( isset($objUser) && in_array(strtolower($strPage), Module::getRowUrl()) ){ 
        //module táblában mentett url-ek közül való
        $objPage = new Module(strtolower($strPage));
        $objPage->run(); //tpl fájlt a module típusától függően választ osztályon belül
    
    }else{ //login oldal
        $strTplPagePath = "page".DIRECTORY_SEPARATOR."Login.tpl";
        require_once "page".DIRECTORY_SEPARATOR."Login.php";
        
        $strPage = "Login";
    }
    
    if( !isset($objPage) || !is_object($objPage) ){
        $objPage = new $strPage();
        $objPage->run($strTplPagePath);
    }
    