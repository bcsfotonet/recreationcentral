<?php 
    class Home{
        private $_objUser;
        private $_tblData;
        private $_tblModule;
        
        function __construct(){
            if( isset($_SESSION['admin_user']) ){
                $this->_objUser =& $_SESSION['admin_user'];
            }
            
            $this->_tblData = array();
            $this->_tblModule = array();
        }
        
        private function getData(){
            global $CONF;
            
            if( is_object($this->_objUser) && $this->_objUser->isLogged() === true ){
                $this->_tblData = $this->_objUser->getRowUserData();
                $this->_tblModule = $this->_objUser->getTblModule();
            }else{
                unset($_SESSION['admin_user']);
                header("Location: {$CONF['admin_base_url']}");
                exit;
            }            
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblModule", $this->_tblModule);
            }
//            print_r($this->_tblModule);
            $objSmarty->display($strTplPagePath);
        }
    }
    