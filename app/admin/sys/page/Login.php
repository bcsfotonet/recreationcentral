<?php 
    class Login{
        private $_objUser;
//        private $_tblData;
//        private $_tblModule;
        private $_strMsg;
        
        function __construct(){
            if( !isset($_SESSION['admin_user']) ){
                $this->_objUser = new Admin_user();
                $_SESSION['admin_user'] =& $this->_objUser;
            }else{
                $this->_objUser =& $_SESSION['admin_user'];
            }
            
//            $this->_tblData = array();
//            $this->_tblModule = array();
            $this->_strMsg = null;
        }
        
        private function getData(){
//            global $objDb;
            global $CONF;
            
            $this->_objUser->login($_POST['username_input'], $_POST['passwd_input']);
            if( $this->_objUser->isLogged() === true ){
                header("Location: {$CONF['admin_base_url']}Home");
                exit;
                
                //$this->_tblData = $this->_objUser->getRowUserData();
                //$this->_tblModule = $this->_objUser->getTblModule();
            }else{
                unset($_SESSION['admin_user']);
                $this->_strMsg = "A megadott felhasználónév vagy jelszó nem megfelelő!";
            }
            
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->_objUser->isLogged() === true ){
                header("Location: {$CONF['admin_base_url']}Home");
                exit;
            }
            //TODO levizsgálni a post adatokat
            if( isset($_POST['username_input']) && isset($_POST['passwd_input']) ){
                $this->getData();
                
            }
            $objSmarty->assign("strMsg", $this->_strMsg);
            $objSmarty->display($strTplPagePath);
        }
    }
    