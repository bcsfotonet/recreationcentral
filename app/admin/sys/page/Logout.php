<?php 
    class Logout{
        private $_objUser;
        
        function __construct(){
            if( !isset($_SESSION['admin_user']) ){
                $this->_objUser = new Admin_user();
                $_SESSION['admin_user'] =& $this->_objUser;
            }else{
                $this->_objUser =& $_SESSION['admin_user'];
            }
        }
        
        private function getData(){
            global $CONF;
            
            if( $this->_objUser->isLogged() === true ){
                $this->_objUser->logout();
                header("Location: {$CONF['admin_base_url']}Login");
                exit;
            }else{
                unset($_SESSION['admin_user']);
            }
            
            return true;
        }
        
        public function run($strTplPagePath = null){
            $this->getData();
            
            return true;
        }
    }
    