<?php 
    require_once "Email.php";
    
    class Password{
        private $_objEmail;
        
        function __construct(){
            $this->_objEmail = new Email();
        }
        
        private function getData(){
            global $CONF;
            
            $this->_objEmail->changeAdminPassword(array());
//            if( $this->_objEmail->changeAdminPassword() === true ){
//                $this->_objUser->logout();
//                header("Location: {$CONF['admin_base_url']}Login");
//                exit;
//            }else{
//                unset($_SESSION['admin_user']);
//            }
            
            return true;
        }
        
        public function run($strTplPagePath = null){
            $this->getData();
            
            return true;
        }
    }
    