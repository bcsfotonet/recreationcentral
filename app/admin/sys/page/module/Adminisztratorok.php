<?php 
    class Adminisztratorok extends ModulePage {
        private $_strTable;
        private $_numEditedUserId;
        private $_rowEditedUserData;
        
        function __construct( $numModuleId ){
            parent::__construct( $numModuleId );            
            
            $this->_numEditedUserId = null;
            $this->_tblListData = array(
                "column" => array(
                     "name" => "Név"
                    ,"login" => "Azonosító"
                )
                ,"search" => array()
            );
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedUserId = $this->_rowEvent['id'];
//                var_dump($_POST);
//TODO post ellenőrzés
                if (isset($_POST['adminisztratorok_field_name'])) {
                    $this->saveData($_POST);
                }
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedUserId = $this->_rowEvent['id'];
                $this->removeData($this->_numEditedUserId);
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {
                if (isset($_POST['adminisztratorok_field_name'])) {
                    if ($this->saveData($_POST) === TRUE){
                        $this->setHeaderLocation("adminisztratorok/edit/".$this->_numEditedUserId);
                    }
                }
            }
            $this->run();
        }
        
        private function getData(){
            global $objDb;
            $tblEditedUserAccessModule = array();
            
            $this->_tblData = $objDb->getAllIdIdx("
                SELECT 
                    * 
                FROM 
                    sys_user 
                WHERE 
                    delete_date IS NULL
            ");
            
            if (is_numeric($this->_numEditedUserId)) {
                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!isset($this->_tblData[$this->_numEditedUserId])) {
                    $this->setHeaderLocation("adminisztratorok");
                }
                $objEditedUser = new Admin_user($this->_numEditedUserId);
                $tblEditedUserAccessModule = $objEditedUser->getTblModule();
                $this->_rowEditedUserData = $objEditedUser->getRowUserData();
//                print_r($tblEditedUserAccessModule);
            }
            $tblAllModuleData = Module::getAllModuleData();
            foreach ($tblAllModuleData as $rowModuleData) {
                if (is_numeric($rowModuleData['module_group_id'])) {
                    $this->_tblPermission[$rowModuleData['module_group_id'].'_g']['name'] = $rowModuleData['module_group_name'];
                    $this->_tblPermission[$rowModuleData['module_group_id'].'_g']['icon_class'] = $rowModuleData['module_group_icon_class'];
                    $this->_tblPermission[$rowModuleData['module_group_id'].'_g']['modules'][] = $rowModuleData['module_id'];
                    if (!empty($tblEditedUserAccessModule) && isset($tblEditedUserAccessModule[$rowModuleData['module_group_id'].'_g'])) {
                        $this->_tblPermission[$rowModuleData['module_group_id'].'_g']['access_user'] = true;
                    }else{
                        $this->_tblPermission[$rowModuleData['module_group_id'].'_g']['access_user'] = false;
                    }
                }else{
                    $this->_tblPermission[$rowModuleData['module_id'].'_m']['name'] = $rowModuleData['module_name'];
                    $this->_tblPermission[$rowModuleData['module_id'].'_m']['icon_class'] = $rowModuleData['module_icon_class'];
                    $this->_tblPermission[$rowModuleData['module_id'].'_m']['modules'][] = $rowModuleData['module_id'];
                    if (!empty($tblEditedUserAccessModule) && isset($tblEditedUserAccessModule[$rowModuleData['module_id'].'_m'])) {
                        $this->_tblPermission[$rowModuleData['module_id'].'_m']['access_user'] = true;
                    }else{
                        $this->_tblPermission[$rowModuleData['module_id'].'_m']['access_user'] = false;
                    }
                }
                
            }
//            print_r($this->_tblPermission);
            return true;
        }
        
        /**
         * Törli a usert
         * @global type $objDb
         * @param type $numUserId
         */
        private function removeData ($numUserId)
        {
            global $objDb;
//TODO js confirm-ra kell tenni
            $objDelUser = new Admin_user($numUserId);
            $rowDelUserData = $objDelUser->getRowUserData();
            
            if (
                    isset($this->_tblUserModule['is_delete_allowed']) 
                    && $this->_tblUserModule['is_delete_allowed'] == 1
                    && $rowDelUserData['id'] != $this->_tblLoggedUserData['id'] //saját magát nem törölheti
                    && ( //super usert csak super user törölhet
                            $rowDelUserData['is_super_user'] != 1
                            || $this->_tblLoggedUserData['is_super_user'] == 1
                    )
            ) {
                $dbres = $objDb->query("
                    UPDATE
                        sys_user
                    SET
                         delete_date = now()
                        ,delete_user_id = {$this->_tblLoggedUserData['id']}
                    WHERE 
                        id = {$numUserId}
                ");
                 
            }
            
            unset($objDelUser);
        }
        
        private function saveData ($tblNewData) 
        {
            global $objDb;
            
            $strQueryUser = "";
            $strQueryPermission = "";
            
            if ( //szerkesztés
                isset($tblNewData['adminisztratorok_field_id']) && is_numeric($tblNewData['adminisztratorok_field_id'])
                && $this->_rowEvent['do'] == 'edit'
                && isset($this->_tblUserModule['is_edit_allowed']) 
                && $this->_tblUserModule['is_edit_allowed'] == 1
            ) {
                $objEditUser = new Admin_user($tblNewData['adminisztratorok_field_id']);
                $rowEditUserOldData = $objEditUser->getRowUserData();
                                
                //super user jogait nem szerkeszthetik
                if ($rowEditUserOldData['is_super_user'] != 1) {
                    if (isset($tblNewData['adminisztratorok_field_permission']) && !empty($tblNewData['adminisztratorok_field_permission'])){
                        $strModuleId = implode(",", array_keys($tblNewData['adminisztratorok_field_permission']));
                        $rowModuleId = explode(",", $strModuleId);
                        
                        $dbres = $objDb->query("
                            DELETE 
                            FROM
                                sys_user_access
                            WHERE
                                user_id = {$tblNewData['adminisztratorok_field_id']}
                                AND module_id NOT IN ({$strModuleId})
                        ");
                        $rowInsert = array();
                        foreach ($rowModuleId as $numModuleId) {
                            if (!$objEditUser->isAccessModule($numModuleId)) {
                                $rowInsert[] = "
                                    ({$tblNewData['adminisztratorok_field_id']}, {$numModuleId})
                                ";
                            }
                        }
                        if (!empty($rowInsert)) {
                            $strQueryPermission = "
                                INSERT INTO 
                                    sys_user_access(user_id, module_id)
                                VALUES
                            " . implode(",", $rowInsert);
                        }
                        
//                        $dbres = $objDb->query("
//                            
//                        ");
                    }
                }
                
                
                $strQueryUser = "
                    UPDATE
                        sys_user
                    SET
                         modify_date = now()
                        ,modify_user_id = {$this->_tblLoggedUserData['id']}
                        ".( $rowEditUserOldData['name'] != $tblNewData['adminisztratorok_field_name'] ? ",name = '{$tblNewData['adminisztratorok_field_name']}'" : "")."
                        ".( $rowEditUserOldData['login'] != $tblNewData['adminisztratorok_field_login'] ? ",login = '{$tblNewData['adminisztratorok_field_login']}'" : "")."
                        ".( $rowEditUserOldData['email'] != $tblNewData['adminisztratorok_field_email'] ? ",email = '{$tblNewData['adminisztratorok_field_email']}'" : "")."
                ";
                
                if ( //a user a saját jelszavát megváltoztathatja
                    intval($rowEditUserOldData['id']) === intval($this->_tblLoggedUserData['id']) 
                    && !empty($tblNewData['adminisztratorok_field_password'])
                    && strlen($tblNewData['adminisztratorok_field_password']) >= 8
                )
                {   
                    $strQueryUser .= "
                        ,passwd = '".password_hash( $tblNewData['adminisztratorok_field_password'], PASSWORD_BCRYPT )."'
                    ";
                }    
                $strQueryUser .= "
                    WHERE
                        id = {$tblNewData['adminisztratorok_field_id']}
                ";
                $dbres = $objDb->query($strQueryUser);
                unset($objEditUser);
                
            } else if ( //új hozzáadása
                (!isset($tblNewData['adminisztratorok_field_id']) || $tblNewData['adminisztratorok_field_id'] == "")
                && $this->_rowEvent['do'] == 'new'
                && isset($this->_tblUserModule['is_new_allowed']) 
                && $this->_tblUserModule['is_new_allowed'] == 1
            )
            {
                $strQueryUser = "
                    INSERT INTO
                        sys_user(
                             name
                            ,login
                            ,email
                            ,create_user_id
                        )VALUES(
                             '{$tblNewData['adminisztratorok_field_name']}'
                            ,'{$tblNewData['adminisztratorok_field_login']}'
                            ,'{$tblNewData['adminisztratorok_field_email']}'
                            ,{$this->_tblLoggedUserData['id']}
                        )
                ";
                $dbres = $objDb->query($strQueryUser);
                $numLastId = null;
                if (isset($tblNewData['adminisztratorok_field_permission']) && !empty($tblNewData['adminisztratorok_field_permission'])){
                    if($dbres){
                        $numLastId = $objDb->getLastInsertId();
                        $this->_numEditedUserId = $numLastId;
                    }
                    if (is_numeric($numLastId)) {
                        $strModuleId = implode(",", array_keys($tblNewData['adminisztratorok_field_permission']));
                        $rowModuleId = explode(",", $strModuleId);

                        $rowInsert = array();
                        foreach ($rowModuleId as $numModuleId) {
                            $rowInsert[] = "
                                ({$numLastId}, {$numModuleId})
                            ";
                        }
                        if (!empty($rowInsert)) {
                            $strQueryPermission = "
                                INSERT INTO 
                                    sys_user_access(user_id, module_id)
                                VALUES
                            " . implode(",", $rowInsert);
                        }
                    }
                }
            }
            
            if (!empty($strQueryPermission)) {
                $dbres = $objDb->query($strQueryPermission);
            }
            // die();
            //TODO ellenőrizni, hogy tényleg rendben volt-e a mentés során minden
            return TRUE;
        }
        
        public function run(){
            global $objSmarty;
            
           // $objSmarty->assign("","");
            if ($this->getData() === true) {
                $objSmarty->assign("tblPermission", $this->_tblPermission);
                $objSmarty->assign("rowEditedUserData", $this->_rowEditedUserData);
            }
            
//            print_r($this->_tblUserModule);
            //var_dump($this->_tblData);
//            print_r($this->_tblModule);
        }
        
    }
    