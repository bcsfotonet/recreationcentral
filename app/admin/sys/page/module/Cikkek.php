<?php 
    require_once 'ModulePage.php';
    
    class Cikkek extends ModulePage {
        private static $_strTable = 'news';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_tblCity;
        private $_tblType;
        private $_tblGallery;
        private $_tblVideo;
        private $_tblDocument;
        private $_strImageDirUrl;
        private $_strImageDir;
        
        function __construct( $numModuleId ){
            global $CONF;
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_tblCity = array();
            $this->_tblType = array();
            $this->_tblGallery = array();
            $this->_tblVideo = array();
            $this->_tblDocument = array();
            $this->_strImageDirUrl = $CONF['base_url']."media/pub/cikkek/";
            $this->_strImageDir = $CONF['pub_dir']."cikkek/";
            
            $this->_tblListData = array(
                "column" => array(
                     "title" => "Cím"
                    ,"start_date" => "Publikálás dátuma"
                )
                ,"search" => array(
//                     "type_id" => array("text" => "Kategória", "type" => "select", "options" => array(), "multiple" => 1)
//                    ,"title" => array("text" => "Cikk címe", "type" => "input") 
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                $this->_strImageDirUrl .= $this->_numEditedRowId."/small/";
                $this->_strImageDir .= $this->_numEditedRowId."/small/";
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'list'){
                $this->_objPagination->setNumCurrPage($this->_rowEvent['page']);
            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                    n.id
                    ,n.city_id
                    ,n.title
                    ,n.title_en
                    ,n.title_ro
                    ,n.url
                    ,n.url_en
                    ,n.url_ro
                    ,n.start_date
                    ,n.end_date
                    ,n.lead
                    ,n.lead_en
                    ,n.lead_ro
                    ,n.description
                    ,n.description_en
                    ,n.description_ro
                    ,COALESCE(n.publication_start_date, n.start_date) AS publication_start_date 
                    ,n.publication_end_date
                    ,n.image
                    ,n.is_active
                    ,n.is_highlighted
                    ,n.gallery_id
                    ,n.video_id                
                    ,n.create_user_id
                    ,n.create_date
                    ,n.modify_user_id
                    ,n.modify_date
                FROM 
                	".self::$_strTable." AS n 
                WHERE 
                	n.delete_date IS NULL
                ORDER BY
                    n.publication_start_date DESC
            ");
            
            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("cikkek");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];

                    if (!empty($this->_tblEditedRowData['image']) && file_exists($this->_strImageDir.$this->_tblEditedRowData['image'])) {
                        $this->_tblEditedRowData['image'] = $this->_strImageDirUrl.$this->_tblEditedRowData['image'];
                    } else {
                        $this->_tblEditedRowData['image'] = null;
                    }
                    
                    //kapcsolódó típusok lekérése
                    $tblTypeId = $objDb->getCol("
                        SELECT
                            type_id
                        FROM
                            news_type
                        WHERE
                            news_id = {$this->_numEditedRowId} 
                    ");
                    if ($tblTypeId !== FALSE) {
                        $this->_tblEditedRowData['type_id'] = $tblTypeId;
                    }
                    
                    //kapcsolódó dokumentumok lekérése
                    $tblDocumentId = $objDb->getCol("
                        SELECT
                            document_id
                        FROM
                            news_document
                        WHERE
                            news_id = {$this->_numEditedRowId}
                    ");
                    if ($tblDocumentId !== FALSE) {
                        $this->_tblEditedRowData['document_id'] = $tblDocumentId;
                    }
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
                
                if ($this->_rowEvent['do'] == "list") {
                    $this->_objPagination->setTblAllData($this->_tblData);
                    if ($this->_objPagination->getMaxPage() < $this->_rowEvent['page'] || $this->_rowEvent['page'] < 1) {
                        $this->setHeaderLocation("cikkek");
                    }
                }
            }
            
            //Választható típusok lekérése
            $tblType = $objDb->getAll("
                SELECT
                     id AS value
                    ,name AS text
                FROM
                    type
                WHERE
                    delete_date IS NULL
                    AND type_group = 'news'
                    AND is_active = 1
                ORDER BY
                    name
            ");
            if ($tblType !== FALSE) {
                foreach ($tblType as $numIdx => $rowType) {
                    $this->_tblType[$numIdx] = $rowType;
                    $this->_tblType[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['type_id']) && !empty($this->_tblEditedRowData['type_id'])) {
                        if (in_array($rowType['value'], $this->_tblEditedRowData['type_id'])) {
                            $this->_tblType[$numIdx]['selected'] = "selected"; 
                        }
                    }
                }
//                $this->_tblListData["search"]["type_id"]["options"] = $this->_tblType;
            }
            
            //Választható városok lekérése
            $tblCity = $objDb->getAll("
                SELECT
                     id AS value
                    ,name AS text
                FROM
                    city
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    name
            ");
            if ($tblCity !== FALSE) {
                $this->_tblCity[-1]['value'] = "";
                $this->_tblCity[-1]['text'] = "Nem tartozik hozzá város";
                
                foreach ($tblCity as $numIdx => $rowCity) {
                    $this->_tblCity[$numIdx] = $rowCity;
                    $this->_tblCity[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['city_id']) && is_numeric($this->_tblEditedRowData['city_id'])) {
                        if ($rowCity['value'] == $this->_tblEditedRowData['city_id']) {
                            $this->_tblCity[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            //Választható galériák lekérése
            $tblGallery = $objDb->getAll("
                SELECT
                     id AS value
                    ,title AS text
                FROM
                    gallery
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    title
            ");
            if ($tblGallery !== FALSE) {
                $this->_tblGallery[-1]['value'] = "";
                $this->_tblGallery[-1]['text'] = "Nem tartozik hozzá galéria";
                
                foreach ($tblGallery as $numIdx => $rowGallery) {
                    $this->_tblGallery[$numIdx] = $rowGallery;
                    $this->_tblGallery[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['gallery_id']) && is_numeric($this->_tblEditedRowData['gallery_id'])) {
                        if ($rowGallery['value'] == $this->_tblEditedRowData['gallery_id']) {
                            $this->_tblGallery[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            //Választható videók lekérése
            $tblVideo = $objDb->getAll("
                SELECT
                     id AS value
                    ,title AS text
                FROM
                    video
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    title
            ");
            if ($tblVideo !== FALSE) {
                $this->_tblVideo[-1]['value'] = "";
                $this->_tblVideo[-1]['text'] = "Nem tartozik hozzá videó";
                
                foreach ($tblVideo as $numIdx => $rowVideo) {
                    $this->_tblVideo[$numIdx] = $rowVideo;
                    $this->_tblVideo[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['video_id']) && is_numeric($this->_tblEditedRowData['video_id'])) {
                        if ($rowVideo['value'] == $this->_tblEditedRowData['video_id']) {
                            $this->_tblVideo[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            //Választható dokumentumok lekérése
            $tblDocument = $objDb->getAll("
                SELECT
                     id AS value
                    ,title AS text
                FROM
                    document
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    title
            ");
            if ($tblDocument !== FALSE) {
//                 $this->_tblDocument[-1]['value'] = "";
//                 $this->_tblDocument[-1]['text'] = "Nem tartozik hozzá dokumentum";
                
                foreach ($tblDocument as $numIdx => $rowDocument) {
                    $this->_tblDocument[$numIdx] = $rowDocument;
                    $this->_tblDocument[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['document_id']) && !empty($this->_tblEditedRowData['document_id'])) {
                        if (in_array($rowDocument['value'], $this->_tblEditedRowData['document_id'])) {
                            $this->_tblDocument[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            return parent::removeMainTableData ($numRowId, self::$_strTable);
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData, $tblFileData = null)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            //TODO ellenőrzések 
            
            return $rowMessage;
        }
        
        
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData, $tblFileData = null)
        {
            global $objDb, $objUser, $CONF;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO 
                        ".self::$_strTable."(
                             city_id
                            ,title
                            ,title_en
                            ,title_ro
                            ,url
                            ,url_en
                            ,url_ro
                            ,start_date
                            ,end_date
                            ,lead
                            ,lead_en
                            ,lead_ro
                            ,description
                            ,description_en
                            ,description_ro
                            ,publication_start_date
                            ,publication_end_date
                            ,is_highlighted
                            ,gallery_id
                            ,video_id
                            ,create_user_id
                        ) VALUES (
                             ".(isset($tblPostData['city_id']) && is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL")."
                            ,'{$tblPostData['title']}'
                            ,".(isset($tblPostData['title_en']) && !empty($tblPostData['title_en']) ? "'{$tblPostData['title_en']}'" : "NULL")."
                            ,".(isset($tblPostData['title_ro']) && !empty($tblPostData['title_ro']) ? "'{$tblPostData['title_ro']}'" : "NULL")."
                            ,'{$tblPostData['url']}'
                            ,".(isset($tblPostData['url_en']) && !empty($tblPostData['url_en']) ? "'{$tblPostData['url_en']}'" : "NULL")."
                            ,".(isset($tblPostData['url_ro']) && !empty($tblPostData['url_ro']) ? "'{$tblPostData['url_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['start_date']) && !empty($tblPostData['start_date']) ? "'{$tblPostData['start_date']}'" : "NULL")."
                            ,".(isset($tblPostData['end_date']) && !empty($tblPostData['end_date']) ? "'{$tblPostData['end_date']}'" : "NULL")."
                            ,".(isset($tblPostData['lead']) && !empty($tblPostData['lead']) ? "'{$tblPostData['lead']}'" : "NULL")."
                            ,".(isset($tblPostData['lead_en']) && !empty($tblPostData['lead_en']) ? "'{$tblPostData['lead_en']}'" : "NULL")."
                            ,".(isset($tblPostData['lead_ro']) && !empty($tblPostData['lead_ro']) ? "'{$tblPostData['lead_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['description']) && !empty($tblPostData['description']) ? "'{$tblPostData['description']}'" : "NULL")."
                            ,".(isset($tblPostData['description_en']) && !empty($tblPostData['description_en']) ? "'{$tblPostData['description_en']}'" : "NULL")."
                            ,".(isset($tblPostData['description_ro']) && !empty($tblPostData['description_ro']) ? "'{$tblPostData['description_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['publication_start_date']) && !empty($tblPostData['publication_start_date']) ? "'{$tblPostData['publication_start_date']}'" : "NULL")."
                            ,".(isset($tblPostData['publication_end_date']) && !empty($tblPostData['publication_end_date']) ? "'{$tblPostData['publication_end_date']}'" : "NULL")."
                            ,".(isset($tblPostData['is_highlighted']) ? ($tblPostData['is_highlighted'] == 'true' ? "1" : "0") : "0")."
                            ,".(isset($tblPostData['gallery_id']) && is_numeric($tblPostData['gallery_id']) ? "{$tblPostData['gallery_id']}" : "NULL")."
                            ,".(isset($tblPostData['video_id']) && is_numeric($tblPostData['video_id']) ? "{$tblPostData['video_id']}" : "NULL")."
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    // Kép mentés
                    if (!empty($tblFileData)) {
                        if (self::saveFile($tblFileData, $tblPostData, $numNewId) === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                        }
                    }
                    
                    // type_id mentések
                    if (isset($tblPostData['type_id']) && !empty($tblPostData['type_id']) ){
                        foreach ($tblPostData['type_id'] as $numTypeId) {
                            $strQueryInsert = "
                                INSERT INTO 
                                    news_type(
                                         news_id
                                        ,type_id
                                    ) VALUES (
                                         {$numNewId}
                                        ,{$numTypeId}
                                    )
                            ";
//                                          print $strQueryInsert;
                            $dbres2 = $objDb->query($strQueryInsert);
                            if ($dbres2 === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                break;
                            }
                        }
                    }
                    
                    // document_id mentések
                    if (isset($tblPostData['document_id']) && !empty($tblPostData['document_id']) ){
                        foreach ($tblPostData['document_id'] as $numDocumentId) {
                            $strQueryInsert = "
                                INSERT INTO
                                    news_document(
                                         news_id
                                        ,document_id
                                    ) VALUES (
                                         {$numNewId}
                                        ,{$numDocumentId}
                                    )
                            ";
//                                          print $strQueryInsert;
                            $dbres2 = $objDb->query($strQueryInsert);
                            if ($dbres2 === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                break;
                            }
                        }
                    }
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }

            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['city_id']) && $rowOldData['city_id'] != $tblPostData['city_id'] ? ",city_id = ".(is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL") : "")."
                            ".(isset($tblPostData['title']) && $rowOldData['title'] != $tblPostData['title'] ? ",title = '".addslashes(trim($tblPostData['title']))."'" : "")."
                            ".(isset($tblPostData['title_en']) && $rowOldData['title_en'] != $tblPostData['title_en'] ? ",title_en = '{$tblPostData['title_en']}'" : "")."
                            ".(isset($tblPostData['title_ro']) && $rowOldData['title_ro'] != $tblPostData['title_ro'] ? ",title_ro = '{$tblPostData['title_ro']}'" : "")."
                            ".(isset($tblPostData['url']) && $rowOldData['url'] != $tblPostData['url'] ? ",url = '{$tblPostData['url']}'" : "")."
                            ".(isset($tblPostData['url_en']) && $rowOldData['url_en'] != $tblPostData['url_en'] ? ",url_en = '{$tblPostData['url_en']}'" : "")."
                            ".(isset($tblPostData['url_ro']) && $rowOldData['url_ro'] != $tblPostData['url_ro'] ? ",url_ro = '{$tblPostData['url_ro']}'" : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                            ".(isset($tblPostData['start_date']) && $rowOldData['start_date'] != $tblPostData['start_date'] ? ",start_date = ".(empty($tblPostData['start_date']) ? "NULL" : "'{$tblPostData['start_date']}'") : "")."
                            ".(isset($tblPostData['end_date']) && $rowOldData['end_date'] != $tblPostData['end_date'] ? ",end_date = ".(empty($tblPostData['end_date']) ? "NULL" : "'{$tblPostData['end_date']}'") : "")."
                            ".(isset($tblPostData['lead']) && $rowOldData['lead'] != $tblPostData['lead'] ? ",lead = '{$tblPostData['lead']}'" : "")."
                            ".(isset($tblPostData['lead_en']) && $rowOldData['lead_en'] != $tblPostData['lead_en'] ? ",lead_en = '{$tblPostData['lead_en']}'" : "")."
                            ".(isset($tblPostData['lead_ro']) && $rowOldData['lead_ro'] != $tblPostData['lead_ro'] ? ",lead_ro = '{$tblPostData['lead_ro']}'" : "")."
                            ".(isset($tblPostData['description']) && $rowOldData['description'] != $tblPostData['description'] ? ",description = '{$tblPostData['description']}'" : "")."
                            ".(isset($tblPostData['description_en']) && $rowOldData['description_en'] != $tblPostData['description_en'] ? ",description_en = '{$tblPostData['description_en']}'" : "")."
                            ".(isset($tblPostData['description_ro']) && $rowOldData['description_ro'] != $tblPostData['description_ro'] ? ",description_ro = '{$tblPostData['description_ro']}'" : "")."
                            ".(isset($tblPostData['publication_start_date']) && $rowOldData['publication_start_date'] != $tblPostData['publication_start_date'] ? ",publication_start_date = ".(empty($tblPostData['publication_start_date']) ? "NULL" : "'{$tblPostData['publication_start_date']}'") : "")."
                            ".(isset($tblPostData['publication_end_date']) && $rowOldData['publication_end_date'] != $tblPostData['publication_end_date'] ? ",publication_end_date = ".(empty($tblPostData['publication_end_date']) ? "NULL" : "'{$tblPostData['publication_end_date']}'") : "")."
                            "./*(!isset($tblPostData['is_highlighted']) || (isset($tblPostData['is_highlighted']) && $rowOldData['is_highlighted'] != $tblPostData['is_highlighted']) ? ",is_highlighted = ".(isset($tblPostData['is_highlighted']) && $tblPostData['is_highlighted'] == 'true' ? "1" : "0") : "").*/"
                            ".(isset($tblPostData['row_save']) && $tblPostData['row_save'] === false ? (isset($tblPostData['is_highlighted']) && $rowOldData['is_highlighted'] != $tblPostData['is_highlighted'] ? ",is_highlighted = ".($tblPostData['is_highlighted'] == 'true' ? "1" : "0") : ",is_highlighted = 0") : "")."
                            ".(isset($tblPostData['gallery_id']) && $rowOldData['gallery_id'] != $tblPostData['gallery_id'] ? ",gallery_id = ".(is_numeric($tblPostData['gallery_id']) ? "{$tblPostData['gallery_id']}" : "NULL") : "")."
                            ".(isset($tblPostData['video_id']) && $rowOldData['video_id'] != $tblPostData['video_id'] ? ",video_id = ".(is_numeric($tblPostData['video_id']) ? "{$tblPostData['video_id']}" : "NULL") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE && $tblPostData['row_save'] === false) {
                        
                        // Kép módosítás
                        if (!empty($tblFileData)) {
                            if (self::saveFile($tblFileData, $tblPostData, $tblPostData['id']) === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                            }
                        }

                        // type_id módosítások
                        $dbres = $objDb->query("
                            DELETE FROM
                                news_type
                            WHERE
                                news_id = {$tblPostData['id']}
                        ");                                               
                        if (isset($tblPostData['type_id']) && !empty($tblPostData['type_id']) ){
                            foreach ($tblPostData['type_id'] as $numTypeId) {
                                $strQueryInsert = "
                                    INSERT INTO
                                        news_type(
                                             news_id
                                            ,type_id
                                        ) VALUES (
                                             {$tblPostData['id']}
                                            ,{$numTypeId}
                                        )
                                ";
//                                          print $strQueryInsert;
                                 $dbres = $objDb->query($strQueryInsert);
                                 if ($dbres === FALSE) {
                                     $rowMessage['type'] = 'error';
                                     $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                     break;
                                 }
                            }
                        }
                        
                        // document_id módosítások
                        $dbres = $objDb->query("
                            DELETE FROM
                                news_document
                            WHeRE
                                news_id = {$tblPostData['id']}
                        ");
                        if (isset($tblPostData['document_id']) && !empty($tblPostData['document_id']) ){
                            foreach ($tblPostData['document_id'] as $numDocumentId) {
                                $strQueryInsert = "
                                    INSERT INTO
                                        news_document(
                                             news_id
                                            ,document_id
                                        ) VALUES (
                                             {$tblPostData['id']}
                                            ,{$numDocumentId}
                                        )
                                ";
                                //                                          print $strQueryInsert;
                                $dbres = $objDb->query($strQueryInsert);
                                if ($dbres === FALSE) {
                                    $rowMessage['type'] = 'error';
                                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                    break;
                                }
                            }
                        }
                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }

                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        /**
         * Modulhoz tartozó fájlfeltöltések kezelésére
         * @param array $tblFileData
         * @param array $tblPostData
         * @param integer $numId
         * @return boolean
         */
        public static function saveFile($tblFileData, $tblPostData, $numId)
        {
            $tblImgSize['big']['width'] = 600;
            $tblImgSize['big']['height'] = 315;
            $tblImgSize['small']['width'] = 300;
            $tblImgSize['small']['height'] = 157;
            
            $rowTable['table'] = self::$_strTable;
            $rowTable['column'] = 'image';
            
            return parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize);
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("objPagination", $this->_objPagination);
                
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblCity", $this->_tblCity);
                $objSmarty->assign("tblType", $this->_tblType);
                $objSmarty->assign("tblGallery", $this->_tblGallery);
                $objSmarty->assign("tblVideo", $this->_tblVideo);
                $objSmarty->assign("tblDocument", $this->_tblDocument);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
        }
        
    }
    