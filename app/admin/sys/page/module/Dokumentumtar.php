<?php 
    require_once 'ModulePage.php';
    
    class Dokumentumtar extends ModulePage {
        private static $_strTable = 'document';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_strFileDirUrl;
        private $_strFileDir;
        
        function __construct( $numModuleId ){
            global $CONF;
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_strFileDirUrl = $CONF['base_url']."media/pub/dokumentumtar/";
            $this->_strFileDir = $CONF['pub_dir']."dokumentumtar".DIRECTORY_SEPARATOR;
            
            $this->_tblListData = array(
                "column" => array(
                     "title" => "Cím"
                )
                ,"search" => array(
//                    "title" => array("text" => "Dokumentum címe", "type" => "input") 
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                $this->_strFileDirUrl .= $this->_numEditedRowId."/";
                $this->_strFileDir .= $this->_numEditedRowId.DIRECTORY_SEPARATOR;
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                	 id
                    ,title
                    ,title_en
                    ,title_ro   
                    ,lead
                    ,lead_en
                    ,lead_ro
                    ,filename
                    ,filename_en
                    ,filename_ro
                    ,is_active
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                	".self::$_strTable." 
                WHERE 
                	delete_date IS NULL
                ORDER BY
                    title
            ");
            
            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("dokumentumtar");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                    
                    if (!empty($this->_tblEditedRowData['filename']) && file_exists($this->_strFileDir.$this->_tblEditedRowData['filename'])) {
                        $this->_tblEditedRowData['filename'] = $this->_strFileDirUrl.$this->_tblEditedRowData['filename'];
                    } else {
                        $this->_tblEditedRowData['filename'] = null;
                    }
                    if (!empty($this->_tblEditedRowData['filename_en']) && file_exists($this->_strFileDir.'en/'.$this->_tblEditedRowData['filename_en'])) {
                        $this->_tblEditedRowData['filename_en'] = $this->_strFileDirUrl.'en/'.$this->_tblEditedRowData['filename_en'];
                    } else {
                        $this->_tblEditedRowData['filename_en'] = null;
                    }
                    if (!empty($this->_tblEditedRowData['filename_ro']) && file_exists($this->_strFileDir.'ro/'.$this->_tblEditedRowData['filename_ro'])) {
                        $this->_tblEditedRowData['filename_ro'] = $this->_strFileDirUrl.'ro/'.$this->_tblEditedRowData['filename_ro'];
                    } else {
                        $this->_tblEditedRowData['filename_ro'] = null;
                    }
//                     print_r($this->_tblEditedRowData);
                    //kapcsolódó típusok lekérése
                    $tblTypeId = $objDb->getCol("
                        SELECT
                            type_id
                        FROM
                            document_type
                        WHERE
                            document_id = {$this->_numEditedRowId}
                    ");
                    if ($tblTypeId !== FALSE) {
                        $this->_tblEditedRowData['type_id'] = $tblTypeId;
                    }
                }
            } else {
                if (!empty($tblData) && $tblData !== FALSE) {
                    foreach ($tblData as $numIdx => $rowData) {
                        if (!empty($rowData['filename']) && file_exists($this->_strFileDir.$rowData['id'].DIRECTORY_SEPARATOR.$rowData['filename'])) {
                            $tblData[$numIdx]['filename'] = $this->_strFileDirUrl.$rowData['id']."/".$rowData['filename'];
                        } else {
                            $tblData[$numIdx]['filename'] = null;
                        }
                    }
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            //Választható típusok lekérése
            $tblType = $objDb->getAll("
                SELECT
                     id AS value
                    ,name AS text
                FROM
                    type
                WHERE
                    delete_date IS NULL
                    AND type_group = 'document'
                    AND is_active = 1
                ORDER BY
                    name
            ");
            if ($tblType !== FALSE) {
                foreach ($tblType as $numIdx => $rowType) {
                    $this->_tblType[$numIdx] = $rowType;
                    $this->_tblType[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['type_id']) && !empty($this->_tblEditedRowData['type_id'])) {
                        if (in_array($rowType['value'], $this->_tblEditedRowData['type_id'])) {
                            $this->_tblType[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            return parent::removeMainTableData ($numRowId, self::$_strTable);
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData, $tblFileData = null)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            //TODO ellenőrzések 
            
            return $rowMessage;
        }
        
        
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData, $tblFileData = null)
        {
            global $objDb, $objUser, $CONF;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO 
                        ".self::$_strTable."(
                             title
                            ,title_en
                            ,title_ro
                            ,lead
                            ,lead_en
                            ,lead_ro
                            ,create_user_id
                        ) VALUES (
                             '{$tblPostData['title']}'
                            ,".(!empty($tblPostData['title_en']) ? "'{$tblPostData['title_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['title_ro']) ? "'{$tblPostData['title_ro']}'" : "NULL")."
                            ,'{$tblPostData['lead']}'
                            ,".(!empty($tblPostData['lead_en']) ? "'{$tblPostData['lead_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['lead_ro']) ? "'{$tblPostData['lead_ro']}'" : "NULL")."
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    // Kép mentés
                    if (!empty($tblFileData)) {
                        if (self::saveFile($tblFileData, $tblPostData, $numNewId) === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                        }
                    }
                    
                    // type_id mentések
                    if (isset($tblPostData['type_id']) && !empty($tblPostData['type_id']) ){
                        foreach ($tblPostData['type_id'] as $numTypeId) {
                            $strQueryInsert = "
                                INSERT INTO
                                    document_type(
                                         document_id
                                        ,type_id
                                    ) VALUES (
                                         {$numNewId}
                                        ,{$numTypeId}
                                    )
                            ";
                             //                                          print $strQueryInsert;
                             $dbres2 = $objDb->query($strQueryInsert);
                             if ($dbres2 === FALSE) {
                                 $rowMessage['type'] = 'error';
                                 $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                 break;
                             }
                        }
                    }
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }

            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['title']) && $rowOldData['title'] != $tblPostData['title'] ? ",title = '{$tblPostData['title']}'" : "")."
                            ".(isset($tblPostData['title_en']) && $rowOldData['title_en'] != $tblPostData['title_en'] ? ",title_en = '{$tblPostData['title_en']}'" : "")."
                            ".(isset($tblPostData['title_ro']) && $rowOldData['title_ro'] != $tblPostData['title_ro'] ? ",title_ro = '{$tblPostData['title_ro']}'" : "")."
                            ".(isset($tblPostData['lead']) && $rowOldData['lead'] != $tblPostData['lead'] ? ",lead = '{$tblPostData['lead']}'" : "")."
                            ".(isset($tblPostData['lead_en']) && $rowOldData['lead_en'] != $tblPostData['lead_en'] ? ",lead_en = '{$tblPostData['lead_en']}'" : "")."
                            ".(isset($tblPostData['lead_ro']) && $rowOldData['lead_ro'] != $tblPostData['lead_ro'] ? ",lead_ro = '{$tblPostData['lead_ro']}'" : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE && $tblPostData['row_save'] === false) {
                        
                        // Kép módosítás
                        if (!empty($tblFileData)) {
                            if (self::saveFile($tblFileData, $tblPostData, $tblPostData['id']) === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                            }
                        }
                        
                        // type_id módosítások
                        $dbres = $objDb->query("
                            DELETE FROM
                                document_type
                            WHERE
                                document_id = {$tblPostData['id']}
                        ");
                        if (isset($tblPostData['type_id']) && !empty($tblPostData['type_id']) ){
                            foreach ($tblPostData['type_id'] as $numTypeId) {
                                $strQueryInsert = "
                                    INSERT INTO
                                        document_type(
                                             document_id
                                            ,type_id
                                        ) VALUES (
                                             {$tblPostData['id']}
                                            ,{$numTypeId}
                                        )
                                ";
                                 //                                          print $strQueryInsert;
                                 $dbres = $objDb->query($strQueryInsert);
                                 if ($dbres === FALSE) {
                                     $rowMessage['type'] = 'error';
                                     $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                     break;
                                 }
                            }
                        }

                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }

                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        /**
         * Modulhoz tartozó fájlfeltöltések kezelésére
         * @param array $tblFileData
         * @param array $tblPostData
         * @param integer $numId
         * @return boolean
         */
        public static function saveFile($tblFileData, $tblPostData, $numId)
        {
//             var_dump($tblFileData);
//             print_r($tblPostData);

            $isOk = TRUE;
            $rowTable['table'] = self::$_strTable;
//          $rowTable['column'] = 'filename';
            
            foreach ($tblFileData['name'] as $strColumn => $strFilename) {
//                 var_dump($strColumn);
                $rowTable['column'] = $strColumn;
                $isOk = parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable);
                if ($isOk === FALSE) {
                    break;
                }
            }
            return $isOk;
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblType", $this->_tblType);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
        }
        
    }
    