<?php 
    require_once 'ModulePage.php';
    
    class Egyedi_oldalak extends ModulePage {
        private static $_strTable = 'type';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_tblStaticContent;
        
        function __construct( $numModuleId ){
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_tblStaticContent = array();
            
            $this->_tblListData = array(
                "column" => array(
                     "name" => "Név"
                )
                ,"search" => array(
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                    id
                    ,name
                    ,name_en
                    ,name_ro
                    ,type_group
                    ,url
                    ,url_en
                    ,url_ro
                    ,static_id
                    ,is_active
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                    ".self::$_strTable."
                WHERE
                    delete_date IS NULL
                ORDER BY
                    name
            ");
            
            if (is_numeric($this->_numEditedRowId)) {
                $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                
                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("egyedi_oldalak");
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                
                //Tartalom beállítása - content_url
                $tblStaticContent = $objDb->getAll("
                    SELECT
                    	 title AS name
                        ,url
                        ,id
                    FROM
                    	static
                    WHERE
                    	delete_date IS NULL
                        AND is_active = 1
                        AND url IS NOT NULL
                    ORDER BY
                    	title
                ");
                if (!empty($tblStaticContent) && $tblStaticContent !== FALSE) {
                    foreach ($tblStaticContent as $numId => $rowStaticContent) {
                        $this->_tblStaticContent[$numId]['value'] = $rowStaticContent['id'];
                        $this->_tblStaticContent[$numId]['text'] = $rowStaticContent['name'];
                        $this->_tblStaticContent[$numId]['selected'] = "";
                        if (!empty($this->_tblEditedRowData) && $rowStaticContent['id'] == $this->_tblEditedRowData['static_id']) {
                            $this->_tblStaticContent[$numId]['selected'] = "selected";
                        }
                    }
                    array_unshift($this->_tblStaticContent, array("text" => "Kérem válasszon", "value" => ""));
                }
                
                $this->_tblData = $tblData;
            }

            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            return parent::removeMainTableData ($numRowId, self::$_strTable);
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            
            //TODO ellenőrzések 
            
            return $rowMessage;
        }

        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData)
        {
            global $objDb, $objUser;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO
                        ".self::$_strTable."(
                             name
                            ,name_en
                            ,name_ro
                            ,type_group
                            ,url
                            ,url_en
                            ,url_ro
                            ,static_id
                            ,create_user_id
                        ) VALUES (
                             '{$tblPostData['name']}'
                            ,".(!empty($tblPostData['name_en']) ? "'{$tblPostData['name_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['name_ro']) ? "'{$tblPostData['name_ro']}'" : "NULL")."                           
                            ,'{$tblPostData['type_group']}'
                            ,'{$tblPostData['url']}'
                            ,".(!empty($tblPostData['url_en']) ? "'{$tblPostData['url_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['url_ro']) ? "'{$tblPostData['url_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['static_id']) && is_numeric($tblPostData['static_id']) ? $tblPostData['static_id'] : "NULL") ."
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
                //     var_dump($dbres);
                //     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $rowMessage['type'] = 'ok';
                    $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    $rowMessage['new_id'] = $dbres;
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }
            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['name']) && $rowOldData['name'] != $tblPostData['name'] ? ",name = '{$tblPostData['name']}'" : "")."
                            ".(isset($tblPostData['name_en']) && $rowOldData['name_en'] != $tblPostData['name_en'] ? ",name_en = ".(!empty($tblPostData['name_en']) ? "'{$tblPostData['name_en']}'" : "NULL") : "")."
                            ".(isset($tblPostData['name_ro']) && $rowOldData['name_ro'] != $tblPostData['name_ro'] ? ",name_ro = ".(!empty($tblPostData['name_ro']) ? "'{$tblPostData['name_ro']}'" : "NULL") : "")."                          
                            ".(isset($tblPostData['type_group']) && $rowOldData['type_group'] != $tblPostData['type_group'] ? ",type_group = '{$tblPostData['type_group']}'" : "")."
                            ".(isset($tblPostData['url']) && $rowOldData['url'] != $tblPostData['url'] ? ",url = '{$tblPostData['url']}'" : "")."
                            ".(isset($tblPostData['url_en']) && $rowOldData['url_en'] != $tblPostData['url_en'] ? ",url_en = ".(!empty($tblPostData['url_en']) ? "'{$tblPostData['url_en']}'" : "NULL") : "")."
                            ".(isset($tblPostData['url_ro']) && $rowOldData['url_ro'] != $tblPostData['url_ro'] ? ",url_ro = ".(!empty($tblPostData['url_ro']) ? "'{$tblPostData['url_ro']}'" : "NULL") : "")."                        
                            ".(isset($tblPostData['static_id']) && $rowOldData['static_id'] != $tblPostData['static_id'] ? ",static_id = ".(is_numeric($tblPostData['static_id']) ? "{$tblPostData['static_id']}" : "NULL") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
                    //             print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE) {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblStaticContent", $this->_tblStaticContent);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
        }
        
    }
    