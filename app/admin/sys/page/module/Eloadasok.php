<?php 
    require_once 'ModulePage.php';
    
    class Eloadasok extends ModulePage {
        private static $_strTable = 'presentation';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_tblCity;
        
        function __construct( $numModuleId ){
            global $CONF;
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_tblCity = array();
            
            $this->_tblListData = array(
                "column" => array(
                     "title" => "Cím"
                    ,"name" => "Előadó"
                )
                ,"search" => array(
//                     "title" => array("text" => "Előadás címe", "type" => "input") 
//                    ,"name" => array("text" => "Előadó", "type" => "input")
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'list'){
                $this->_objPagination->setNumCurrPage($this->_rowEvent['page']);
            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                	 n.id
                    ,n.city_id
                    ,n.title
                    ,n.title_en
                    ,n.title_ro
                    ,n.name
                    ,n.start_date
                    ,n.is_active
                    ,n.create_user_id
                    ,n.create_date
                    ,n.modify_user_id
                    ,n.modify_date
                FROM 
                	".self::$_strTable." AS n 
                WHERE 
                	n.delete_date IS NULL
                ORDER BY
                    n.start_date DESC
            ");
            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("eloadasok");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
                
                if ($this->_rowEvent['do'] == "list") {
                    $this->_objPagination->setTblAllData($this->_tblData);
                    if ($this->_objPagination->getMaxPage() < $this->_rowEvent['page'] || $this->_rowEvent['page'] < 1) {
                        $this->setHeaderLocation("eloadasok");
                    }
                }
            }
            
            //Választható városok lekérése
            $tblCity = $objDb->getAll("
                SELECT
                     id AS value
                    ,name AS text
                FROM
                    city
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    name
            ");
            if ($tblCity !== FALSE) {
                $this->_tblCity[-1]['value'] = "";
                $this->_tblCity[-1]['text'] = "Nem tartozik hozzá város";
                
                foreach ($tblCity as $numIdx => $rowCity) {
                    $this->_tblCity[$numIdx] = $rowCity;
                    $this->_tblCity[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['city_id']) && is_numeric($this->_tblEditedRowData['city_id'])) {
                        if ($rowCity['value'] == $this->_tblEditedRowData['city_id']) {
                            $this->_tblCity[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
                
            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            return parent::removeMainTableData ($numRowId, self::$_strTable);
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData, $tblFileData = null)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            //TODO ellenőrzések 
            
            return $rowMessage;
        }
        
        
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData, $tblFileData = null)
        {
            global $objDb, $objUser, $CONF;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO 
                        ".self::$_strTable."(
                             city_id
                            ,title
                            ,title_en
                            ,title_ro
                            ,name
                            ,start_date
                            ,create_user_id
                        ) VALUES (
                             ".(isset($tblPostData['city_id']) && is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL")."
                            ,'{$tblPostData['title']}'
                            ,".(!empty($tblPostData['title_en']) ? "'{$tblPostData['title_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['title_ro']) ? "'{$tblPostData['title_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['name']) && !empty($tblPostData['name']) ? "'{$tblPostData['name']}'" : "NULL")."
                            ,".(isset($tblPostData['start_date']) && !empty($tblPostData['start_date']) ? "'{$tblPostData['start_date']}'" : "NULL")."
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }

            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['city_id']) && $rowOldData['city_id'] != $tblPostData['city_id'] ? ",city_id = ".(is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL") : "")."
                            ".(isset($tblPostData['title']) && $rowOldData['title'] != $tblPostData['title'] ? ",title = '{$tblPostData['title']}'" : "")."
                            ".(isset($tblPostData['title_en']) && $rowOldData['title_en'] != $tblPostData['title_en'] ? ",title_en = '{$tblPostData['title_en']}'" : "")."
                            ".(isset($tblPostData['title_ro']) && $rowOldData['title_ro'] != $tblPostData['title_ro'] ? ",title_ro = '{$tblPostData['title_ro']}'" : "")."
                            ".(isset($tblPostData['name']) && $rowOldData['name'] != $tblPostData['name'] ? ",name = '{$tblPostData['name']}'" : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                            ".(isset($tblPostData['start_date']) && $rowOldData['start_date'] != $tblPostData['start_date'] ? ",start_date = ".(empty($tblPostData['start_date']) ? "NULL" : "'{$tblPostData['start_date']}'") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE && $tblPostData['row_save'] === false) {
                        
                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }

                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("objPagination", $this->_objPagination);
                
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblCity", $this->_tblCity);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
        }
        
    }
    