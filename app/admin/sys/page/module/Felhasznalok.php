<?php 
    require_once 'ModulePage.php';
    
    class Felhasznalok extends ModulePage {
        private static $_strTable = 'customer';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        
        function __construct( $numModuleId ){
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            
            $this->_tblListData = array(
                "column" => array(
                     "name" => "Név"
                    ,"email" => "E-mail"
                )
                ,"search" => array(
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
//                 $this->removeData($this->_numEditedRowId);
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                     id
                    ,is_person
                    ,is_company
                    ,last_name
                    ,first_name
                    ,company_name
                    ,email
                    ,tel
                    ,country
                    ,zip
                    ,city
                    ,street
                    ,house_number
                    ,tax_number
                    ,is_active
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                    ".self::$_strTable."
                WHERE
                    delete_date IS NULL
                ORDER BY
                    is_active DESC
                    ,(CASE WHEN is_person = 1 THEN CONCAT(last_name, first_name) ELSE company_name END)
                    ,create_date DESC
            ");
            
            if (is_numeric($this->_numEditedRowId)) {
                $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                
                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("felhasznalok");
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }

            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            global $objDb, $objUser;
            $rowMessage = parent::removeMainTableData ($numRowId, self::$_strTable);
            
            return $rowMessage;
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            //TODO ellenőrzések 
            
            return $rowMessage;
        }

        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData)
        {
            global $objDb, $objUser;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO
                        ".self::$_strTable."(
                             is_person
                            ,is_company
                            ,last_name
                            ,first_name
                            ,company_name
                            ,email
                            ,tel
                            ,country
                            ,zip
                            ,city
                            ,street
                            ,house_number
                            ,tax_number
                            ,create_user_id
                        ) VALUES (
                            ".(!empty($tblPostData['is_person']) ? "{$tblPostData['is_person']}" : 0)."
                            ,".(!empty($tblPostData['is_company']) ? "{$tblPostData['is_company']}" : 0)."
                            ,".(!empty($tblPostData['last_name']) ? "'{$tblPostData['last_name']}'" : "NULL")."
                            ,".(!empty($tblPostData['first_name']) ? "'{$tblPostData['first_name']}'" : "NULL")."
                            ,".(!empty($tblPostData['company_name']) ? "'{$tblPostData['company_name']}'" : "NULL")."
                            ,".(!empty($tblPostData['email']) ? "'{$tblPostData['email']}'" : "")."
                            ,".(!empty($tblPostData['tel']) ? "'{$tblPostData['tel']}'" : "")."
                            ,".(!empty($tblPostData['country']) ? "'{$tblPostData['country']}'" : "")."
                            ,".(!empty($tblPostData['zip']) ? "'{$tblPostData['zip']}'" : "")."
                            ,".(!empty($tblPostData['city']) ? "'{$tblPostData['city']}'" : "")."
                            ,".(!empty($tblPostData['street']) ? "'{$tblPostData['street']}'" : "")."
                            ,".(!empty($tblPostData['house_number']) ? "'{$tblPostData['house_number']}'" : "")."
                            ,".(!empty($tblPostData['tax_number']) ? "'{$tblPostData['tax_number']}'" : "")."
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $rowMessage['type'] = 'ok';
                    $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    $rowMessage['new_id'] = $dbres;
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }
            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                   
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".
                            /*
                              (isset($tblPostData['is_person']) && $rowOldData['is_person'] != $tblPostData['is_person'] ? ",is_person = ".($tblPostData['is_person'] == 'true' ? "1" : "0") : "")."
                            ".(isset($tblPostData['is_company']) && $rowOldData['is_company'] != $tblPostData['is_company'] ? ",is_company = ".($tblPostData['is_company'] == 'true' ? "1" : "0") : "")
                            .*/
                            "
                            ".(isset($tblPostData['last_name']) && $rowOldData['last_name'] != $tblPostData['last_name'] ? ",last_name = '{$tblPostData['last_name']}'" : "")."
                            ".(isset($tblPostData['first_name']) && $rowOldData['first_name'] != $tblPostData['first_name'] ? ",first_name = '{$tblPostData['first_name']}'" : "")."
                            ".(isset($tblPostData['company_name']) && $rowOldData['company_name'] != $tblPostData['company_name'] ? ",company_name = '{$tblPostData['company_name']}'" : "")."
                            ".(isset($tblPostData['email']) && $rowOldData['email'] != $tblPostData['email'] ? ",email = '{$tblPostData['email']}'" : "")."
                            ".(isset($tblPostData['tel']) && $rowOldData['tel'] != $tblPostData['tel'] ? ",tel = '{$tblPostData['tel']}'" : "")."
                            ".(isset($tblPostData['country']) && $rowOldData['country'] != $tblPostData['country'] ? ",country = '{$tblPostData['country']}'" : "")."
                            ".(isset($tblPostData['zip']) && $rowOldData['zip'] != $tblPostData['zip'] ? ",zip = '{$tblPostData['zip']}'" : "")."
                            ".(isset($tblPostData['city']) && $rowOldData['city'] != $tblPostData['city'] ? ",city = '{$tblPostData['city']}'" : "")."
                            ".(isset($tblPostData['street']) && $rowOldData['street'] != $tblPostData['street'] ? ",street = '{$tblPostData['street']}'" : "")."
                            ".(isset($tblPostData['house_number']) && $rowOldData['house_number'] != $tblPostData['house_number'] ? ",house_number = '{$tblPostData['house_number']}'" : "")."
                            ".(isset($tblPostData['tax_number']) && $rowOldData['tax_number'] != $tblPostData['tax_number'] ? ",tax_number = '{$tblPostData['tax_number']}'" : "")."                            
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE) {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
        }
        
    }
    