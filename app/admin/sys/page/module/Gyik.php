<?php 
    require_once 'ModulePage.php';
    
    class Gyik extends ModulePage {
        private static $_strTable = 'faq';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        
        function __construct( $numModuleId ){
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            
            $this->_tblListData = array(
                "column" => array(
                     "name" => "Név"
                    ,"priority" => "Sorrend"
                )
                ,"search" => array(
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
//                 $this->removeData($this->_numEditedRowId);
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                	 id
                    ,question
                    ,question_en
                    ,question_ro
                    ,answer
                    ,answer_en
                    ,answer_ro
                    ,is_active
                    ,priority
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                	".self::$_strTable."
                WHERE
                	delete_date IS NULL
                ORDER BY
                	priority
            ");
            
            if (is_numeric($this->_numEditedRowId)) {
                $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                
                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("gyik");
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }

            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            global $objDb, $objUser;
            $rowMessage = parent::removeMainTableData ($numRowId, self::$_strTable);
            
            // sorrend újragenerálása miatt ebben a modulban nem elég a sima törlés
            if ($rowMessage['type'] == 'ok') {
                $dbres = $objDb->query("
                    UPDATE
                        ".self::$_strTable."
                    SET
                         modify_date = now()
                        ,modify_user_id = {$objUser->getNumUserId()}
                        ,priority = priority-1
                    WHERE
                        delete_date IS NULL
                        AND priority > (SELECT t.priority FROM (select * from ".self::$_strTable.") AS t WHERE t.id = {$numRowId})
                ");
                if ($dbres === true) {
                    $rowMessage = array('type' => 'ok', 'msg' => 'Az adatok törlése sikeresen megtörtént.', 'refresh' => 'true');
                } else {
                    $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt az adatok törlése során.');
                }
            }
            
            return $rowMessage;
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            //TODO ellenőrzések 
            
            return $rowMessage;
        }

        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData)
        {
            global $objDb, $objUser;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO
                        ".self::$_strTable."(
                             question
                            ,question_en
                            ,question_ro
                            ,answer
                            ,answer_en
                            ,answer_ro
                            ,priority
                            ,create_user_id
                        ) VALUES (
                             '{$tblPostData['question']}'
                            ,".(!empty($tblPostData['question_en']) ? "'{$tblPostData['question_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['question_ro']) ? "'{$tblPostData['question_ro']}'" : "NULL")."
                            ,'{$tblPostData['answer']}'
                            ,".(!empty($tblPostData['answer_en']) ? "'{$tblPostData['answer_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['answer_ro']) ? "'{$tblPostData['answer_ro']}'" : "NULL")."
                            ,(SELECT COALESCE(max(t.priority)+1, 1) FROM ".self::$_strTable." AS t WHERE t.delete_date IS NULL)  
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $rowMessage['type'] = 'ok';
                    $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    $rowMessage['new_id'] = $dbres;
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }
            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['question']) && $rowOldData['question'] != $tblPostData['question'] ? ",question = '{$tblPostData['question']}'" : "")."
                            ".(isset($tblPostData['question_en']) && $rowOldData['question_en'] != $tblPostData['question_en'] ? ",question_en = '{$tblPostData['question_en']}'" : "")."
                            ".(isset($tblPostData['question_ro']) && $rowOldData['question_ro'] != $tblPostData['question_ro'] ? ",question_ro = '{$tblPostData['question_ro']}'" : "")."
                            ".(isset($tblPostData['answer']) && $rowOldData['answer'] != $tblPostData['answer'] ? ",answer = '{$tblPostData['answer']}'" : "")."
                            ".(isset($tblPostData['answer']) && $rowOldData['answer_en'] != $tblPostData['answer_en'] ? ",answer_en = '{$tblPostData['answer_en']}'" : "")."
                            ".(isset($tblPostData['answer']) && $rowOldData['answer_ro'] != $tblPostData['answer_ro'] ? ",answer_ro = '{$tblPostData['answer_ro']}'" : "")."
                            ".(isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority'] ? ",priority = ".(is_numeric($tblPostData['priority']) ? "{$tblPostData['priority']}" : "1") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
                    //             print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE) {
                        
                        if (isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority']) {
                            $strQueryUpdate = "
                                UPDATE
                                    ".self::$_strTable."
                                SET
                                     modify_date = now()
                                    ,modify_user_id = {$objUser->getNumUserId()}
                                    ,priority = priority ".($tblPostData['priority'] < $rowOldData['priority'] ? "+1" : "-1")."
                                WHERE
                                    id != {$tblPostData['id']}
                                    AND delete_date IS NULL
                                    AND priority BETWEEN ".($tblPostData['priority'] < $rowOldData['priority'] ? $tblPostData['priority']." AND ".$rowOldData['priority'] : "greatest(".$rowOldData['priority'].",2) AND greatest(".$tblPostData['priority'].",2)")."
                            ";
                            $dbres = $objDb->query($strQueryUpdate);
                            if ($dbres !== FALSE) {
                                $rowMessage['refresh'] = 'true';
                            }
                        }
                        if ($dbres !== FALSE) {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        } else {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                        }
                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
        }
        
    }
    