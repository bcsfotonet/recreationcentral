<?php 

    require_once 'ModulePage.php';

    

    class Kozponti_galeria extends ModulePage {

        private static $_strTable = 'gallery';

        private $_numEditedRowId;

        private $_tblEditedRowData;

        

        function __construct( $numModuleId ){

            parent::__construct( $numModuleId );            

            

            $this->_tblValidate = self::getValidateData(self::$_strTable);

            $this->_numEditedRowId = null;

            $this->_tblEditedRowData = array();

            

            $this->_tblListData = array(

                "column" => array(

                    "title" => "Cím"

                )

                ,"search" => array(

//                    "title" => array("text" => "Galéria címe", "type" => "input")

                )

            );

            

            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {

                $this->_numEditedRowId = $this->_rowEvent['id'];

                

            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {

                $this->_numEditedRowId = $this->_rowEvent['id'];

                

            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {



            }



            $this->run();

        }

        

        private function getData(){

            global $CONF, $objDb;

            

            $tblData = $objDb->getAll("

                SELECT 

                	 g.id

                    ,g.title

                    ,g.url

                    ,g.description

                    ,g.city_id

                    ,g.is_active

                    ,i.id AS img_id

                    ,i.image AS img_image

                    ,i.priority AS img_priority

                    ,i.alt AS img_alt

                    ,i.is_active AS img_is_active

                    ,GREATEST(COALESCE(g.modify_date, g.create_date, 0), COALESCE(i.modify_date, i.create_date, 0)) AS modify_date

                FROM 

                	".self::$_strTable." AS g 

                LEFT JOIN

                	gallery_image AS i ON(i.gallery_id = g.id AND i.delete_date IS NULL)

                WHERE 

                	g.delete_date IS NULL

                ORDER BY

                	 GREATEST(COALESCE(g.modify_date, g.create_date, 0), COALESCE(i.modify_date, i.create_date, 0)) DESC

                    ,g.title

                    ,i.is_active desc

                    ,i.priority

            ");

            

            if (!empty($tblData) && $tblData !== FALSE) {

                foreach ($tblData as $rowData) {

                    $numIdx = $rowData['id'];

                    

                    $this->_tblData[$numIdx]['id'] = $rowData['id'];

                    $this->_tblData[$numIdx]['title'] = $rowData['title'];

                    $this->_tblData[$numIdx]['url'] = $rowData['url'];

                    $this->_tblData[$numIdx]['description'] = $rowData['description'];

                    $this->_tblData[$numIdx]['city_id'] = $rowData['city_id'];

                    $this->_tblData[$numIdx]['is_active'] = $rowData['is_active'];

                    $this->_tblData[$numIdx]['modify_date'] = $rowData['modify_date'];



                    if (is_numeric($rowData['img_id']) && is_file($CONF['pub_dir']."kozponti-galeria".DIRECTORY_SEPARATOR.$rowData['id'].DIRECTORY_SEPARATOR."small".DIRECTORY_SEPARATOR.$rowData['img_image'])) {

                        $this->_tblData[$numIdx]['images'][$rowData['img_id']]['id'] = $rowData['img_id'];

                        $this->_tblData[$numIdx]['images'][$rowData['img_id']]['image_url'] = "{$CONF['base_url']}media/pub/kozponti-galeria/{$rowData['id']}/small/{$rowData['img_image']}";

                        $this->_tblData[$numIdx]['images'][$rowData['img_id']]['image'] = $rowData['img_image'];

                        $this->_tblData[$numIdx]['images'][$rowData['img_id']]['priority'] = $rowData['img_priority'];

                        $this->_tblData[$numIdx]['images'][$rowData['img_id']]['alt'] = $rowData['img_alt'];

                        $this->_tblData[$numIdx]['images'][$rowData['img_id']]['is_active'] = $rowData['img_is_active'];

                    }

                }

            }

//             print_r($this->_tblData);

            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk

                if (!is_array($this->_tblData) || !isset($this->_tblData[$this->_numEditedRowId])) {

                    $this->setHeaderLocation("kozponti-galeria");

                } else {

                    $this->_tblEditedRowData = $this->_tblData[$this->_numEditedRowId];

                    

                    //kapcsolódó típusok lekérése

                    $tblTypeId = $objDb->getCol("

                        SELECT

                            type_id

                        FROM

                            gallery_type

                        WHERE

                            gallery_id = {$this->_numEditedRowId}

                    ");

                    if ($tblTypeId !== FALSE) {

                        $this->_tblEditedRowData['type_id'] = $tblTypeId;

                    }

                }

            }

            

            //Választható típusok lekérése

            $tblType = $objDb->getAll("

                SELECT

                     id AS value

                    ,name AS text

                FROM

                    type

                WHERE

                    delete_date IS NULL

                    AND type_group = 'gallery'

                    AND is_active = 1

                ORDER BY

                    name

            ");

            if ($tblType !== FALSE) {

                foreach ($tblType as $numIdx => $rowType) {

                    $this->_tblType[$numIdx] = $rowType;

                    $this->_tblType[$numIdx]['selected'] = "";

                    if (isset($this->_tblEditedRowData['type_id']) && !empty($this->_tblEditedRowData['type_id'])) {

                        if (in_array($rowType['value'], $this->_tblEditedRowData['type_id'])) {

                            $this->_tblType[$numIdx]['selected'] = "selected";

                        }

                    }

                }

            }

            

            //Választható városok lekérése

            $tblCity = $objDb->getAll("

                SELECT

                     id AS value

                    ,name AS text

                FROM

                    city

                WHERE

                    delete_date IS NULL

                    AND is_active = 1

                ORDER BY

                    name

            ");

            if ($tblCity !== FALSE) {

                $this->_tblCity[-1]['value'] = "";

                $this->_tblCity[-1]['text'] = "Nem tartozik hozzá város";

                

                foreach ($tblCity as $numIdx => $rowCity) {

                    $this->_tblCity[$numIdx] = $rowCity;

                    $this->_tblCity[$numIdx]['selected'] = "";

                    

                    if (isset($this->_tblEditedRowData['city_id']) && is_numeric($this->_tblEditedRowData['city_id'])) {

                        if ($rowCity['value'] == $this->_tblEditedRowData['city_id']) {

                            $this->_tblCity[$numIdx]['selected'] = "selected";

                        }

                    }

                }

            }

            

            return true;

        }

        

        /**

         * Törli a sort 

         * @global type $objDb

         * @param integer $numRowId

         */

        public static function removeModuleData ($numRowId)

        {

            global $objDb, $objUser;

            $rowMessage = parent::removeMainTableData ($numRowId, self::$_strTable);

            

            // a kapcsolótábla miatt ebben a modulban nem elég a sima törlés

            if ($rowMessage['type'] == 'ok') {

                $dbres = $objDb->query("

                    UPDATE

                        gallery_image

                    SET

                         delete_date = now()

                        ,delete_user_id = {$objUser->getNumUserId()}

                    WHERE

                        gallery_id = {$numRowId}

                        AND delete_date IS NULL

                ");

                if ($dbres === true) {

                    //TODO képek fizikai törlése?

                    $rowMessage = array('type' => 'ok', 'msg' => 'Az adatok törlése sikeresen megtörtént.');

                } else {

                    $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt az adatok törlése során.');

                }

            }

            return $rowMessage;

        }

        

        /**

         * Validálja a mentéskor post-ban kapott értékeket

         * @param array $tblPostData

         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek

         */

        public static function saveModuleValidation ($tblPostData, $tblFileData = null)

        {

            $rowMessage = array('type' => 'ok', 'msg' => null);

            $tblValidate = self::getValidateData(self::$_strTable);

            

            //TODO ellenőrzések 

//             $rowMessage 

            return $rowMessage;

        }



        /**

         * Menti a post-ban kapott értékeket

         * @param array $tblPostData

         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek

         */

        public static function saveModuleData ($tblPostData, $tblFileData = null)

        {

            global $objDb, $objUser, $CONF;

            $rowMessage = array('type' => null, 'msg' => null);

            

            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {

                //Insert

                $strQueryInsert = "

                    INSERT INTO

                        ".self::$_strTable."(

                             title

                            ,url

                            ,description

                            ,city_id

                            ,create_user_id

                        ) VALUES (

                             '{$tblPostData['title']}'

                            ,'{$tblPostData['url']}'

                            ,".(isset($tblPostData['description']) && !empty($tblPostData['description']) ? "'{$tblPostData['description']}'" : "NULL")."

                            ,".(isset($tblPostData['city_id']) && is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL")."

                            ,{$objUser->getNumUserId()}

                        )

                ";

                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t

                //     var_dump($dbres);

                //     print $strQueryInsert;

                if (is_numeric($dbres)) {

                    $numNewId = $dbres;

                    

                    // Képek mentés

                    if (!empty($tblFileData)) {

                        if (self::saveFile($tblFileData, $tblPostData, $numNewId) === FALSE) {

                            $rowMessage['type'] = 'error';

                            $rowMessage['msg'] = 'Hiba történt a képek mentése során.';

                        }

                    }

                    

                    // type_id mentések

                    if (isset($tblPostData['type_id']) && !empty($tblPostData['type_id']) ){

                        foreach ($tblPostData['type_id'] as $numTypeId) {

                            $strQueryInsert = "

                                INSERT INTO

                                    gallery_type(

                                         gallery_id

                                        ,type_id

                                    ) VALUES (

                                         {$numNewId}

                                        ,{$numTypeId}

                                    )

                            ";

                             $dbres2 = $objDb->query($strQueryInsert);

                             if ($dbres2 === FALSE) {

                                 $rowMessage['type'] = 'error';

                                 $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';

                                 break;

                             }

                        }

                    }

                    

                    if ($rowMessage['type'] != 'error') {

                        $rowMessage['type'] = 'ok';

                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';

                        $rowMessage['new_id'] = $numNewId;

                    }

                } else {

                    $rowMessage['type'] = 'error';

                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';

                }

            } else {

                //Update

                

                $rowOldData = $objDb->getRow("

                    SELECT

                        *

                    FROM

                        ".self::$_strTable."

                    WHERE

                        id = {$tblPostData['id']}

                ");

                if ($rowOldData !== FALSE) {

                    

                    $strQueryUpdate = "

                        UPDATE

                            ".self::$_strTable."

                        SET

                             modify_date = now()

                            ,modify_user_id = {$objUser->getNumUserId()}

                            ".(isset($tblPostData['title']) && $rowOldData['title'] != $tblPostData['title'] ? ",title = '{$tblPostData['title']}'" : "")."

                            ".(isset($tblPostData['url']) && $rowOldData['url'] != $tblPostData['url'] ? ",url = '{$tblPostData['url']}'" : "")."

                            ".(isset($tblPostData['description']) && $rowOldData['description'] != $tblPostData['description'] ? ",description = '{$tblPostData['description']}'" : "")."

                            ".(isset($tblPostData['city_id']) && $rowOldData['city_id'] != $tblPostData['city_id'] ? ",city_id = ".(is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL") : "")."

                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."

                        WHERE

                            id = {$tblPostData['id']}

                    ";

                    //             print $strQueryUpdate;

                    $dbres = $objDb->query($strQueryUpdate);

                    

                    if ($dbres !== FALSE && $tblPostData['row_save'] === false) {

                        

                        // Képek módosítás

                        if (isset($tblPostData['images']) && !empty($tblPostData['images'])) {

                            $isUpdate = FALSE;

                            

                            foreach ($tblPostData['images'] as $numImageId => $rowImage) {

                                if (is_numeric($numImageId)) {

                                    $rowOldImageData = $objDb->getRow("

                                        SELECT

                                        	*

                                        FROM

                                        	gallery_image

                                        WHERE

                                        	delete_date IS NULL

                                            AND id = {$numImageId}

                                    ");

                                    if ($rowOldImageData !== FALSE) {

                                        $isUpdate = TRUE;

                                        

                                        $strQueryUpdate = "

                                            UPDATE

                                                gallery_image

                                            SET

                                                 modify_date = now()

                                                ,modify_user_id = {$objUser->getNumUserId()}

                                                ,is_active = ".(isset($rowImage['is_active']) && $rowImage['is_active'] != 'false' ? "1" : "0")."

                                                ".(isset($rowImage['priority']) && $rowOldImageData['priority'] != $rowImage['priority'] ? ",priority = {$rowImage['priority']}" : "")."

                                                ".(isset($rowImage['delete_date']) && $rowImage['delete_date'] == 'true' ? ",delete_date = now()" : "")."

                                                ".(isset($rowImage['delete_date']) && $rowImage['delete_date'] == 'true' ? ",delete_user_id = {$objUser->getNumUserId()}" : "")."

                                            WHERE

                                                id = {$numImageId}

                                        ";

                                        $dbres = $objDb->query($strQueryUpdate);

                                    

                                        if ($dbres !== FALSE && isset($rowImage['delete_date']) && $rowImage['delete_date'] == "true") {

                                            

                                            //TODO törlést rekurzív függvénnyel megoldani és kivinni a function.php-ba

                                            $rowDelFile = glob($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$tblPostData['id'].DIRECTORY_SEPARATOR.'*'); //fájlnevek lekérése



                                            foreach($rowDelFile as $strFile){

                                                if (is_dir($strFile)) {

                                                    $strFile2 = $strFile.DIRECTORY_SEPARATOR.$rowOldImageData['image']; //törlendő fájl lekérése

                                                        if (is_file($strFile2)) {

                                                            unlink($strFile2);

                                                        }

                                                    if (empty(glob($strFile.DIRECTORY_SEPARATOR.'*'))) {

                                                        rmdir($strFile);

                                                    }

                                                    

                                                } elseif (is_file($strFile)) {

                                                    unlink($strFile);

                                                }

                                            }                                            

                                        }

                                    }

                                }

                            }

                            

                            if ($isUpdate === TRUE) {

                                $rowImagePriority = $objDb->getCol("

                                    SELECT

                                    	id

                                    FROM

                                    	gallery_image

                                    WHERE

                                    	delete_date IS NULL

                                        AND gallery_id = {$tblPostData['id']}

                                    ORDER BY

                                        priority

                                ");

                                if ($rowImagePriority !== FALSE && !empty($rowImagePriority)) {

                                    foreach ($rowImagePriority as $numIdx => $numImgId) {

                                        $dbres = $objDb->query("

                                            UPDATE

                                            	gallery_image

                                            SET

                                                 priority = ".($numIdx+1)."

                                                ,modify_date = now()

                                                ,modify_user_id = {$objUser->getNumUserId()}

                                            WHERE

                                                gallery_id = {$tblPostData['id']}

                                                AND id = {$numImgId}

                                        ");

                                    }

                                }

                            }

                        

                        }

                        

                        // Képek hozzáadása

                        if (!empty($tblFileData)) {

                            if (self::saveFile($tblFileData, $tblPostData, $tblPostData['id']) === FALSE) {

                                $rowMessage['type'] = 'error';

                                $rowMessage['msg'] = 'Hiba történt a képek mentése során.';

                            }

                        }

                        

                        // type_id módosítások

                        $dbres = $objDb->query("

                            DELETE FROM

                                gallery_type

                            WHERE

                                gallery_id = {$tblPostData['id']}

                        ");

                        if (isset($tblPostData['type_id']) && !empty($tblPostData['type_id']) ){

                            foreach ($tblPostData['type_id'] as $numTypeId) {

                                $strQueryInsert = "

                                    INSERT INTO

                                        gallery_type(

                                             gallery_id

                                            ,type_id

                                        ) VALUES (

                                             {$tblPostData['id']}

                                            ,{$numTypeId}

                                        )

                                ";

                                 //                                          print $strQueryInsert;

                                 $dbres = $objDb->query($strQueryInsert);

                                 if ($dbres === FALSE) {

                                     $rowMessage['type'] = 'error';

                                     $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';

                                     break;

                                 }

                            }

                        }

                        

                        if ($rowMessage['type'] != 'error') {

                            $rowMessage['type'] = 'ok';

                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';

                        }

                    } else {

                        $rowMessage['type'] = 'error';

                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';

                    }

                }

            }

            

            return $rowMessage;

        }

        

        /**

         * Modulhoz tartozó fájlfeltöltések kezelésére

         * @param array $tblFileData

         * @param array $tblPostData

         * @param integer $numId

         * @return boolean

         */

        public static function saveFile($tblFileData, $tblPostData, $numId)

        {

//             Még nem lehet a modulepage függvény alá tenni muliupload insert miatt

//             $tblImgSize['big']['width'] = 600;

//             $tblImgSize['big']['height'] = 315;

//             $tblImgSize['small']['width'] = 515;

//             $tblImgSize['small']['height'] = 270;

            

//             $rowTable['table'] = self::$_strTable;

//             $rowTable['column'] = 'gallery_image';

            

//             return parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize);

            

            global $objDb, $CONF, $objUser;

            

            if(

                isset($tblFileData['name']['gallery_image']) && isset($tblFileData['tmp_name']['gallery_image'])

                && !empty($tblFileData['name']['gallery_image']) && !empty($tblFileData['tmp_name']['gallery_image'])

                && !empty($tblFileData['name']['gallery_image'][0]) && !empty($tblFileData['tmp_name']['gallery_image'][0])

            ){

                foreach ($tblFileData['name']['gallery_image'] as $numIdx => $strImageName) {

                    $strNewFileName = createUrl(pathinfo($strImageName)['filename'], 64)."-".date("ymdHis");

                    

                    $returnSaveFile = saveFile (

                        $CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."original".DIRECTORY_SEPARATOR

                        ,$strImageName

                        ,$tblFileData['tmp_name']['gallery_image'][$numIdx]

                        ,$strNewFileName

                        );



                    if ($returnSaveFile === FALSE) {

                        return FALSE;

                    } else { //ha rendben volt a mentés, visszaadja a mentett nevet kiterjesztéssel együtt

                        

                        if (!file_exists($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."big") && !is_dir($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."big")) {

                            mkdir($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."big", 0777, true);

                        }

                        if (!file_exists($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."small") && !is_dir($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."small")) {

                            mkdir($CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."small", 0777, true);

                        }

                        

                        //A big könyvtárba kerül az átméretezett kép a háttérképhez képest középre helyezve

                        $bigDst=$CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."big".DIRECTORY_SEPARATOR.$returnSaveFile;

                        $bigWidth = 600;

                        $bigHeight = 315;

                        bgImgDisplayer(

                            $CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."original".DIRECTORY_SEPARATOR.$returnSaveFile

                            ,$bigDst

                            ,$bigWidth

                            ,$bigHeight

                            ,255,255,255

                        );

                        

                        //A small könyvtárba kerül a kép

                        $bigDst=$CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."small".DIRECTORY_SEPARATOR.$returnSaveFile;

                        $bigWidth = 515;

                        $bigHeight = 270;

                        bgImgDisplayer(

                            $CONF['pub_dir'].$tblPostData['module_url'].DIRECTORY_SEPARATOR.$numId.DIRECTORY_SEPARATOR."original".DIRECTORY_SEPARATOR.$returnSaveFile

                            ,$bigDst

                            ,$bigWidth

                            ,$bigHeight

                            ,255,255,255

                        );

                        

                        $strQueryInsert = "

                            INSERT INTO

                                gallery_image(

                                     gallery_id

                                    ,image

                                    ,priority

                                    ,create_user_id

                                ) VALUES (

                                     {$numId}

                                    ,'{$returnSaveFile}'

                                    ,(SELECT COALESCE(max(t.priority)+1, 1) FROM gallery_image AS t WHERE t.delete_date IS NULL AND t.gallery_id = {$numId})

                                    ,{$objUser->getNumUserId()}

                                )

                        ";

                        $dbres = $objDb->query($strQueryInsert);

                        if ($dbres === FALSE) {

                            return FALSE;

                        }

                    }

                }       

            }

            

            return TRUE;

        }

        

        public function run(){ 

            global $objSmarty;

            

            if ($this->getData() === true) {

                $objSmarty->assign("tblData", $this->_tblData);

                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);

                $objSmarty->assign("tblType", $this->_tblType);

                $objSmarty->assign("tblCity", $this->_tblCity);

            }

            

        }

        

    }

    