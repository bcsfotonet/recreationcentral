<?php 
    require_once 'ModulePage.php';
    
    class Magazinok extends ModulePage {
        private static $_strTable = 'magazine';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_strImageDirUrl;
        private $_strImageDir;
        private $_strFileDirUrl;
        private $_strFileDir;
        private $_tblYearAndIssue;
        private $_tblStudy;
        private $_tblEditor;
        
        function __construct( $numModuleId ){
            global $CONF;
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_strImageDirUrl = $CONF['base_url']."media/pub/magazinok/";
            $this->_strImageDir = $CONF['pub_dir']."magazinok/";
            $this->_strFileDirUrl = $CONF['base_url']."media/pub/magazinok/";
            $this->_strFileDir = $CONF['pub_dir']."magazinok/";
            $this->_tblYearAndIssue = array();
            $this->_tblStudy = array();
            $this->_tblEditor = array();
            $this->_tblListData = array(
//                "column" => array(
//                    "title" => "Cím"
//                )
//                ,"search" => array(
//                    "title" => array("text" => "Cím", "type" => "input")
//                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                $this->_strImageDirUrl .= $this->_numEditedRowId."/small/";
                $this->_strImageDir .= $this->_numEditedRowId."/small/";
                $this->_strFileDirUrl .= $this->_numEditedRowId."/";
                $this->_strFileDir .= $this->_numEditedRowId."/";
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                    id
                    ,year_and_issue_id
                    ,( SELECT title FROM year_and_issue WHERE delete_date IS NULL AND id = year_and_issue_id ) as year_and_issue_title
                    ,main_editor
                    ,main_editor_en
                    ,main_editor_ro
                    ,orderable
                    ,lang
                    ,lang_en
                    ,lang_ro
                    ,foundation_year
                    ,paper_issn
                    ,online_issn
                    ,editorial_address
                    ,editorial_address_en
                    ,editorial_address_ro
                    ,publication_year
                    ,volume
                    ,description
                    ,description_en
                    ,description_ro
                    ,image
                    ,filename
                    ,is_active
                    ,priority
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                    ".self::$_strTable." 
                WHERE 
                    delete_date IS NULL
                ORDER BY
                    priority
            ");
            
            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("magazinok");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                    
                    if (!empty($this->_tblEditedRowData['image']) && file_exists($this->_strImageDir.$this->_tblEditedRowData['image'])) {
                        $this->_tblEditedRowData['image'] = $this->_strImageDirUrl.$this->_tblEditedRowData['image'];
                    } else {
                        $this->_tblEditedRowData['image'] = null;
                    }
                    
                    if (!empty($this->_tblEditedRowData['filename']) && file_exists($this->_strFileDir.$this->_tblEditedRowData['filename'])) {
                        $this->_tblEditedRowData['filename'] = $this->_strFileDirUrl.$this->_tblEditedRowData['filename'];
                    } else {
                        $this->_tblEditedRowData['filename'] = null;
                    }
                }
                
                //kapcsolódó tanulmányok lekérése
                $tblStudyId = $objDb->getCol("
                    SELECT
                        study_id
                    FROM
                        magazine_study
                    WHERE
                        magazine_id = {$this->_numEditedRowId} 
                ");
                if ($tblStudyId !== FALSE) {
                    $this->_tblEditedRowData['study_id'] = $tblStudyId;
                }
                
                // kapcsolódó szerkesztobizottsagi tagok lekérése
                $tblStaffId = $objDb->getCol("
                    SELECT
                        staff_id
                    FROM
                        magazine_staff
                    WHERE
                        magazine_id = {$this->_numEditedRowId} 
                ");
                if ($tblStaffId !== FALSE) {
                    $this->_tblEditedRowData['staff_id'] = $tblStaffId;
                }
     
            } else {
                if (!empty($tblData) && $tblData !== FALSE) {
                    foreach ($tblData as $numIdx => $rowData) {
                        if (!empty($rowData['image']) && file_exists($this->_strImageDir.$rowData['id']."/small/".$rowData['image'])) {
                            $tblData[$numIdx]['image'] = $this->_strImageDirUrl.$rowData['id']."/small/".$rowData['image'];
                        } else {
                            $tblData[$numIdx]['image'] = null;
                        }
                        
//                        if (!empty($rowData['filename']) && file_exists($this->_strFileDir.$rowData['id']."/".$rowData['filename'])) {
//                            $tblData[$numIdx]['filename'] = $this->_strFileDirUrl.$rowData['id']."/".$rowData['filename'];
//                        } else {
//                            $tblData[$numIdx]['filename'] = null;
//                        }
                    }
                }
            }   
            
            // Választható évfolyam és szám lekérése
            $tblYearAndIssue = $objDb->getAll("
                SELECT
                    id AS value
                    ,coalesce(NULLIF(title,''),NULLIF(title_en,''),title_ro) AS text
                FROM
                    year_and_issue
                WHERE
                    delete_date IS NULL
                ORDER BY
                    priority
            ");
            
            if ($tblYearAndIssue !== FALSE) {
                $this->_tblYearAndIssue[-1]['value'] = "";
                $this->_tblYearAndIssue[-1]['text'] = "Nem tartozik hozzá évfolyam és szám";
                
                foreach ($tblYearAndIssue as $numIdx => $rowYearAndIssue) {
                    $this->_tblYearAndIssue[$numIdx] = $rowYearAndIssue;
                    $this->_tblYearAndIssue[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['year_and_issue_id']) && is_numeric($this->_tblEditedRowData['year_and_issue_id'])) {
                        if ($rowYearAndIssue['value'] == $this->_tblEditedRowData['year_and_issue_id']) {
                            $this->_tblYearAndIssue[$numIdx]['selected'] = "selected";
                        }
                    }
                    
                }
     
            }
            
            // kapcsolódó tanulmányok
            $tblStudy = $objDb->getAll("
                SELECT
                     id AS value
                    ,concat(coalesce(NULLIF(title,''),NULLIF(title_en,''),title_ro), case when is_active = 0 then ' (inaktív)' else '' end) AS text
                FROM
                    study
                WHERE
                    delete_date IS NULL
                ORDER BY
                    title
            ");
                    
            if ($tblStudy!== FALSE) {
                
                $this->_tblStudy[-1]['value'] = "";
                $this->_tblStudy[-1]['text'] = "Nem tartozik hozzá tanulmány";
                
                foreach ($tblStudy as $numIdx => $rowStudy) {
                    $this->_tblStudy[$numIdx] = $rowStudy;
                    $this->_tblStudy[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['study_id']) && !empty($this->_tblEditedRowData['study_id'])) {
                        if (in_array($rowStudy['value'], $this->_tblEditedRowData['study_id'])) {
                            $this->_tblStudy[$numIdx]['selected'] = "selected"; 
                        }
                    }
                }
            }
            
            // kapcsolódó szerkesztőbizottsági tagok lekérése
            $tblEditor = $objDb->getAll("
                SELECT
                     id AS value
                    ,concat(name, case when is_active = 0 then ' (inaktív)' else '' end)  AS text
                FROM
                    staff
                WHERE
                    delete_date IS NULL
                    AND type_id = 6 -- szerkesztobizottsag
                    -- AND is_active = 1
                ORDER BY
                    name
            ");
                    
            if ($tblEditor!== FALSE) {
                
                $this->_tblEditor[-1]['value'] = "";
                $this->_tblEditor[-1]['text'] = "Nem tartozik hozzá szerkesztőbizottsági tag";
                
                foreach ($tblEditor as $numIdx => $rowEditor) {
                    $this->_tblEditor[$numIdx] = $rowEditor;
                    $this->_tblEditor[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['staff_id']) && !empty($this->_tblEditedRowData['staff_id'])) {
                        if (in_array($rowEditor['value'], $this->_tblEditedRowData['staff_id'])) {
                            $this->_tblEditor[$numIdx]['selected'] = "selected"; 
                        }
                    }
                }
            }
            
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            global $objDb, $objUser;
            $rowMessage = parent::removeMainTableData ($numRowId, self::$_strTable);
            
            // sorrend újragenerálása miatt ebben a modulban nem elég a sima törlés
            if ($rowMessage['type'] == 'ok') {
                $dbres = $objDb->query("
                    UPDATE
                        ".self::$_strTable."
                    SET
                         modify_date = now()
                        ,modify_user_id = {$objUser->getNumUserId()}
                        ,priority = priority-1
                    WHERE
                        delete_date IS NULL
                        AND priority > (SELECT t.priority FROM (select * from ".self::$_strTable.") AS t WHERE t.id = {$numRowId})
                ");
                if ($dbres === true) {
                    $rowMessage = array('type' => 'ok', 'msg' => 'Az adatok törlése sikeresen megtörtént.', 'refresh' => 'true');
                } else {
                    $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt az adatok törlése során.');
                }
            }
            return $rowMessage;
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData, $tblFileData = null)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            
            //TODO ellenőrzések 
            
            return $rowMessage;
        }
           
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData, $tblFileData = null)
        {
            global $objDb, $objUser, $CONF;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO 
                        ".self::$_strTable."(
                            year_and_issue_id
                            ,main_editor
                            ,main_editor_en
                            ,main_editor_ro
                            ,orderable
                            ,lang
                            ,lang_en
                            ,lang_ro
                            ,foundation_year
                            ,paper_issn
                            ,online_issn
                            ,editorial_address
                            ,editorial_address_en
                            ,editorial_address_ro
                            ,publication_year
                            ,volume
                            ,description
                            ,description_en
                            ,description_ro
                            ,priority
                            ,create_user_id
                        ) VALUES (
                            ".(isset($tblPostData['year_and_issue_id']) && !empty($tblPostData['year_and_issue_id']) ? "'{$tblPostData['year_and_issue_id']}'" : "NULL")."
                            ,".(isset($tblPostData['main_editor']) && !empty($tblPostData['main_editor']) ? "'{$tblPostData['main_editor']}'" : "''")."
                            ,".(isset($tblPostData['main_editor_en']) && !empty($tblPostData['main_editor_en']) ? "'{$tblPostData['main_editor_en']}'" : "''")."
                            ,".(isset($tblPostData['main_editor_ro']) && !empty($tblPostData['main_editor_ro']) ? "'{$tblPostData['main_editor_ro']}'" : "''")."                            
                            ,".(isset($tblPostData['orderable']) ? ($tblPostData['orderable'] == 'true' ? "1" : "0") : "0")."
                            ,".(isset($tblPostData['lang']) && !empty($tblPostData['lang']) ? "'{$tblPostData['lang']}'" : "''")."
                            ,".(isset($tblPostData['lang_en']) && !empty($tblPostData['lang_en']) ? "'{$tblPostData['lang_en']}'" : "''")."
                            ,".(isset($tblPostData['lang_ro']) && !empty($tblPostData['lang_ro']) ? "'{$tblPostData['lang_ro']}'" : "''")."
                            ,".(isset($tblPostData['foundation_year']) && !empty($tblPostData['foundation_year']) ? "'{$tblPostData['foundation_year']}'" : "''")."
                            ,".(isset($tblPostData['paper_issn']) && !empty($tblPostData['paper_issn']) ? "'{$tblPostData['paper_issn']}'" : "''")."
                            ,".(isset($tblPostData['online_issn']) && !empty($tblPostData['online_issn']) ? "'{$tblPostData['online_issn']}'" : "''")."
                            ,".(isset($tblPostData['editorial_address']) && !empty($tblPostData['editorial_address']) ? "'{$tblPostData['editorial_address']}'" : "''")."
                            ,".(isset($tblPostData['editorial_address_en']) && !empty($tblPostData['editorial_address_en']) ? "'{$tblPostData['editorial_address_en']}'" : "''")."
                            ,".(isset($tblPostData['editorial_address_ro']) && !empty($tblPostData['editorial_address_ro']) ? "'{$tblPostData['editorial_address_ro']}'" : "''")."
                            ,".(isset($tblPostData['publication_year']) && !empty($tblPostData['publication_year']) ? "'{$tblPostData['publication_year']}'" : "''")."
                            ,".(isset($tblPostData['volume']) && !empty($tblPostData['volume']) ? "'{$tblPostData['volume']}'" : "''")."                         
                            ,".(isset($tblPostData['description']) && !empty($tblPostData['description']) ? "'".addslashes(trim($tblPostData['description']))."'" : "NULL")."
                            ,".(isset($tblPostData['description_en']) && !empty($tblPostData['description_en']) ? "'".addslashes(trim($tblPostData['description_en']))."'" : "NULL")."
                            ,".(isset($tblPostData['description_ro']) && !empty($tblPostData['description_ro']) ? "'".addslashes(trim($tblPostData['description_ro']))."'" : "NULL")."
                            ,(SELECT COALESCE(max(t.priority)+1, 1) FROM ".self::$_strTable." AS t WHERE t.delete_date IS NULL) 
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
//                     die;
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    // study_id mentések
                    if (isset($tblPostData['study_id']) && !empty($tblPostData['study_id']) ){
                        foreach ($tblPostData['study_id'] as $numStudyId) {
                            $strQueryInsert = "
                                INSERT INTO 
                                    magazine_study(
                                        magazine_id
                                        ,study_id
                                    ) VALUES (
                                         {$numNewId}
                                        ,{$numStudyId}
                                    )
                            ";
//                                          print $strQueryInsert;
                            $dbres2 = $objDb->query($strQueryInsert);
                            if ($dbres2 === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                break;
                            }
                        }
                    }
                    
                    // staff_id mentések
                    if (isset($tblPostData['staff_id']) && !empty($tblPostData['staff_id']) ){
                        foreach ($tblPostData['staff_id'] as $numStaffId) {
                            $strQueryInsert = "
                                INSERT INTO 
                                    magazine_staff(
                                        magazine_id
                                        ,staff_id
                                    ) VALUES (
                                         {$numNewId}
                                        ,{$numStaffId}
                                    )
                            ";
//                                          print $strQueryInsert;
                            $dbresStaff = $objDb->query($strQueryInsert);
                            if ($dbresStaff === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                break;
                            }
                        }
                    }
                    
                    // Kép mentés
                    if (!empty($tblFileData)) {
                        if (self::saveFile($tblFileData, $tblPostData, $numNewId) === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                        }
                    }
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }

            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['year_and_issue_id']) && $rowOldData['year_and_issue_id'] != $tblPostData['year_and_issue_id'] ? ",year_and_issue_id = '{$tblPostData['year_and_issue_id']}'" : "")."
                            ".(isset($tblPostData['main_editor']) && $rowOldData['main_editor'] != $tblPostData['main_editor'] ? ",main_editor = '{$tblPostData['main_editor']}'" : "")."
                            ".(isset($tblPostData['main_editor_en']) && $rowOldData['main_editor_en'] != $tblPostData['main_editor_en'] ? ",main_editor_en = '{$tblPostData['main_editor_en']}'" : "")."
                            ".(isset($tblPostData['main_editor_ro']) && $rowOldData['main_editor_ro'] != $tblPostData['main_editor_ro'] ? ",main_editor_ro = '{$tblPostData['main_editor_ro']}'" : "")."
                            ".(isset($tblPostData['row_save']) && $tblPostData['row_save'] === false ? (isset($tblPostData['orderable']) && $rowOldData['orderable'] != $tblPostData['orderable'] ? ",orderable = ".($tblPostData['orderable'] == 'true' ? "1" : "0") : ",orderable = 0") : "")."
                            ".(isset($tblPostData['lang']) && $rowOldData['lang'] != $tblPostData['lang'] ? ",lang = '{$tblPostData['lang']}'" : "")."
                            ".(isset($tblPostData['lang_en']) && $rowOldData['lang_en'] != $tblPostData['lang_en'] ? ",lang_en = '{$tblPostData['lang_en']}'" : "")."
                            ".(isset($tblPostData['lang_ro']) && $rowOldData['lang_ro'] != $tblPostData['lang_ro'] ? ",lang_ro = '{$tblPostData['lang_ro']}'" : "")."
                            ".(isset($tblPostData['foundation_year']) && $rowOldData['foundation_year'] != $tblPostData['foundation_year'] ? ",foundation_year = '{$tblPostData['foundation_year']}'" : "")."
                            ".(isset($tblPostData['paper_issn']) && $rowOldData['paper_issn'] != $tblPostData['paper_issn'] ? ",paper_issn = '{$tblPostData['paper_issn']}'" : "")."
                            ".(isset($tblPostData['online_issn']) && $rowOldData['online_issn'] != $tblPostData['online_issn'] ? ",online_issn = '{$tblPostData['online_issn']}'" : "")."
                            ".(isset($tblPostData['editorial_address']) && $rowOldData['editorial_address'] != $tblPostData['editorial_address'] ? ",editorial_address = '{$tblPostData['editorial_address']}'" : "")."
                            ".(isset($tblPostData['editorial_address_en']) && $rowOldData['editorial_address_en'] != $tblPostData['editorial_address_en'] ? ",editorial_address_en = '{$tblPostData['editorial_address_en']}'" : "")."
                            ".(isset($tblPostData['editorial_address_ro']) && $rowOldData['editorial_address_ro'] != $tblPostData['editorial_address_ro'] ? ",editorial_address_ro = '{$tblPostData['editorial_address_ro']}'" : "")."
                            ".(isset($tblPostData['publication_year']) && $rowOldData['publication_year'] != $tblPostData['publication_year'] ? ",publication_year = '{$tblPostData['publication_year']}'" : "")."
                            ".(isset($tblPostData['volume']) && $rowOldData['volume'] != $tblPostData['volume'] ? ",volume = '{$tblPostData['volume']}'" : "")."
                            ".(isset($tblPostData['description']) && $rowOldData['description'] != $tblPostData['description'] ? ",description = '".addslashes(trim($tblPostData['description']))."'" : "")."
                            ".(isset($tblPostData['description_en']) && $rowOldData['description_en'] != $tblPostData['description_en'] ? ",description_en = '".addslashes(trim($tblPostData['description_en']))."'" : "")."
                            ".(isset($tblPostData['description_ro']) && $rowOldData['description_ro'] != $tblPostData['description_ro'] ? ",description_ro = '".addslashes(trim($tblPostData['description_ro']))."'" : "")."
                            ".(isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority'] ? ",priority = ".(is_numeric($tblPostData['priority']) ? "{$tblPostData['priority']}" : "1") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE) {
                        
                        if (isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority']) {
                            $strQueryUpdate = "
                                UPDATE
                                    ".self::$_strTable."
                                SET
                                     modify_date = now()
                                    ,modify_user_id = {$objUser->getNumUserId()}
                                    ,priority = priority ".($tblPostData['priority'] < $rowOldData['priority'] ? "+1" : "-1")."
                                WHERE
                                    id != {$tblPostData['id']}
                                    AND delete_date IS NULL
                                    AND priority BETWEEN ".($tblPostData['priority'] < $rowOldData['priority'] ? $tblPostData['priority']." AND ".$rowOldData['priority'] : "greatest(".$rowOldData['priority'].",2) AND greatest(".$tblPostData['priority'].",2)")."
                            ";
                            $dbres = $objDb->query($strQueryUpdate);
                            if ($dbres !== FALSE) {
                                $rowMessage['refresh'] = 'true';
                            }
                        }
                        
                        // study_id módosítások
                        $dbres = $objDb->query("
                            DELETE FROM
                                magazine_study
                            WHERE
                                magazine_id = {$tblPostData['id']}
                        ");                                               
                        if (isset($tblPostData['study_id']) && !empty($tblPostData['study_id']) ){
                            foreach ($tblPostData['study_id'] as $numStudyId) {
                                $strQueryInsert = "
                                    INSERT INTO
                                        magazine_study(
                                            magazine_id
                                            ,study_id
                                        ) VALUES (
                                             {$tblPostData['id']}
                                            ,{$numStudyId}
                                        )
                                ";
//                                          print $strQueryInsert;
                                 $dbres = $objDb->query($strQueryInsert);
                                 if ($dbres === FALSE) {
                                     $rowMessage['type'] = 'error';
                                     $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                     break;
                                 }
                            }
                        }
                        
                        if ($dbres === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                        }
                        
                        // staff_id módosítások
                        $dbresStaff = $objDb->query("
                            DELETE FROM
                                magazine_staff
                            WHERE
                                magazine_id = {$tblPostData['id']}
                        ");     
                                
                        if (isset($tblPostData['staff_id']) && !empty($tblPostData['staff_id']) ){
                            foreach ($tblPostData['staff_id'] as $numStaffId) {
                                $strQueryInsert = "
                                    INSERT INTO
                                        magazine_staff(
                                             magazine_id
                                            ,staff_id
                                        ) VALUES (
                                             {$tblPostData['id']}
                                            ,{$numStaffId}
                                        )
                                ";
//                                          print $strQueryInsert;
                                 $dbresStaff = $objDb->query($strQueryInsert);
                                 if ($dbresStaff === FALSE) {
                                     $rowMessage['type'] = 'error';
                                     $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                     break;
                                 }
                            }
                        }
                        
                        if ($dbresStaff === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                        }
                        
                        // Kép módosítás
//                        var_dump($tblFileData);
                        if (!empty($tblFileData)) {
                            if (self::saveFile($tblFileData, $tblPostData, $tblPostData['id']) === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                            }
                        }

                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }

                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        /**
         * Modulhoz tartozó fájlfeltöltések kezelésére
         * @param array $tblFileData
         * @param array $tblPostData
         * @param integer $numId
         * @return boolean
         */
        public static function saveFile($tblFileData, $tblPostData, $numId)
        {
            $isOk = TRUE;
            
            $tblImgSize['big']['width'] = 545;
            $tblImgSize['big']['height'] = 768;
            $tblImgSize['small']['width'] = 545;
            $tblImgSize['small']['height'] = 768;
            
//            $rowTable['table'] = self::$_strTable;
//            $rowTable['column'] = 'image';       
            
            $rowTable['table'] = self::$_strTable;
//          $rowTable['column'] = 'filename';
            
            foreach ($tblFileData['name'] as $strColumn => $strFilename) {
//                 var_dump($strColumn);
                $rowTable['column'] = $strColumn;
                if($strColumn=='image'){
                    $isOk = parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize);
                }else{
                   $isOk = parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable); 
                }
                if ($isOk === FALSE) {
                    break;
                }
            }
            
            return $isOk;
//            return parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize);
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
                $objSmarty->assign("tblYearAndIssue", $this->_tblYearAndIssue);
                $objSmarty->assign("tblStudy", $this->_tblStudy);
                $objSmarty->assign("tblEditor", $this->_tblEditor);
            }
            
        }
        
    }
    