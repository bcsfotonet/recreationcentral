<?php 
    require_once 'ModulePage.php';
    
    class Menukezelo extends ModulePage {
        private static $_strTable = 'menu';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_tblParentMenu;
        private $_tblTargetValue;
        private $_tblContentUrl;
        
        function __construct( $numModuleId ){
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_tblParentMenu = array();
            $this->_tblTargetValue = array();
            $this->_tblContentUrl = array();
            
            $this->_tblListData = array(
                "column" => array(
                     "name" => "Név"
                    ,"priority" => "Sorrend"
                )
                ,"search" => array(
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
//                var_dump($_POST);die();
//TODO post ellenőrzés
//                 if (isset($_POST['adminisztratorok_field_name'])) {
//                     $this->saveData($_POST);
//                 }
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {
//                 var_dump($_POST);die();
                if (isset($_POST['menukezelo_field']) && !empty($_POST['menukezelo_field'])) {
                    
                    
//                     if ($this->saveValidation($_POST['menukezelo_field']) !== TRUE) {
//                         var_dump($this->_rowMessage);
                        
// die();
//                     } else {
//                         $this->saveData($_POST['menukezelo_field']);
//                     }
                }
//                 if (isset($_POST['adminisztratorok_field_name'])) {
//                     if ($this->saveData($_POST) === TRUE){
//                         $this->setHeaderLocation("adminisztratorok/edit/".$this->_numEditedRowId);
//                     }
//                 }
            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                    id
                    ,name
                    ,name_en
                    ,name_ro
                    ,parent_menu_id
                    ,is_visible_main_menu
                    ,is_visible_footer_menu
                    ,is_active
                    ,priority
                    ,content_table
                    ,content_id
                    ,ext_url
                    ,ext_url_en
                    ,ext_url_ro
                    ,target_value
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                    menu
                WHERE
                    delete_date IS NULL
                ORDER BY
                    parent_menu_id, priority
            ");
            
            if (is_numeric($this->_numEditedRowId)) {
                $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                
                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("menukezelo");
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                foreach ($tblData as $numRowId => $rowData) {
                    
                    if (is_numeric($rowData['parent_menu_id'])) {
                        $this->_tblData[$rowData['parent_menu_id']]['child'][$numRowId] = $rowData;
                    } else {
                        $this->_tblData[$numRowId]['main'] = $rowData;
                    }
                }    
            }
//             print_r($tblData);die();
            
            //Lehetséges főmenük listájának lekérése
            $tblParentMenu = $objDb->getAllIdIdx("
                SELECT 
                	 id
                    ,concat(
                        name, 
                        (CASE WHEN is_active = 0 THEN ' - inaktív' ELSE '' END)
                    ) AS name
                FROM 
                	menu 
                WHERE 
                	delete_date is null
                    /* AND is_active = 1 */
                    AND parent_menu_id IS NULL
                    AND is_visible_footer_menu = 0 /* láblécben lévő menü nem lehet kétszintű */
                    ".(is_numeric($this->_numEditedRowId) ? "AND id != {$this->_numEditedRowId}" : "")." /* saját maga ne lehessen saját maga őse */
                ORDER BY
                	priority, name
            ");
            if (!empty($tblParentMenu) && $tblParentMenu !== FALSE) {
                $this->_tblParentMenu[0]['value'] = "";
                $this->_tblParentMenu[0]['text'] = "Nem tartozik menüpont alá";
                foreach ($tblParentMenu as $numId => $rowParentMenu) {
                    $this->_tblParentMenu[$numId]['value'] = $rowParentMenu['id'];
                    $this->_tblParentMenu[$numId]['text'] = $rowParentMenu['name'];
                    $this->_tblParentMenu[$numId]['selected'] = "";
                    if (!empty($this->_tblEditedRowData) && $rowParentMenu['id'] == $this->_tblEditedRowData['parent_menu_id']) {
                        $this->_tblParentMenu[$numId]['selected'] = "selected";
                    }
                }
            }
            
            //Külső url megnyitásának lehetőségeti - target_value
            $this->_tblTargetValue = array(
                array(
                    "value" => ""
                    ,"text" => "Nincs külső URL"
                    ,"selected" => ""
                )
                ,
                array(
                     "value" => "_blank"
                    ,"text" => "Új ablakban"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "_blank" == $this->_tblEditedRowData['target_value'] ? "selected" : "")  
                )
                ,
                array(
                    "value" => "_self"
                    ,"text" => "Ugyanabban az ablakban"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "_self" == $this->_tblEditedRowData['target_value'] ? "selected" : "")
                )
            );
            
            //Tartalom beállítása - content_url
            $tblContentUrl = $objDb->getAll("
                ( /* Egyedi oldaltartalmak, típusok alapján */
                    SELECT
                         name
                        ,url
                        ,id AS content_id
                        ,'type' AS content_table
                        ,concat(name, ' (egyedi oldal)') AS select_text
                    FROM 
                    	type 
                    WHERE 
                    	delete_date IS NULL
                        AND is_active = 1
                        AND url IS NOT NULL
                ) 
                UNION
                ( /* Statikus oldaltartalmak */
                    SELECT 
                    	 title AS name
                        ,url
                        ,id AS content_id
                        ,'static' AS content_table
                        ,concat(title, ' (statikus tartalom)') AS select_text
                    FROM 
                    	static 
                    WHERE 
                    	delete_date IS NULL
                        AND is_active = 1
                        AND url IS NOT NULL
                    ORDER BY
                    	title
                )
                ORDER BY 
                	name, content_table
            ");
            if (!empty($tblContentUrl) && $tblContentUrl !== FALSE) {
                foreach ($tblContentUrl as $numId => $rowContentUrl) {
                    $this->_tblContentUrl[$numId]['value'] = $rowContentUrl['content_table'].'#'.$rowContentUrl['content_id'];
                    $this->_tblContentUrl[$numId]['text'] = $rowContentUrl['select_text']; //$rowContentUrl['name']
                    $this->_tblContentUrl[$numId]['selected'] = "";
                    if (!empty($this->_tblEditedRowData) && $rowContentUrl['content_id'] == $this->_tblEditedRowData['content_id'] && $rowContentUrl['content_table'] == $this->_tblEditedRowData['content_table']) {
                        $this->_tblContentUrl[$numId]['selected'] = "selected";
                    }
                }
                array_unshift($this->_tblContentUrl, array("text" => "Kérem válasszon", "value" => ""));
            }

            return true;
        }
          
        /**
         * Törli a sort
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            global $objDb, $objUser;
            $rowMessage = array('type' => 'error', 'msg' => null);
            
            // kétszintű menü miatt nem használható az ősosztály alap törlése
            $dbres = $objDb->query("
                UPDATE
                    ".self::$_strTable."
                SET
                     delete_date = now()
                    ,delete_user_id = {$objUser->getNumUserId()}
                WHERE
                    (
                        id = {$numRowId}
                        OR parent_menu_id = {$numRowId}
                    )
                    AND delete_date IS NULL
            ");
            if ($dbres === true) {
                
                $dbres = $objDb->query("
                    UPDATE
                        ".self::$_strTable."
                    SET
                         modify_date = now()
                        ,modify_user_id = {$objUser->getNumUserId()}
                        ,priority = priority-1
                    WHERE
                        delete_date IS NULL
                        AND priority > (SELECT t.priority FROM (select * from ".self::$_strTable.") AS t WHERE t.id = {$numRowId})
                        AND 
                            (CASE 
                            	WHEN (SELECT t.parent_menu_id FROM (select * from ".self::$_strTable.") AS t WHERE t.id = {$numRowId}) IS NULL 
                            	THEN parent_menu_id IS NULL 
                             	ELSE parent_menu_id = (SELECT t.parent_menu_id FROM (select * from ".self::$_strTable.") AS t WHERE t.id = {$numRowId}) 
                            END)
                ");
                $rowMessage = array('type' => 'ok', 'msg' => 'Az adatok törlése sikeresen megtörtént.', 'refresh' => 'true');
            } else {
                $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt az adatok törlése során.');
            }
            
            return $rowMessage;
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            //TODO ellenőrzések
            
            // vagy belső vagy külső tartalomra vinnie kell a menüpontnak, ha nem főmenü
            if (
                isset($tblPostData['parent_menu_id']) && is_numeric($tblPostData['parent_menu_id'])
                && empty($tblPostData['content_url']) && empty($tblPostData['ext_url'])
            ) {
                $rowMessage['type'] = 'error';
                $rowMessage['msg'] = 'Ha nem főmenü, akkor belső vagy külső tartalomra hivatkoznia kell a menüpontnak.';
            }
            
            return $rowMessage;
        }
        
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData)
        {
            global $objDb, $objUser;
            $rowMessage = array('type' => null, 'msg' => null); 
            
            if (isset($tblPostData['content_url']) && strpos($tblPostData['content_url'], '#') !== false) {
                $rowContentUrl = explode("#", $tblPostData['content_url']);
            }
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO
                        ".self::$_strTable."(
                            name
                            ,name_en
                            ,name_ro
                            ,parent_menu_id
                            ,is_visible_main_menu
                            ,is_visible_footer_menu
                            ,content_table
                            ,content_id
                            ,ext_url
                            ,ext_url_en
                            ,ext_url_ro
                            ,target_value
                            ,priority
                            ,create_user_id
                        ) VALUES (
                             '{$tblPostData['name']}'
                            ,".(!empty($tblPostData['name_en']) ? "'{$tblPostData['name_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['name_ro']) ? "'{$tblPostData['name_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['parent_menu_id']) && is_numeric($tblPostData['parent_menu_id']) ? "{$tblPostData['parent_menu_id']}" : "NULL")."
                            ,0
                            ,0
                            ,".(!empty($rowContentUrl) ? "'{$rowContentUrl[0]}'" : "NULL")." /* table része */
                            ,".(!empty($rowContentUrl) ? "'{$rowContentUrl[1]}'" : "NULL")." /* id része */
                            ,".(!empty($tblPostData['ext_url']) ? "'{$tblPostData['ext_url']}'" : "NULL")."
                            ,".(!empty($tblPostData['ext_url_en']) ? "'{$tblPostData['ext_url_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['ext_url_ro']) ? "'{$tblPostData['ext_url_ro']}'" : "NULL")."
                            ,".(!empty($tblPostData['target_value']) ? "'{$tblPostData['target_value']}'" : "NULL")."                            
                            ,(SELECT COALESCE(max(t.priority)+1, 1) FROM ".self::$_strTable." AS t WHERE t.delete_date IS NULL AND ".(isset($tblPostData['parent_menu_id']) && is_numeric($tblPostData['parent_menu_id']) ? "parent_menu_id = ".$tblPostData['parent_menu_id'] : "parent_menu_id IS NULL").") 
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $rowMessage['type'] = 'ok';
                    $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    $rowMessage['new_id'] = $dbres;
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }
            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            menu
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['name']) && $rowOldData['name'] != $tblPostData['name'] ? ",name = '{$tblPostData['name']}'" : "")."
                            ".(isset($tblPostData['name_en']) && $rowOldData['name_en'] != $tblPostData['name_en'] ? ",name_en = ".(!empty($tblPostData['name_en']) ? "'{$tblPostData['name_en']}'" : "NULL") : "")."
                            ".(isset($tblPostData['name_ro']) && $rowOldData['name_ro'] != $tblPostData['name_ro'] ? ",name_ro = ".(!empty($tblPostData['name_ro']) ? "'{$tblPostData['name_ro']}'" : "NULL") : "")."                           
                            ".(isset($tblPostData['parent_menu_id']) && $rowOldData['parent_menu_id'] != $tblPostData['parent_menu_id'] ? ",parent_menu_id = ".(is_numeric($tblPostData['parent_menu_id']) ? "{$tblPostData['parent_menu_id']}" : "NULL") : "")."
                            ".(isset($tblPostData['is_visible_main_menu']) && $rowOldData['is_visible_main_menu'] != $tblPostData['is_visible_main_menu'] ? ",is_visible_main_menu = ".($tblPostData['is_visible_main_menu'] == 'true' ? "1" : "0") : "")."
                            ".(isset($tblPostData['is_visible_footer_menu']) && $rowOldData['is_visible_footer_menu'] != $tblPostData['is_visible_footer_menu'] ? ",is_visible_footer_menu = ".($tblPostData['is_visible_footer_menu'] == 'true' ? "1" : "0") : "")."
                            ".(isset($rowContentUrl) && $rowOldData['content_table'] != $rowContentUrl ? ",content_table = ".(!empty($rowContentUrl[0]) ? "'{$rowContentUrl[0]}'" : "NULL") : "")."
                            ".(isset($rowContentUrl) && $rowOldData['content_id'] != $rowContentUrl ? ",content_id = ".(is_numeric($rowContentUrl[1]) ? "{$rowContentUrl[1]}" : "NULL") : "")."
                            ".(isset($tblPostData['ext_url']) && $rowOldData['ext_url'] != $tblPostData['ext_url'] ? ",ext_url = ".(!empty($tblPostData['ext_url']) ? "'{$tblPostData['ext_url']}'" : "NULL") : "")."
                            ".(isset($tblPostData['ext_url_en']) && $rowOldData['ext_url_en'] != $tblPostData['ext_url_en'] ? ",ext_url_en = ".(!empty($tblPostData['ext_url_en']) ? "'{$tblPostData['ext_url_en']}'" : "NULL") : "")."
                            ".(isset($tblPostData['ext_url_ro']) && $rowOldData['ext_url_ro'] != $tblPostData['ext_url_ro'] ? ",ext_url_ro = ".(!empty($tblPostData['ext_url_ro']) ? "'{$tblPostData['ext_url_ro']}'" : "NULL") : "")."
                            ".(isset($tblPostData['target_value']) && $rowOldData['target_value'] != $tblPostData['target_value'] ? ",target_value = ".(!empty($tblPostData['target_value']) ? "'{$tblPostData['target_value']}'" : "NULL") : "")."
                            ".(isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority'] ? ",priority = ".(is_numeric($tblPostData['priority']) ? "{$tblPostData['priority']}" : "0") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
                    //             print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    //ha menü sorrendet változtat
                    if ($dbres !== FALSE && isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority']) {
                        $strQueryUpdate = "
                            UPDATE
                                ".self::$_strTable."
                            SET
                                 modify_date = now()
                                ,modify_user_id = {$objUser->getNumUserId()}
                                ,priority = priority ".($tblPostData['priority'] < $rowOldData['priority'] ? "+1" : "-1")."
                            WHERE
                                id != {$tblPostData['id']}
                                AND parent_menu_id ".(empty($rowOldData['parent_menu_id']) ? "IS NULL" : "= ".$rowOldData['parent_menu_id'])."
                                AND delete_date IS NULL
                                AND priority BETWEEN ".($tblPostData['priority'] < $rowOldData['priority'] ? $tblPostData['priority']." AND ".$rowOldData['priority'] : "greatest(".$rowOldData['priority'].",2) AND greatest(".$tblPostData['priority'].",2)")."
                        ";
                        $dbres = $objDb->query($strQueryUpdate);
                        if ($dbres !== FALSE) {
                            $rowMessage['refresh'] = 'true';
                        }
                    }
                    
                    if ($dbres !== FALSE && empty($rowOldData['parent_menu_id']) && $rowOldData['is_active'] == 1 && isset($tblPostData['is_active']) && $tblPostData['is_active'] == 'false') {
                        //ha főmenüt inaktivál, akkor a gyerekeit is
                        $strQueryUpdate = "
                            UPDATE
                                ".self::$_strTable."
                            SET
                                 modify_date = now()
                                ,modify_user_id = {$objUser->getNumUserId()}
                                ,is_active = 0
                            WHERE
                                parent_menu_id = {$tblPostData['id']}
                        ";
                        $dbres = $objDb->query($strQueryUpdate);
                    } else if ($dbres !== FALSE && is_numeric($rowOldData['parent_menu_id']) && $rowOldData['is_active'] == 0 && isset($tblPostData['is_active']) && $tblPostData['is_active'] == 'true'){
                        //ha almenüt aktivál, akkor a főmenüjét is
                        $strQueryUpdate = "
                            UPDATE
                                ".self::$_strTable."
                            SET
                                 modify_date = now()
                                ,modify_user_id = {$objUser->getNumUserId()}
                                ,is_active = 1
                            WHERE
                                id = {$rowOldData['parent_menu_id']}
                        ";
                        $dbres = $objDb->query($strQueryUpdate);
                    }
                    
                    
                    //             var_dump($dbres);
                    if ($dbres !== FALSE) {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblParentMenu", $this->_tblParentMenu);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblTargetValue", $this->_tblTargetValue);
                $objSmarty->assign("tblContentUrl", $this->_tblContentUrl);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
//            print_r($this->_tblUserModule);
            //var_dump($this->_tblData);
//            print_r($this->_tblModule);
        }
        
    }
    