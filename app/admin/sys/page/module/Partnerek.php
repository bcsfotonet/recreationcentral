<?php 
    require_once 'ModulePage.php';
    
    class Partnerek extends ModulePage {
        private static $_strTable = 'partner';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_strImageDirUrl;
        private $_strImageDir;
        private $_tblTargetValue;
        
        function __construct( $numModuleId ){
            global $CONF;
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_tblTargetValue = array();
            $this->_strImageDirUrl = $CONF['base_url']."media/pub/partnerek/";
            $this->_strImageDir = $CONF['pub_dir']."partnerek/";
            $this->_tblListData = array(
                "column" => array(
                     "name" => "Név"
                )
                ,"search" => array(
//                    "name" => array("text" => "Név", "type" => "input")
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                $this->_strImageDirUrl .= $this->_numEditedRowId."/small/";
                $this->_strImageDir .= $this->_numEditedRowId."/small/";
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                	 id
                    ,name
                    ,name_en
                    ,name_ro
                    ,ext_url
                    ,ext_url_en
                    ,ext_url_ro
                    ,target_value
                    ,priority
                    ,is_active
                    ,image
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                	".self::$_strTable." 
                WHERE 
                	delete_date IS NULL
                ORDER BY
                    priority
            ");
            
            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("partnerek");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                    
                    if (!empty($this->_tblEditedRowData['image']) && file_exists($this->_strImageDir.$this->_tblEditedRowData['image'])) {
                        $this->_tblEditedRowData['image'] = $this->_strImageDirUrl.$this->_tblEditedRowData['image'];
                    } else {
                        $this->_tblEditedRowData['image'] = null;
                    }
                }
            } else {
                if (!empty($tblData) && $tblData !== FALSE) {
                    foreach ($tblData as $numIdx => $rowData) {
                        if (!empty($rowData['image']) && file_exists($this->_strImageDir.$rowData['id']."/small/".$rowData['image'])) {
                            $tblData[$numIdx]['image'] = $this->_strImageDirUrl.$rowData['id']."/small/".$rowData['image'];
                        } else {
                            $tblData[$numIdx]['image'] = null;
                        }
                    }
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            //Külső url megnyitásának lehetőségeti - target_value
            $this->_tblTargetValue = array(
                array(
                    "value" => ""
                    ,"text" => "Nincs külső URL"
                    ,"selected" => ""
                )
                ,
                array(
                    "value" => "_blank"
                    ,"text" => "Új ablakban"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "_blank" == $this->_tblEditedRowData['target_value'] ? "selected" : "")
                )
                ,
                array(
                    "value" => "_self"
                    ,"text" => "Ugyanabban az ablakban"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "_self" == $this->_tblEditedRowData['target_value'] ? "selected" : "")
                )
            );
            
            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            global $objDb, $objUser;
            $rowMessage = parent::removeMainTableData ($numRowId, self::$_strTable);
            
            // sorrend újragenerálása miatt ebben a modulban nem elég a sima törlés
            if ($rowMessage['type'] == 'ok') {
                $dbres = $objDb->query("
                    UPDATE
                        ".self::$_strTable."
                    SET
                         modify_date = now()
                        ,modify_user_id = {$objUser->getNumUserId()}
                        ,priority = priority-1
                    WHERE
                        delete_date IS NULL
                        AND priority > (SELECT t.priority FROM (select * from ".self::$_strTable.") AS t WHERE t.id = {$numRowId})
                ");
                if ($dbres === true) {
                    $rowMessage = array('type' => 'ok', 'msg' => 'Az adatok törlése sikeresen megtörtént.', 'refresh' => 'true');
                } else {
                    $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt az adatok törlése során.');
                }
            }

            return $rowMessage;
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData, $tblFileData = null)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            
//             var_dump(self::$_strTable);
//             var_dump($tblValidate);
            
            //TODO ellenőrzések 
            
            return $rowMessage;
        }
        
        
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData, $tblFileData = null)
        {
            global $objDb, $objUser, $CONF;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO 
                        ".self::$_strTable."(
                            name
                            ,name_en
                            ,name_ro
                            ,ext_url
                            ,ext_url_en
                            ,ext_url_ro
                            ,target_value
                            ,priority
                            ,create_user_id
                        ) VALUES (
                            '{$tblPostData['name']}'
                            ,".(isset($tblPostData['name_en']) && !empty($tblPostData['name_en']) ? "'{$tblPostData['name_en']}'" : "NULL")."
                            ,".(isset($tblPostData['name_ro']) && !empty($tblPostData['name_ro']) ? "'{$tblPostData['name_ro']}'" : "NULL")."
                            ,'{$tblPostData['ext_url']}'
                            ,".(isset($tblPostData['ext_url_en']) && !empty($tblPostData['ext_url_en']) ? "'{$tblPostData['ext_url_en']}'" : "NULL")."
                            ,".(isset($tblPostData['ext_url_ro']) && !empty($tblPostData['ext_url_ro']) ? "'{$tblPostData['ext_url_ro']}'" : "NULL")."
                            ,'{$tblPostData['target_value']}'
                            ,(SELECT COALESCE(max(t.priority)+1, 1) FROM ".self::$_strTable." AS t WHERE t.delete_date IS NULL) 
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    // Kép mentés
                    if (!empty($tblFileData)) {
                        if (self::saveFile($tblFileData, $tblPostData, $numNewId) === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                        }
                    }
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }

            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['name']) && $rowOldData['name'] != $tblPostData['name'] ? ",name = '{$tblPostData['name']}'" : "")."
                            ".(isset($tblPostData['name_en']) && $rowOldData['name_en'] != $tblPostData['name_en'] ? ",name_en = '{$tblPostData['name_en']}'" : "")."
                            ".(isset($tblPostData['name_ro']) && $rowOldData['name_ro'] != $tblPostData['name_ro'] ? ",name_ro = '{$tblPostData['name_ro']}'" : "")."
                            ".(isset($tblPostData['ext_url']) && $rowOldData['ext_url'] != $tblPostData['ext_url'] ? ",ext_url = ".(!empty($tblPostData['ext_url']) ? "'{$tblPostData['ext_url']}'" : "NULL") : "")."
                            ".(isset($tblPostData['ext_url_en']) && $rowOldData['ext_url_en'] != $tblPostData['ext_url_en'] ? ",ext_url_en = ".(!empty($tblPostData['ext_url_en']) ? "'{$tblPostData['ext_url_en']}'" : "NULL") : "")."
                            ".(isset($tblPostData['ext_url_ro']) && $rowOldData['ext_url_ro'] != $tblPostData['ext_url_ro'] ? ",ext_url_ro= ".(!empty($tblPostData['ext_url_ro']) ? "'{$tblPostData['ext_url_ro']}'" : "NULL") : "")."
                            ".(isset($tblPostData['target_value']) && $rowOldData['target_value'] != $tblPostData['target_value'] ? ",target_value = ".(!empty($tblPostData['target_value']) ? "'{$tblPostData['target_value']}'" : "NULL") : "")."
                            ".(isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority'] ? ",priority = ".(is_numeric($tblPostData['priority']) ? "{$tblPostData['priority']}" : "1") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE) {
                        
                        if (isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority']) {
                            $strQueryUpdate = "
                                UPDATE
                                    ".self::$_strTable."
                                SET
                                     modify_date = now()
                                    ,modify_user_id = {$objUser->getNumUserId()}
                                    ,priority = priority ".($tblPostData['priority'] < $rowOldData['priority'] ? "+1" : "-1")."
                                WHERE
                                    id != {$tblPostData['id']}
                                    AND delete_date IS NULL
                                    AND priority BETWEEN ".($tblPostData['priority'] < $rowOldData['priority'] ? $tblPostData['priority']." AND ".$rowOldData['priority'] : "greatest(".$rowOldData['priority'].",2) AND greatest(".$tblPostData['priority'].",2)")."
                            ";
//                             print($strQueryUpdate);
                            $dbres = $objDb->query($strQueryUpdate);
                            if ($dbres !== FALSE) {
                                $rowMessage['refresh'] = 'true';
                            }
                        }
                        if ($dbres === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                        }
                        
                        // Kép módosítás
                        if (!empty($tblFileData)) {
                            if (self::saveFile($tblFileData, $tblPostData, $tblPostData['id']) === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                            }
                        }

                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }

                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        /**
         * Modulhoz tartozó fájlfeltöltések kezelésére
         * @param array $tblFileData
         * @param array $tblPostData
         * @param integer $numId
         * @return boolean
         */
        public static function saveFile($tblFileData, $tblPostData, $numId)
        {
            $tblImgSize['big']['width'] = 540;
            $tblImgSize['big']['height'] = 360;
            $tblImgSize['small']['width'] = 180;
            $tblImgSize['small']['height'] = 120;
            
            $rowTable['table'] = self::$_strTable;
            $rowTable['column'] = 'image';
            
            return parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize);
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblTargetValue", $this->_tblTargetValue);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
        }
        
    }
    