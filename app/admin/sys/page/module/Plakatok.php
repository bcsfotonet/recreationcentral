<?php 
    require_once 'ModulePage.php';
    
    class Plakatok extends ModulePage {
        private static $_strTable = 'poster';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_strImageDirUrl;
        private $_strImageDir;
        
        function __construct( $numModuleId ){
            global $CONF;
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_strImageDirUrl = $CONF['base_url']."media/pub/plakatok/";
            $this->_strImageDir = $CONF['pub_dir']."plakatok/";
            
            $this->_tblListData = array(
                "column" => array(
                     "title" => "Cím"
                )
                ,"search" => array(
//                     "title" => array("text" => "Cím", "type" => "input")
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                $this->_strImageDirUrl .= $this->_numEditedRowId."/small/";
                $this->_strImageDir .= $this->_numEditedRowId."/small/";
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                    id
                    ,title
                    ,title_en
                    ,title_ro
                    ,image
                    ,priority
                    ,city_id
                    ,is_active
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                    ".self::$_strTable." 
                WHERE 
                    delete_date IS NULL
                ORDER BY
                    priority
            ");
            
            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("plakatok");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                    
                    if (!empty($this->_tblEditedRowData['image']) && file_exists($this->_strImageDir.$this->_tblEditedRowData['image'])) {
                        $this->_tblEditedRowData['image'] = $this->_strImageDirUrl.$this->_tblEditedRowData['image'];
                    } else {
                        $this->_tblEditedRowData['image'] = null;
                    }
                }
            } else {
                if (!empty($tblData) && $tblData !== FALSE) {
                    foreach ($tblData as $numIdx => $rowData) {
                        if (!empty($rowData['image']) && file_exists($this->_strImageDir.$rowData['id']."/small/".$rowData['image'])) {
                            $tblData[$numIdx]['image'] = $this->_strImageDirUrl.$rowData['id']."/small/".$rowData['image'];
                        } else {
                            $tblData[$numIdx]['image'] = null;
                        }
                    }
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            //Választható városok lekérése
            $tblCity = $objDb->getAll("
                SELECT
                     id AS value
                    ,name AS text
                FROM
                    city
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    name
            ");
            if ($tblCity !== FALSE) {
                $this->_tblCity[-1]['value'] = "";
                $this->_tblCity[-1]['text'] = "Nem tartozik hozzá város";
                
                foreach ($tblCity as $numIdx => $rowCity) {
                    $this->_tblCity[$numIdx] = $rowCity;
                    $this->_tblCity[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['city_id']) && is_numeric($this->_tblEditedRowData['city_id'])) {
                        if ($rowCity['value'] == $this->_tblEditedRowData['city_id']) {
                            $this->_tblCity[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            global $objDb, $objUser;
            $rowMessage = parent::removeMainTableData ($numRowId, self::$_strTable);
            
            // sorrend újragenerálása miatt ebben a modulban nem elég a sima törlés
            if ($rowMessage['type'] == 'ok') {
                $dbres = $objDb->query("
                    UPDATE
                        ".self::$_strTable."
                    SET
                         modify_date = now()
                        ,modify_user_id = {$objUser->getNumUserId()}
                        ,priority = priority-1
                    WHERE
                        delete_date IS NULL
                        AND priority > (SELECT t.priority FROM (select * from ".self::$_strTable.") AS t WHERE t.id = {$numRowId})
                ");
                if ($dbres === true) {
                    $rowMessage = array('type' => 'ok', 'msg' => 'Az adatok törlése sikeresen megtörtént.', 'refresh' => 'true');
                } else {
                    $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt az adatok törlése során.');
                }
            }
            return $rowMessage;
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData, $tblFileData = null)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            
            //TODO ellenőrzések 
            
            return $rowMessage;
        }
        
        
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData, $tblFileData = null)
        {
            global $objDb, $objUser, $CONF;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO 
                        ".self::$_strTable."(
                            title
                            ,title_en
                            ,title_ro
                            ,priority
                            ,city_id
                            ,create_user_id
                        ) VALUES (
                            '{$tblPostData['title']}'
                            ,".(isset($tblPostData['title_en']) && !empty($tblPostData['title_en']) ? "'{$tblPostData['title_en']}'" : "NULL")."
                            ,".(isset($tblPostData['title_ro']) && !empty($tblPostData['title_ro']) ? "'{$tblPostData['title_ro']}'" : "NULL")."
                            ,(SELECT COALESCE(max(t.priority)+1, 1) FROM ".self::$_strTable." AS t WHERE t.delete_date IS NULL)
                            ,".(isset($tblPostData['city_id']) && is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL")." 
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    // Kép mentés
                    if (!empty($tblFileData)) {
                        if (self::saveFile($tblFileData, $tblPostData, $numNewId) === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                        }
                    }
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }

            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['title']) && $rowOldData['title'] != $tblPostData['title'] ? ",title = '{$tblPostData['title']}'" : "")."
                            ".(isset($tblPostData['title_en']) && $rowOldData['title_en'] != $tblPostData['title_en'] ? ",title_en = '{$tblPostData['title_en']}'" : "")."
                            ".(isset($tblPostData['title_ro']) && $rowOldData['title_ro'] != $tblPostData['title_ro'] ? ",title_ro = '{$tblPostData['title_ro']}'" : "")."
                            ".(isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority'] ? ",priority = ".(is_numeric($tblPostData['priority']) ? "{$tblPostData['priority']}" : "1") : "")."
                            ".(isset($tblPostData['city_id']) && $rowOldData['city_id'] != $tblPostData['city_id'] ? ",city_id = ".(is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE) {
                        
                        if (isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority']) {
                            $strQueryUpdate = "
                                UPDATE
                                    ".self::$_strTable."
                                SET
                                     modify_date = now()
                                    ,modify_user_id = {$objUser->getNumUserId()}
                                    ,priority = priority ".($tblPostData['priority'] < $rowOldData['priority'] ? "+1" : "-1")."
                                WHERE
                                    id != {$tblPostData['id']}
                                    AND delete_date IS NULL
                                    AND priority BETWEEN ".($tblPostData['priority'] < $rowOldData['priority'] ? $tblPostData['priority']." AND ".$rowOldData['priority'] : "greatest(".$rowOldData['priority'].",2) AND greatest(".$tblPostData['priority'].",2)")."
                            ";
                            $dbres = $objDb->query($strQueryUpdate);
                            if ($dbres !== FALSE) {
                                $rowMessage['refresh'] = 'true';
                            }
                        }
                        if ($dbres === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                        }
                        
                        // Kép módosítás
                        if (!empty($tblFileData)) {
                            if (self::saveFile($tblFileData, $tblPostData, $tblPostData['id']) === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                            }
                        }

                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }

                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        /**
         * Modulhoz tartozó fájlfeltöltések kezelésére
         * @param array $tblFileData
         * @param array $tblPostData
         * @param integer $numId
         * @return boolean
         */
        public static function saveFile($tblFileData, $tblPostData, $numId)
        {
            $tblImgSize['big']['width'] = 515;
            $tblImgSize['big']['height'] = 728;
            $tblImgSize['small']['width'] = 210;
            $tblImgSize['small']['height'] = 297;
            
            $rowTable['table'] = self::$_strTable;
            $rowTable['column'] = 'image';
             
            return parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize);
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
                $objSmarty->assign("tblCity", $this->_tblCity);
            }
            
        }
        
    }
    