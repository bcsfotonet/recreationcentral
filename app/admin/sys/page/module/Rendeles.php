<?php 
    require_once 'ModulePage.php';
    
    class Rendeles extends ModulePage {
        private static $_strTable = '`order`';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_tblMagazineData;
        
        function __construct( $numModuleId ){
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_tblMagazineData = array();
            
            $this->_tblListData = array(
                "column" => array(
                     "name" => "Név"
                )
                ,"search" => array(
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
//                 $this->removeData($this->_numEditedRowId);
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                      o.id
                    , o.`customer_id`
                    , (CASE WHEN c.is_person = 1 THEN CONCAT(c.last_name, ' ', c.first_name, ' (magánszemély)') WHEN c.is_company = 1 THEN CONCAT(c.company_name, ' (cég)') ELSE '' END) AS customer
                    , c.email
                    , o.`receive_mode`
                    , o.`post_different`
                    , o.`post_city`
                    , o.`post_name`
                    , o.`post_address`
                    , o.`privacy_policy`
                    , o.`post_price`
                    , o.`total_price`
                    , o.`currency`
                    , o.`order_date`
                    , o.`create_date`
                    , o.`modify_date`
                    , c.id AS customer_id
                    , c.is_person
                    , c.is_company
                    , c.last_name
                    , c.first_name
                    , c.company_name
                    , c.email
                    , c.tel
                    , c.country
                    , c.zip
                    , c.city
                    , c.street
                    , c.house_number
                    , c.tax_number
                FROM 
                	".self::$_strTable." as o
                JOIN
                	`customer` as c on(o.`customer_id` = c.id)
                WHERE
                	o.delete_date IS NULL
                    AND c.delete_date IS NULL
                    AND c.is_active = 1
                    AND o.order_date IS NOT NULL
                ORDER BY
                	o.order_date DESC
            ");
            if (is_numeric($this->_numEditedRowId)) {
                $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                
                $tblMagazineData = $objDb->getAll("
                    SELECT 
                    	 mo.num_magazine
                        ,yi.title
                    FROM 
                    	magazine_order AS mo
                    JOIN
                    	magazine AS m ON (mo.magazine_id = m.id)
                    JOIN
                    	year_and_issue AS yi ON (yi.id = m.year_and_issue_id)
                    WHERE
                    	mo.order_id = {$this->_numEditedRowId}
                    ORDER BY
                    	yi.title   
                ");
                if (!empty($tblMagazineData) && $tblMagazineData !== FALSE) {
                    $this->_tblMagazineData = $tblMagazineData;
                }
                
                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("rendeles");
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }

            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            global $objDb, $objUser;
            $rowMessage = parent::removeMainTableData ($numRowId, self::$_strTable);
            
            return $rowMessage;
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            //TODO ellenőrzések 
            
            return $rowMessage;
        }

        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData)
        {
            global $objDb, $objUser;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                /*
                $strQueryInsert = "
                    INSERT INTO
                        ".self::$_strTable."(
                              `customer_id`
                            , `receive_mode`
                            , `post_different`
                            , `post_city`
                            , `post_name`
                            , `post_address`
                            , `privacy_policy`
                            , `post_price`
                            , `total_price`
                            , `currency`
                            , `order_date`
                        ) VALUES (
                             '{$tblPostData['customer_id']}'
                            ,".(!empty($tblPostData['question_en']) ? "'{$tblPostData['question_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['question_ro']) ? "'{$tblPostData['question_ro']}'" : "NULL")."
                            ,'{$tblPostData['answer']}'
                            ,".(!empty($tblPostData['answer_en']) ? "'{$tblPostData['answer_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['answer_ro']) ? "'{$tblPostData['answer_ro']}'" : "NULL")."
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $rowMessage['type'] = 'ok';
                    $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    $rowMessage['new_id'] = $dbres;
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }*/
            } else {
                //Update
                /*
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['question']) && $rowOldData['question'] != $tblPostData['question'] ? ",question = '{$tblPostData['question']}'" : "")."
                            ".(isset($tblPostData['question_en']) && $rowOldData['question_en'] != $tblPostData['question_en'] ? ",question_en = '{$tblPostData['question_en']}'" : "")."
                            ".(isset($tblPostData['question_ro']) && $rowOldData['question_ro'] != $tblPostData['question_ro'] ? ",question_ro = '{$tblPostData['question_ro']}'" : "")."
                            ".(isset($tblPostData['answer']) && $rowOldData['answer'] != $tblPostData['answer'] ? ",answer = '{$tblPostData['answer']}'" : "")."
                            ".(isset($tblPostData['answer']) && $rowOldData['answer_en'] != $tblPostData['answer_en'] ? ",answer_en = '{$tblPostData['answer_en']}'" : "")."
                            ".(isset($tblPostData['answer']) && $rowOldData['answer_ro'] != $tblPostData['answer_ro'] ? ",answer_ro = '{$tblPostData['answer_ro']}'" : "")."
                            ".(isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority'] ? ",priority = ".(is_numeric($tblPostData['priority']) ? "{$tblPostData['priority']}" : "1") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
                    //             print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE) {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
                */
            }
            
            return $rowMessage;
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
                $objSmarty->assign("tblMagazineData", $this->_tblMagazineData);
            }
            
        }
        
    }
    