<?php 
    require_once 'ModulePage.php';
    
    class Statikus extends ModulePage {
        private static $_strTable = 'static';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_tblGallery;
        private $_tblVideo;
        private $_tblDocument;
        
        function __construct( $numModuleId ){
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_tblGallery = array();
            $this->_tblVideo = array();
            $this->_tblDocument = array();
            
            $this->_tblListData = array(
                "column" => array(
                    "title" => "Cím"
                )
                ,"search" => array(
//                    "title" => array("text" => "Cím", "type" => "input")
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                    id
                    ,title
                    ,title_en
                    ,title_ro
                    ,url
                    ,url_en
                    ,url_ro
                    ,lead
                    ,lead_en
                    ,lead_ro
                    ,description
                    ,description_en
                    ,description_ro
                    ,is_active
                    ,gallery_id
                    ,video_id  
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                    ".self::$_strTable."
                WHERE 
                    delete_date IS NULL
                ORDER BY
                    title
            ");
            
            if (is_numeric($this->_numEditedRowId)) {
                
                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("statikus");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                    
                    //kapcsolódó dokumentumok lekérése
                    $tblDocumentId = $objDb->getCol("
                        SELECT
                            document_id
                        FROM
                            static_document
                        WHERE
                            static_id = {$this->_numEditedRowId}
                    ");
                    if ($tblDocumentId !== FALSE) {
                        $this->_tblEditedRowData['document_id'] = $tblDocumentId;
                    }
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            //Választható galériák lekérése
            $tblGallery = $objDb->getAll("
                SELECT
                     id AS value
                    ,title AS text
                FROM
                    gallery
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    title
            ");
            if ($tblGallery !== FALSE) {
                $this->_tblGallery[-1]['value'] = "";
                $this->_tblGallery[-1]['text'] = "Nem tartozik hozzá galéria";
                
                foreach ($tblGallery as $numIdx => $rowGallery) {
                    $this->_tblGallery[$numIdx] = $rowGallery;
                    $this->_tblGallery[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['gallery_id']) && is_numeric($this->_tblEditedRowData['gallery_id'])) {
                        if ($rowGallery['value'] == $this->_tblEditedRowData['gallery_id']) {
                            $this->_tblGallery[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            //Választható videók lekérése
            $tblVideo = $objDb->getAll("
                SELECT
                     id AS value
                    ,title AS text
                FROM
                    video
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    title
            ");
            if ($tblVideo !== FALSE) {
                $this->_tblVideo[-1]['value'] = "";
                $this->_tblVideo[-1]['text'] = "Nem tartozik hozzá videó";
                
                foreach ($tblVideo as $numIdx => $rowVideo) {
                    $this->_tblVideo[$numIdx] = $rowVideo;
                    $this->_tblVideo[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['video_id']) && is_numeric($this->_tblEditedRowData['video_id'])) {
                        if ($rowVideo['value'] == $this->_tblEditedRowData['video_id']) {
                            $this->_tblVideo[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            //Választható dokumentumok lekérése
            $tblDocument = $objDb->getAll("
                SELECT
                     id AS value
                    ,title AS text
                FROM
                    document
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    title
            ");
            if ($tblDocument !== FALSE) {
                foreach ($tblDocument as $numIdx => $rowDocument) {
                    $this->_tblDocument[$numIdx] = $rowDocument;
                    $this->_tblDocument[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['document_id']) && !empty($this->_tblEditedRowData['document_id'])) {
                        if (in_array($rowDocument['value'], $this->_tblEditedRowData['document_id'])) {
                            $this->_tblDocument[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }

            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            return parent::removeMainTableData ($numRowId, self::$_strTable);
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            
            //TODO ellenőrzések 
//             $rowMessage 
            return $rowMessage;
        }

        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData)
        {
            global $objDb, $objUser;
            $rowMessage = array('type' => null, 'msg' => null);
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO
                        ".self::$_strTable."(
                            title
                            ,title_en
                            ,title_ro
                            ,url
                            ,url_en
                            ,url_ro
                            ,lead
                            ,lead_en
                            ,lead_ro
                            ,description
                            ,description_en
                            ,description_ro
                            ,gallery_id
                            ,video_id
                            ,create_user_id
                        ) VALUES (
                             '{$tblPostData['title']}'
                            ,".(isset($tblPostData['title_en']) && !empty($tblPostData['title_en']) ? "'{$tblPostData['title_en']}'" : "NULL")."
                            ,".(isset($tblPostData['title_ro']) && !empty($tblPostData['title_ro']) ? "'{$tblPostData['title_ro']}'" : "NULL")."
                            ,'{$tblPostData['url']}'
                            ,".(isset($tblPostData['url_en']) && !empty($tblPostData['url_en']) ? "'{$tblPostData['url_en']}'" : "NULL")."
                            ,".(isset($tblPostData['url_ro']) && !empty($tblPostData['url_ro']) ? "'{$tblPostData['url_ro']}'" : "NULL")."                            
                            ,".(isset($tblPostData['lead']) && !empty($tblPostData['lead']) ? "'{$tblPostData['lead']}'" : "NULL")."
                            ,".(isset($tblPostData['lead_en']) && !empty($tblPostData['lead_en']) ? "'{$tblPostData['lead_en']}'" : "NULL")."
                            ,".(isset($tblPostData['lead_ro']) && !empty($tblPostData['lead_ro']) ? "'{$tblPostData['lead_ro']}'" : "NULL")."    
                            ,'{$tblPostData['description']}'
                            ,".(isset($tblPostData['description_en']) && !empty($tblPostData['description_en']) ? "'{$tblPostData['description_en']}'" : "NULL")."
                            ,".(isset($tblPostData['description_ro']) && !empty($tblPostData['description_ro']) ? "'{$tblPostData['description_ro']}'" : "NULL")."                           
                            ".(isset($tblPostData['gallery_id']) && is_numeric($tblPostData['gallery_id']) ? ",{$tblPostData['gallery_id']}" : ",NULL")."
                            ".(isset($tblPostData['video_id']) && is_numeric($tblPostData['video_id']) ? ",{$tblPostData['video_id']}" : ",NULL")."
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
                //     var_dump($dbres);
                //     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    // document_id mentések
                    if (isset($tblPostData['document_id']) && !empty($tblPostData['document_id']) ){
                        foreach ($tblPostData['document_id'] as $numDocumentId) {
                            $strQueryInsert = "
                                INSERT INTO
                                    static_document(
                                         static_id
                                        ,document_id
                                    ) VALUES (
                                         {$numNewId}
                                        ,{$numDocumentId}
                                    )
                            ";
                             $dbres2 = $objDb->query($strQueryInsert);
                             if ($dbres2 === FALSE) {
                                 $rowMessage['type'] = 'error';
                                 $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                 break;
                             }
                        }
                    }
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }
            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['title']) && $rowOldData['title'] != $tblPostData['title'] ? ",title = '{$tblPostData['title']}'" : "")."
                            ".(isset($tblPostData['title_en']) && $rowOldData['title_en'] != $tblPostData['title_en'] ? ",title_en = '{$tblPostData['title_en']}'" : "")."
                            ".(isset($tblPostData['title_ro']) && $rowOldData['title_ro'] != $tblPostData['title_ro'] ? ",title_ro = '{$tblPostData['title_ro']}'" : "")."
                            ".(isset($tblPostData['url']) && $rowOldData['url'] != $tblPostData['url'] ? ",url = '{$tblPostData['url']}'" : "")."
                            ".(isset($tblPostData['url_en']) && $rowOldData['url_en'] != $tblPostData['url_en'] ? ",url_en = '{$tblPostData['url_en']}'" : "")."
                            ".(isset($tblPostData['url_ro']) && $rowOldData['url_ro'] != $tblPostData['url_ro'] ? ",url_ro = '{$tblPostData['url_ro']}'" : "")."
                            ".(isset($tblPostData['lead']) && $rowOldData['lead'] != $tblPostData['lead'] ? ",lead = '{$tblPostData['lead']}'" : "")."
                            ".(isset($tblPostData['lead_en']) && $rowOldData['lead_en'] != $tblPostData['lead_en'] ? ",lead_en = '{$tblPostData['lead_en']}'" : "")."
                            ".(isset($tblPostData['lead_ro']) && $rowOldData['lead_ro'] != $tblPostData['lead_ro'] ? ",lead_ro = '{$tblPostData['lead_ro']}'" : "")."                                                      
                            ".(isset($tblPostData['description']) && $rowOldData['description'] != $tblPostData['description'] ? ",description = '{$tblPostData['description']}'" : "")."
                            ".(isset($tblPostData['description_en']) && $rowOldData['description_en'] != $tblPostData['description_en'] ? ",description_en = '{$tblPostData['description_en']}'" : "")."
                            ".(isset($tblPostData['description_ro']) && $rowOldData['description_ro'] != $tblPostData['description_ro'] ? ",description_ro = '{$tblPostData['description_ro']}'" : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                            ".(isset($tblPostData['gallery_id']) && $rowOldData['gallery_id'] != $tblPostData['gallery_id'] ? ",gallery_id = ".(is_numeric($tblPostData['gallery_id']) ? "{$tblPostData['gallery_id']}" : "NULL") : "")."
                            ".(isset($tblPostData['video_id']) && $rowOldData['video_id'] != $tblPostData['video_id'] ? ",video_id = ".(is_numeric($tblPostData['video_id']) ? "{$tblPostData['video_id']}" : "NULL") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
                    //             print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE && $tblPostData['row_save'] === false) {
                        
                        // document_id módosítások
                        $dbres = $objDb->query("
                            DELETE FROM
                                static_document
                            WHeRE
                                static_id = {$tblPostData['id']}
                        ");
                        if (isset($tblPostData['document_id']) && !empty($tblPostData['document_id']) ){
                            foreach ($tblPostData['document_id'] as $numDocumentId) {
                                $strQueryInsert = "
                                    INSERT INTO
                                        static_document(
                                             static_id
                                            ,document_id
                                        ) VALUES (
                                             {$tblPostData['id']}
                                            ,{$numDocumentId}
                                        )
                                ";
                                 $dbres = $objDb->query($strQueryInsert);
                                 if ($dbres === FALSE) {
                                     $rowMessage['type'] = 'error';
                                     $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                     break;
                                 }
                            }
                        }
                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }
                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblGallery", $this->_tblGallery);
                $objSmarty->assign("tblVideo", $this->_tblVideo);
                $objSmarty->assign("tblDocument", $this->_tblDocument);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
            }
            
        }
        
    }
    