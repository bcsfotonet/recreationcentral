<?php 
    require_once 'ModulePage.php';
    
    class Tanulmanyok extends ModulePage {
        private static $_strTable = 'study';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_strImageDirUrl;
        private $_strImageDir;
        private $_tblAuthor;
        private $_tblCoEditor;
        private $_tblEditor;
        private $_tblYear;
        
        function __construct( $numModuleId ){
            global $CONF;
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_strImageDirUrl = $CONF['base_url']."media/pub/tanulmanyok/";
            $this->_strImageDir = $CONF['pub_dir']."tanulmanyok/";
            $this->_tblAuthor = array();
            $this->_tblCoEditor = array();
            $this->_tblEditor = array();
            $this->_tblYear = array();
            $this->_tblListData = array(
//                "column" => array(
//                    "title" => "Cím"
//                )
//                ,"search" => array(
//                    "title" => array("text" => "Cím", "type" => "input")
//                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                $this->_strImageDirUrl .= $this->_numEditedRowId."/small/";
                $this->_strImageDir .= $this->_numEditedRowId."/small/";
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                    id
                    ,author_staff_id
                    ,co_editor_staff_id
                    ,title
                    ,title_en
                    ,title_ro
                    ,url
                    ,url_en
                    ,url_ro
                    ,magazine
                    ,magazine_en
                    ,magazine_ro
                    ,authors
                    ,authors_en
                    ,authors_ro
                    ,keywords
                    ,keywords_en
                    ,keywords_ro
                    ,year
                    ,doi
                    ,doi_url
                    ,abstract
                    ,abstract_en
                    ,abstract_ro
                    ,bibliography
                    ,bibliography_en
                    ,bibliography_ro
                    ,lead
                    ,lead_en
                    ,lead_ro
                    ,description
                    ,description_en
                    ,description_ro
                    ,image      
                    ,is_active
                    ,priority
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                    ".self::$_strTable." 
                WHERE 
                    delete_date IS NULL
                ORDER BY
                    priority
            ");
            
            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("tanulmanyok");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                    
                    if (!empty($this->_tblEditedRowData['image']) && file_exists($this->_strImageDir.$this->_tblEditedRowData['image'])) {
                        $this->_tblEditedRowData['image'] = $this->_strImageDirUrl.$this->_tblEditedRowData['image'];
                    } else {
                        $this->_tblEditedRowData['image'] = null;
                    }
                }
                
                // kapcsolódó szerkesztobizottsagi tagok lekérése
                $tblStaffId = $objDb->getCol("
                    SELECT
                        staff_id
                    FROM
                        study_editor_staff
                    WHERE
                        study_id = {$this->_numEditedRowId} 
                ");
                if ($tblStaffId !== FALSE) {
                    $this->_tblEditedRowData['staff_id'] = $tblStaffId;
                }
     
            } else {
                if (!empty($tblData) && $tblData !== FALSE) {
                    foreach ($tblData as $numIdx => $rowData) {
                        if (!empty($rowData['image']) && file_exists($this->_strImageDir.$rowData['id']."/small/".$rowData['image'])) {
                            $tblData[$numIdx]['image'] = $this->_strImageDirUrl.$rowData['id']."/small/".$rowData['image'];
                        } else {
                            $tblData[$numIdx]['image'] = null;
                        }
                    }
                }
            }
            
            // Választható évek
            $this->_tblYear = array(
                array(
                    "value" => ""
                    ,"text" => "Nincs év kiválasztva"
                    ,"selected" => ""
                )
                ,
                array(
                    "value" => "2019"
                    ,"text" => "2019"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2019" == $this->_tblEditedRowData['year'] ? "selected" : "")
                )
                ,
                array(
                    "value" => "2018"
                    ,"text" => "2018"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2018" == $this->_tblEditedRowData['year'] ? "selected" : "")
                )
                ,
                array(
                    "value" => "2017"
                    ,"text" => "2017"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2017" == $this->_tblEditedRowData['year'] ? "selected" : "")
                )
                ,
                array(
                    "value" => "2016"
                    ,"text" => "2016"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2016" == $this->_tblEditedRowData['year'] ? "selected" : "")
                )
                ,
                array(
                    "value" => "2015"
                    ,"text" => "2015"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2015" == $this->_tblEditedRowData['year'] ? "selected" : "")
                )
                ,
                array(
                    "value" => "2014"
                    ,"text" => "2014"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2014" == $this->_tblEditedRowData['year'] ? "selected" : "")
                )
                ,
                array(
                    "value" => "2013"
                    ,"text" => "2013"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2013" == $this->_tblEditedRowData['year'] ? "selected" : "")
                )
                ,
                array(
                    "value" => "2012"
                    ,"text" => "2012"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2012" == $this->_tblEditedRowData['year'] ? "selected" : "")
                )
                ,
                array(
                     "value" => "2011"
                    ,"text" => "2011"
                    ,"selected" => (!empty($this->_tblEditedRowData) && "2011" == $this->_tblEditedRowData['year'] ? "selected" : "")  
                )
            );     
            
            // Választható szerzők lekérése
            $tblAuthor = $objDb->getAll("
                SELECT
                    id AS value
                    ,concat(name, case when is_active = 0 then ' (inaktív)' else '' end)  AS text
                FROM
                    staff
                WHERE
                    delete_date IS NULL
                    AND type_id = 6 -- szerkesztobizottsag és nem munkatársak (4) már
                    -- AND is_active = 1
                ORDER BY
                    name
            ");
            if ($tblAuthor !== FALSE) {
                $this->_tblAuthor[-1]['value'] = "";
                $this->_tblAuthor[-1]['text'] = "Nem tartozik hozzá szerző";
                
                $this->_tblCoEditor[-1]['value'] = "";
                $this->_tblCoEditor[-1]['text'] = "Nem tartozik hozzá társszerkesztő";
                
                foreach ($tblAuthor as $numIdx => $rowAuthor) {
                    $this->_tblAuthor[$numIdx] = $rowAuthor;
                    $this->_tblAuthor[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['author_staff_id']) && is_numeric($this->_tblEditedRowData['author_staff_id'])) {
                        if ($rowAuthor['value'] == $this->_tblEditedRowData['author_staff_id']) {
                            $this->_tblAuthor[$numIdx]['selected'] = "selected";
                        }
                    }
                    
                    $this->_tblCoEditor[$numIdx] = $rowAuthor;
                    $this->_tblCoEditor[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['co_editor_staff_id']) && is_numeric($this->_tblEditedRowData['co_editor_staff_id'])) {
                        if ($rowAuthor['value'] == $this->_tblEditedRowData['co_editor_staff_id']) {
                            $this->_tblCoEditor[$numIdx]['selected'] = "selected";
                        }
                    }
                    
                }
                
//                $this->_tblListData["search"]["author_staff_id"]["options"] = $this->_tblAuthor;
     
            }
            
            // kapcsolódó szerkesztőbizottsági tagok lekérése
            $tblEditor = $objDb->getAll("
                SELECT
                     id AS value
                    ,concat(name, case when is_active = 0 then ' (inaktív)' else '' end)  AS text
                FROM
                    staff
                WHERE
                    delete_date IS NULL
                    AND type_id = 6 -- szerkesztobizottsag
                    -- AND is_active = 1
                ORDER BY
                    name
            ");
                    
            if ($tblEditor!== FALSE) {
                
                $this->_tblEditor[-1]['value'] = "";
                $this->_tblEditor[-1]['text'] = "Nem tartozik hozzá szerkesztőbizottsági tag";
                
                foreach ($tblEditor as $numIdx => $rowEditor) {
                    $this->_tblEditor[$numIdx] = $rowEditor;
                    $this->_tblEditor[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['staff_id']) && !empty($this->_tblEditedRowData['staff_id'])) {
                        if (in_array($rowEditor['value'], $this->_tblEditedRowData['staff_id'])) {
                            $this->_tblEditor[$numIdx]['selected'] = "selected"; 
                        }
                    }
                }
            }
            
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            global $objDb, $objUser;
            $rowMessage = parent::removeMainTableData ($numRowId, self::$_strTable);
            
            // sorrend újragenerálása miatt ebben a modulban nem elég a sima törlés
            if ($rowMessage['type'] == 'ok') {
                $dbres = $objDb->query("
                    UPDATE
                        ".self::$_strTable."
                    SET
                         modify_date = now()
                        ,modify_user_id = {$objUser->getNumUserId()}
                        ,priority = priority-1
                    WHERE
                        delete_date IS NULL
                        AND priority > (SELECT t.priority FROM (select * from ".self::$_strTable.") AS t WHERE t.id = {$numRowId})
                ");
                if ($dbres === true) {
                    $rowMessage = array('type' => 'ok', 'msg' => 'Az adatok törlése sikeresen megtörtént.', 'refresh' => 'true');
                } else {
                    $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt az adatok törlése során.');
                }
            }
            return $rowMessage;
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData, $tblFileData = null)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            
            //TODO ellenőrzések 
            
            return $rowMessage;
        }
           
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData, $tblFileData = null)
        {
            global $objDb, $objUser, $CONF;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO 
                        ".self::$_strTable."(
                            author_staff_id
                            ,co_editor_staff_id
                            ,title
                            ,title_en
                            ,title_ro
                            ,url
                            ,url_en
                            ,url_ro
                            ,magazine
                            ,magazine_en
                            ,magazine_ro
                            ,authors
                            ,authors_en
                            ,authors_ro
                            ,keywords
                            ,keywords_en
                            ,keywords_ro
                            ,year
                            ,doi
                            ,doi_url
                            ,abstract
                            ,abstract_en
                            ,abstract_ro
                            ,bibliography
                            ,bibliography_en
                            ,bibliography_ro
                            ,lead
                            ,lead_en
                            ,lead_ro
                            ,description
                            ,description_en
                            ,description_ro  
                            ,priority
                            ,create_user_id
                        ) VALUES (
                            ".(isset($tblPostData['author_staff_id']) && !empty($tblPostData['author_staff_id']) ? "'{$tblPostData['author_staff_id']}'" : "NULL")."
                            ,".(isset($tblPostData['co_editor_staff_id']) && !empty($tblPostData['co_editor_staff_id']) ? "'{$tblPostData['co_editor_staff_id']}'" : "NULL")."
                            ,".(isset($tblPostData['title']) && !empty($tblPostData['title']) ? "'{$tblPostData['title']}'" : "''")."
                            ,".(isset($tblPostData['title_en']) && !empty($tblPostData['title_en']) ? "'{$tblPostData['title_en']}'" : "''")."
                            ,".(isset($tblPostData['title_ro']) && !empty($tblPostData['title_ro']) ? "'{$tblPostData['title_ro']}'" : "''")."                            
                            ,".(isset($tblPostData['url']) && !empty($tblPostData['url']) ? "'{$tblPostData['url']}'" : "''")."
                            ,".(isset($tblPostData['url_en']) && !empty($tblPostData['url_en']) ? "'{$tblPostData['url_en']}'" : "''")."
                            ,".(isset($tblPostData['url_ro']) && !empty($tblPostData['url_ro']) ? "'{$tblPostData['url_ro']}'" : "''")."
                            ,".(isset($tblPostData['magazine']) && !empty($tblPostData['magazine']) ? "'{$tblPostData['magazine']}'" : "''")."
                            ,".(isset($tblPostData['magazine_en']) && !empty($tblPostData['magazine_en']) ? "'{$tblPostData['magazine_en']}'" : "''")."
                            ,".(isset($tblPostData['magazine_ro']) && !empty($tblPostData['magazine_ro']) ? "'{$tblPostData['magazine_ro']}'" : "''")."
                            ,".(isset($tblPostData['authors']) && !empty($tblPostData['authors']) ? "'{$tblPostData['authors']}'" : "''")."
                            ,".(isset($tblPostData['authors_en']) && !empty($tblPostData['authors_en']) ? "'{$tblPostData['authors_en']}'" : "''")."
                            ,".(isset($tblPostData['authors_ro']) && !empty($tblPostData['authors_ro']) ? "'{$tblPostData['authors_ro']}'" : "''")."
                            ,".(isset($tblPostData['keywords']) && !empty($tblPostData['keywords']) ? "'{$tblPostData['keywords']}'" : "''")."
                            ,".(isset($tblPostData['keywords_en']) && !empty($tblPostData['keywords_en']) ? "'{$tblPostData['keywords_en']}'" : "''")."
                            ,".(isset($tblPostData['keywords_ro']) && !empty($tblPostData['keywords_ro']) ? "'{$tblPostData['keywords_ro']}'" : "''")."
                            ,".(isset($tblPostData['year']) && !empty($tblPostData['year']) ? "'{$tblPostData['year']}'" : "''")."
                            ,".(isset($tblPostData['doi']) && !empty($tblPostData['doi']) ? "'{$tblPostData['doi']}'" : "''")."
                            ,".(isset($tblPostData['doi_url']) && !empty($tblPostData['doi_url']) ? "'{$tblPostData['doi_url']}'" : "''")."                          
                            ,".(isset($tblPostData['abstract']) && !empty($tblPostData['abstract']) ? "'".addslashes(trim($tblPostData['abstract']))."'" : "NULL")."
                            ,".(isset($tblPostData['abstract_en']) && !empty($tblPostData['abstract_en']) ? "'".addslashes(trim($tblPostData['abstract_en']))."'" : "NULL")."
                            ,".(isset($tblPostData['abstract_ro']) && !empty($tblPostData['abstract_ro']) ? "'".addslashes(trim($tblPostData['abstract_ro']))."'" : "NULL")."
                            ,".(isset($tblPostData['bibliography']) && !empty($tblPostData['bibliography']) ? "'{$tblPostData['bibliography']}'" : "NULL")."
                            ,".(isset($tblPostData['bibliography_en']) && !empty($tblPostData['bibliography_en']) ? "'{$tblPostData['bibliography_en']}'" : "NULL")."
                            ,".(isset($tblPostData['bibliography_ro']) && !empty($tblPostData['bibliography_ro']) ? "'{$tblPostData['bibliography_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['lead']) && !empty($tblPostData['lead']) ? "'{$tblPostData['lead']}'" : "NULL")."
                            ,".(isset($tblPostData['lead_en']) && !empty($tblPostData['lead_en']) ? "'{$tblPostData['lead_en']}'" : "NULL")."
                            ,".(isset($tblPostData['lead_ro']) && !empty($tblPostData['lead_ro']) ? "'{$tblPostData['lead_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['description']) && !empty($tblPostData['description']) ? "'".addslashes(trim($tblPostData['description']))."'" : "NULL")."
                            ,".(isset($tblPostData['description_en']) && !empty($tblPostData['description_en']) ? "'".addslashes(trim($tblPostData['description_en']))."'" : "NULL")."
                            ,".(isset($tblPostData['description_ro']) && !empty($tblPostData['description_ro']) ? "'".addslashes(trim($tblPostData['description_ro']))."'" : "NULL")."
                            ,(SELECT COALESCE(max(t.priority)+1, 1) FROM ".self::$_strTable." AS t WHERE t.delete_date IS NULL) 
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
//                     die;
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    // staff_id mentések
                    if (isset($tblPostData['staff_id']) && !empty($tblPostData['staff_id']) ){
                        foreach ($tblPostData['staff_id'] as $numStaffId) {
                            $strQueryInsert = "
                                INSERT INTO 
                                    study_editor_staff(
                                        study_id
                                        ,staff_id
                                    ) VALUES (
                                         {$numNewId}
                                        ,{$numStaffId}
                                    )
                            ";
//                                          print $strQueryInsert;
                            $dbres2 = $objDb->query($strQueryInsert);
                            if ($dbres2 === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                break;
                            }
                        }
                    }
                    
                    // Kép mentés
                    if (!empty($tblFileData)) {
                        if (self::saveFile($tblFileData, $tblPostData, $numNewId) === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                        }
                    }
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }

            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['author_staff_id']) && $rowOldData['author_staff_id'] != $tblPostData['author_staff_id'] ? ",author_staff_id = '{$tblPostData['author_staff_id']}'" : "")."
                            ".(isset($tblPostData['co_editor_staff_id']) && $rowOldData['co_editor_staff_id'] != $tblPostData['co_editor_staff_id'] ? ",co_editor_staff_id = '{$tblPostData['co_editor_staff_id']}'" : "")."
                            ".(isset($tblPostData['title']) && $rowOldData['title'] != $tblPostData['title'] ? ",title = '{$tblPostData['title']}'" : "")."
                            ".(isset($tblPostData['title_en']) && $rowOldData['title_en'] != $tblPostData['title_en'] ? ",title_en = '{$tblPostData['title_en']}'" : "")."
                            ".(isset($tblPostData['title_ro']) && $rowOldData['title_ro'] != $tblPostData['title_ro'] ? ",title_ro = '{$tblPostData['title_ro']}'" : "")."
                            ".(isset($tblPostData['url']) && $rowOldData['url'] != $tblPostData['url'] ? ",url = '{$tblPostData['url']}'" : "")."
                            ".(isset($tblPostData['url_en']) && $rowOldData['url_en'] != $tblPostData['url_en'] ? ",url_en = '{$tblPostData['url_en']}'" : "")."
                            ".(isset($tblPostData['url_ro']) && $rowOldData['url_ro'] != $tblPostData['url_ro'] ? ",url_ro = '{$tblPostData['url_ro']}'" : "")."
                            ".(isset($tblPostData['magazine']) && $rowOldData['magazine'] != $tblPostData['magazine'] ? ",magazine = '{$tblPostData['magazine']}'" : "")."
                            ".(isset($tblPostData['magazine_en']) && $rowOldData['magazine_en'] != $tblPostData['magazine_en'] ? ",magazine_en = '{$tblPostData['magazine_en']}'" : "")."
                            ".(isset($tblPostData['magazine_ro']) && $rowOldData['magazine_ro'] != $tblPostData['magazine_ro'] ? ",magazine_ro = '{$tblPostData['magazine_ro']}'" : "")."
                            ".(isset($tblPostData['authors']) && $rowOldData['authors'] != $tblPostData['authors'] ? ",authors = '{$tblPostData['authors']}'" : "")."
                            ".(isset($tblPostData['authors_en']) && $rowOldData['authors_en'] != $tblPostData['authors_en'] ? ",authors_en = '{$tblPostData['authors_en']}'" : "")."
                            ".(isset($tblPostData['authors_ro']) && $rowOldData['authors_ro'] != $tblPostData['authors_ro'] ? ",authors_ro = '{$tblPostData['authors_ro']}'" : "")."
                            ".(isset($tblPostData['keywords']) && $rowOldData['keywords'] != $tblPostData['keywords'] ? ",keywords = '{$tblPostData['keywords']}'" : "")."
                            ".(isset($tblPostData['keywords_en']) && $rowOldData['keywords_en'] != $tblPostData['keywords_en'] ? ",keywords_en = '{$tblPostData['keywords_en']}'" : "")."
                            ".(isset($tblPostData['keywords_ro']) && $rowOldData['keywords_ro'] != $tblPostData['keywords_ro'] ? ",keywords_ro = '{$tblPostData['keywords_ro']}'" : "")."
                            ".(isset($tblPostData['year']) && $rowOldData['year'] != $tblPostData['year'] ? ",year = '{$tblPostData['year']}'" : "")."
                            ".(isset($tblPostData['doi']) && $rowOldData['doi'] != $tblPostData['doi'] ? ",doi = '{$tblPostData['doi']}'" : "")."
                            ".(isset($tblPostData['doi_url']) && $rowOldData['doi_url'] != $tblPostData['doi_url'] ? ",doi_url = '{$tblPostData['doi_url']}'" : "")."
                            ".(isset($tblPostData['abstract']) && $rowOldData['abstract'] != $tblPostData['abstract'] ? ",abstract = '".addslashes(trim($tblPostData['abstract']))."'" : "")."
                            ".(isset($tblPostData['abstract_en']) && $rowOldData['abstract_en'] != $tblPostData['abstract_en'] ? ",abstract_en = '".addslashes(trim($tblPostData['abstract_en']))."'" : "")."
                            ".(isset($tblPostData['abstract_ro']) && $rowOldData['abstract_ro'] != $tblPostData['abstract_ro'] ? ",abstract_ro = '".addslashes(trim($tblPostData['abstract_ro']))."'" : "")."
                            ".(isset($tblPostData['bibliography']) && $rowOldData['bibliography'] != $tblPostData['bibliography'] ? ",bibliography = '{$tblPostData['bibliography']}'" : "")."
                            ".(isset($tblPostData['bibliography_en']) && $rowOldData['bibliography_en'] != $tblPostData['bibliography_en'] ? ",bibliography_en = '{$tblPostData['bibliography_en']}'" : "")."
                            ".(isset($tblPostData['bibliography_ro']) && $rowOldData['bibliography_ro'] != $tblPostData['bibliography_ro'] ? ",bibliography_ro = '{$tblPostData['bibliography_ro']}'" : "")."
                            ".(isset($tblPostData['lead']) && $rowOldData['lead'] != $tblPostData['lead'] ? ",lead = '{$tblPostData['lead']}'" : "")."
                            ".(isset($tblPostData['lead_en']) && $rowOldData['lead_en'] != $tblPostData['lead_en'] ? ",lead_en = '{$tblPostData['lead_en']}'" : "")."
                            ".(isset($tblPostData['lead_ro']) && $rowOldData['lead_ro'] != $tblPostData['lead_ro'] ? ",lead_ro = '{$tblPostData['lead_ro']}'" : "")."
                            ".(isset($tblPostData['description']) && $rowOldData['description'] != $tblPostData['description'] ? ",description = '".addslashes(trim($tblPostData['description']))."'" : "")."
                            ".(isset($tblPostData['description_en']) && $rowOldData['description_en'] != $tblPostData['description_en'] ? ",description_en = '".addslashes(trim($tblPostData['description_en']))."'" : "")."
                            ".(isset($tblPostData['description_ro']) && $rowOldData['description_ro'] != $tblPostData['description_ro'] ? ",description_ro = '".addslashes(trim($tblPostData['description_ro']))."'" : "")."
                            ".(isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority'] ? ",priority = ".(is_numeric($tblPostData['priority']) ? "{$tblPostData['priority']}" : "1") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE) {
                        
                        if (isset($tblPostData['priority']) && $rowOldData['priority'] != $tblPostData['priority']) {
                            $strQueryUpdate = "
                                UPDATE
                                    ".self::$_strTable."
                                SET
                                     modify_date = now()
                                    ,modify_user_id = {$objUser->getNumUserId()}
                                    ,priority = priority ".($tblPostData['priority'] < $rowOldData['priority'] ? "+1" : "-1")."
                                WHERE
                                    id != {$tblPostData['id']}
                                    AND delete_date IS NULL
                                    AND priority BETWEEN ".($tblPostData['priority'] < $rowOldData['priority'] ? $tblPostData['priority']." AND ".$rowOldData['priority'] : "greatest(".$rowOldData['priority'].",2) AND greatest(".$tblPostData['priority'].",2)")."
                            ";
                            $dbres = $objDb->query($strQueryUpdate);
                            if ($dbres !== FALSE) {
                                $rowMessage['refresh'] = 'true';
                            }
                        }
                        
                        // staff_id módosítások
                        $dbres = $objDb->query("
                            DELETE FROM
                                study_editor_staff
                            WHERE
                                study_id = {$tblPostData['id']}
                        ");                                               
                        if (isset($tblPostData['staff_id']) && !empty($tblPostData['staff_id']) ){
                            foreach ($tblPostData['staff_id'] as $numStaffId) {
                                $strQueryInsert = "
                                    INSERT INTO
                                        study_editor_staff(
                                             study_id
                                            ,staff_id
                                        ) VALUES (
                                             {$tblPostData['id']}
                                            ,{$numStaffId}
                                        )
                                ";
//                                          print $strQueryInsert;
                                 $dbres = $objDb->query($strQueryInsert);
                                 if ($dbres === FALSE) {
                                     $rowMessage['type'] = 'error';
                                     $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                     break;
                                 }
                            }
                        }
                        
                        if ($dbres === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                        }
                        
                        // Kép módosítás
                        if (!empty($tblFileData)) {
                            if (self::saveFile($tblFileData, $tblPostData, $tblPostData['id']) === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                            }
                        }

                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }

                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        /**
         * Modulhoz tartozó fájlfeltöltések kezelésére
         * @param array $tblFileData
         * @param array $tblPostData
         * @param integer $numId
         * @return boolean
         */
        public static function saveFile($tblFileData, $tblPostData, $numId)
        {
            $tblImgSize['big']['width'] = 690;
            $tblImgSize['big']['height'] = 460;
            $tblImgSize['small']['width'] = 690;
            $tblImgSize['small']['height'] = 460;
            
            $rowTable['table'] = self::$_strTable;
            $rowTable['column'] = 'image';
             
            return parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize);
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblValidate", $this->_tblValidate);
                $objSmarty->assign("tblAuthor", $this->_tblAuthor);
                $objSmarty->assign("tblCoEditor", $this->_tblCoEditor);
                $objSmarty->assign("tblEditorStaffID", $this->_tblEditor);
                $objSmarty->assign("tblYear", $this->_tblYear);
            }
            
        }
        
    }
    