<?php 
    require_once 'ModulePage.php';
    
    class Video extends ModulePage {
        private static $_strTable = 'video';
        private $_numEditedRowId;
        private $_tblEditedRowData;
        private $_strImageDirUrl;
        private $_strImageDir;
        
        function __construct( $numModuleId ){
            global $CONF;
            parent::__construct( $numModuleId );            
            
            $this->_tblValidate = self::getValidateData(self::$_strTable);
            $this->_numEditedRowId = null;
            $this->_tblEditedRowData = array();
            $this->_strImageDirUrl = $CONF['base_url']."media/pub/video/";
            $this->_strImageDir = $CONF['pub_dir']."video/";
            
            $this->_tblListData = array(
                "column" => array(
                    "title" => "Cím"
                )
                ,"search" => array(
//                    "title" => array("text" => "Videó címe", "type" => "input")
                )
            );
            
            if (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'edit' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                $this->_strImageDirUrl .= $this->_numEditedRowId."/small/";
                $this->_strImageDir .= $this->_numEditedRowId."/small/";
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'del' && isset($this->_rowEvent['id']) && is_numeric($this->_rowEvent['id'])) {
                $this->_numEditedRowId = $this->_rowEvent['id'];
                
            } elseif (isset($this->_rowEvent['do']) && $this->_rowEvent['do'] == 'new' && !isset($this->_rowEvent['id'])) {

            }

            $this->run();
        }
        
        private function getData(){
            global $objDb;
            
            $tblData = $objDb->getAllIdIdx("
                SELECT 
                	 id
                    ,title
                    ,title_en
                    ,title_ro
                   -- ,url
                    ,code
                    ,description
                    ,description_en
                    ,description_ro
                    ,image
                    ,city_id
                    ,is_active
                    ,create_user_id
                    ,create_date
                    ,modify_user_id
                    ,modify_date
                FROM 
                	".self::$_strTable." 
                WHERE 
                	delete_date IS NULL
                ORDER BY
                    title
            ");
            
            if (is_numeric($this->_numEditedRowId)) {

                //Ha olyan id-t akar megnyitni szerkesztésre, ami nem létezik, akkor listára dobjuk
                if (!is_array($tblData) || !isset($tblData[$this->_numEditedRowId])) {
                    $this->setHeaderLocation("video");
                } else {
                    $this->_tblEditedRowData = $tblData[$this->_numEditedRowId];
                    
                    if (!empty($this->_tblEditedRowData['image']) && file_exists($this->_strImageDir.$this->_tblEditedRowData['image'])) {
                        $this->_tblEditedRowData['image'] = $this->_strImageDirUrl.$this->_tblEditedRowData['image'];
                    } else {
                        $this->_tblEditedRowData['image'] = null;
                    }
                    
                    //kapcsolódó típusok lekérése
                    $tblTypeId = $objDb->getCol("
                        SELECT
                            type_id
                        FROM
                            video_type
                        WHERE
                            video_id = {$this->_numEditedRowId}
                    ");
                    if ($tblTypeId !== FALSE) {
                        $this->_tblEditedRowData['type_id'] = $tblTypeId;
                    }
                }
            } else {
                if (!empty($tblData) && $tblData !== FALSE) {
                    foreach ($tblData as $numIdx => $rowData) {
                        if (!empty($rowData['image']) && file_exists($this->_strImageDir.$rowData['id']."/small/".$rowData['image'])) {
                            $tblData[$numIdx]['image'] = $this->_strImageDirUrl.$rowData['id']."/small/".$rowData['image'];
                        } else {
                            $tblData[$numIdx]['image'] = null;
                        }
                    }
                }
            }
            if (!empty($tblData) && $tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            //Választható típusok lekérése
            $tblType = $objDb->getAll("
                SELECT
                     id AS value
                    ,name AS text
                FROM
                    type
                WHERE
                    delete_date IS NULL
                    AND type_group = 'video'
                    AND is_active = 1
                ORDER BY
                    name
            ");
            if ($tblType !== FALSE) {
                foreach ($tblType as $numIdx => $rowType) {
                    $this->_tblType[$numIdx] = $rowType;
                    $this->_tblType[$numIdx]['selected'] = "";
                    if (isset($this->_tblEditedRowData['type_id']) && !empty($this->_tblEditedRowData['type_id'])) {
                        if (in_array($rowType['value'], $this->_tblEditedRowData['type_id'])) {
                            $this->_tblType[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            //Választható városok lekérése
            $tblCity = $objDb->getAll("
                SELECT
                     id AS value
                    ,name AS text
                FROM
                    city
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                ORDER BY
                    name
            ");
            if ($tblCity !== FALSE) {
                $this->_tblCity[-1]['value'] = "";
                $this->_tblCity[-1]['text'] = "Nem tartozik hozzá város";
                
                foreach ($tblCity as $numIdx => $rowCity) {
                    $this->_tblCity[$numIdx] = $rowCity;
                    $this->_tblCity[$numIdx]['selected'] = "";
                    
                    if (isset($this->_tblEditedRowData['city_id']) && is_numeric($this->_tblEditedRowData['city_id'])) {
                        if ($rowCity['value'] == $this->_tblEditedRowData['city_id']) {
                            $this->_tblCity[$numIdx]['selected'] = "selected";
                        }
                    }
                }
            }
            
            return true;
        }
        
        /**
         * Törli a sort 
         * @global type $objDb
         * @param integer $numRowId
         */
        public static function removeModuleData ($numRowId)
        {
            return parent::removeMainTableData ($numRowId, self::$_strTable);
        }
        
        /**
         * Validálja a mentéskor post-ban kapott értékeket
         * @param array $tblPostData
         * @return string[]|NULL[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleValidation ($tblPostData, $tblFileData = null)
        {
            $rowMessage = array('type' => 'ok', 'msg' => null);
            $tblValidate = self::getValidateData(self::$_strTable);
            
            //TODO ellenőrzések 
            
            return $rowMessage;
        }
        
        
        /**
         * Menti a post-ban kapott értékeket
         * @param array $tblPostData
         * @return NULL[]|string[] - $rowMessage hibás vagy helyes futás, megjelenítendő üzenetek
         */
        public static function saveModuleData ($tblPostData, $tblFileData = null)
        {
            global $objDb, $objUser, $CONF;
            $rowMessage = array('type' => null, 'msg' => null);            
            
            if (!isset($tblPostData['id']) || !is_numeric($tblPostData['id'])) {
                //Insert
                $strQueryInsert = "
                    INSERT INTO 
                        ".self::$_strTable." (
                             title
                            ,title_en
                            ,title_ro
                           -- ,url
                            ,code
                            ,description
                            ,description_en
                            ,description_ro
                            ,city_id
                            ,create_user_id
                        ) VALUES (
                             '{$tblPostData['title']}'
                            ,".(!empty($tblPostData['title_en']) ? "'{$tblPostData['title_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['title_ro']) ? "'{$tblPostData['title_ro']}'" : "NULL")."
                            ,'{$tblPostData['code']}'
                            ,'{$tblPostData['description']}'
                            ,".(!empty($tblPostData['description_en']) ? "'{$tblPostData['description_en']}'" : "NULL")."
                            ,".(!empty($tblPostData['description_ro']) ? "'{$tblPostData['description_ro']}'" : "NULL")."
                            ,".(isset($tblPostData['city_id']) && is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL")."
                            ,{$objUser->getNumUserId()}
                        )
                ";
                $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
//                     var_dump($dbres);
//                     print $strQueryInsert;
                if (is_numeric($dbres)) {
                    $numNewId = $dbres;
                    
                    // Kép mentés
                    if (!empty($tblFileData)) {
                        if (self::saveFile($tblFileData, $tblPostData, $numNewId) === FALSE) {
                            $rowMessage['type'] = 'error';
                            $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                        }
                    }
                    
                    // type_id mentések
                    if (isset($tblPostData['type_id']) && !empty($tblPostData['type_id']) ){
                        foreach ($tblPostData['type_id'] as $numTypeId) {
                            $strQueryInsert = "
                                INSERT INTO
                                    video_type(
                                         video_id
                                        ,type_id
                                    ) VALUES (
                                         {$numNewId}
                                        ,{$numTypeId}
                                    )
                            ";
                             $dbres2 = $objDb->query($strQueryInsert);
                             if ($dbres2 === FALSE) {
                                 $rowMessage['type'] = 'error';
                                 $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                 break;
                             }
                        }
                    }
                    
                    if ($rowMessage['type'] != 'error') {
                        $rowMessage['type'] = 'ok';
                        $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        $rowMessage['new_id'] = $numNewId;
                    }
                } else {
                    $rowMessage['type'] = 'error';
                    $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                }

            } else {
                //Update
                
                $rowOldData = $objDb->getRow("
                    SELECT
                        *
                    FROM
                        ".self::$_strTable."
                    WHERE
                        id = {$tblPostData['id']}
                ");
                if ($rowOldData !== FALSE) {
                    
                    $strQueryUpdate = "
                        UPDATE
                            ".self::$_strTable."
                        SET
                             modify_date = now()
                            ,modify_user_id = {$objUser->getNumUserId()}
                            ".(isset($tblPostData['title']) && $rowOldData['title'] != $tblPostData['title'] ? ",title = '{$tblPostData['title']}'" : "")."
                            ".(isset($tblPostData['title_en']) && $rowOldData['title_en'] != $tblPostData['title_en'] ? ",title_en = '{$tblPostData['title_en']}'" : "")."
                            ".(isset($tblPostData['title_ro']) && $rowOldData['title_ro'] != $tblPostData['title_ro'] ? ",title_ro = '{$tblPostData['title_ro']}'" : "")."
                            ".(isset($tblPostData['code']) && $rowOldData['code'] != $tblPostData['code'] ? ",code = '{$tblPostData['code']}'" : "")."
                            ".(isset($tblPostData['description']) && $rowOldData['description'] != $tblPostData['description'] ? ",description = '{$tblPostData['description']}'" : "")."
                            ".(isset($tblPostData['description_en']) && $rowOldData['description_en'] != $tblPostData['description_en'] ? ",description_en = '{$tblPostData['description_en']}'" : "")."
                            ".(isset($tblPostData['description_ro']) && $rowOldData['description_ro'] != $tblPostData['description_ro'] ? ",description_ro = '{$tblPostData['description_ro']}'" : "")."
                            ".(isset($tblPostData['city_id']) && $rowOldData['city_id'] != $tblPostData['city_id'] ? ",city_id = ".(is_numeric($tblPostData['city_id']) ? "{$tblPostData['city_id']}" : "NULL") : "")."
                            ".(isset($tblPostData['is_active']) && $rowOldData['is_active'] != $tblPostData['is_active'] ? ",is_active = ".($tblPostData['is_active'] == 'true' ? "1" : "0") : "")."
                        WHERE
                            id = {$tblPostData['id']}
                    ";
//                                 print $strQueryUpdate;
                    $dbres = $objDb->query($strQueryUpdate);
                    
                    if ($dbres !== FALSE && $tblPostData['row_save'] === false) {
                        
                        // Kép módosítás
                        if (!empty($tblFileData)) {
                            if (self::saveFile($tblFileData, $tblPostData, $tblPostData['id']) === FALSE) {
                                $rowMessage['type'] = 'error';
                                $rowMessage['msg'] = 'Hiba történt a kép mentése során.';
                            }
                        }
                        
                        // type_id módosítások
                        $dbres = $objDb->query("
                            DELETE FROM
                                video_type
                            WHERE
                                video_id = {$tblPostData['id']}
                        ");
                        if (isset($tblPostData['type_id']) && !empty($tblPostData['type_id']) ){
                            foreach ($tblPostData['type_id'] as $numTypeId) {
                                $strQueryInsert = "
                                    INSERT INTO
                                        video_type(
                                             video_id
                                            ,type_id
                                        ) VALUES (
                                             {$tblPostData['id']}
                                            ,{$numTypeId}
                                        )
                                ";
                                 //                                          print $strQueryInsert;
                                 $dbres = $objDb->query($strQueryInsert);
                                 if ($dbres === FALSE) {
                                     $rowMessage['type'] = 'error';
                                     $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                                     break;
                                 }
                            }
                        }

                        if ($rowMessage['type'] != 'error') {
                            $rowMessage['type'] = 'ok';
                            $rowMessage['msg'] = 'Adatok mentése sikeresen megtörtént.';
                        }

                    } else {
                        $rowMessage['type'] = 'error';
                        $rowMessage['msg'] = 'Hiba történt az adatok mentése során.';
                    }
                }
            }
            
            return $rowMessage;
        }
        
        /**
         * Modulhoz tartozó fájlfeltöltések kezelésére
         * @param array $tblFileData
         * @param array $tblPostData
         * @param integer $numId
         * @return boolean
         */
        public static function saveFile($tblFileData, $tblPostData, $numId)
        {
            $tblImgSize['big']['width'] = 600;
            $tblImgSize['big']['height'] = 315;
            $tblImgSize['small']['width'] = 515;
            $tblImgSize['small']['height'] = 270;
            
            $rowTable['table'] = self::$_strTable;
            $rowTable['column'] = 'image';
            
            return parent::saveMainTableFile($tblFileData, $tblPostData, $numId, $rowTable, $tblImgSize);
        }
        
        public function run(){ 
            global $objSmarty;
            
            if ($this->getData() === true) {
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblEditedRowData", $this->_tblEditedRowData);
                $objSmarty->assign("tblType", $this->_tblType);
                $objSmarty->assign("tblCity", $this->_tblCity);
            }
            
        }
        
    }
    