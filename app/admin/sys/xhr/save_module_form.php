<?php
// var_dump($_POST);
// var_dump($_FILES);

// die('kk');
    if (isset($_POST['module_url']) && !empty($_POST['module_url']) && $_POST['module_url'] != "undefined") {
        $strModuleUrl = $_POST['module_url'];
    } elseif (isset($rowCurrModuleData['url']) && !empty($rowCurrModuleData['url'])) {
        $strModuleUrl = $rowCurrModuleData['url'];
    } else {
        return false;    
    }

    $strFileName = str_replace("-", "_", ucfirst($strModuleUrl));

    require_once 'page/module/'.$strFileName.'.php';
    
    $tblPost = array();
    $tblFile = array();
    $rowMessage = array('type' => 'error', 'msg' => 'Hiba történt a mentés során.');
    $strDo = "";
    
    if (isset($_POST['row_field_data']) && !empty($_POST['row_field_data']) && is_numeric(key($_POST['row_field_data']))) {
        //lista nézetből mentés
        if (isset($rowCurrModuleData['is_edit_allowed']) && $rowCurrModuleData['is_edit_allowed'] == 1) {
            $tblPost = array_values($_POST['row_field_data'])[0];
            $tblPost[key($tblPost)]['id'] = key($_POST['row_field_data']);
            $tblPost[key($tblPost)]['row_save'] = true;
            $strDo = "save";
        }
        
    } elseif (isset($_POST['do']) && $_POST['do'] == 'del' && isset($_POST['id']) && is_numeric($_POST['id'])) {
        //törlés
        if (isset($rowCurrModuleData['is_delete_allowed']) && $rowCurrModuleData['is_delete_allowed'] == 1) {
            $rowMessage = $strFileName::removeModuleData($_POST['id']);
        }
    
    } else {
        //form nézetből mentés
        if ((
                isset($_POST[$strModuleUrl.'_field']['id']) && is_numeric($_POST[$strModuleUrl.'_field']['id']) 
                && isset($rowCurrModuleData['is_edit_allowed']) && $rowCurrModuleData['is_edit_allowed'] == 1
            ) || (
                (!isset($_POST[$strModuleUrl.'_field']['id']) || !is_numeric($_POST[$strModuleUrl.'_field']['id'])) 
                && isset($rowCurrModuleData['is_new_allowed']) && $rowCurrModuleData['is_new_allowed'] == 1
        )) {
            $tblPost = $_POST;
            $tblPost[$strModuleUrl.'_field']['module_url'] = $strModuleUrl;
            $tblFile = $_FILES;
            $strDo = "save";
        }
        $tblPost[$strModuleUrl.'_field']['row_save'] = false;
    }
    
//     var_dump($tblPost);
//     die();
    if ($strDo == "save") {
        $rowMessage = $strFileName::saveModuleValidation($tblPost[$strModuleUrl.'_field'], (!empty($tblFile) && isset($tblFile[$strModuleUrl.'_field']) ? $tblFile[$strModuleUrl.'_field'] : null));
//         var_dump($rowMessage);
        if ($rowMessage['type'] == 'ok') {
            $rowMessage = $strFileName::saveModuleData($tblPost[$strModuleUrl.'_field'], (!empty($tblFile) && isset($tblFile[$strModuleUrl.'_field']) ? $tblFile[$strModuleUrl.'_field'] : null));    
        }
//         var_dump($rowMessage);
    }
    
    print json_encode($rowMessage);



