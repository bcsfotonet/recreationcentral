<?php
    function smarty_insert_backbtn ($params, &$smarty) {

        if(!empty($params['url'])){
            $smarty->assign('backbtn_url', $params['url']);
        }
        $strTpl = $smarty->fetch('content/backbtn.tpl');
        return $strTpl;
}

