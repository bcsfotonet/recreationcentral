<?php
    function smarty_insert_checkboxitem ($params, &$smarty) {

        if (!empty($params['label'])) {
            $smarty->assign('strLabel', $params['label']);
        } else {
            $smarty->assign('strLabel', "");
        }
        if (!empty($params['input_name'])) {
            $smarty->assign('strInputName', $params['input_name']);
        } else {
            $smarty->assign('strInputName', "");
        }
        if (!empty($params['value'])) {
            $smarty->assign('strValue', $params['value']);
        } else {
            $smarty->assign('strValue', "");
        }
        
        $strTpl = $smarty->fetch('content/checkboxitem.tpl');
        return $strTpl;
}

