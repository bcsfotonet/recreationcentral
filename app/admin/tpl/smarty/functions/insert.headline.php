<?php
    function smarty_insert_headline ($params, &$smarty) {

        if(!empty($params['title'])){
            $smarty->assign('headline_title', $params['title']);
        }
        $strTpl = $smarty->fetch('content/headline.tpl');
        return $strTpl;
}

