<?php
    function smarty_insert_imageupload ($params, &$smarty) {

        if (!empty($params['label'])) {
            $smarty->assign('strLabel', $params['label']);
        } else {
            $smarty->assign('strLabel', "");
        }
        if (!empty($params['btn_title'])) {
            $smarty->assign('strBtnTitle', $params['btn_title']);
        } else {
            $smarty->assign('strBtnTitle', "");
        }
        if (!empty($params['input_name'])) {
            $smarty->assign('strInputName', $params['input_name']);
        } else {
            $smarty->assign('strInputName', "");
        }
        if (!empty($params['is_img']) && is_numeric($params['is_img'])) {
            $smarty->assign('isImage', $params['is_img']);
        } else {
            $smarty->assign('isImage', "null");
        }
        if (!empty($params['file_url'])) {
            $smarty->assign('strFileUrl', $params['file_url']);
        } else {
            $smarty->assign('strFileUrl', "");
        }
        if (!empty($params['is_multiple']) && is_numeric($params['is_multiple']) && $params['is_multiple'] == 1) {
            $smarty->assign('strMultiple', 'multiple="multiple"'); 
        } else {
            $smarty->assign('strMultiple', "");
        }
        if (!empty($params['extList'])) {
            $smarty->assign('strExtList', $params['extList']);
        } else {
            $smarty->assign('strExtList', "");
        }
        if (!empty($params['column'])) {
            $smarty->assign('strColumn', $params['column']);
        } else {
            $smarty->assign('strColumn', "");
        }
            
        $strTpl = $smarty->fetch('content/imageupload.tpl');
        return $strTpl;
}

