<?php
    function smarty_insert_inputitem ($params, &$smarty) {

        if(!empty($params['label'])){
            $smarty->assign('strInputLabel', $params['label']);
        }else{
            $smarty->assign('strInputLabel', "");
        }
        if(!empty($params['type'])){
            $smarty->assign('strInputType', $params['type']);
        }else{
            $smarty->assign('strInputType', "");
        }
        if(!empty($params['maxlength'])){
            $smarty->assign('numMaxlength', $params['maxlength']);
        }else{
            $smarty->assign('numMaxlength', "");
        }
        if(!empty($params['placeholder'])){
            $smarty->assign('strInputPlaceholder', $params['placeholder']);
        }else{
            $smarty->assign('strInputPlaceholder', "");
        }
        if(!empty($params['input_name'])){
            $smarty->assign('strInputName', $params['input_name']);
        }else{
            $smarty->assign('strInputName', "");
        }
        if(!empty($params['value'])){
            $smarty->assign('strInputValue', $params['value']);
        }else{
            $smarty->assign('strInputValue', "");
        }
        if(!empty($params['fill'])){
            $smarty->assign('numInputFill', $params['fill']);
        }else{
            $smarty->assign('numInputFill', "");
        }
        if (!empty($params['class'])) {
            $smarty->assign('strInputClass', $params['class']);
        }else{
            $smarty->assign('strInputClass', "");
        }
        if (!empty($params['disabled'])) {
            $smarty->assign('strDisabled', $params['disabled']);
        }else{
            $smarty->assign('strDisabled', "");
        }
        $strTpl = $smarty->fetch('content/inputitem.tpl');
        return $strTpl;
}

