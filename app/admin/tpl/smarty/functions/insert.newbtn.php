<?php
    function smarty_insert_newbtn ($params, &$smarty) {

        if(!empty($params['title'])){
                $smarty->assign('btn_title', $params['title']);
        }
        if(!empty($params['class'])){
                $smarty->assign('btn_class', $params['class']);
        }
        if(!empty($params['url'])){
                $smarty->assign('btn_url', $params['url']);
        }

        $strTpl = $smarty->fetch('content/newbtn.tpl');
        return $strTpl;
    }
        