<?php
    function smarty_insert_pagination ($params, &$smarty) {

        if(!empty($params['objPagination'])){
            $smarty->assign('objPagination', $params['objPagination']);
        } else {
            $smarty->assign('objPagination', "");
        }
        if(!empty($params['url'])){
            $smarty->assign('strUrl', $params['url']);
        } else {
            $smarty->assign('strUrl', "");
        }
        
        $strTpl = $smarty->fetch('content/pagination.tpl');
        return $strTpl;
}

