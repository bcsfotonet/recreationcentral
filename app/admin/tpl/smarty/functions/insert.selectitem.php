<?php
    function smarty_insert_selectitem ($params, &$smarty) {

        if (!empty($params['label'])) {
            $smarty->assign('strLabel', $params['label']);
        } else {
            $smarty->assign('strLabel', "");
        }
        if (!empty($params['options'])) {
            $smarty->assign('tblOtion', $params['options']);
        } else {
            $smarty->assign('tblOtion', array());
        }
        if (!empty($params['select_name'])) {
            $smarty->assign('strSelectName', $params['select_name']);
        } else {
            $smarty->assign('strSelectName', "");
        }
        if (!empty($params['multiple'])) {
            $smarty->assign('isMultiple', $params['multiple']);
        } else {
            $smarty->assign('isMultiple', 0);
        }
        if(!empty($params['fill'])){
            $smarty->assign('numInputFill', $params['fill']);
        }else{
            $smarty->assign('numInputFill', "");
        }
        if (!empty($params['class'])) {
            $smarty->assign('strInputClass', $params['class']);
        }else{
            $smarty->assign('strInputClass', "");
        }
        $strTpl = $smarty->fetch('content/selectitem.tpl');
        return $strTpl;
}

