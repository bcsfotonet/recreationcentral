<?php
    function smarty_insert_submitcancel ($params, &$smarty) {
        if(!empty($params['cancel_url'])){
            $smarty->assign('strCancelUrl', $params['cancel_url']);
        }
        $strTpl = $smarty->fetch('content/submitcancel.tpl');
        return $strTpl;
    }
