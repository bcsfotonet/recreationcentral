<div class="form__item">
    <div class="form__group">
        <label>
            {$strLabel}
            <input type="checkbox" name="{$strInputName}" value="true" {if $strValue == 1}checked{/if}>
            <span class="checkmark"></span>
        </label>
    </div>
</div>