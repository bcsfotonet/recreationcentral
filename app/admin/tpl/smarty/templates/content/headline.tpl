<div class="headline">
    <div class="hamburger-menu">
            <span></span>
            <span></span>
            <span></span>
    </div>
    <div class="headline__title">
        {if !empty($headline_title)}
            {$headline_title}
        {else}
            {if !empty($rowModuleData.module_group_name)}{$rowModuleData.module_group_name} / {/if}{$rowModuleData.module_name}
        {/if}
    </div>
    <div class="headline__user">
        <span>Belépve: </span><b>{$tblLoggedUserData.name}</b>
    </div>
</div>
    