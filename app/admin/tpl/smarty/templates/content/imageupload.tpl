<div class="form__item">
	{if $strLabel != ""} 
    	<label>{$strLabel}</label>
    {/if}
    <div class="form__group">
        <button class="button button--upload">{$strBtnTitle}</button>
        <input 
        	class="file_upload_input {if $isImage == 1}image_upload_input{/if}" 
        	type="file" 
        	{$strMultiple} 
        	name="{$strInputName}"
        	data-column_name="{$strColumn}"
        	{if !empty($strExtList)} accept="{$strExtList}" {else if $isImage == 1} accept="image/*" {/if}
    	>
    </div>
</div>
        
        {if $isImage == "1"}
        	{if empty($strMultiple)}
            	<div class="form__item preview d-none">
            		<label>Feltöltésre kiválasztott kép előnézet</label>
        			<img data-input_name="{$strInputName}" src="" class="image-upload-preview w-auto img-thumbnail mt-2 mx-auto d-block" />
    			</div>
    			
    			<div class="form__item {if empty($strFileUrl)}d-none{/if}">
    				<label>Mentett kép</label>
                	<div class="col-12 mt-2 grid-view__row {*grid_row_{$rowImage.id}*} ">
                        <div class="tiled-view">
                            <div class="tiled-view__header">
                                <div class="tiled-view__icon-wrapper">
                                    {if $rowModuleData.is_edit_allowed == 1}
                                        <div class="form__group">
                                            <label class="pl-0 mb-0 mt-1">
                                                <input class="del_checkbox" type="checkbox" name="{$rowModuleData.url}_field[image_delete]" value="" >
                                                <span class="delicon remove"><i class="fas fa-trash"></i></span>
                                            </label>
                                        </div>
                                    {/if}
                                </div>
                            </div>
                            <hr>
                            <div class="tiled-view__content">
                                <div class="tiled-view__img">
                                    <img {*data-input_name="{$strInputName}"*} src="{$strFileUrl}" class="image-upload-preview w-auto {*img-thumbnail*} img-fluid mt-2 mx-auto d-block" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            {else}
            	<div class="form__item preview d-none preview_{$strColumn}">
            		<label>Feltöltésre kiválasztott képek</label>
        			<div class="preview-list"></div>
    			</div>
            {/if}
    	{else if $isImage == "0"}
    		<div class="form__item preview d-none preview_{$strColumn}">
        		<label>Feltöltésre kiválasztott dokumentum</label>
    			<div class="preview-list"></div>
			</div>
			<div class="form__item {if empty($strFileUrl)}d-none{/if}">
				<label>Mentett dokumentum</label>
        		<div class="col-12 mt-2 ">
        			<div class="tiled-view p-2">
        				<div class="tiled-view__icon-wrapper">
                            {if $rowModuleData.is_edit_allowed == 1}
                                <div class="form__group">
                                    <label class="pl-0 mb-0 mr-3">
                                        <input class="del_checkbox" type="checkbox" name="{$rowModuleData.url}_field[{$strColumn}_delete]" value="" >
                                        <span class="delicon remove"><i class="fas fa-trash"></i></span>
                                    </label>
                                </div>
                            {/if}
                            <div class="form__item">
        	    				<a class="document__download" href="{$strFileUrl}" target="_blank">Letöltés</a>
        	    			</div>
                        </div>
                    </div>
        		</div>
    		</div>
        {/if}
    
<!-- </div> -->