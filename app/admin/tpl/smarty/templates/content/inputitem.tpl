{if $strInputType != "hidden"}
<div {if strpos($strInputClass, 'search_input') === false} class="form__item" {/if}>
    <label>{$strInputLabel}{if isset($numInputFill) && $numInputFill == 1}*{/if}</label>
{/if}
    <input 
    	class="{if isset($numInputFill) && $numInputFill == 1}fill{/if} {if isset($strInputClass)}{$strInputClass}{/if}" 
    	type="{$strInputType}" 
    	placeholder="{$strInputPlaceholder}" 
    	name="{$strInputName}" 
    	value="{$strInputValue}"
    	{$strDisabled}
    	{if !empty($numMaxlength)}maxlength="{$numMaxlength}"{/if}
	>
    <span class="form__error"></span>
{if $strInputType != "hidden"}
</div>
{/if}