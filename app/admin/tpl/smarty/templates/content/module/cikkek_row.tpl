
<!-- {*foreach from=$tblData key=numRowIdx item=rowData*} -->
{if !empty($objPagination->getTblPageData())}
{foreach from=$objPagination->getTblPageData() key=numRowIdx item=rowData}
    <div class="col-12 col-sm-6 col-xl-4 grid-view__row grid_row_{$rowData.id} {if $rowData.is_active == 0}inactive{/if}">
        <div class="tiled-view">
            <div class="tiled-view__header">
            	{if $rowModuleData.is_edit_allowed == 1}
                    <a href="{$CONF.admin_base_url}{$rowModuleData.url}/edit/{$rowData.id}" class="button button--edit">
                        <i class="far fa-edit"></i>Szerkesztés
                    </a>
                {/if}
                <div class="tiled-view__icon-wrapper">
    				<div class="form__item js-row-menu-status">
                        <div class="form__group">
                            <label>
                                <input class="module-row-save-field is_active_main_field" type="checkbox" name="row_field_data[{$rowData.id}][{$rowModuleData.url}_field][is_active]" value="" {if $rowData.is_active == 1}checked{/if}>
                                <span class="eyeicon "><i class="far fa-eye {if $rowData.is_active == 0}fa-eye-slash{/if}"></i></span>
                            </label>
                        </div>
                    </div>
                    {if $rowModuleData.is_delete_allowed == 1}
                        <a href="#" class="remove js-remove" data-row_id="{$rowData.id}" data-toggle="tooltip" data-placement="top" title="Törlés">
                            <i class="fas fa-trash"></i>
                        </a>
                    {/if}
                </div>
            </div>
            <hr>
            <div class="tiled-view__content">
                <div class="tiled-view__data">
                    <b>Cím:</b>{$rowData.title}
                </div>
                <div class="tiled-view__data">
                    <b>Dátum kezdete:</b>{$rowData.start_date|date_format:"%Y. %m. %d. %H:%M"}
                </div>
            </div>
        </div>
    </div>
{/foreach}                
{/if}

<!-- {* TODO csak akkor legyen lapozó, ha kell *} -->
{if $objPagination->getMaxPage() > 1}
	{insert name="pagination" objPagination="$objPagination" url="`$CONF.admin_base_url``$rowModuleData.url`/"}
{/if}
