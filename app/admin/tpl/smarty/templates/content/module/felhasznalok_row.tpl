
{foreach from=$tblData key=numRowIdx item=rowData} 
	<div class="list-view__row list_row_{$rowData.id} {if $rowData.is_active == 0}inactive{/if}">
        <div class="row align-items-center">
            <div class="col-12 col-sm-5">
                <div class="list-view__name">{if $rowData.is_person == 1}{$rowData.last_name} {$rowData.first_name}{elseif $rowData.is_company == 1}{$rowData.company_name}{/if}</div>
            </div>
            <div class="col-12 col-sm-5">
                <div class="list-view__name">{$rowData.email}</div>
            </div>
            <div class="col-12 col-sm-2 flex flex--end">
                <div class="form__item js-row-menu-status">
                    <div class="form__group">
                        <label>
                            <input class="module-row-save-field is_active_main_field" type="checkbox" name="row_field_data[{$rowData.id}][{$rowModuleData.url}_field][is_active]" value="" {if $rowData.is_active == 1}checked{/if}>
                            <span class="eyeicon "><i class="far fa-eye {if $rowData.is_active == 0}fa-eye-slash{/if}"></i></span>
                        </label>
                    </div>
                </div>
                {if $rowModuleData.is_edit_allowed == 1}
                    <a href="{$CONF.admin_base_url}{$rowModuleData.url}/edit/{$rowData.id}" class="edit" data-toggle="tooltip" data-placement="top" title="Szerkesztés">
                        <i class="far fa-edit"></i>
                    </a>
                {/if}
                {if $rowModuleData.is_delete_allowed == 1}
                    <a href="#" class="remove js-remove" data-row_id="{$rowData.id}" data-toggle="tooltip" data-placement="top" title="Törlés">
                        <i class="fas fa-trash"></i>
                    </a>
                {/if}
            </div>
        </div>
    </div>
{/foreach}