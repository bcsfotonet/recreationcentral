
{foreach from=$tblData key=numRowIdx item=rowData} 
	<div class="list-view__row list_row_{$rowData.id} {if $rowData.is_active == 0}inactive{/if}">
        <div class="row align-items-center">
            <div class="col-12 col-sm-12 col-xl-1">
                <div class="list-view__order-edit">
                    <div class="form__item">
                        <div class="form__group">
                        	{assign var="numMaxPriority" value=$tblData|@count}
                            <select name="row_field_data[{$rowData.id}][{$rowModuleData.url}_field][priority]" id="" class="js-example-basic-single module-row-save-field">    
    							{section name=numOrder start=1 loop=$numMaxPriority+1 step=1}
    								<option 
    									value="{$smarty.section.numOrder.index}" 
    									{if $smarty.section.numOrder.index == $rowData.priority}selected="selected"{/if}
									>
										{$smarty.section.numOrder.index}
									</option>
    							{/section}
    						</select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-10 col-xl-9">
                <div class="list-view__name">{$rowData.question}</div>
            </div>
            <div class="col-12 col-sm-2 flex flex--end">
                <div class="form__item js-row-menu-status">
                    <div class="form__group">
                        <label>
                            <input class="module-row-save-field is_active_main_field" type="checkbox" name="row_field_data[{$rowData.id}][{$rowModuleData.url}_field][is_active]" value="" {if $rowData.is_active == 1}checked{/if}>
                            <span class="eyeicon "><i class="far fa-eye {if $rowData.is_active == 0}fa-eye-slash{/if}"></i></span>
                        </label>
                    </div>
                </div>
                {if $rowModuleData.is_edit_allowed == 1}
                    <a href="{$CONF.admin_base_url}{$rowModuleData.url}/edit/{$rowData.id}" class="edit" data-toggle="tooltip" data-placement="top" title="Szerkesztés">
                        <i class="far fa-edit"></i>
                    </a>
                {/if}
                {if $rowModuleData.is_delete_allowed == 1}
                    <a href="#" class="remove js-remove" data-row_id="{$rowData.id}" data-toggle="tooltip" data-placement="top" title="Törlés">
                        <i class="fas fa-trash"></i>
                    </a>
                {/if}
            </div>
        </div>
    </div>
{/foreach}