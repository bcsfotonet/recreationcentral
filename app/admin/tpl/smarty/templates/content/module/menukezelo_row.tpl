
{foreach from=$tblData key=numRowIdx item=tblMainData} 
	<div class="list-view__row list_row_{$tblMainData.main.id} {if $tblMainData.main.is_active == 0}inactive{/if}">
        <div class="row align-items-center">
            <div class="col-12 col-md-2">
                <div class="list-view__order-edit">
                    <div class="{if !empty($tblMainData.child)}list-view__btn{/if} js-list-view-dropdown" {if empty($tblMainData.child)}style="width: 40px;"{/if}></div>
                    <div class="form__item">
                        <div class="form__group">
                        	{assign var="numMaxPriority" value=$tblData|@count}
                            <select name="row_field_data[{$tblMainData.main.id}][{$rowModuleData.url}_field][priority]" id="" class="js-example-basic-single module-row-save-field">    
    							{section name=numOrder start=1 loop=$numMaxPriority+1 step=1}
    								<option 
    									value="{$smarty.section.numOrder.index}" 
    									{if $smarty.section.numOrder.index == $tblMainData.main.priority}selected="selected"{/if}
									>
										{$smarty.section.numOrder.index}
									</option>
    							{/section}
    						</select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-10 col-xl-4">
                <div class="list-view__name">{$tblMainData.main.name}</div>
            </div>
            <div class="col-12 col-sm-10 col-xl-4 flex flex--end">
                <div class="list-view__settings">
                    <div class="form__item">
                        <div class="form__group">
                            <label>
                                Főmenü
                                <input class="module-row-save-field" type="checkbox" name="row_field_data[{$tblMainData.main.id}][{$rowModuleData.url}_field][is_visible_main_menu]" value="" {if $tblMainData.main.is_visible_main_menu == 1}checked{/if} >
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form__item">
                        <div class="form__group">
                            <label>
                                Footer menü
                                <input class="module-row-save-field" type="checkbox" name="row_field_data[{$tblMainData.main.id}][{$rowModuleData.url}_field][is_visible_footer_menu]" value="" {if $tblMainData.main.is_visible_footer_menu == 1}checked{/if} >
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-2 col-xl-2 flex flex--end">
                <div class="form__item js-row-menu-status">
                    <div class="form__group">
                        <label>
                            <input class="module-row-save-field is_active_main_field" type="checkbox" name="row_field_data[{$tblMainData.main.id}][{$rowModuleData.url}_field][is_active]" value="" {if $tblMainData.main.is_active == 1}checked{/if}>
                            <span class="eyeicon "><i class="far fa-eye {if $tblMainData.main.is_active == 0}fa-eye-slash{/if}"></i></span>
                        </label>
                    </div>
                </div>
                {if $rowModuleData.is_edit_allowed == 1}
                	{* "{$CONF.admin_base_url}{$rowModuleData.url}/edit/{$tblMainData.main.id}" *}
                    <a href="{$CONF.admin_base_url}{$rowModuleData.url}/edit/{$tblMainData.main.id}" class="edit" data-toggle="tooltip" data-placement="top" title="Szerkesztés">
                        <i class="far fa-edit"></i>
                    </a>
                {/if}
                {if $rowModuleData.is_delete_allowed == 1}
                    <a href="#" class="remove js-remove" data-row_id="{$tblMainData.main.id}" data-toggle="tooltip" data-placement="top" title="Törlés">
                        <i class="fas fa-trash"></i>
                    </a>
                {/if}
            </div>
        </div>
        
        {* almenük *}
        {foreach from=$tblMainData.child key=numRowChildIdx item=tblChildData} 
            <div class="list-view__sub list_row_{$tblChildData.id} {if $tblChildData.is_active == 0}inactive{/if}">
                <hr>
<!--                 <div class="container-fluid"> -->
                    <div class="row align-items-center pl-5">
                        <div class="col-12 col-md-2">
                            <div class="list-view__order-edit">
                                <div class="list-view__btn" style="opacity:0; cursor:unset;"></div>
                                <div class="form__item">
                                    <div class="form__group">
                                    	{assign var="numMaxChildPriority" value=$tblMainData.child|@count}
                                    	
                                        <select name="row_field_data[{$tblChildData.id}][{$rowModuleData.url}_field][priority]" id="" class="js-example-basic-single module-row-save-field">
                                            {section name=numOrder start=1 loop=$numMaxChildPriority+1 step=1} 
                								<option 
                									value="{$smarty.section.numOrder.index}" 
                									{if $smarty.section.numOrder.index == $tblChildData.priority}selected="selected"{/if}
            									>
            										{$smarty.section.numOrder.index}
        										</option>
                							{/section}
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-10 col-xl-4">
                            <div class="list-view__name">{$tblChildData.name}</div>
                        </div>
                        <div class="col-12 col-sm-10 col-xl-4 flex flex--end">
                            <div class="list-view__settings">
                                <div class="form__item">
                                    <div class="form__group">
                                        <label>
                                            Főmenü
                                            <input class="module-row-save-field" type="checkbox" name="row_field_data[{$tblChildData.id}][{$rowModuleData.url}_field][is_visible_main_menu]" value="" {if $tblChildData.is_visible_main_menu == 1}checked{/if} >
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form__item">
                                    <div class="form__group">
                                        <label>
                                            Footer menü
                                            <input class="module-row-save-field" type="checkbox" name="row_field_data[{$tblChildData.id}][{$rowModuleData.url}_field][is_visible_footer_menu]" value="" {if $tblChildData.is_visible_footer_menu == 1}checked{/if} >
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-2 col-xl-2 flex flex--end">
                            <div class="form__item js-row-menu-status">
                                <div class="form__group">
                                    <label>
                                        <input class="module-row-save-field is_active_sub_field" type="checkbox" name="row_field_data[{$tblChildData.id}][{$rowModuleData.url}_field][is_active]" value="" {if $tblChildData.is_active == 1}checked{/if}>
                                        <span class="eyeicon "><i class="far fa-eye {if $tblChildData.is_active == 0}fa-eye-slash{/if}"></i></span>
                                    </label>
                                </div>
                            </div>
                            {if $rowModuleData.is_edit_allowed == 1}
                                <a href="{$CONF.admin_base_url}{$rowModuleData.url}/edit/{$tblChildData.id}" class="edit" data-toggle="tooltip" data-placement="top" title="Szerkesztés">
                                    <i class="far fa-edit"></i>
                                </a>
                            {/if}
                            {if $rowModuleData.is_delete_allowed == 1}
                                <a href="#" class="remove js-remove" data-row_id="{$tblChildData.id}" data-toggle="tooltip" data-placement="top" title="Törlés">
                                    <i class="fas fa-trash"></i>
                                </a>
                            {/if}
                            
                        </div>
                        
                    </div>
<!--                 </div> -->
            </div>
        {/foreach}
	</div>
{/foreach}