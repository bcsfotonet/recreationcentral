<body>
    <div class="sidebar">
        <a href="{$CONF.admin_base_url}" class="sidebar__title">recreationcentral.eu </a>
        <div class="sidebar__logout">
            <i class="fas fa-sign-out-alt"></i>
            <a href="{$CONF.admin_base_url}Logout">Kijelentkezés</a>
        </div>
        
        <div class="nav">
            <ul class="nav__list">        
                {foreach from=$tblModule item=tblModuleTmp}
                    {if !empty($tblModuleTmp.group)}
                        <li class="nav__li nav__dropdown js-dropdown">
                                <a class="parent-li" href="#">
                                    <i class="fa {$tblModuleTmp.group.icon_class}"></i>
                                    <span>{$tblModuleTmp.group.name}</span>
                                </a>
                            <ul class="nav__subnav">
                    {/if}
                            {foreach from=$tblModuleTmp.submodule item=tblSubmoduleTmp}
                                <li class="nav__li">
                                    <a href="{$CONF.admin_base_url}{$tblSubmoduleTmp.module.url}">
                                        <i class="fa {$tblSubmoduleTmp.module.icon_class}"></i>
                                        <span>{$tblSubmoduleTmp.module.name}</span>
                                    </a>
                                </li>
                            {foreachelse}
                                <li class="nav__li">
                                    <a href="{$CONF.admin_base_url}{$tblModuleTmp.module.url}">
                                        <i class="fa {$tblModuleTmp.module.icon_class}"></i>
                                        <span>{$tblModuleTmp.module.name}</span>
                                    </a>
                                </li>
                            {/foreach}
                    {if !empty($tblModuleTmp.group)}
                            </ul>
                        </li>
                    {/if}
                {/foreach}
            </ul>
        </div>       
    </div>
      