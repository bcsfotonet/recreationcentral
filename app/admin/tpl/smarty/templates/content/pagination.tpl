<div class="col-12">
    <nav aria-label="Page navigation">
        <ul class="pagination">
          	<li class="page-item {if $objPagination->getNumCurrPage() == 1}disabled{/if}">
        		<a class="page-link" href="{$strUrl}{$objPagination->getNumCurrPage()-1}" aria-label="Previous">
              		<span aria-hidden="true">&laquo;</span>
              		<span class="sr-only">Previous</span>
            	</a>
          	</li>
          	{assign var="numMaxPage" value=$objPagination->getMaxPage()}
			{section name=numPagination start=1 loop=$numMaxPage+1 step=1}
				<li class="page-item {if $objPagination->getNumCurrPage() == $smarty.section.numPagination.index}active{/if}">
					<a class="page-link" href="{$strUrl}{$smarty.section.numPagination.index}">{$smarty.section.numPagination.index}</a>
				</li>
			{/section}
<!--           <li class="page-item active"><a class="page-link" href="#">2</a></li> -->
<!--           <li class="page-item"><a class="page-link" href="#">3</a></li> -->
      		<li class="page-item {if $objPagination->getNumCurrPage() == $numMaxPage}disabled{/if}">
            	<a class="page-link" href="{$strUrl}{$objPagination->getNumCurrPage()+1}" aria-label="Next">
              		<span aria-hidden="true">&raquo;</span>
              		<span class="sr-only">Next</span>
           		</a>
          	</li>
        </ul>
    </nav>
</div>