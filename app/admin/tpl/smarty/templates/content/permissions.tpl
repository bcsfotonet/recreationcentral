<div class="col-12 col-lg-4">
    <div class="permissions">
        <div class="permissions__title">Jogosultságok</div>
        <hr>
        <div class="row">
            {foreach from=$tblPermission item=rowPermission}
                <div class="col-12 col-sm-6 col-lg-12 form__item">
                    <div class="form__group">
                        <label>
                            <i class="fas {$rowPermission.icon_class}"></i>
                            {$rowPermission.name}
                            <input name="adminisztratorok_field_permission[{','|implode:$rowPermission.modules}]" type="checkbox" {if $rowEditedUserData.is_super_user == true}disabled{/if} {if $rowPermission.access_user == true}checked{/if}>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            {/foreach}
        </div>
    </div>
</div>