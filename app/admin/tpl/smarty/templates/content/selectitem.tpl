{if !empty($tblOtion)}
<!--     <div class="form__item"> -->
    <div {if strpos($strInputClass, 'search_input') === false} class="form__item" {/if}>
        <label>{$strLabel}{if isset($numInputFill) && $numInputFill == 1}*{/if}</label>
        <div class="form__group">
            <select 
            	name="{$strSelectName}" 
            	id="" 
            	class="{if isset($numInputFill) && $numInputFill == 1}fill{/if} {if isset($strInputClass)}{$strInputClass}{/if} js-example-basic-single"  
            	{if $isMultiple == 1}multiple="multiple"{/if}
            >
            	{foreach from=$tblOtion item=rowOtion}
                <option value="{$rowOtion.value}" {$rowOtion.selected}>{$rowOtion.text}</option>
                {/foreach}
            </select>
            <div class="form__error"></div>
        </div>
	</div>
{/if}