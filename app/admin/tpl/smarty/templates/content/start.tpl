<!DOCTYPE html>
<html lang="hu">
    <head>
        <title>{$strTitle}</title>

        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
        <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
        <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
        <link href="{$CONF.admin_web_url}vendor/vendor.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{$CONF.admin_web_url}css/main.css">
        {literal}
            <script>
            	var strAdminBaseUrl = '{/literal}{$CONF.admin_base_url}{literal}';
            </script>
        {/literal}
</head>