{insert name="start" title="Adminisztrációs felület"}
{insert name="nav"}

<div class="content">
    {insert name="headline"}
                
    <div class="container-fluid">
        {if $rowModuleData.is_new_allowed == 1}
            <div class="row">
            	{insert 
                	name="newbtn" 
                	title="`$rowModuleData.module_name` - Új elem hozzáadása" 
                	class="col-12 col-md-6 col-lg-6" 
                	url="`$CONF.admin_base_url``$rowModuleData.url`/edit"}
            </div>
        {/if}
        {if !empty($tblList.search)}
        	<div class="row justify-content-center">
            	{foreach from=$tblList.search key=strColumn item=rowSearchField}
                	<div class='col-12 col-md-{math equation="min(6, floor( x / y ))" x=12 y=$tblList.search|@count}'>
                		{if $rowSearchField.type == "select" && $rowSearchField.multiple == 1}
                			{insert name="selectitem" class="search_input" label=$rowSearchField.text select_name="`$rowModuleData.url`_searchfield[`$strColumn`][]" options=$rowSearchField.options multiple=$rowSearchField.multiple}
            			{elseif $rowSearchField.type == "select" && $rowSearchField.multiple != 1}
                			{insert name="selectitem" class="search_input" label=$rowSearchField.text select_name="`$rowModuleData.url`_searchfield[`$strColumn`]" options=$rowSearchField.options multiple=0}
                		{else}
                			{insert name="inputitem" class="search_input filterInput" label=$rowSearchField.text placeholder="" type="text" input_name="`$rowModuleData.url`_searchfield[`$strColumn`]" }
                		{/if}
                    </div>
            	{/foreach}
        	</div>
        {/if}
        <div class="row">
        	{insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
        	
            {if $rowModuleData.url == 'adminisztratorok'}
            	{foreach from=$tblListData key=numIdx item=rowData}
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="admin-user">
                            {if $rowModuleData.is_edit_allowed == 1}
                                <a href="{$CONF.admin_base_url}{$rowModuleData.url}/edit/{$rowData.id}" class="button button--edit"><i class="far fa-edit"></i>Szerkesztés</a>
                            {/if}
                            {if $rowModuleData.is_delete_allowed == 1}
                                <a href="{$CONF.admin_base_url}{$rowModuleData.url}/del/{$rowData.id}" class="remove js-remove"><i class="fas fa-trash"></i></a>
                            {/if}
                            {if $rowModuleData.is_edit_allowed == 1 || $rowModuleData.is_delete_allowed == 1}
                                <hr>
                            {/if}
                            {foreach from=$tblList.column key=strColumn item=strColumnText}
                            <div class="admin-user__name"><b>{$strColumnText}: </b>{$rowData.$strColumn}</div>
                            {/foreach}
                        </div>
                    </div>
                {/foreach}
       		{else}
       			{insert name="module_list_row" module_name="`$rowModuleData.file_name`"}
       		{/if}
        </div>
    </div>
</div>
{insert name="end"}