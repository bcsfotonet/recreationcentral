{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="Új adminisztrátor létrehozása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="Adminisztrátor szerkesztése"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}

            <div class="row">
                {insert name="permissions" permissions="$tblPermission"}
    
                <div class="col-12 col-lg-8">
                    <div class="form__title">Adminisztrátor adatai</div>
                    <hr>
                    {insert name="inputitem" label="Id" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field_id" value="`$rowEditedUserData.id`"}
                    {insert name="inputitem" label="Név" type="text" fill="1" placeholder="Add meg a neved!" input_name="`$rowModuleData.url`_field_name" value="`$rowEditedUserData.name`"}
                    {insert name="inputitem" label="Azonosító" type="text" fill="1" placeholder="Add meg az azonosítót!" input_name="`$rowModuleData.url`_field_login" value="`$rowEditedUserData.login`"}
                    {insert name="inputitem" label="E-mail cím" type="email" fill="1" placeholder="Add meg az e-mail címet!" input_name="`$rowModuleData.url`_field_email" value="`$rowEditedUserData.email`"}
                    
                    {* Minden user csak a saját jelszavát változtathatja meg *}
                    {if $rowEditedUserData.id == $tblLoggedUserData.id}
                        {insert name="inputitem" label="Jelszó" type="password" placeholder="Add meg a jelszót!" input_name="`$rowModuleData.url`_field_password"}
                    {/if}
                    <div class="form__item">
                        <a href="{$CONF.admin_base_url}password" class="button">Jelszó küldése</a>
                    </div>
                </div>
            </div>
            {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}

        </form>
    </div>
</div>
{insert name="end"}