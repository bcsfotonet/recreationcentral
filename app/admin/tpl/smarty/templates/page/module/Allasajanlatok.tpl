{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                                        
                    {insert name="inputitem" label="Jelentkezés határideje" class="js-datetime" type="text" placeholder="Add meg a jelentkezés határidejét!" input_name="`$rowModuleData.url`_field[end_date]" value="`$tblEditedRowData.end_date`"}
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
{* Nyelvesítés                        
<!--                         <li class="nav-item"> -->
<!--                             <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a> -->
<!--                         </li> -->
<!--                         <li class="nav-item"> -->
<!--                             <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a> -->
<!--                         </li> -->
*}
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	{insert name="inputitem" class="url_parent_input" label="Magyar cím" type="text" fill="1" placeholder="Add meg a tartalom címét!" input_name="`$rowModuleData.url`_field[title]" value="`$tblEditedRowData.title`"}
                            	<div id="url_to_hu" class="url_input_div" data-url_field="{$rowModuleData.url}_field[title]">
        							<div class="form__item">
        								<label>Magyar url</label><br />
                                		<span class="url_span pl-3">{$tblEditedRowData.url}</span>
                                	</div>
                                	{insert name="inputitem" class="url_input" label="Magyar url" type="hidden" input_name="`$rowModuleData.url`_field[url]" value="`$tblEditedRowData.url`"}
                                </div>
                                
                                <div class="form__item">
    								<label>Bevezetés</label>
                            		<textarea rows="5" name="{$rowModuleData.url}_field[lead]" >{$tblEditedRowData.lead}</textarea>
                            	</div>
                            	
                            	<div class="editor">
                                    <div id="hu_editor" data-input_name="{$rowModuleData.url}_field[description]">{$tblEditedRowData.description}</div>
                                </div>
                        </div>
{*  Nyelvesítés                       
<!--                         <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab"> -->
<!--                             <div class="form__title">Tartalom adatai</div> -->
<!--                             <hr> -->
*}
{*<!--                             @@include('includes/_input-item.html', { "label": "MenĂĽpont elnevezĂ©se", "type": "text", "placeholder": "Add meg a menĂĽpont nevĂ©t!" }) @@include('includes/_input-item.html', { "label": "KĂĽlsĹ‘ Url", "type": "text", "placeholder": "Add meg az urlt!" }) @@include('includes/_select-item.html', { "label": "Url megnyitĂˇsa", "option1": "Ugyanabban az ablakban", "option2": "Ăšj ablakban" }) @@include('includes/_select-item.html', { "label": "Mely tartalom jelenjen meg?", "option1": "Hello world", "option2": "" }) -->*}
{*  
<!--                         </div> -->
<!--                         <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab"> -->
<!--                             <div class="form__title">Tartalom adatai</div> -->
<!--                             <hr> -->
*}
{*<!--                             @@include('includes/_input-item.html', { "label": "MenĂĽpont elnevezĂ©se", "type": "text", "placeholder": "Add meg a menĂĽpont nevĂ©t!" }) @@include('includes/_input-item.html', { "label": "KĂĽlsĹ‘ Url", "type": "text", "placeholder": "Add meg az urlt!" }) @@include('includes/_select-item.html', { "label": "Url megnyitĂˇsa", "option1": "Ugyanabban az ablakban", "option2": "Ăšj ablakban" }) @@include('includes/_select-item.html', { "label": "Mely tartalom jelenjen meg?", "option1": "Hello world", "option2": "" }) -->*}
{*    
<!--                         </div> -->
*}
                    </div>
                </div>
    	    </div>
    	    
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}