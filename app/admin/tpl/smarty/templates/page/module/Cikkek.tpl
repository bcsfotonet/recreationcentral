{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                                        
                    {insert name="selectitem" label="Város" select_name="`$rowModuleData.url`_field[city_id]" options=$tblCity}
                    {insert name="selectitem" label="Hol jelenjen meg a cikk?" select_name="`$rowModuleData.url`_field[type_id][]" options=$tblType multiple="1"}
                    
                    {insert name="checkboxitem" label="Kiemelt hír" input_name="`$rowModuleData.url`_field[is_highlighted]" value="`$tblEditedRowData.is_highlighted`"}
                    
                    {insert name="imageupload" label="Cikk kiemelt képe (600x315px, Maximum 8 MB)" btn_title="Kép feltöltése" input_name="`$rowModuleData.url`_field[image]" is_img="1" file_url="`$tblEditedRowData.image`"}
                    
                    {insert name="inputitem" label="Dátum kezdete" class="js-datetime" type="text" placeholder="Add meg a kezdés dátumát!" input_name="`$rowModuleData.url`_field[start_date]" value="`$tblEditedRowData.start_date`"}
                    {insert name="inputitem" label="Dátum vége" class="js-datetime" type="text" placeholder="Add meg a befejezés dátumát!" input_name="`$rowModuleData.url`_field[end_date]" value="`$tblEditedRowData.end_date`"}
                    
                    {insert name="selectitem" label="Galéria csatolása" select_name="`$rowModuleData.url`_field[gallery_id]" options=$tblGallery}
                    {insert name="selectitem" label="Videó csatolása" select_name="`$rowModuleData.url`_field[video_id]" options=$tblVideo}
                    {insert name="selectitem" label="Dokumentumok csatolása" select_name="`$rowModuleData.url`_field[document_id][]" options=$tblDocument multiple="1"}
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	{insert name="inputitem" class="url_parent_input" label="Magyar cím" type="text" fill="1" placeholder="Add meg a tartalom címét!" input_name="`$rowModuleData.url`_field[title]" value="`$tblEditedRowData.title`"}
                            	<div id="url_to_hu" class="url_input_div" data-url_field="{$rowModuleData.url}_field[id];{$rowModuleData.url}_field[title]">
        							<div class="form__item">
        								<label>Magyar url</label><br />
                                		<span class="url_span pl-3">{$tblEditedRowData.url}</span>
                                	</div>
                                	{insert name="inputitem" class="url_input" label="Magyar url" type="hidden" input_name="`$rowModuleData.url`_field[url]" value="`$tblEditedRowData.url`"}
                                </div>
                                
                                <div class="form__item">
                                    <label>Bevezetés</label>
                                    <textarea rows="5" name="{$rowModuleData.url}_field[lead]" >{$tblEditedRowData.lead}</textarea>
                            	</div>
                            	
                            	<div class="editor">
                                    <div id="hu_editor" data-input_name="{$rowModuleData.url}_field[description]">{$tblEditedRowData.description}</div>
                                </div>
                        </div>
                        {*  Nyelvesítés *}    
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Tartalom angol adatai</div>
                            <hr>
                            {insert name="inputitem" class="url_parent_input" label="Angol cím" type="text" fill="1" placeholder="Add meg az oldal címét!" input_name="`$rowModuleData.url`_field[title_en]" value="`$tblEditedRowData.title_en`"}
                            <div id="url_to_en" class="url_input_div" data-url_field="{$rowModuleData.url}_field[title_en]">
                                <div class="form__item">
                                    <label>Angol url</label><br />
                                    <span class="url_span pl-3">{$tblEditedRowData.url_en}</span>
                            	</div>
                            	{insert name="inputitem" class="url_input" label="Angol url" type="hidden" input_name="`$rowModuleData.url`_field[url_en]" value="`$tblEditedRowData.url_en`"}
                            </div>
                            <div class="form__item">
                                <label>Bevezetés angol</label>
                                <textarea rows="5" name="{$rowModuleData.url}_field[lead_en]" >{$tblEditedRowData.lead_en}</textarea>
                            </div>
                            <div class="editor">
                                <div id="en_editor" data-input_name="{$rowModuleData.url}_field[description_en]">{$tblEditedRowData.description_en}</div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Tartalom román adatai</div>
                            <hr>
                            {insert name="inputitem" class="url_parent_input" label="Román cím" type="text" fill="1" placeholder="Add meg az oldal címét!" input_name="`$rowModuleData.url`_field[title_ro]" value="`$tblEditedRowData.title_ro`"}
                            <div id="url_to_ro" class="url_input_div" data-url_field="{$rowModuleData.url}_field[title_ro]">
                                <div class="form__item">
                                    <label>Román url</label><br />
                                    <span class="url_span pl-3">{$tblEditedRowData.url_ro}</span>
                            	</div>
                            	{insert name="inputitem" class="url_input" label="Román url" type="hidden" input_name="`$rowModuleData.url`_field[url_ro]" value="`$tblEditedRowData.url_ro`"}
                            </div>
                            <div class="form__item">
                                <label>Bevezetés román</label>
                                <textarea rows="5" name="{$rowModuleData.url}_field[lead_ro]" >{$tblEditedRowData.lead_ro}</textarea>
                            </div>
                            <div class="editor">
                                <div id="ro_editor" data-input_name="{$rowModuleData.url}_field[description_ro]">{$tblEditedRowData.description_ro}</div>
                            </div>
                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}