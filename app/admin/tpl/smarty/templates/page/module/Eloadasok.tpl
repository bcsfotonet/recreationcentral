{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                                        
                    {insert name="selectitem" label="Város" select_name="`$rowModuleData.url`_field[city_id]" options=$tblCity}
                    {insert name="inputitem" label="Előadó" type="text" placeholder="Add meg az előadó nevét!" input_name="`$rowModuleData.url`_field[name]" value="`$tblEditedRowData.name`"}                    
                    {insert name="inputitem" label="Dátum" class="js-datetime" type="text" placeholder="Add meg a kezdés dátumát!" input_name="`$rowModuleData.url`_field[start_date]" value="`$tblEditedRowData.start_date`"}
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	{insert name="inputitem" label="Magyar cím" type="text" fill="1" placeholder="Add meg a tartalom címét!" input_name="`$rowModuleData.url`_field[title]" value="`$tblEditedRowData.title`"}
                        </div>
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	{insert name="inputitem" label="Angol cím" type="text" fill="0" placeholder="Add meg a tartalom címét!" input_name="`$rowModuleData.url`_field[title_en]" value="`$tblEditedRowData.title_en`"}
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	{insert name="inputitem" label="Román cím" type="text" fill="0" placeholder="Add meg a tartalom címét!" input_name="`$rowModuleData.url`_field[title_ro]" value="`$tblEditedRowData.title_ro`"}
                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}