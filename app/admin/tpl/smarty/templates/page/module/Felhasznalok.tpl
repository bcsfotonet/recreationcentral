{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
            <div class="row justify-content-center">
                {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                                  
            	{if $tblEditedRowData.is_person == 1}
                <div class="col-12 {*col-xl-6*}">
                    <div class="form__title">Magánszemély beállítások</div>
                    <hr>
                          
                    {*insert name="checkboxitem" label="Magánszemély" input_name="`$rowModuleData.url`_field[is_person]" value="`$tblEditedRowData.is_person`"*}
                    {insert name="inputitem" label="Vezetéknév" placeholder="Add meg a vezetéknevet!" input_name="`$rowModuleData.url`_field[last_name]"  value="`$tblEditedRowData.last_name`"}
                    {insert name="inputitem" label="Keresztnév" placeholder="Add meg a keresztnevet!" input_name="`$rowModuleData.url`_field[first_name]"  value="`$tblEditedRowData.first_name`"}
                </div>
                {elseif $tblEditedRowData.is_company == 1}
                <div class="col-12 {*col-xl-6*}">
                    <div class="form__title">Cég beállítások</div>
                    <hr>
                    {*insert name="checkboxitem" label="Cég" input_name="`$rowModuleData.url`_field[is_company]" value="`$tblEditedRowData.is_company`"*}
                    {insert name="inputitem" label="Cégnév" placeholder="Add meg a cégnevet!" input_name="`$rowModuleData.url`_field[company_name]"  value="`$tblEditedRowData.company_name`"}
                	{insert name="inputitem" label="Adószám" placeholder="Add meg az adószámot!" input_name="`$rowModuleData.url`_field[tax_number]"  value="`$tblEditedRowData.tax_number`"}
                </div>
                {/if}
    	    </div>
    	    <div class="row">
                <div class="col-12">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                </div>
            	<div class="col-12 col-xl-6">
            		{insert name="inputitem" label="E-mail cím" placeholder="Add meg az e-mail címet!" input_name="`$rowModuleData.url`_field[email]"  value="`$tblEditedRowData.email`"}
            	</div>
            	<div class="col-12 col-xl-6">
            		{insert name="inputitem" label="Telefonszám" placeholder="Add meg a telefonszámot!" input_name="`$rowModuleData.url`_field[tel]"  value="`$tblEditedRowData.tel`"}
            	</div>
            </div>
            <div class="row">
            	<div class="col-12">
            		{insert name="inputitem" label="Ország" placeholder="Add meg az országot!" input_name="`$rowModuleData.url`_field[country]"  value="`$tblEditedRowData.country`"}
            	</div>
            	<div class="col-12 col-xl-6">
            		{insert name="inputitem" label="Irányítószám" placeholder="Add meg az irányítószámot!" input_name="`$rowModuleData.url`_field[zip]"  value="`$tblEditedRowData.zip`"}
            	</div>
            	<div class="col-12 col-xl-6">
            		{insert name="inputitem" label="Város" placeholder="Add meg a várost!" input_name="`$rowModuleData.url`_field[city]"  value="`$tblEditedRowData.city`"}
            	</div>
        	</div>
            <div class="row">
            	<div class="col-12 col-xl-6">
            		{insert name="inputitem" label="Utca" placeholder="Add meg az utcát!" input_name="`$rowModuleData.url`_field[street]"  value="`$tblEditedRowData.street`"}
            	</div>
            	<div class="col-12 col-xl-6">
            		{insert name="inputitem" label="Házszám" placeholder="Add meg a házszámot!" input_name="`$rowModuleData.url`_field[house_number]"  value="`$tblEditedRowData.house_number`"}
            	</div>
            </div>
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}
            
