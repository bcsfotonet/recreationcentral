{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
{*    
            <div class="row justify-content-center">
                <div class="col-12 col-lx-8">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                                        
                    {insert name="selectitem" label="Mely menüpont alá tartozik?" select_name="`$rowModuleData.url`_field[parent_menu_id]" options=$tblParentMenu}
                    {insert name="selectitem" label="Mely tartalom jelenjen meg?" select_name="`$rowModuleData.url`_field[content_url]" options=$tblContentUrl}
                    {insert name="selectitem" label="Külső hivatkozás megnyitása" select_name="`$rowModuleData.url`_field[target_value]" options=$tblTargetValue}
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	{insert name="inputitem" label="Menüpont elnevezése" type="text" fill="1" placeholder="Add meg a menüpont nevét!" input_name="`$rowModuleData.url`_field[name]" value="`$tblEditedRowData.name`"}
                            	{insert name="inputitem" label="Külső tartalomra hivatkozás" type="text" fill="0" placeholder="Add meg a URL-t!" input_name="`$rowModuleData.url`_field[ext_url]" value="`$tblEditedRowData.ext_url`"}
                        </div>
                    </div>
                </div>
    	    </div>
 *}
 			<div class="row justify-content-center">
                <div class="col-12 col-lx-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom magyar adatai</div>
                            <hr>
							{insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
							{insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                            
                          	{insert name="inputitem" label="Kérdés" type="text" fill="1" placeholder="Add meg a kérdést!" input_name="`$rowModuleData.url`_field[question]" value="`$tblEditedRowData.question`"}
                            <div class="editor">
                                <div id="hu_editor" data-input_name="{$rowModuleData.url}_field[answer]">{$tblEditedRowData.answer}</div>
                            </div>
<!--                             {$rowModuleData.url}_field[answer] -->
<!--                             <div class="editor"> -->
<!--                                 <textarea class="w-100" name="{$rowModuleData.url}_field[answer]" id="editor">{$tblEditedRowData.answer}</textarea> -->
<!--                             </div> -->
                        </div>

                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Tartalom angol adatai</div>
                            <hr>
                              	{insert name="inputitem" label="Kérdés" type="text" fill="0" placeholder="Add meg a kérdést!" input_name="`$rowModuleData.url`_field[question_en]" value="`$tblEditedRowData.question_en`"}
    							<div class="editor">
                                    <div id="en_editor" data-input_name="{$rowModuleData.url}_field[answer_en]">{$tblEditedRowData.answer_en}</div>
                                </div>
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Tartalom román adatai</div>
                            <hr>
                              	{insert name="inputitem" label="Kérdés" type="text" fill="0" placeholder="Add meg a kérdést!" input_name="`$rowModuleData.url`_field[question_ro]" value="`$tblEditedRowData.question_ro`"}
    							<div class="editor">
                                    <div id="ro_editor" data-input_name="{$rowModuleData.url}_field[answer_ro]">{$tblEditedRowData.answer_ro}</div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>   	    
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}
            
