{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}

			<div class="row mtb-30">
				<div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                    
                    {insert name="selectitem" label="Melyik galériatárban jelenjen meg?" select_name="`$rowModuleData.url`_field[type_id][]" options=$tblType multiple="1"}
					{insert name="selectitem" label="Város" select_name="`$rowModuleData.url`_field[city_id]" options=$tblCity}
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu"
                                aria-selected="true">Magyar</a>
                        </li>
{*                        
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en"
                                aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro"
                                aria-selected="false">Román</a>
                        </li>
*}                        
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                        	{insert name="inputitem" class="url_parent_input" label="Magyar cím" type="text" fill="1" placeholder="Add meg a galéria címét!" input_name="`$rowModuleData.url`_field[title]" value="`$tblEditedRowData.title`"}
                        	<div id="url_to_hu" class="url_input_div" data-url_field="{$rowModuleData.url}_field[title]">
    							<div class="form__item">
    								<label>Magyar url</label><br />
                            		<span class="url_span pl-3">{$tblEditedRowData.url}</span>
                            	</div>
                            	{insert name="inputitem" class="url_input" label="Magyar url" type="hidden" input_name="`$rowModuleData.url`_field[url]" value="`$tblEditedRowData.url`"}
                            </div>
                        	
                        	
                        	<div class="form__item">
								<label>Rövid leírás</label>
                        		<textarea rows="5" name="{$rowModuleData.url}_field[description]" >{$tblEditedRowData.description}</textarea>
							</div>
                        </div>
{*
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                        	{insert name="inputitem" label="Angol cím" type="text" fill="1" placeholder="Add meg a galéria címét!" input_name="`$rowModuleData.url`_field[title]" value="`$tblEditedRowData.title`"}
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                        	{insert name="inputitem" label="Román cím" type="text" fill="1" placeholder="Add meg a galéria címét!" input_name="`$rowModuleData.url`_field[title]" value="`$tblEditedRowData.title`"}
                        </div>
*}  
                    </div>
                </div>
            </div>

            {insert name="imageupload" is_img="1" label="" btn_title="Képek feltöltése" input_name="`$rowModuleData.url`_field[gallery_image][]" file_url="" is_multiple="1"}

            <div class="row" id="sortable">
            	{if !empty($tblEditedRowData.images)}
                 	{foreach from=$tblEditedRowData.images item="rowImage"}
                 		{if !empty($rowImage.image)}
                            <div class="col-12 col-sm-6 col-xl-3 grid-view__row grid_row_{$rowImage.id}">
                                <div class="tiled-view {if $rowImage.is_active == 0}inactive{/if}">
            	                    {*insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[images][`$rowImage.id`][id]" value="`$rowImage.id`"*}
                                    <div class="tiled-view__header">
                                    	{* SEO-val jön be
                                    	{if $rowModuleData.is_edit_allowed == 1}
                                            <a href="#" class="button button--edit">
                                                Szerkesztés
                                            </a>
                                        {/if}
                                        *}
                                        <div class="tiled-view__icon-wrapper">
                                            <div class="form__item js-row-menu-status">
                                                <div class="form__group">
                                                    <label>
                                                        <input class="eye_checkbox" type="checkbox" name="{$rowModuleData.url}_field[images][{$rowImage.id}][is_active]" value="" {if $rowImage.is_active == 1}checked{/if}>
                                                        <span class="eyeicon "><i class="far fa-eye {if $rowImage.is_active == 0}fa-eye-slash{/if}"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                            {if $rowModuleData.is_delete_allowed == 1}
                                                <div class="form__group">
                                                    <label class="pl-0 mb-0 mt-1">
                                                        <input class="del_checkbox" type="checkbox" name="{$rowModuleData.url}_field[images][{$rowImage.id}][delete_date]" value="" >
                                                        <span class="delicon remove"><i class="fas fa-trash"></i></span>
                                                    </label>
                                                </div>
                                            {/if}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="tiled-view__content">
                                        <div class="tiled-view__img">
                                            <img src="{$rowImage.image_url}" alt="{$rowImage.alt}">
                                        </div>
                                    </div>
                                    <div class="form__item">
                                        <label>&nbsp;</label>
                                        <div class="form__group">
                                            {assign var="numMaxPriority" value=$tblEditedRowData.images|@count}
                                            <select name="{$rowModuleData.url}_field[images][{$rowImage.id}][priority]" id="" class="js-example-basic-single">    
                    							{section name=numOrder start=1 loop=$numMaxPriority+1 step=1}
                    								<option 
                    									value="{$smarty.section.numOrder.index}" 
                    									{if $smarty.section.numOrder.index == $rowImage.priority}selected="selected"{/if}
                									>
                										{$smarty.section.numOrder.index}
                									</option>
                    							{/section}
                    						</select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
    				{/foreach}
    			{/if}
            </div>
            
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}
            
