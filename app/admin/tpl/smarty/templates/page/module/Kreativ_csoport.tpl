{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}

                    {insert name="inputitem" label="E-mail cím" type="text" placeholder="Add meg az e-mail címet!" input_name="`$rowModuleData.url`_field[email]" value="`$tblEditedRowData.email`"}
                    {insert name="selectitem" label="Város" select_name="`$rowModuleData.url`_field[city_id]" options=$tblCity}                    
                    {insert name="imageupload" label="Kép (475x475px)" btn_title="Kép feltöltése" input_name="`$rowModuleData.url`_field[image]" is_img="1" file_url="`$tblEditedRowData.image`"}
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Alapadatok</div>
                            <hr>
                            {insert name="inputitem" class="url_parent_input" label="Név" type="text" fill="1" placeholder="Add meg a nevet!" input_name="`$rowModuleData.url`_field[name]" value="`$tblEditedRowData.name`"}
                            <div id="url_to_hu" class="url_input_div" data-url_field="{$rowModuleData.url}_field[id];{$rowModuleData.url}_field[name]">
                                <div class="form__item">
                                    <label>Url</label><br />
                                    <span class="url_span pl-3">{$tblEditedRowData.url}</span>
                                </div>
                                {insert name="inputitem" class="url_input" label="Url" type="hidden" input_name="`$rowModuleData.url`_field[url]" value="`$tblEditedRowData.url`"}
                            </div>
                            {insert name="inputitem" label="Titulus" type="text" fill="1" placeholder="Add meg a titulust!" input_name="`$rowModuleData.url`_field[title]" value="`$tblEditedRowData.title`"}
                            <div class="editor">
                                <div id="hu_editor" data-input_name="{$rowModuleData.url}_field[description]">{$tblEditedRowData.description}</div>
                            </div>
                        </div>
                        {*  Nyelvesítés *}              
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Munkatárs angol adatai</div>
                            <hr>
                            {insert name="inputitem" class="url_parent_input" label="Név angol" type="text" fill="1" placeholder="Add meg a munkatárs nevét!" input_name="`$rowModuleData.url`_field[name_en]" value="`$tblEditedRowData.name_en`"}
                            <div id="url_to_en" class="url_input_div" data-url_field="{$rowModuleData.url}_field[id];{$rowModuleData.url}_field[name_en]">
                                <div class="form__item">
                                    <label>Url angol</label><br />
                                    <span class="url_span pl-3">{$tblEditedRowData.url_en}</span>
                                </div>
                                {insert name="inputitem" class="url_input" label="Url angol" type="hidden" input_name="`$rowModuleData.url`_field[url_en]" value="`$tblEditedRowData.url_en`"}
                            </div>                           
                            {insert name="inputitem" label="Titulus angol" type="text" fill="1" placeholder="Add meg a titulust!" input_name="`$rowModuleData.url`_field[title_en]" value="`$tblEditedRowData.title_en`"}                          	
                            <div class="editor">
                                <div id="en_editor" data-input_name="{$rowModuleData.url}_field[description_en]">{$tblEditedRowData.description_en}</div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Munkatárs román adatai</div>
                            <hr>
                            {insert name="inputitem" class="url_parent_input" label="Név román" type="text" fill="1" placeholder="Add meg a munkatárs nevét!" input_name="`$rowModuleData.url`_field[name_ro]" value="`$tblEditedRowData.name_ro`"}
                            <div id="url_to_ro" class="url_input_div" data-url_field="{$rowModuleData.url}_field[id];{$rowModuleData.url}_field[name_ro]">
                                <div class="form__item">
                                    <label>Url román</label><br />
                                    <span class="url_span pl-3">{$tblEditedRowData.url_ro}</span>
                                </div>
                                {insert name="inputitem" class="url_input" label="Url román" type="hidden" input_name="`$rowModuleData.url`_field[url_ro]" value="`$tblEditedRowData.url_ro`"}
                            </div>
                            {insert name="inputitem" label="Titulus román" type="text" fill="1" placeholder="Add meg a titulust!" input_name="`$rowModuleData.url`_field[title_ro]" value="`$tblEditedRowData.title_ro`"}                           	
                            <div class="editor">
                                <div id="ro_editor" data-input_name="{$rowModuleData.url}_field[description_ro]">{$tblEditedRowData.description_ro}</div>
                            </div>
                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}