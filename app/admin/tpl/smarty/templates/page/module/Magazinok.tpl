{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}

                    {insert name="checkboxitem" label="Rendelhető" input_name="`$rowModuleData.url`_field[orderable]" value="`$tblEditedRowData.orderable`"}
                    
                    {insert name="selectitem" label="Évfolyam és szám" select_name="`$rowModuleData.url`_field[year_and_issue_id]" options=$tblYearAndIssue}
                    
                    {insert name="selectitem" label="Szerkesztői bizottsági tag" select_name="`$rowModuleData.url`_field[staff_id][]" options=$tblEditor multiple="1"}
                    
                    {insert name="selectitem" label="Tanulmány csatolás" select_name="`$rowModuleData.url`_field[study_id][]" options=$tblStudy multiple="1"}                
                    
                    {insert name="inputitem" label="Alapítva" type="text" placeholder="Add meg a alapítás évét!" input_name="`$rowModuleData.url`_field[foundation_year]" value="`$tblEditedRowData.foundation_year`"}
                    {insert name="inputitem" label="Papír ISSN" type="text" placeholder="Add meg a papír ISSN-t!" input_name="`$rowModuleData.url`_field[paper_issn]" value="`$tblEditedRowData.paper_issn`"}
                    {insert name="inputitem" label="Online ISSN" type="text" placeholder="Add meg az online ISSN-t!" input_name="`$rowModuleData.url`_field[online_issn]" value="`$tblEditedRowData.online_issn`"}
                    {insert name="inputitem" label="Megjelenés éve" type="text" placeholder="Add meg a megjelenés évét!" input_name="`$rowModuleData.url`_field[publication_year]" value="`$tblEditedRowData.publication_year`"}
                    {insert name="inputitem" label="Tervezett kötetek" type="text" placeholder="Add meg a tervezett kötetek számát!" input_name="`$rowModuleData.url`_field[volume]" value="`$tblEditedRowData.volume`"}          
                    
                    {insert name="imageupload" label="Magazin pdf feltöltése" btn_title="Fájl feltöltése" column="filename" input_name="`$rowModuleData.url`_field[filename]" is_img="0" extList=".pdf" file_url="`$tblEditedRowData.filename`"}
                        
                    {insert name="imageupload" label="Magazin képe (545x768px, Maximum 8 MB)" btn_title="Kép feltöltése" column="image" input_name="`$rowModuleData.url`_field[image]" is_img="1" file_url="`$tblEditedRowData.image`"}
                    
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Magazin adatai</div>
                            <hr>
                                {insert name="inputitem" label="Főszerkesztő" type="text" placeholder="Add meg a főszerkesztőt!" input_name="`$rowModuleData.url`_field[main_editor]" value="`$tblEditedRowData.main_editor`"}
                                {insert name="inputitem" label="Nyelv" type="text" placeholder="Add meg a nyelvet!" input_name="`$rowModuleData.url`_field[lang]" value="`$tblEditedRowData.lang`"}
                                {insert name="inputitem" label="Szerkesztőség címe" type="text" placeholder="Add meg a szerkesztőség címét!" input_name="`$rowModuleData.url`_field[editorial_address]" value="`$tblEditedRowData.editorial_address`"}
                                {*
                            	{insert name="inputitem" class="url_parent_input" label="Magyar cím" type="text" fill="1" placeholder="Add meg a tanulmány címét!" input_name="`$rowModuleData.url`_field[title]" value="`$tblEditedRowData.title`"}
                            	<div id="url_to_hu" class="url_input_div" data-url_field="{$rowModuleData.url}_field[title]">
                                    <div class="form__item">
                                        <label>Magyar url</label><br />
                                        <span class="url_span pl-3">{$tblEditedRowData.url}</span>
                                    </div>
                                    {insert name="inputitem" class="url_input" label="Magyar url" type="hidden" input_name="`$rowModuleData.url`_field[url]" value="`$tblEditedRowData.url`"}
                                </div>
                                {insert name="inputitem" label="Magazin" type="text" placeholder="Add meg a magazint!" input_name="`$rowModuleData.url`_field[magazine]" value="`$tblEditedRowData.magazine`"}
                                
                                {insert name="inputitem" label="Szerzők" type="text" placeholder="Add meg a szerzőket!" input_name="`$rowModuleData.url`_field[authors]" value="`$tblEditedRowData.authors`"}
                                
                                {insert name="inputitem" label="Kulcszavak" type="text" placeholder="Add meg a kulcszavakat!" input_name="`$rowModuleData.url`_field[keywords]" value="`$tblEditedRowData.keywords`"}
                                
                                <div class="form__item">
                                    <label>Bevezetés</label>
                                    <textarea rows="5" name="{$rowModuleData.url}_field[lead]" >{$tblEditedRowData.lead}</textarea>
                            	</div>
                                *}  
                                <div class="form__item">
                                    <label>Tartalomjegyzék</label>
                                    <div class="editor">    
                                        <div id="hu_editor" data-input_name="{$rowModuleData.url}_field[description]">{$tblEditedRowData.description}</div>
                                    </div>
                                </div>
                        </div>
                        {*  Nyelvesítés *}    
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Tartalom angol adatai</div>
                            <hr>
                                {insert name="inputitem" label="Főszerkesztő" type="text" placeholder="Add meg a főszerkesztőt!" input_name="`$rowModuleData.url`_field[main_editor_en]" value="`$tblEditedRowData.main_editor_en`"}
                                {insert name="inputitem" label="Nyelv" type="text" placeholder="Add meg a nyelvet!" input_name="`$rowModuleData.url`_field[lang_en]" value="`$tblEditedRowData.lang_en`"}
                                {insert name="inputitem" label="Szerkesztőség címe" type="text" placeholder="Add meg a szerkesztőség címét!" input_name="`$rowModuleData.url`_field[editorial_address_en]" value="`$tblEditedRowData.editorial_address_en`"}
                                
                                {*
                                {insert name="inputitem" class="url_parent_input" label="Angol cím" type="text" fill="1" placeholder="Add meg a tanulmány címét!" input_name="`$rowModuleData.url`_field[title_en]" value="`$tblEditedRowData.title_en`"}
                                <div id="url_to_en" class="url_input_div" data-url_field="{$rowModuleData.url}_field[title_en]">
                                    <div class="form__item">
                                        <label>Angol url</label><br />
                                        <span class="url_span pl-3">{$tblEditedRowData.url_en}</span>
                                    </div>
                                    {insert name="inputitem" class="url_input" label="Angol url" type="hidden" input_name="`$rowModuleData.url`_field[url_en]" value="`$tblEditedRowData.url_en`"}
                                </div>
                                {insert name="inputitem" label="Magazin" type="text" placeholder="Add meg a magazint!" input_name="`$rowModuleData.url`_field[magazine_en]" value="`$tblEditedRowData.magazine_en`"}

                                {insert name="inputitem" label="Szerzők" type="text" placeholder="Add meg a szerzőket!" input_name="`$rowModuleData.url`_field[authors_en]" value="`$tblEditedRowData.authors_en`"}

                                {insert name="inputitem" label="Kulcszavak" type="text" placeholder="Add meg a kulcszavakat!" input_name="`$rowModuleData.url`_field[keywords_en]" value="`$tblEditedRowData.keywords_en`"}

                                <div class="form__item">
                                    <label>Bevezetés angol</label>
                                    <textarea rows="5" name="{$rowModuleData.url}_field[lead_en]" >{$tblEditedRowData.lead_en}</textarea>
                                </div>
                                *}

                            <div class="editor">
                                <div id="en_editor" data-input_name="{$rowModuleData.url}_field[description_en]">{$tblEditedRowData.description_en}</div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Tartalom román adatai</div>
                            <hr>
                                {insert name="inputitem" label="Főszerkesztő" type="text" placeholder="Add meg a főszerkesztőt!" input_name="`$rowModuleData.url`_field[main_editor_ro]" value="`$tblEditedRowData.main_editor_ro`"}
                                {insert name="inputitem" label="Nyelv" type="text" placeholder="Add meg a nyelvet!" input_name="`$rowModuleData.url`_field[lang_ro]" value="`$tblEditedRowData.lang_ro`"}
                                {insert name="inputitem" label="Szerkesztőség címe" type="text" placeholder="Add meg a szerkesztőség címét!" input_name="`$rowModuleData.url`_field[editorial_address_ro]" value="`$tblEditedRowData.editorial_address_ro`"}
                                
                                {*
                                {insert name="inputitem" class="url_parent_input" label="Román cím" type="text" fill="1" placeholder="Add meg a tanulmány címét!" input_name="`$rowModuleData.url`_field[title_ro]" value="`$tblEditedRowData.title_ro`"}
                                <div id="url_to_ro" class="url_input_div" data-url_field="{$rowModuleData.url}_field[title_ro]">
                                    <div class="form__item">
                                        <label>Román url</label><br />
                                        <span class="url_span pl-3">{$tblEditedRowData.url_ro}</span>
                                    </div>
                                    {insert name="inputitem" class="url_input" label="Román url" type="hidden" input_name="`$rowModuleData.url`_field[url_ro]" value="`$tblEditedRowData.url_ro`"}
                                </div>
                                {insert name="inputitem" label="Magazin" type="text" placeholder="Add meg a magazint!" input_name="`$rowModuleData.url`_field[magazine_ro]" value="`$tblEditedRowData.magazine_ro`"}

                                {insert name="inputitem" label="Szerzők" type="text" placeholder="Add meg a szerzőket!" input_name="`$rowModuleData.url`_field[authors_ro]" value="`$tblEditedRowData.authors_ro`"}

                                {insert name="inputitem" label="Kulcszavak" type="text" placeholder="Add meg a kulcszavakat!" input_name="`$rowModuleData.url`_field[keywords_ro]" value="`$tblEditedRowData.keywords_ro`"}

                                <div class="form__item">
                                    <label>Bevezetés román</label>
                                    <textarea rows="5" name="{$rowModuleData.url}_field[lead_ro]" >{$tblEditedRowData.lead_ro}</textarea>
                                </div>
                                *}

                            <div class="editor">
                                <div id="ro_editor" data-input_name="{$rowModuleData.url}_field[description_ro]">{$tblEditedRowData.description_ro}</div>
                            </div>
                        </div>       
                    </div>
                </div>
    	    </div>
    	    
            {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}