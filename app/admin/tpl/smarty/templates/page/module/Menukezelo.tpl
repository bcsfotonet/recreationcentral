{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                                        
                    {insert name="selectitem" label="Mely menüpont alá tartozik?" select_name="`$rowModuleData.url`_field[parent_menu_id]" options=$tblParentMenu}
                    {insert name="selectitem" label="Mely tartalom jelenjen meg?" select_name="`$rowModuleData.url`_field[content_url]" options=$tblContentUrl}
                    {insert name="selectitem" label="Külső hivatkozás megnyitása" select_name="`$rowModuleData.url`_field[target_value]" options=$tblTargetValue}
                </div>
                <div id="tabs" class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#tabs_hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>                    
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#tabs_en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#tabs_ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="tabs_hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	{insert name="inputitem" label="Menüpont elnevezése" type="text" fill="1" placeholder="Add meg a menüpont nevét!" input_name="`$rowModuleData.url`_field[name]" value="`$tblEditedRowData.name`"}
                            	{insert name="inputitem" label="Külső tartalomra hivatkozás" type="text" fill="0" placeholder="Add meg a URL-t!" input_name="`$rowModuleData.url`_field[ext_url]" value="`$tblEditedRowData.ext_url`"}
                        </div>
                        {*  Nyelvesítés *}                       
                        <div class="tab-pane fade" id="tabs_en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Angol tartalom adatai</div>
                            <hr>
                            {insert name="inputitem" label="Menüpont elnevezése" type="text" fill="1" placeholder="Add meg a menüpont nevét!" input_name="`$rowModuleData.url`_field[name_en]" value="`$tblEditedRowData.name_en`"}
                            {insert name="inputitem" label="Külső tartalomra hivatkozás" type="text" fill="0" placeholder="Add meg a URL-t!" input_name="`$rowModuleData.url`_field[ext_url_en]" value="`$tblEditedRowData.ext_url_en`"}
                        </div>
                        <div class="tab-pane fade" id="tabs_ro" role="tabpanel" aria-labelledby="ro-tab">
                               <div class="form__title">Román tartalom adatai</div>
                            <hr>
                            {insert name="inputitem" label="Menüpont elnevezése" type="text" fill="1" placeholder="Add meg a menüpont nevét!" input_name="`$rowModuleData.url`_field[name_ro]" value="`$tblEditedRowData.name_ro`"}
                            {insert name="inputitem" label="Külső tartalomra hivatkozás" type="text" fill="0" placeholder="Add meg a URL-t!" input_name="`$rowModuleData.url`_field[ext_url_ro]" value="`$tblEditedRowData.ext_url_ro`"}
                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}