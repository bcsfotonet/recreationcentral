{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                    
                    
					{insert name="selectitem" label="Külső hivatkozás megnyitása" fill="`$tblValidate.target_value.is_fill`" select_name="`$rowModuleData.url`_field[target_value]" options=$tblTargetValue}                    
                    {insert name="imageupload" label="Partner képe (180x120px)" btn_title="Kép feltöltése" input_name="`$rowModuleData.url`_field[image]" is_img="1" file_url="`$tblEditedRowData.image`"}
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Partner adatai</div>
                            <hr>
                            {insert name="inputitem" label="Név" type="text" maxlength="`$tblValidate.name.maxlength`" fill="`$tblValidate.name.is_fill`" placeholder="Add meg a partner nevét!" input_name="`$rowModuleData.url`_field[name]" value="`$tblEditedRowData.name`"}
                            {insert name="inputitem" label="Külső tartalomra hivatkozás" maxlength="`$tblValidate.ext_url.maxlength`" fill="`$tblValidate.ext_url.is_fill`" type="text" placeholder="Add meg a URL-t!" input_name="`$rowModuleData.url`_field[ext_url]" value="`$tblEditedRowData.ext_url`"}
                        </div>
                        {*  Nyelvesítés *}              

                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Partner angol adatai</div>
                            <hr>
                                {insert name="inputitem" label="Név angol" type="text" maxlength="`$tblValidate.name.maxlength`" fill="`$tblValidate.name.is_fill`" placeholder="Add meg a partner nevét!" input_name="`$rowModuleData.url`_field[name_en]" value="`$tblEditedRowData.name_en`"}
                                {insert name="inputitem" label="Külső tartalomra hivatkozás" maxlength="`$tblValidate.ext_url.maxlength`" fill="`$tblValidate.ext_url.is_fill`" type="text" placeholder="Add meg a URL-t!" input_name="`$rowModuleData.url`_field[ext_url_en]" value="`$tblEditedRowData.ext_url_en`"}
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Partner román adatai</div>
                            <hr>
                            
                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        {insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"}
        </form>
    </div>
    
</div>
{insert name="end"}