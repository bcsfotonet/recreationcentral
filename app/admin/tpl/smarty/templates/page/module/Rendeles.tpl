{insert name="start"}
{insert name="nav"}

<div class="content">

    {if $strTplPage == 'new'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Új hozzáadása"}
    {elseif $strTplPage == 'edit'}
        {assign var="strTitle" value="`$rowModuleData.module_name` - Szerkesztés"}
    {else}
        {assign var="strTitle" value=""}
    {/if}
    {insert name="headline" title=$strTitle}

    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            {insert name="backbtn" url="`$CONF.admin_base_url``$rowModuleData.url`"}
            <div class="row justify-content-center">
                <div class="col-12 {*col-xl-6*}">
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="module_url" value="`$rowModuleData.url`"}
                    {insert name="inputitem" label="" type="hidden" placeholder="" input_name="`$rowModuleData.url`_field[id]" value="`$tblEditedRowData.id`"}
                        
                    {if $tblEditedRowData.is_person == 1}
                        <div class="form__title">Magánszemély</div>
                        <hr>
                                            
                        {*insert name="checkboxitem" label="Magánszemély" input_name="`$rowModuleData.url`_field[is_person]" value="`$tblEditedRowData.is_person`"*}
                        {insert disabled="disabled" name="inputitem" label="Vezetéknév" placeholder="Add meg a vezetéknevet!" input_name="`$rowModuleData.url`_field[last_name]"  value="`$tblEditedRowData.last_name`"}
                        {insert disabled="disabled" name="inputitem" label="Keresztnév" placeholder="Add meg a keresztnevet!" input_name="`$rowModuleData.url`_field[first_name]"  value="`$tblEditedRowData.first_name`"}
                    
                    {elseif $tblEditedRowData.is_company == 1}
                        <div class="form__title">Cég</div>
                        <hr>
                        {*insert name="checkboxitem" label="Cég" input_name="`$rowModuleData.url`_field[is_company]" value="`$tblEditedRowData.is_company`"*}
                        {insert disabled="disabled" name="inputitem" label="Cégnév" placeholder="Add meg a cégnevet!" input_name="`$rowModuleData.url`_field[company_name]"  value="`$tblEditedRowData.company_name`"}
                    	{insert disabled="disabled" name="inputitem" label="Adószám" placeholder="Add meg az adószámot!" input_name="`$rowModuleData.url`_field[tax_number]"  value="`$tblEditedRowData.tax_number`"}
                    {/if}
                </div>
    	    </div>
    	    <div class="row">
                <div class="col-12 col-xl-6">
            		{insert disabled="disabled" name="inputitem" label="E-mail cím" placeholder="Add meg az e-mail címet!" input_name="`$rowModuleData.url`_field[email]"  value="`$tblEditedRowData.email`"}
            	</div>
            	<div class="col-12 col-xl-6">
            		{insert disabled="disabled" name="inputitem" label="Telefonszám" placeholder="Add meg a telefonszámot!" input_name="`$rowModuleData.url`_field[tel]"  value="`$tblEditedRowData.tel`"}
            	</div>
    	    </div>
    	    <div class="row">
                <div class="col-12">
                    <div class="form__title">Számlázási adatok</div>
                    <hr>
                </div>
            </div>
            <div class="row">
            	<div class="col-12">
            		{insert disabled="disabled" name="inputitem" label="Ország" placeholder="Add meg az országot!" input_name="`$rowModuleData.url`_field[country]"  value="`$tblEditedRowData.country`"}
            	</div>
            	<div class="col-12 col-xl-6">
            		{insert disabled="disabled" name="inputitem" label="Irányítószám" placeholder="Add meg az irányítószámot!" input_name="`$rowModuleData.url`_field[zip]"  value="`$tblEditedRowData.zip`"}
            	</div>
            	<div class="col-12 col-xl-6">
            		{insert disabled="disabled" name="inputitem" label="Város" placeholder="Add meg a várost!" input_name="`$rowModuleData.url`_field[city]"  value="`$tblEditedRowData.city`"}
            	</div>
        	</div>
            <div class="row">
            	<div class="col-12 col-xl-6">
            		{insert disabled="disabled" name="inputitem" label="Utca" placeholder="Add meg az utcát!" input_name="`$rowModuleData.url`_field[street]"  value="`$tblEditedRowData.street`"}
            	</div>
            	<div class="col-12 col-xl-6">
            		{insert disabled="disabled" name="inputitem" label="Házszám" placeholder="Add meg a házszámot!" input_name="`$rowModuleData.url`_field[house_number]"  value="`$tblEditedRowData.house_number`"}
            	</div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form__title">Rendelés adatok</div>
                    <hr>
                    {if $tblEditedRowData.receive_mode == 1 && !empty($tblEditedRowData.post_city)}
                    	{insert disabled="disabled" name="inputitem" label="Személyes átvétel - város" input_name="`$rowModuleData.url`_field[post_city]"  value="`$tblEditedRowData.post_city`"}
                	{/if}
                	{if $tblEditedRowData.receive_mode == 0 && !empty($tblEditedRowData.post_name) && !empty($tblEditedRowData.post_address)}
                    	{insert disabled="disabled" name="inputitem" label="Postázási adatok - név" input_name="`$rowModuleData.url`_field[post_name]"  value="`$tblEditedRowData.post_name`"}
                    	{insert disabled="disabled" name="inputitem" label="Postázási adatok - cím" input_name="`$rowModuleData.url`_field[post_address]"  value="`$tblEditedRowData.post_address`"}
                	{/if}
                    {insert disabled="disabled" name="inputitem" label="Összesen fizetendő" input_name="`$rowModuleData.url`_field[total_price]"  value="`$tblEditedRowData.total_price`"}
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form__title">Magazin adatok</div>
                    <hr>
                    {foreach from=$tblMagazineData key=numIdx item=rowMagazineData}
                    	{insert disabled="disabled" name="inputitem" label="`$rowMagazineData.title`" input_name="`$rowModuleData.url`_field[num_magazine]"  value="`$rowMagazineData.num_magazine` db"}
                    {/foreach}
            </div>
	        {*insert name="submitcancel" cancel_url="`$CONF.admin_base_url``$rowModuleData.url`"*}
        </form>
    </div>
    
</div>
{insert name="end"}
            
