<?php
    require_once '../config/app.php';
    require_once "DB.php";
    require_once "Admin_user.php";
    
    session_name($CONF['project_name']."_ADMSID");
    session_start();
    
    $objDb = new DB();
    $objDb->connect();
    
    if (isset($_SESSION['admin_user'])) {
        $objUser =& $_SESSION['admin_user'];
    }   
    if (isset($_SESSION['module_data'])) {
        $rowCurrModuleData =& $_SESSION['module_data'];
    }
    $p = $_REQUEST['p'];
    
    if( $objUser->isLogged() === TRUE && is_numeric($objUser->getNumUserId()) ) {
        switch($p){
            case 'save_module_form':
                $strPage = $p; break;
            
            default:
                $strPage = '';
        }
        
        if($strPage != ''){
            header('content-type: text/plain');
            require_once "xhr".DIRECTORY_SEPARATOR.$strPage.".php";
        }
    } else {
        header('location: index.php');
    }