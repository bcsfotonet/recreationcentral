<?php
    $rowLabel = array();

    $rowLabel['idezet'] = '"Az élmény önmagát írja; az ötletet Te írod" – Márai Sándor';

    // breadcrumb
    $rowLabel['nyitooldal'] = 'Nyitóoldal';
    $rowLabel['media'] = 'Média';
    $rowLabel['tarsasag'] = 'Társaság';
    $rowLabel['recreation_tudomanyos_magazin'] = 'Recreation tudományos magazin';

    // fooldal
    $rowLabel['kiemelt_hirek'] = 'Kiemelt hírek';
    $rowLabel['partnereink'] = 'Partnereink';
    $rowLabel['reszletek'] = 'Részletek';
    $rowLabel['kereses'] = 'Keresés';
    $rowLabel['bejelentkezes'] = 'Bejelentkezés';
    $rowLabel['bejelentkezes_url'] = 'bejelentkezes';
    $rowLabel['regisztracio'] = 'Regisztráció';
    $rowLabel['regisztracio_url'] = 'regisztracio';
    $rowLabel['esemenynaptar'] = 'Eseménynaptár';
    $rowLabel['esemenynaptar_url'] = 'esemenynaptar';   
    $rowLabel['kozlesi_feltetelek'] = 'Közlési feltételek';
    $rowLabel['kozlesi_feltetelek_url'] = 'kozlesi-feltetelek';
    
    // 404 oldal
    $rowLabel['a_keresett_oldal_nem_talalhato'] = 'A keresett oldal nem található.';
    $rowLabel['vissza_a_fooldalra'] = 'Vissza a főoldalra';
    
    // kereses talalati lista oldal
    $rowLabel['keresett_szoveg'] = 'keresett-szoveg';
    $rowLabel['a_kereses_eredmenye'] = 'A keresés eredménye';
    $rowLabel['kereses_url'] = 'kereses';
    $rowLabel['nem_talalhato'] = 'A keresésnek megfelelő tartalom nem található.';
    $rowLabel['minimum_3_karakter'] = 'A kereséshez minimum 3 karakter szükséges.';
    
    // footer
    $rowLabel['elerhetosegi_adatok'] = 'Elérhetőségi adatok';
    $rowLabel['tovabbi_tartalom'] = 'További tartalom';
    $rowLabel['kozossegi_media'] = 'Közösségi média';
    $rowLabel['roviditett_nev'] = 'Rövidített név';
    $rowLabel['levelezesi_cim'] = 'Levelezési cím';
    $rowLabel['szekhely'] = 'Székhely';
       
    // attachments
    $rowLabel['galeria'] = 'Galéria';
    $rowLabel['video'] = 'Videó';
    $rowLabel['dokumentumok'] = 'Dokumentumok';
    $rowLabel['letoltes'] = 'Letöltés';
    
    // varos select
    $rowLabel['minden_varos'] = 'Minden város';
    $rowLabel['nem_talalhato'] = 'A szűrésnek megfelelő tartalom nem található.';
    
    // esemenynaptar
    $rowLabel['esemenyek_url'] = 'esemenyek';
    
    // tanulmanyok
    $rowLabel['bovebben'] = 'Bővebben';
    $rowLabel['cim'] = 'Cím';
    $rowLabel['szerzok'] = 'Szerzők';
    $rowLabel['kulcsszavak'] = 'Kulcsszavak';
    $rowLabel['absztrakt'] = 'Absztrakt';
    $rowLabel['tanulmany'] = 'Tanulmány';
    $rowLabel['osszes'] = 'Összes';
    
    // magazinok
    $rowLabel['rendelheto'] = 'Rendelhető';
    $rowLabel['adatlap'] = 'Adatlap';
    $rowLabel['kosarba'] = 'Kosárba';
    
?>