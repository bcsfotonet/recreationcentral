<?php
    $rowLabel = array();
    
    // breadcrumb
    $rowLabel['nyitooldal'] = 'Mainpage';
    $rowLabel['media'] = 'Mass-media';
    $rowLabel['tarsasag'] = 'Association';
    $rowLabel['recreation_tudomanyos_magazin'] = 'Recreation scientific magazine';
        
    // fooldal
    $rowLabel['kiemelt_hirek'] = 'Important news';
    $rowLabel['partnereink'] = 'Partners';
    $rowLabel['reszletek'] = 'More';
    $rowLabel['kereses'] = 'Search';
    $rowLabel['bejelentkezes'] = 'Login';
    $rowLabel['bejelentkezes_url'] = 'login';
    $rowLabel['regisztracio'] = 'Register';
    $rowLabel['regisztracio_url'] = 'register';
    $rowLabel['esemenynaptar'] = 'Calendar of Events';
    $rowLabel['kozlesi_feltetelek'] = 'Terms of publication';
    $rowLabel['esemenynaptar_url'] = '';   
    $rowLabel['kozlesi_feltetelek_url'] = '';
        
    // 404 oldal
    $rowLabel['a_keresett_oldal_nem_talalhato'] = 'The page you are looking for could not be found.';
    $rowLabel['vissza_a_fooldalra'] = 'Back to mainpage';
    
    // kereses talalati lista oldal
    $rowLabel['keresett_szoveg'] = 'searched-text';
    $rowLabel['a_kereses_eredmenye'] = 'Search results';
    $rowLabel['kereses_url'] = 'search';
    $rowLabel['nem_talalhato'] = 'No content matching your search.';
    $rowLabel['minimum_3_karakter'] = 'Search requires at least 3 characters.';
    
    // footer
    $rowLabel['elerhetosegi_adatok'] = 'Contact information';
    $rowLabel['tovabbi_tartalom'] = 'More content';
    $rowLabel['kozossegi_media'] = 'Social media';
    $rowLabel['roviditett_nev'] = 'Short name';
    $rowLabel['levelezesi_cim'] = 'Mailing address';
    $rowLabel['szekhely'] = 'Head office';
        
    // attachments
    $rowLabel['galeria'] = 'Gallery';
    $rowLabel['video'] = 'Video';
    $rowLabel['dokumentumok'] = 'Documents';
    $rowLabel['letoltes'] = 'Download';
        
    // varos select
    $rowLabel['minden_varos'] = 'All cities';
    $rowLabel['nem_talalhato'] = 'No matching content found.';
        
    // esemenynaptar
    $rowLabel['esemenyek_url'] = 'events';
            
    // tanulmanyok
    $rowLabel['bovebben'] = 'More';
    $rowLabel['cim'] = 'Title';
    $rowLabel['szerzok'] = 'Authors';
    $rowLabel['kulcsszavak'] = 'Keywords';
    $rowLabel['absztrakt'] = 'Abstract';
    $rowLabel['tanulmany'] = 'Report';
    $rowLabel['osszes'] = 'All';
           
    // magazinok
    $rowLabel['rendelheto'] = 'Must order';
    $rowLabel['adatlap'] = 'Datasheet';
    $rowLabel['kosarba'] = 'Basket';
?>