<?php 
    require_once $CONF['config_dir']."db.php";
    
    class DB{
        private $_strServerName;
        private $_strDbName;
        private $_strUserName;
        private $_strPassword;
        private $_isError;
        
        private $_objSqlConn;
        
        function __construct( $strDbName = "MYSQL" ){
            global ${$strDbName};
            
            $rowDbData = ${$strDbName};
            
            $this->_strServerName = $rowDbData['serverName'];
            $this->_strDbName = $rowDbData['dbName'];
            $this->_strUserName = $rowDbData['userName'];
            $this->_strPassword = $rowDbData['password'];
            
            $this->_isError = false;
        }
        
        
        function connect(){
            $this->_objSqlConn = new mysqli($this->_strServerName, $this->_strUserName, $this->_strPassword, $this->_strDbName);
            
            if( $this->_objSqlConn->connect_error ){
                die("Connection failed: " . $this->_objSqlConn->connect_error);
            }
            mysqli_set_charset($this->_objSqlConn, "utf8");
            return true;
        } 
        
        function getLastInsertId(){         
            return $this->_objSqlConn->insert_id;
        }
        
        /**
         * Ha egy rekordhoz tartozó több mező értékét kérjük le, akkor egydimenziós asszociatív tömbben adja vissza az eredményt 
         * @param type $strQuery
         * @return type
         */
        function getRow( $strQuery ){
            $objResult = $this->_objSqlConn->query($strQuery);
            $rowData = array();
            if( $objResult === FALSE ){
                $this->_isError = true;
                //TODO log fájlba vele
                //var_dump( $this->_objSqlConn->error);
            }else{
                $this->_isError = false;
                
                if( $objResult->num_rows > 0 ){
                    while($row = $objResult->fetch_assoc()) {
                        $rowData = $row;
                    }
                } 
            }

            return $rowData;
        }
        
        /**
         * Ha több rekordhoz tartozó több mező értékeit kérjük le, akkor többdimenziós tömbben adja vissza az eredményt
         * @param type $strQuery
         * @return type
         */
        function getAll( $strQuery ){
            $objResult = $this->_objSqlConn->query($strQuery);
            $tblData = array();
            
            if( $objResult === FALSE ){
                $this->_isError = true;
                //TODO log fájlba vele
                //var_dump( $this->_objSqlConn->error);
            }else{
                $this->_isError = false;
                
                if( $objResult->num_rows > 0 ){
                    while($row = $objResult->fetch_assoc()) {
                        $tblData[] = $row;
                    }
                }
            }
            return $tblData;
        }
        
        /**
         * getAll eredményét adja vissza, de a tömböt a sornak megfelelő id-val indexeli, ha van ilyen (ha nincs, akkor return false)
         * @param type $strQuery
         * @return boolean
         */
        function getAllIdIdx ($strQuery)
        {
            $tblData = array();
            $tblDataTmp = $this->getAll($strQuery);
            
            foreach ($tblDataTmp as $rowData) {
                if (isset($rowData['id'])) {
                    $tblData[$rowData['id']] = $rowData; 
                } else {
                    return FALSE;
                }
            }
            return $tblData;
        }
        
        /**
         * Ha több rekordhoz tartozó, de csak egy mező értékeinek lekérésére van szükség, akkor egydimenziós tömbben adja vissza az eredményt
         * @param type $strQuery
         * @return type
         */
        function getCol( $strQuery ){
            $rowData = array();
            $tblData = $this->getAll($strQuery);
            //array_column
            foreach( $tblData as $rowAllData ){
                foreach( $rowAllData as $strData ){
                    $rowData[] = $strData;
                }                
            }
            return $rowData;
        }
        
        /**
         * Ha csak egyetlen értéket kérünk le és nem számít a mező (pl. count, id, létezik-e vizsgálat)
         * @param string $strQuery
         * @return string
         */
        function getOne ($strQuery) 
        {
            $strData = "";
            $rowData = $this->getCol($strQuery);
            if ($rowData !== FALSE && !empty($rowData)) {
                $strData = $rowData[0];
            }
            return $strData;
        }
        
        function query( $strQuery ){
//             print $strQuery;
            $objResult = $this->_objSqlConn->query($strQuery);
            
            if( $objResult === FALSE ){
                $this->_isError = true;
                //TODO log fájlba vele
//                 var_dump( $this->_objSqlConn->error);
            }else{
                $this->_isError = false;
            }
            return $objResult;
        }
        
        function insert ($strQuery)
        {
            $objResult = $this->_objSqlConn->query($strQuery);
            
            if( $objResult === FALSE ){
                $this->_isError = true;
                //TODO log fájlba vele
                //var_dump( $this->_objSqlConn->error);
            }else{
                $this->_isError = false;
                $numLastId = $this->_objSqlConn->insert_id;
                return $numLastId;
            }
            return NULL;
        }
        
        function getIsError(){
            return $this->_isError;
        }
    }