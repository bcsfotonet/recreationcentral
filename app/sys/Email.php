<?php 
    require_once $CONF['config_dir']."mail.php";
    
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require_once "vendor/PHPMailer/Exception.php";
    require_once "vendor/PHPMailer/PHPMailer.php";
    require_once "vendor/PHPMailer/SMTP.php";

    
    class Email{
        private $_objMail;
        
        function __construct(){
            global $MAIL;
            $rowMailData = $MAIL;
            
            $this->_objMail = new PHPMailer();
//            $this->_objMail->SMTPDebug = 2;
            $this->_objMail->Mailer = "smtp";
            $this->_objMail->isSMTP();                
            $this->_objMail->Host = $rowMailData['host'];
            $this->_objMail->SMTPSecure = 'ssl'; 
            $this->_objMail->Port = $rowMailData['port'];
            $this->_objMail->SMTPAuth = true; 
            $this->_objMail->SMTPKeepAlive=true;
            $this->_objMail->Username = $rowMailData['user'];
            $this->_objMail->Password = $rowMailData['password']; 
            $this->_objMail->WordWrap = 50;           
            $this->_objMail->isHTML(true);
            $this->_objMail->setFrom($this->_objMail->Username, 'Recreation Central');           
            $this->_objMail->addReplyTo('noreply@recreationcentral.eu');

            //Attachments
//            $this->_objMail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
//            $this->_objMail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
        }
        
        /**
         * 
         * @param type $rowUserData (name, hash, e-mail)
         */
        public function changeAdminPassword ($rowUserData)
        {
            global $CONF;
//            print_r($this->_objMail);
            if ($CONF['isTest'] !== FALSE) {
                $this->_objMail->addAddress('foleri@gmail.com');
            } else {
                $this->_objMail->addAddress($rowUserData['email']);
            }
            
            $this->_objMail->Subject = ($CONF['isTest'] !== FALSE ? 'TESZT LEVÉL ' : '') . 'Recreation Central - Jelszómódosítás';
            $this->_objMail->Body    = "
                teszt    
            ";
//            $this->_objMail->AltBody = '';

            if ($this->_objMail->send() === true) {
                return true;
            }
            print $this->_objMail->ErrorInfo;
            return false;
        }
        
        public function sendMessage($senderMail='',$receiverMail='',$subject='',$message='',$ccMail='',$bccMail=''){
            
            //var_dump(function_exists(emailCheck));die;
            
            if(!empty($receiverMail)){
                $this->_objMail->addAddress($receiverMail);
            }
            
            $this->_objMail->addCC($ccMail);
            
            $this->_objMail->addBCC($bccMail);
            
            if(!empty($subject)){
                $this->_objMail->Subject = mb_convert_encoding($subject,'ISO-8859-2','UTF-8');
            }
            
            if(!empty($message)){
                $this->_objMail->Body = mb_convert_encoding($message,'ISO-8859-2','UTF-8');
            }
            
            if ($this->_objMail->send() === true) {
                return true;
            }else{
                print $this->_objMail->ErrorInfo;
                return false;
            }
        }
        
    }