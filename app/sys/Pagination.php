<?php
	
	class Pagination {
		private $_numTotal;
		private $_numPerPage;
		private $_numCurrPage;
		private $_rowQuery;
		private $_tblAllData;
		
		public function __construct ($numPerPage = 6, $numCurrPage = 1) {
			global $objDb;
			$this->_objDb = $objDb;
			$this->_numTotal = null;
			$this->_numPerPage = $numPerPage;
			$this->_numCurrPage = $numCurrPage;
			$this->_rowQuery = array();
			$this->_tblAllData = array();
		}

		/**
		 * összes találat száma
		 */
		public function setNumTotal( $numTotal ){
			$this->_numTotal = $numTotal;
			return TRUE;
		}
		public function getNumTotal(){
			return $this->_numTotal;
		}
		
		/**
		 * egy oldalon megjeleníthető találatok száma
		 */
		public function setNumPerPage( $numPerPage ){
			$this->_numPerPage = $numPerPage;
			return TRUE;
		}
		public function getNumPerPage(){
			return $this->_numPerPage;
		}
		
		/**
		 * aktuális oldal
		 */
		public function setNumCurrPage( $numCurrPage ){
			$this->_numCurrPage = $numCurrPage;
			return TRUE;
		}
		public function getNumCurrPage(){
			return $this->_numCurrPage;
		}
		
		/**
		 * Lekéri az adatokat az adatbázisból
		 * @param $rowQuery - indexek: fields (select), tables (from, join, left join), conditions (where), group (group by), order (order by), limit (limit), offset (offset)
		 * @return boolean
		 */
		public function getTblAllData( $rowQuery ){
			if( is_array($rowQuery) && isset($rowQuery['fields']) && isset($rowQuery['tables']) && !empty($rowQuery['fields']) && !empty($rowQuery['tables']) ){
				$this->_rowQuery['fields'] = $rowQuery['fields'];
				$this->_rowQuery['tables'] = $rowQuery['tables'];
				$this->_rowQuery['conditions'] = $rowQuery['conditions'];
				$this->_rowQuery['group'] = $rowQuery['group'];
				$this->_rowQuery['order'] = $rowQuery['order'];
				$this->_rowQuery['limit'] = $rowQuery['limit'];
				$this->_rowQuery['offset'] = $rowQuery['offset'];
			}
			
			if( is_object($this->_objDb) === TRUE && !empty($this->_rowQuery) ){
				$strQuery = "
					SELECT	{$this->_rowQuery['fields']}
					FROM	{$this->_rowQuery['tables']}
					".( !emtpy($this->_rowQuery['conditions']) ? "WHERE {$this->_rowQuery['conditions']}" : "")."
					".( !emtpy($this->_rowQuery['group']) ? "GROUP BY {$this->_rowQuery['group']}" : "")."
					".( !emtpy($this->_rowQuery['order']) ? "ORDER BY {$this->_rowQuery['order']}" : "")."
					".( !emtpy($this->_rowQuery['limit']) && is_numeric($this->_rowQuery['limit']) ? "LIMIT {$this->_rowQuery['limit']}" : "")."
					".( !emtpy($this->_rowQuery['offset']) && is_numeric($this->_rowQuery['offset']) ? "OFFSET {$this->_rowQuery['offset']}" : "")."
				";
				$tblData = $this->_objDb->getAll($strQuery);
				if( !DB::isError($tblData) && !empty($tblData) ){
					$this->_tblAllData = $tblData;
					return TRUE;
				}
			}
			return FALSE;
		}
		
		/**
		 * Összes elemet tartalmazó tömb közvetlen feltöltése 
		 * @param $tblAllData - összes elem
		 */
		public function setTblAllData( $tblAllData ){
			$this->_tblAllData = $tblAllData;
			return TRUE;
		}
		
		/**
		 * Lekéri az oldalak számát, amin elfér az összes találat
		 */
		public function getMaxPage(){
			if( is_array($this->_tblAllData) && !empty($this->_tblAllData) ){
				return ceil(count($this->_tblAllData) / $this->_numPerPage);
			}
			return FALSE;
		}
		
		/**
		 * Az egy oldalra kerülő adatokat tartalmazó tömböt kéri le
		 */
		public function getTblPageData(){
			if( !empty($this->_tblAllData) ){
				return array_slice($this->_tblAllData, (($this->_numCurrPage - 1) * $this->_numPerPage), $this->_numPerPage);
			}
			return FALSE;
		}
		
	}
?>