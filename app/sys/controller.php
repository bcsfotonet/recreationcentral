<?php 

    require_once "vendor".DIRECTORY_SEPARATOR."smarty-2.6.31".DIRECTORY_SEPARATOR."libs".DIRECTORY_SEPARATOR."Smarty.class.php";

    require_once "DB.php";

    require_once "function.php";

    //require_once($CONF['project_dir'] . "/lang/label".(!empty($strLanguage) ? "_{$strLanguage}" : "").".php");

    

    session_name($CONF['project_name']."_RCSID");

    session_start();

    

    $objDb = new DB();

    $objDb->connect();



    $strFileName = 'Home';

    $strPageUrl = 'home';

    

    $strTplPagePath = 'page'.DIRECTORY_SEPARATOR.$strFileName;

    

    $objSmarty = new Smarty; 

    $objSmarty->template_dir = $CONF['tpl_dir'];

    $objSmarty->compile_dir = $CONF['smarty_dir']."templates_c".DIRECTORY_SEPARATOR;

    $objSmarty->config_dir = $CONF['smarty_dir']."configs".DIRECTORY_SEPARATOR;

    $objSmarty->cache_dir = $CONF['smarty_dir']."cache".DIRECTORY_SEPARATOR;

    

    $objSmarty->caching = false;

    $objSmarty->compile_check = false;

    $objSmarty->force_compile = true;

    

    $objSmarty->plugins_dir[] = "vendor".DIRECTORY_SEPARATOR."smarty-2.6.30".DIRECTORY_SEPARATOR."libs".DIRECTORY_SEPARATOR;

    $objSmarty->plugins_dir[] = $CONF['smarty_dir']."functions".DIRECTORY_SEPARATOR;

    

    $objSmarty->assign("CONF", $CONF);

    

    //$objSmarty->setPage();

    //$objSmarty->setFilters();

    

    if( !empty($rowUrl[0]) ){

        $strPageUrl = trim($rowUrl[0]);

        $strPageUrl = mb_strtolower($strPageUrl,"UTF-8");

        

        //$strFileName = str_replace("-", "_", ucfirst($strPageUrl));

    

        $strFileName = $objDb->getOne("

            SELECT 

                filename

            FROM

                type

            WHERE

                url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$strPageUrl}'

                AND delete_date IS NULL

                AND is_active = 1

            LIMIT

                1

        ");

    }

    /*

    if( isset($_SESSION['admin_user']) ){

        $objUser =& $_SESSION['admin_user'];

        $objSmarty->assign("tblLoggedUserData", $objUser->getRowUserData());

        $objSmarty->assign("tblModule", $objUser->getTblModule());

    }    

    */

    

    $isStatic = $objDb->getOne("

        SELECT 

            1

        FROM

            static

        WHERE

            url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$strPageUrl}'

            AND delete_date IS NULL

            AND is_active = 1

        LIMIT

            1

    ");



    if(

        is_file($CONF['sys_dir']."page".DIRECTORY_SEPARATOR.$strFileName.".php") 

        && is_file($CONF['tpl_dir']."page".DIRECTORY_SEPARATOR.$strFileName.".tpl")

    ){ // létezik azonos nevű php és tpl is

        $strTplPagePath = "page".DIRECTORY_SEPARATOR.$strFileName.".tpl";

        require_once "page".DIRECTORY_SEPARATOR.$strFileName.".php";

        

    }elseif( is_file($CONF['sys_dir']."page".DIRECTORY_SEPARATOR.$strFileName.".php") ){

        // csak azonos nevű php létezik pl. logout

        require_once "page".DIRECTORY_SEPARATOR.$strFileName.".php";

        

    }elseif( 

        !is_file($CONF['sys_dir']."page".DIRECTORY_SEPARATOR.$strFileName.".php")

        && !is_file($CONF['tpl_dir']."page".DIRECTORY_SEPARATOR.$strFileName.".tpl")

        && $isStatic == 1

    ){ // nem létezik se php, se tpl, de static táblában van ilyen url -> Statikus

        $strFileName = "Statikus";

        

        $strTplPagePath = "page".DIRECTORY_SEPARATOR.$strFileName.".tpl";

        require_once "page".DIRECTORY_SEPARATOR.$strFileName.".php";



        

    }else{ // home oldal - ez lehet majd 404

        $strFileName = "Ismeretlen_oldal";



        $strTplPagePath = "page".DIRECTORY_SEPARATOR.$strFileName.".tpl";

        require_once "page".DIRECTORY_SEPARATOR.$strFileName.".php";

    }

    

    // if( !isset($objPage) || !is_object($objPage) ){

        $objPage = new $strFileName();

        $objPage->run($strTplPagePath);

    //}

?>