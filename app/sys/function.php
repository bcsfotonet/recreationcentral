<?php 

/**
 * Kis info
Kép méretek:
Cikk végoldalon: 600x315 (Opengraph miatt)
Cikk lista: 515x270 (nekem jó ha ide is a 600x315 kép kerül)

Partnerek: 180x120 (majd ekkora képeket fogunk feltölteni)
Videó: 600x315 legyen (SB-t majd finomítom ekkorára)
Munkatársak, szerzők: 475x475
Eseménynaptár: 300x157 (ez a 600x315-nek a fele) 
 */

function bgImgDisplayer($filename, $destination, $numBgWidth, $numBgHeight, $r, $g, $b){
    
    $type = strtolower(substr(strrchr($filename,"."),1));
    if($type == 'jpeg') $type = 'jpg';
    switch($type){
        case 'bmp': $objImage = imagecreatefromwbmp($filename); break;
        case 'gif': $objImage = imagecreatefromgif($filename); break;
        case 'jpg': $objImage = imagecreatefromjpeg($filename); break;
        case 'png': $objImage = imagecreatefrompng($filename); break;
        default : return "Unsupported picture type!";
    }
    
    $objNewImage  = imagecreatetruecolor($numBgWidth, $numBgHeight);
    $bgColor = imagecolorallocate($objNewImage, $r, $g, $b); //itt állítható a háttérszín jelenleg
    imagefill($objNewImage, 0, 0, $bgColor);
    
    $numOrigiWidth  = imagesx($objImage);
    $numOrigiHeight = imagesy($objImage);
    
    //képarány beállítás, ha kép szélessége, vagy magassága nagyobb a vártnál
    if(($numBgWidth/$numOrigiWidth) > ($numBgHeight/$numOrigiHeight)){
        $numNewHeight = round($numOrigiHeight * ($numBgHeight/$numOrigiHeight));
        $numNewWidth = round($numOrigiWidth * ($numBgHeight/$numOrigiHeight));
    }else{
        $numNewHeight = round($numOrigiHeight * ($numBgWidth/$numOrigiWidth));
        $numNewWidth = round($numOrigiWidth * ($numBgWidth/$numOrigiWidth));
    }
    
    $offset_x = round(abs(($numNewWidth-$numBgWidth)/2));
    $offset_y = round(abs(($numNewHeight-$numBgHeight)/2));
    
    imagecopyresampled( $objNewImage, $objImage, $offset_x, $offset_y, 0, 0, $numNewWidth, $numNewHeight, $numOrigiWidth, $numOrigiHeight );
    
    switch($type){
        case 'bmp': imagewbmp($objNewImage, $destination); break;
        case 'gif': imagegif($objNewImage, $destination); break;
        case 'jpg': imagejpeg($objNewImage, $destination); break;
        case 'png': imagepng($objNewImage, $destination); break;
    }
    
    imagedestroy($objNewImage);
    
}

    /**
     * Fájlok megfelelő mappába történő mentésére szolgál
     * @param string $strSavePath - Fájl mentési helye
     * @param string $strFileName - Fájl eredeti teljes neve (kiterjesztéssel együtt) $_FILES['name']
     * @param string $strTmpPath - Fájl ideiglenes helye $_FILES['tmp_name']
     * @param string $strNewFileName - Fájl új neve, amivel menteni szeretnénk, ha eltér az eredetitől (kiterjesztés nélkül)
     * @return boolean - sikeres / nem sikeres fájlmozgatás
     */
    function saveFile ($strSavePath, $strFileName, $strTmpPath, $strNewFileName = null)
    {
//                     var_dump($strSavePath);
        if (!file_exists($strSavePath) && !is_dir($strSavePath)) {
            rmkdir($strSavePath);
        }
        
        $rowPathInfo = pathinfo($strFileName);
        $strExt = $rowPathInfo['extension']; //fájl kiterjesztése
        $strSaveName = (!empty($strNewFileName) ? $strNewFileName.".".$strExt : $strFileName);
        
        $strSaveFilePath = $strSavePath.$strSaveName;
//         var_dump($strSaveFilePath);
        
        return (move_uploaded_file($strTmpPath, $strSaveFilePath) === TRUE ? $strSaveName : FALSE);
    }
    
    /**
     * A kapott útvonalon rekurzívan létrehozza a mappákat, ha nem léteznek
     * @param string $strPath
     */
    function rmkdir($strPath) 
    {
        $rowPath = explode(DIRECTORY_SEPARATOR, $strPath);
        
        $strRebuild = '';
        foreach($rowPath AS $strDir) {
            
            if(strstr($strDir, ":") !== false) {
                $strRebuild = $strDir;
                continue;
            }
            $strRebuild .= "/$strDir";
            if (!file_exists($strRebuild) && !is_dir($strRebuild)) {
                mkdir($strRebuild, 0777, true);
            }
        }
    }
    
    function createUrl ($strText, $numLimit = null) 
    {
        $strText = mb_strtolower($strText, 'UTF-8');
        
        $strText = str_replace("á", "a", $strText);
        $strText = str_replace("á", "a", $strText);
        $strText = str_replace("é", "e", $strText);
        $strText = str_replace("í", "i", $strText);
        $strText = str_replace("ó", "o", $strText);
        $strText = str_replace("ö", "o", $strText);
        $strText = str_replace("ő", "o", $strText);
        $strText = str_replace("ú", "u", $strText);
        $strText = str_replace("ü", "u", $strText);
        $strText = str_replace("ű", "u", $strText);
        $strText = str_replace("ß", "ss", $strText);
        $strText = str_replace("ä", "ae", $strText);
        $strText = str_replace("ë", "e", $strText);
        
        $strText = (is_numeric($numLimit) && strlen($strText) > $numLimit) ? substr($strText, 0, $numLimit) : $strText;
        
        $strText = preg_replace(array("/\s/"), "_", $strText);
        $strText = str_replace("_", "-", $strText);
        $strText = trim($strText, " \t\n\r-");
        
        return $strText;
    }

    /**
     * Email cím formátumm ellenőrzés
     * @param $email ellenőrizendő email cím
     * @return boolean ami megmondja h valid-e az email
    */
    function emailCheck($email) {
        $email = strip_tags(trim($email));
        if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            return true;
        }
        return false;
    }
    
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    function file_get_contents_utf8($fn) {
        $content = file_get_contents($fn); 
        return mb_convert_encoding($content, 'UTF-8', 
        mb_detect_encoding($content, 'UTF-8, ISO-8859-1', true));        
    } 
    
    function mb_substr_replace($original, $replacement, $position, $length){
        $startString = mb_substr($original, 0, $position, "UTF-8");
        $endString = mb_substr($original, $position + $length, mb_strlen($original), "UTF-8");

        $out = $startString . $replacement . $endString;

        return $out;
    }
    
    function replaceStringBetweenTags($tagOne,$tagTwo,$strOriginal,$strNew){
        $strReturn = '';
//      pl.:   
//      $strNew = "Valaki";
//      $strOriginal = "Hello, my name is [NAME]";
        if(!empty($tagOne) && !empty($tagTwo) && !empty($strOriginal) && !empty($strNew)){
            $startTagPos = strrpos($strOriginal, $tagOne);
            $endTagPos = strrpos($strOriginal, $tagTwo);
            $tagLength = $endTagPos - $startTagPos + 1;
            $strReturn = substr_replace($strOriginal, $strNew, $startTagPos, $tagLength); // substr_replace
        }
        return $strReturn;
    }
    
    function customerIsLoggedIn(){
        $isLogged = false;
        
        if(isset($_SESSION['customer_id']) && !empty($_SESSION['customer_id'])){
            $isLogged = true;
        }
        return $isLogged;
    }
    
    function getCustomerId(){
        $customer_id = 0;
        
        if(isset($_SESSION['customer_id']) && !empty($_SESSION['customer_id'])){
            $customer_id = $_SESSION['customer_id'];
        }
        return $customer_id;
    }
    
    function customerLogout(){
        global $CONF;
        
        unset($_SESSION['customer_id']);
        unset($_SESSION['customer_name']);
        
        header('location: ' . $CONF['base_url']);
        exit;
    }
    
function videoFilterView($city_id, $type_id){
    global $objDb, $CONF;
    $html = '';
    $strQuery = '';
    
    if(!empty($city_id) && !empty($type_id)){
        // eletviteli videok varos kivalasztva
        $strQuery = "
            SELECT 
                v.id
                ,v.title
                ,v.code
                ,v.description
                ,v.image
            FROM
                video as v
            JOIN
                video_type as vt ON vt.video_id = v.id
            WHERE
                v.delete_date IS NULL
                AND v.is_active = 1
                AND vt.type_id = '$type_id'
                AND v.city_id = '$city_id'
            ORDER BY
                v.title
        ";
    }elseif($city_id==0 && !empty($type_id)){
        // eletviteli videok, az osszes varos 
        $strQuery = "
            SELECT 
                v.id
                ,v.title
                ,v.code
                ,v.description
                ,v.image
            FROM
                video as v
            JOIN
                video_type as vt ON vt.video_id = v.id
            WHERE
                v.delete_date IS NULL
                AND v.is_active = 1
                AND vt.type_id = '$type_id'
            ORDER BY
                v.title
        ";
    }
    
    if(!empty($strQuery)){
        $tblData = $objDb->getAll($strQuery);
        if(!empty($tblData)){
            foreach ($tblData as $numIdx=>$rowData) {
                // eletviteli videok
                if (!empty($rowData['image']) && is_file($CONF['pub_dir']."video/".$rowData['id']."/big/".$rowData['image'])) {
                    $img_url = $CONF['base_url']."media/pub/video/".$rowData['id']."/big/".$rowData['image'];
                } else {
                    $img_url = 'http://img.youtube.com/vi/'.$rowData['code'].'/0.jpg';
                }

                $html .= '
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="video">'
                ;
                            if(!empty($rowData['code'])){
                                $html .= '
                                    <div class="video__frame">
                                        <iframe width="100%" height="200" src="https://www.youtube.com/embed/'.$rowData['code'].'" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
                                ';
                            }
                            if(!empty($rowData['title'])){
                                $html .= '
                                    <div class="video__title">'.$rowData['title'].'</div>
                                ';
                            }
                            if(!empty($rowData['description'])){
                                $html .= '
                                    <div class="video__description">'.$rowData['description'].'</div>
                                ';
                            }
                $html .= '
                        </div>
                    </div>'
                ;       
            }
        }
    }
    return $html;
}

function creativeTeamFilterView($city_id,$type_id){
    global $objDb, $CONF;
    $html = '';
    $strQuery = '';
    
    if(!empty($city_id) && !empty($type_id)){
        // kreativ csoport, varos kivalasztva
        $strQuery = "
            SELECT 
                 id
                ,name
                ,url
                ,title
                ,email
                ,image
                ,description 
                ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = staff.city_id) AS city_name
            FROM 
                staff
            WHERE 
                delete_date IS NULL 
                AND is_active = 1
                AND type_id = '$type_id'
                AND city_id = '$city_id'
            ORDER BY
                priority
        ";
    }elseif($city_id==0 && !empty($type_id)){
        // kreativ csoport, az osszes varos
        $strQuery = "
            SELECT 
                 id
                ,name
                ,url
                ,title
                ,email
                ,image
                ,description 
                ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = staff.city_id) AS city_name
            FROM 
                staff
            WHERE 
                delete_date IS NULL 
                AND is_active = 1
                AND type_id = '$type_id'
            ORDER BY
                priority
        ";
    }
    
    if(!empty($strQuery)){
        $tblData = $objDb->getAll($strQuery);
        if(!empty($tblData)){
            foreach ($tblData as $numIdx=>$rowData) {
                // kreativ csoport
                if (!empty($rowData['image']) && is_file($CONF['pub_dir']."szerzok/".$rowData['id']."/big/".$rowData['image'])) {
                    $img_url = $CONF['base_url']."media/pub/szerzok/".$rowData['id']."/big/".$rowData['image'];
                } else {
                    $img_url = $CONF['web_url']."images/user-placeholder.jpg";
                }
                $description_strip =  strip_tags($rowData['description']);

                $html .= '
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="staff">
                            <a class="staff__img" style="cursor:default;">
                                <img src="'.$img_url.'" alt="'.$rowData['name'].'">
                            </a>
                            <span class="staff__name">'.$rowData['name'].'</span>
                            <span class="staff__titulus">'.$rowData['title'].'</span>
                ';
                    if (!empty($rowData['city_name'])){
                        $html .= '
                            <span class="staff__city">'.$rowData['city_name'].'</span>
                        ';
                    }       
                $html .= '
                            <span class="staff__mail">
                                <a href="mailto:'.$rowData['email'].'">'.$rowData['email'].'</a>
                            </span>
                ';
                    if(!empty($rowData['url']) && !empty($description_strip)){
                        $html .= '
                            <a href="'.$CONF['base_url'].'kreativ_csoport/'.$rowData['url'].'" class="staff__more">
                                részletek
                            </a>
                        ';
                    }
                $html .= '
                        </div>
                    </div>
                ';
            }  
        }
    }
    return $html;
}

function galleryFilterView($city_id,$type_id){
    global $objDb, $CONF;
    $html = '';
    $strQuery = '';
    
    if(!empty($city_id) && !empty($type_id)){
        // galeria, varos kivalasztva
        $strQuery = "
            SELECT
                g.id
                ,g.title
                ,g.description
                ,g.url
                ,g.city_id
                ,gi.image as first_image
            FROM
                gallery as g
            JOIN
                gallery_type as gt ON gt.gallery_id = g.id
            JOIN
                gallery_image as gi ON (gi.gallery_id = g.id AND gi.priority = 1 AND gi.delete_date IS NULL)
            WHERE
                g.delete_date IS NULL
                AND g.is_active = 1
                AND gt.type_id = '$type_id'
                AND g.city_id = '$city_id'
            ORDER BY
                g.title
        ";
    }elseif($city_id==0 && !empty($type_id)){
        // galeria, az osszes varos
        $strQuery = "
            SELECT
                g.id
                ,g.title
                ,g.description
                ,g.url
                ,g.city_id
                ,gi.image as first_image
            FROM
                gallery as g
            JOIN
                gallery_type as gt ON gt.gallery_id = g.id
            JOIN
                gallery_image as gi ON (gi.gallery_id = g.id AND gi.priority = 1 AND gi.delete_date IS NULL)
            WHERE
                g.delete_date IS NULL
                AND g.is_active = 1
                AND gt.type_id = 15
            ORDER BY
                g.title
        ";
    }
    
    if(!empty($strQuery)){
        $tblData = $objDb->getAll($strQuery);
        if(!empty($tblData)){
            foreach ($tblData as $numIdx=>$rowData) {
                // galeria
                if (!empty($rowData['first_image']) && is_file($CONF['pub_dir']."kozponti-galeria/".$rowData['id']."/big/".$rowData['first_image'])) {
                    $img_url = $CONF['base_url']."media/pub/kozponti-galeria/".$rowData['id']."/big/".$rowData['first_image'];
                } else {
                    $img_url = $CONF['web_url']."images/news-placeholder.jpg";
                }

                $html .= '
                    <div class="col-12 col-sm-6 col-md-4">
                        <div class="gallery">
                            <a '.(!empty($rowData['url']) ? 'href="'.$CONF['base_url'].'galeria/'.$rowData['url'].'"' : '').' class="gallery__list-img">
                ';
                                if(!empty($img_url)){
                                    $html .= '<img src="'.$img_url.'" alt="'.$rowData['title'].'">';
                                }
                $html .= '
                            </a>
                ';
                            if(!empty($rowData['title'])){
                                $html .= '
                                    <a '.(!empty($rowData['url']) ? 'href="'.$CONF['base_url'].'galeria/'.$rowData['url'].'"' : '').' class="gallery__title">
                                        '.$rowData['title'].'
                                    </a>
                                ';
                            }
                            if(!empty($rowData['description'])){
                                $html .= '
                                    <div class="gallery__desc">
                                        '.$rowData['description'].'
                                    </div>
                                ';
                            }
                $html .= '
                        </div>
                    </div>
                ';
            }
        }
    }
    return $html;
}

function presentationFilterView($city_id,$type_id){
    global $objDb, $CONF;
    $html = '';
    $strQuery = '';
    
    if(!empty($city_id) && !empty($type_id)){
        // Eloadasok, varos kivalasztva
        $strQuery = "
            SELECT 
                p.`id`
                ,p.`title`
                ,p.`start_date`
                ,p.`name`
                ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = p.city_id) AS city_name
            FROM 
                `presentation` as p
            WHERE 
                p.`delete_date` IS NULL 
                AND p.`is_active` = 1
                AND p.`city_id` = '$city_id'
            ORDER BY
                p.`start_date` DESC
        ";
    }elseif($city_id==0 && !empty($type_id)){
        // Eloadasok, az osszes varos
        $strQuery = "
            SELECT 
                p.`id`
                ,p.`title`
                ,p.`start_date`
                ,p.`name`
                ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = p.city_id) AS city_name
            FROM 
                `presentation` as p
            WHERE 
                p.`delete_date` IS NULL 
                AND p.`is_active` = 1
            ORDER BY
                p.`start_date` DESC
        ";
    }
    
    if(!empty($strQuery)){
        $tblData = $objDb->getAll($strQuery);
        if(!empty($tblData)){
            foreach ($tblData as $numIdx=>$rowData) {
                $html .= '
                    <div class="col-12 col-md-6">
                        <div class="lecture">
                            <div class="lecture__title">'.$rowData['name'].(!empty($rowData['name']) && !empty($rowData['title']) ? ' - ' : '').$rowData['title'].'</div>
                            <hr>
                            <div class="lecture__date">
                ';
                                if(!empty($rowData['city_name'])){
                                    $html .= '<b>'.$rowData['city_name'].'</b>';
                                }
                                if(!empty($rowData['start_date'])){
                                    $html .= date('Y/m/d H:i',strtotime($rowData['start_date']));
                                }
                $html .= '
                            </div>              
                        </div>
                    </div>
                ';
            }
        }
    }
    return $html;
}


function posterFilterView($city_id,$type_id){
    global $objDb, $CONF;
    $html = '';
    $strQuery = '';
    
    if(!empty($city_id) && !empty($type_id)){
        // Plakatok, varos kivalasztva
        $strQuery = "
            SELECT 
                p.`id`
                ,p.`title`
                ,p.`image`
                ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = p.city_id) AS city_name
            FROM 
                `poster` as p
            WHERE 
                p.`delete_date` IS NULL 
                AND p.`is_active` = 1
                AND p.`city_id` = '$city_id'
            ORDER BY
                p.`priority`
        ";
    }elseif($city_id==0 && !empty($type_id)){
        // Plakatok, az osszes varos
        $strQuery = "
            SELECT 
                p.`id`
                ,p.`title`
                ,p.`image`
                ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = p.city_id) AS city_name
            FROM 
                `poster` as p
            WHERE 
                p.`delete_date` IS NULL 
                AND p.`is_active` = 1
            ORDER BY
                p.`priority` DESC
        ";
    }
    
    if(!empty($strQuery)){
        $tblData = $objDb->getAll($strQuery);
        if(!empty($tblData)){
            foreach ($tblData as $numIdx=>$rowData) {
                if (!empty($rowData['image']) && is_file($CONF['pub_dir']."plakatok/".$rowData['id']."/big/".$rowData['image'])) {
                    $img_url = $CONF['base_url']."media/pub/plakatok/".$rowData['id']."/big/".$rowData['image'];
                    $image_original_url = $CONF['base_url']."media/pub/plakatok/".$rowData['id']."/original/".$rowData['image'];
                    $originalImgDir = $CONF['pub_dir']."plakatok/".$rowData['id']."/original/".$rowData['image'];                          
                    $tblImageSize = getimagesize($originalImgDir);
                    $rowImgWidth = $tblImageSize[0];
                    $rowImgHeight = $tblImageSize[1];
                    $image_size = $rowImgWidth.'x'.$rowImgHeight;
                } else {
                    $img_url = $CONF['web_url']."images/news-placeholder.jpg";
                    $image_original_url = $CONF['web_url']."images/news-placeholder.jpg";
                    $image_size = '600x315';
                }
                $html .= '
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="poster">
                            <div class="poster__img gallery">
                                <figure itemprop="associatedMedia" itemscope itemtype="https://schema.org/ImageObject">
                                    <a href="'.$image_original_url.'" itemprop="contentUrl" id="gallery-'.$rowData['id'].'" data-size="'.$image_size.'">
                                        <img src="'.$img_url.'" alt="'.$rowData['title'].'" itemprop="thumbnail">
                                    </a>
                                </figure>
                            </div>
                            <div class="poster__title">'.$rowData['title'].'</div>
                        </div>
                    </div>
                ';
            }
        }
    }
    return $html;
}

function orderExistHandle($userId){
    
    global $objDb;    
    $numOrderId = 0;

    if(!empty($userId)){
        // van-e mar elkezdett rendelese, helyezett-e mar termeket a kosarba
        $isOrderExist = $objDb->getOne("
            SELECT
                id
            FROM
                `order`
            WHERE
                delete_date IS NULL
                AND order_date IS NULL
                AND customer_id = '".addslashes($userId)."'
            LIMIT 1
       "); 

        if (empty($isOrderExist)){

            $strQueryInsert = "
                INSERT INTO
                    `order`
                    (
                        customer_id
                        ,currency
                    )
                VALUES
                    (
                        ".$userId."
                        ,'HUF'
                    )
            ";

            $dbres = $objDb->insert($strQueryInsert); //visszaadja az insertelt id-t
             if (is_numeric($dbres)) {
                $numOrderId = $dbres;
             }

        }else{
            $strQueryUpdate ="
                UPDATE 
                    `order`
                SET
                    modify_date = NOW()
                WHERE
                    customer_id = ".$userId."
                    AND order_date IS NULL
                    AND delete_date is NULL
            ";	
            $dbres = $objDb->query($strQueryUpdate);
            if ($dbres !== FALSE){
                $numOrderId = $isOrderExist;
            }
        }
    }
    
    return $numOrderId;
}

function magazineOrder($order_id, $magazine_id, $num_magazine, $action, $receive_mode){
    // TODO insert/update a magazine_order tablaba
    global $objDb;   
    $isSuccessfull = FALSE;
    if($action=='update_total' && !empty($order_id)){
        $numTotal = 0;
        $tblOrderData = $objDb->getAll("
            SELECT
                o.id
                ,o.customer_id
                ,o.receive_mode
                ,o.total_price
                ,mo.num_magazine
            FROM
                `order` as o
            JOIN
                magazine_order as mo ON mo.order_id = o.id
            WHERE
                o.delete_date IS NULL
                AND o.order_date IS NULL
                AND o.id = $order_id
        ");
        
        if(!empty($tblOrderData)){
            foreach ($tblOrderData as $numIdx=>$rowData) {
                if (!empty($rowData['num_magazine'])){       
                    if($receive_mode==0){
                        $numTotal += ($rowData['num_magazine']*750)+($rowData['num_magazine']*300);
                    }else{
                        $numTotal += ($rowData['num_magazine']*750);
                    }
                }
            }
        }
        
        $strQueryUpdate ="
            UPDATE 
                `order`
            SET
                total_price = $numTotal
                ,receive_mode = '".addslashes($receive_mode)."'
            WHERE
                id = '".$order_id."'
        ";	

        $dbres = $objDb->query($strQueryUpdate);

        if ($dbres !== FALSE){
            $isSuccessfull = TRUE;
        }
    }elseif($action=='remove' && !empty($order_id) && !empty($magazine_id)){
        $objDb->query("
            DELETE
            FROM
                `magazine_order`
            WHERE
                magazine_id = '".addslashes($magazine_id)."'
                AND order_id = '".addslashes($order_id)."'
       ");
    }elseif(!empty($order_id) && !empty($magazine_id) && is_numeric($num_magazine) && $num_magazine>=0){
        // van-e mar elkezdett rendelese, helyezett-e mar termeket a kosarba
        $magazineOrderData = $objDb->getRow("
            SELECT
                mo.id
                ,mo.magazine_id
                ,mo.num_magazine
            FROM
                `magazine_order` as mo
            JOIN
                `order` as o ON (o.id = mo.order_id AND o.delete_date IS NULL AND o.order_date IS NULL)
            WHERE
                mo.magazine_id = '".addslashes($magazine_id)."'
                AND mo.order_id = '".addslashes($order_id)."' 
            LIMIT 1
       "); 

        if (empty($magazineOrderData)){

            $strQueryInsert = "
                INSERT INTO
                    `magazine_order`
                    (
                        magazine_id
                        ,order_id
                        ,num_magazine
                    )
                VALUES
                    (
                        '".addslashes($magazine_id)."'
                        ,'".addslashes($order_id)."'
                        ,'".addslashes($num_magazine)."'
                    )
            ";

            $dbres = $objDb->insert($strQueryInsert); // visszaadja az insertelt id-t
             if (is_numeric($dbres)) {
                $isSuccessfull = TRUE;
             }

        }else{
            if($action=='add'){
                $newNumMagazine = $magazineOrderData['num_magazine']+1;
            }else{
                $newNumMagazine = $num_magazine;
            }
            
            $strQueryUpdate ="
                UPDATE 
                    `magazine_order`
                SET
                    num_magazine = '".addslashes($newNumMagazine)."'
                WHERE
                    id = '".$magazineOrderData['id']."'
            ";	

            $dbres = $objDb->query($strQueryUpdate);

            if ($dbres !== FALSE){
                $isSuccessfull = TRUE;
            }
        }
    }
    
    return $isSuccessfull;
}