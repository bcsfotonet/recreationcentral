<?php 
    class Alapdokumentumok{
        private $_tblData;
        private $_rowType;
        
        function __construct(){
            $this->_tblData = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;

            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            $strDocQuery = "
                SELECT
                    d.id
                    ,d.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                    ,d.lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                    ,d.filename".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS filename
                FROM
                    document as d
                JOIN
                    document_type as dt ON dt.document_id = d.id
                WHERE
                    d.delete_date IS NULL
                    AND d.is_active = 1 
            ";

            $_tblDocument = $objDb->getAll($strDocQuery);

            if (!empty($_tblDocument)){
                foreach ($_tblDocument as $numIdx=>$rowDocument) {
                    if (!empty($rowDocument['filename']) && is_file($CONF['pub_dir']."dokumentumtar/".$rowDocument['id'].(!empty($strLanguage) ? "/{$strLanguage}" : "")."/".$rowDocument['filename'])) {
                        $_tblDocument[$numIdx]['filename'] = $CONF['base_url']."media/pub/dokumentumtar/".$rowDocument['id'].(!empty($strLanguage) ? "/{$strLanguage}" : "")."/".$rowDocument['filename'];
                    }
                }
            }

            $this->_tblData = $_tblDocument;
            $this->_rowType = $rowType;
            
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("rowType", $this->_rowType);
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>