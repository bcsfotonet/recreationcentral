<?php 
    class Allasajanlatok{      
        private $_tblData;
        private $_strPageType;
        private $_tblGallery;
        private $_tblVideo;
        private $_tblDocument;
        private $_rowType;
        
        function __construct(){    
            $this->_tblData = array();
            $this->_strPageType = "lista";
            $this->_tblGallery = array();
            $this->_tblVideo = array();
            $this->_tblDocument = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            $strQuery = "";
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // vég oldal
                $this->_strPageType = "veg";
                $strQuery = "
                    SELECT 
                        c.`id`
                        ,c.`title`".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,c.`url`".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        ,c.`end_date`
                        ,c.`lead`".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                        ,c.`description`".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                    FROM 
                        `carrier` as c
                    WHERE 
                        c.`delete_date` IS NULL 
                        AND c.`is_active` = 1
                        AND c.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$rowUrl[1]}'
                    ORDER BY
                        c.`end_date` DESC
                    LIMIT 1
                ";
                $tblData = $objDb->getRow($strQuery);
                
            } else {
                // lista oldal
                $this->_strPageType = "lista";
                $strQuery = "
                    SELECT 
                        c.`id`
                        ,c.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,c.`url`".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        ,c.`end_date`
                        ,c.`lead`".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                        ,c.`description`".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                    FROM 
                        `carrier` as c
                    WHERE 
                        c.`delete_date` IS NULL 
                        AND c.`is_active` = 1
                    ORDER BY
                        c.`end_date` DESC
                ";
                $tblData = $objDb->getAll($strQuery);
               
            }
            $this->_tblData = $tblData;
            $this->_rowType = $rowType;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("rowType", $this->_rowType);  
            }
            
            if ($this->_strPageType == "veg") {
                //vég oldal
                $objSmarty->display("page".DIRECTORY_SEPARATOR."Allasajanlatok_veg.tpl");
            } else {
                //lista oldal
                $objSmarty->display($strTplPagePath);
            }

        }
    }
    