<?php 
    class Bejelentkezes{
        private $_tblData;
        private $_tblFormData;
        private $_tblFormError;
        
        function __construct(){
            $this->_tblData = array();
            $this->_tblFormData = array();
            $this->_tblFormError = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
           
            $tblData = $objDb->getRow("
                SELECT 
                     t.id
                    ,t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                    ,s.title AS static_title 
                    ,s.description AS static_description   
                FROM 
                    type AS t
                LEFT JOIN
                    static AS s ON (s.id = t.static_id)
                WHERE 
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL 
                    AND t.is_active = 1
                    AND 
                    (
                        s.id IS NULL
                        OR    
                        (s.delete_date IS NULL 
                        AND s.is_active = 1)
                    )
           ");
                    
            $tblFormData = array(
                'email' => (isset($_POST['email']) && !empty($_POST['email']) ? stripslashes(trim($_POST['email'])) : '')
                , 'pass' => (isset($_POST['pass']) && !empty($_POST['pass']) ? stripslashes(trim($_POST['pass'])) : '')
            );        
                    
            if ($tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            $this->_tblFormData = $tblFormData; 
            
            return true;
        }
        
        private function validateForm(){ 
            $tblFormError = array(
                 'email' => (isset($_POST['email']) && empty($_POST['email']) ? 'A mező kitöltése kötelező!' : (emailCheck($_POST['email'])==false ? 'Hibás e-mail cím!' : ''))
                , 'pass' => (isset($_POST['pass']) && empty($_POST['pass']) ? 'A mező kitöltése kötelező!' : '')
            );

            if(empty(array_filter($tblFormError))){
                // nincs hiba
                return true;
            }else{                
                $this->_tblFormError = $tblFormError;
                return false;
            }
        }
        
        private function login($tblFormData){
            global $CONF, $objDb;
            
            $this->resetLogin();
            
            $email = !empty($tblFormData['email']) ? stripslashes(trim($tblFormData['email'])) : '';
            $pass = !empty($tblFormData['pass']) ? stripslashes(trim($tblFormData['pass'])) : '';
            
            $strQuery = "
                SELECT 
                    id
                    , last_name
                    , first_name
                    , company_name
                    , passwd
                FROM 
                    customer 
                WHERE 
                    email = '".$email."'
                    AND is_active = 1 
                    AND delete_date IS NULL
                ORDER BY 
                    id
                LIMIT 1
            ";
            
            $rowData = $objDb->getRow($strQuery);
//            var_dump($rowData['passwd']);
//            echo '<br>';
//            var_dump(sha1($pass));
//            die;
            if(isset($rowData) && !empty($rowData['passwd']) && $rowData['passwd'] == sha1($pass)){

                $_SESSION['customer_id'] = $rowData['id'];               
                if(!empty($rowData['last_name']) && !empty($rowData['first_name'])){
                    $_SESSION['customer_name'] = $rowData['last_name']  . ' ' . $rowData['first_name'];
                }elseif(!empty($rowData['company_name'])){
                    $_SESSION['customer_name'] = $rowData['company_name'];             
                }else{
                    $_SESSION['customer_name'] = '';
                }
                
                return true;
            } else {
                return false;
            }
	}
        
        private function resetLogin(){
            if(isset($_SESSION['customer_id'])){
                unset($_SESSION['customer_id']);                             
            }
            
            if(isset($_SESSION['customer_name'])){
                unset($_SESSION['customer_name']);
            }
        }
        
        public function run($strTplPagePath){
            global $objSmarty,$CONF,$rowUrl;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                // ha van post, akkor volt kuldes
                if(!empty($_POST)){
                    // form validalas
                    $isFormValid = $this->validateForm();
                    if($isFormValid == true){    
                        $isLoginSuccessfull = $this->login($this->_tblFormData);
                        if($isLoginSuccessfull==true){
                            header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'profil' : ''));
                            exit;
                        }else{
                            $objSmarty->assign("tblFormData", $this->_tblFormData);
                            $objSmarty->assign("tblFormError", array('loginError'=>'Hibás felhasználónév és jelszó páros!'));
                        }
                    }else{
                        $objSmarty->assign("tblFormData", $this->_tblFormData);
                        $objSmarty->assign("tblFormError", $this->_tblFormError);
                    }
                }else{
                    $objSmarty->assign("tblFormData", $this->_tblFormData);
                }
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>