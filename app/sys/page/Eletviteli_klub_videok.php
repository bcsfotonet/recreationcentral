<?php 
    class Eletviteli_klub_videok{
        private $_tblData;
        private $_tblCityData;
        private $_rowType;
        
        function __construct(){
            $this->_tblData = array();
            $this->_tblCityData = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            $strQuery = "
                SELECT 
                     v.id
                    ,v.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                    ,v.code
                    ,v.description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                    ,v.image
                FROM
                    video as v
                JOIN
                    video_type as vt ON vt.video_id = v.id
                WHERE
                    v.delete_date IS NULL
                    AND v.is_active = 1
                    AND vt.type_id = 14
                ORDER BY
                    v.title
            ";
            
            $tblData = $objDb->getAll($strQuery);
            
            foreach ($tblData as $numIdx=>$rowData) {
                if (!empty($rowData['image']) && is_file($CONF['pub_dir']."video/".$rowData['id']."/big/".$rowData['image'])) {
                    $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/video/".$rowData['id']."/big/".$rowData['image'];
                } else {
                    $tblData[$numIdx]['image'] = 'http://img.youtube.com/vi/'.$rowData['code'].'/0.jpg';
                }
            }
            
            $tblCityData = $objDb->getAll("
                SELECT 
                    c.id
                    ,c.name
                FROM
                    city as c
                WHERE
                    c.delete_date IS NULL
                    AND c.is_active = 1
                ORDER BY
                    c.name
            ");
//            var_dump($tblCityData);die;
            $this->_tblData = $tblData;
            $this->_tblCityData = $tblCityData;
            $this->_rowType = $rowType;
            
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblCityData", $this->_tblCityData);
                $objSmarty->assign("rowType", $this->_rowType);
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>