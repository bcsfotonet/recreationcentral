<?php 
    require_once "Email.php";

    class Elfelejtett_jelszo{
        private $_tblData;
        private $_tblFormData;
        private $_tblFormError;
        private $_objEmail;
        
        function __construct(){
            $this->_tblData = array();
            $this->_tblFormData = array();
            $this->_tblFormError = array();
            $this->_objEmail = new Email();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
           
            $tblData = $objDb->getRow("
                SELECT 
                     t.id
                    ,t.name
                    ,s.title AS static_title 
                    ,s.description AS static_description   
                FROM 
                    type AS t
                LEFT JOIN
                    static AS s ON (s.id = t.static_id)
                WHERE 
                    t.url = '{$rowUrl[0]}'
                    AND t.delete_date IS NULL 
                    AND t.is_active = 1
                    AND 
                    (
                        s.id IS NULL
                        OR    
                        (s.delete_date IS NULL 
                        AND s.is_active = 1)
                    )
           ");
                    
            if ($tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            $tblFormData = array(
                'email' => (isset($_POST['email']) && !empty($_POST['email']) ? stripslashes(trim($_POST['email'])) : '')
            );
            
            $this->_tblFormData = $tblFormData;
            
            return true;
        }
        
        private function validateForm($tblFormData){
            global $objDb;
            
            $tblFormError = array(
                 'email' => (isset($_POST['email']) && empty($_POST['email']) ? 'A mező kitöltése kötelező!' : (emailCheck($_POST['email'])==false ? 'Hibás e-mail cím!' : ''))
            );

            if(empty(array_filter($tblFormError))){
                // nincs hiba
                $isAlreadyCustomer = $objDb->getRow("
                    SELECT
                        id
                    FROM
                        customer
                    WHERE
                        delete_date IS NULL
                        AND is_active = 1
                        AND email = '".$tblFormData['email']."'
                ");
                   
                if(!empty($isAlreadyCustomer)){
                    
                    return true;
                }else{
                    $this->_tblFormError = array('email' => 'Ezzel az e-mail címmel nem létezik regisztrált felhasználó!');
                    return false;
                }
                return true;
            }else{                
                $this->_tblFormError = $tblFormError;
                return false;
            }
        }
        
        private function sendNewPassword($receiverMail=''){
            global $CONF, $objDb;
            $isSendSuccessFull = false;
            if(!empty($receiverMail)){
                $rowCustomer = $objDb->getRow("
                    SELECT
                        id
                        , last_name
                        , first_name
                    FROM
                        customer
                    WHERE
                        delete_date IS NULL
                        AND is_active = 1
                        AND email = '".$receiverMail."'
                ");
                
                $newPassword = generateRandomString(8);
                
                if($this->saveNewPassword($rowCustomer['id'],$receiverMail,$newPassword)){
                    $strOriginal = file_get_contents_utf8($CONF['project_dir'] . 'email_templates/elfelejtett_jelszo.html');
                    $strNewPass = replaceStringBetweenTags('$','~',$strOriginal,$newPassword);
                    $strNewPassAndName = replaceStringBetweenTags('§','+',$strNewPass,$rowCustomer['last_name'].' '.$rowCustomer['first_name']);

                    if($this->_objEmail->sendMessage('',$receiverMail,'Elfeljtett jelszó',$strNewPassAndName)){
                       $isSendSuccessFull = true;
                    }
                }
            }
            return $isSendSuccessFull;
        }
        
        private function saveNewPassword($numCustomerId, $receiverMail='', $newPassword=''){        
            global $objDb;
            $isSaveSuccessfull = false;
            if(!empty($newPassword) && !empty($receiverMail) && !empty($numCustomerId)){            
                $newPassword = sha1($newPassword);
                
                if(!empty($numCustomerId)){
                    $strQueryUpdate = "
                        UPDATE
                            customer
                        SET
                            passwd = '$newPassword'
                        WHERE
                            id = ".$numCustomerId."
                    ";
                    $dbres = $objDb->query($strQueryUpdate);
                    if($dbres !== FALSE){
                        $isSaveSuccessfull = true;
                    }                  
                }
            }
            return $isSaveSuccessfull;
        }
        
        public function run($strTplPagePath){
            global $objSmarty,$CONF,$rowUrl;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                
                // ha van post, akkor volt kuldes
                if(!empty($_POST)){
                    // form validalas
                    $isFormValid = $this->validateForm($this->_tblFormData);
                    if($isFormValid == true){   
                        $isNewPasswordSent = $this->sendNewPassword($this->_tblFormData['email']);
                        if($isNewPasswordSent){                         
                            header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'elfelejtett-jelszo/sikeres' : ''));
                            exit;
                        }else{
                            header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'elfelejtett-jelszo/sikertelen' : ''));
                            exit;
                        }
                    }else{
                        $objSmarty->assign("tblFormData", $this->_tblFormData);
                        $objSmarty->assign("tblFormError", $this->_tblFormError);
                    }
                }elseif(isset($rowUrl[1]) && $rowUrl[1]=='sikeres'){                
                    $strSuccessText = 'Sikeres küldés! A megadott e-mail címre kiküldött jelszóval tud bejelentkezni.';
                    $objSmarty->assign("strSuccessText", $strSuccessText);
                }elseif(isset($rowUrl[1]) && $rowUrl[1]=='sikertelen'){
                    $strSuccessText = 'A küldés közben hiba történt! <br> Kérjük próbálja meg később!';
                    $objSmarty->assign("strSuccessText", $strSuccessText);
                }else{
                    $objSmarty->assign("tblFormData", $this->_tblFormData);
                }
                
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>