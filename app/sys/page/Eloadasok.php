<?php 
    class Eloadasok{      
        private $_tblData;
        private $_strPageType;
        private $_tblCityData;
        private $_rowType;

        function __construct(){    
            $this->_tblData = array();
            $this->_strPageType = "lista";
            $this->_tblCityData = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strQuery = "";
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            $tblCityData = "";
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // ilyen meg nincs 
                // vég oldal
                /*
                $this->_strPageType = "veg";
                $strQuery = "
                    SELECT 
                        p.`id`
                        ,p.`title`
                        ,p.`url`
                        ,p.`start_date`
                        ,p.`name`
                        ,p.`city_id`
                    FROM 
                        `presentation` as p
                    WHERE 
                        p.`delete_date` IS NULL 
                        AND p.`is_active` = 1
                        AND p.url = '{$rowUrl[1]}'
                    ORDER BY
                        p.`start_date` DESC
                    LIMIT 1
                ";
                $tblData = $objDb->getRow($strQuery);
                */
            } else {
                // lista oldal
                $this->_strPageType = "lista";
                $strQuery = "
                    SELECT 
                        p.`id`
                        ,p.`title".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS title
                        ,p.`start_date`
                        ,p.`name".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS name
                        ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = p.city_id) AS city_name
                    FROM 
                        `presentation` as p
                    WHERE 
                        p.`delete_date` IS NULL 
                        AND p.`is_active` = 1
                    ORDER BY
                        p.`start_date` DESC
                ";
                
                $tblData = $objDb->getAll($strQuery);
                
                $tblCityData = $objDb->getAll("
                    SELECT 
                        c.id
                        ,c.name
                    FROM
                        city as c
                    WHERE
                        c.delete_date IS NULL
                        AND c.is_active = 1
                    ORDER BY
                        c.name
                ");
               
            }
            $this->_tblData = $tblData;
            $this->_tblCityData = $tblCityData;
            $this->_rowType = $rowType;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblCityData", $this->_tblCityData);
                $objSmarty->assign("rowType", $this->_rowType);
            }
            
//            if ($this->_strPageType == "veg") {
//                //vég oldal
//                $objSmarty->display("page".DIRECTORY_SEPARATOR."Eloadasok_veg.tpl");
//            } else {
                //lista oldal
                $objSmarty->display($strTplPagePath);
//            }

        }
    }
    