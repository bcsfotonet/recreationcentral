<?php 
    class Esemenynaptar{
//        private $_tblData;
        private $_rowType;
        
        function __construct(){
//            $this->_tblData = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            /*
            global $CONF, $objDb, $rowUrl;
            
            $strQuery = "
                SELECT 
                    n.`id`
                    ,n.`title`
                    ,n.`url`
                    ,n.`start_date`
                    ,n.`end_date`
                    ,n.`lead`
                    ,n.`description`
                    ,n.`image`
                FROM 
                    `news` as n
                JOIN
                    `news_type` as nt ON n.id = nt.news_id
                WHERE 
                    n.`delete_date` IS NULL 
                    AND n.`is_active` = 1
                    AND nt.`type_id` = 2
                    AND (n.`publication_start_date` IS NULL OR n.`publication_start_date`='0000-00-00 00:00:00' OR n.`publication_start_date` <= NOW())
                    AND (n.`publication_end_date` IS NULL OR n.`publication_end_date`='0000-00-00 00:00:00' OR n.`publication_end_date` > NOW())
                ORDER BY
                    n.`start_date` DESC
            ";
            $tblData = $objDb->getAll($strQuery);

            foreach ($tblData as $numIdx=>$rowData) {
                if (!empty($rowData['image']) && is_file($CONF['pub_dir']."cikkek/".$rowData['id']."/small/".$rowData['image'])) {
                    $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/cikkek/".$rowData['id']."/small/".$rowData['image'];
                } else {
                    $tblData[$numIdx]['image'] = $CONF['web_url']."images/news-placeholder.jpg";
                }
            }
            
            $this->_tblData = $tblData;
            */
            $this->_rowType = $rowType;
            return true;

        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
                // $objSmarty->assign("tblLoggedUserData", $this->_tblData);
//                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("rowType", $this->_rowType);
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>