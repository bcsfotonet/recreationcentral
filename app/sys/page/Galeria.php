<?php 
    class Galeria{
        private $_tblData;
        private $_strPageType;
        private $_tblCityData;
        private $_firstImageData;
        private $_rowType;
        
        function __construct(){
            $this->_tblData = array();
            $this->_strPageType = 'lista';
            $this->_tblCityData = array();
            $this->_firstImageData = '';
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strQuery = '';
            $tblCityData = '';
            $firstImageData = '';
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // vég oldal
                $this->_strPageType = "veg";
                
                $strQuery = "
                    SELECT
                        gi.*
                        ,g.id as gallery_id
                        ,g.title
                        ,g.description
                    FROM
                        gallery_image as gi
                    JOIN
                        gallery as g ON gi.gallery_id = g.id
                    JOIN
                        gallery_type as gt ON gt.gallery_id = g.id
                    WHERE
                        g.delete_date IS NULL
                        AND g.is_active = 1
                        AND gt.type_id = 15
                        AND g.url = '{$rowUrl[1]}'
                        AND gi.delete_date IS NULL
                    ORDER BY
                        gi.priority
                ";
                
                $tblData = $objDb->getAll($strQuery);
                
                if(!empty($tblData)){
                    // csak a galeria adatait szedem ebbol a viewban
                    $firstImageData = $tblData[0];
                    
                    foreach ($tblData as $numIdx=>$rowData) {
                        if (!empty($rowData['image']) && is_file($CONF['pub_dir']."kozponti-galeria/".$rowData['gallery_id']."/big/".$rowData['image'])) {
                            $tblData[$numIdx]['image_url'] = $CONF['base_url']."media/pub/kozponti-galeria/".$rowData['gallery_id']."/big/".$rowData['image'];
                            $tblData[$numIdx]['image_original_url'] = $CONF['base_url']."media/pub/kozponti-galeria/".$rowData['gallery_id']."/original/".$rowData['image'];                       
                            $originalImgDir = $CONF['pub_dir']."kozponti-galeria/".$rowData['gallery_id']."/original/".$rowData['image'];                          
			    $tblImageSize = getimagesize($originalImgDir);
                            $rowImgWidth = $tblImageSize[0];
                            $rowImgHeight = $tblImageSize[1];
                            $tblData[$numIdx]['image_size'] = $rowImgWidth.'x'.$rowImgHeight;
                        } else {
                            $tblData[$numIdx]['image_url'] = $CONF['web_url']."images/news-placeholder.jpg";
                            $tblData[$numIdx]['image_original_url'] = $CONF['web_url']."images/news-placeholder.jpg";
                            $tblData[$numIdx]['image_size'] = '600x315';
                        }
                    }
                    
                }
            } else {
                
                // lista oldal
                $this->_strPageType = "lista";
                
                $strQuery = "
                    SELECT
                        g.id
                        ,g.title
                        ,g.url
                        ,g.description
                        ,g.city_id
                        ,gi.image as first_image
                    FROM
                        gallery as g
                    JOIN
                        gallery_type as gt ON gt.gallery_id = g.id
                    JOIN
                        gallery_image as gi ON (gi.gallery_id = g.id AND gi.priority = 1 AND gi.delete_date IS NULL)
                    WHERE
                        g.delete_date IS NULL
                        AND g.is_active = 1
                        AND gt.type_id = 15
                    ORDER BY
                        g.title
                ";
                
                $tblData = $objDb->getAll($strQuery);
                
                if(!empty($tblData)){
                    foreach ($tblData as $numIdx=>$rowData) {
                        if (!empty($rowData['first_image']) && is_file($CONF['pub_dir']."kozponti-galeria/".$rowData['id']."/big/".$rowData['first_image'])) {
                            $tblData[$numIdx]['image_url'] = $CONF['base_url']."media/pub/kozponti-galeria/".$rowData['id']."/big/".$rowData['first_image'];
                        } else {
                            $tblData[$numIdx]['image_url'] = $CONF['web_url']."images/news-placeholder.jpg";
                        }
                    }
                }
                $tblCityData = $objDb->getAll("
                    SELECT 
                        c.id
                        ,c.name
                    FROM
                        city as c
                    WHERE
                        c.delete_date IS NULL
                        AND c.is_active = 1
                    ORDER BY
                        c.name
                ");
            }
            
            $this->_tblData = $tblData;
            $this->_tblCityData = $tblCityData;
//            var_dump($firstImageData['title']);
            $this->_firstImageData = $firstImageData;
            $this->_rowType = $rowType;
            
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblCityData", $this->_tblCityData);
                $objSmarty->assign("rowType", $this->_rowType); 
                
                if ($this->_strPageType == "veg") {
                    //vég oldal
                    $objSmarty->assign("firstImageData", $this->_firstImageData);

                    $objSmarty->display("page".DIRECTORY_SEPARATOR."Galeria_veg.tpl");
                } else {
                    //lista oldal
                    $objSmarty->display($strTplPagePath);
                }
                
            }
        }
    }
?>