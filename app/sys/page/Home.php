<?php 
    class Home{
        private $_tblNewsData, $_tblPartnerData, $_rowType;
        
        function __construct(){    
            $this->_tblNewsData = array();
            $this->_tblPartnerData = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $strLanguage;
            
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    t.id = 1
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            $strNewsQuery = "
                SELECT
                    n.`id`
                    ,n.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                    ,n.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                    ,n.`start_date`
                    ,n.`end_date`
                    ,n.lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                    ,n.description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                    ,n.`image`
                    ,(SELECT url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." FROM type WHERE id = nt.type_id) as type_url
                FROM 
                    `news` as n
                JOIN
                    `news_type` as nt ON n.id = nt.news_id
                WHERE 
                    n.`delete_date` IS NULL 
                    AND n.`is_active` = 1
                    AND ( nt.`type_id` = 1 OR nt.`type_id` = 2 OR nt.`type_id` = 3 OR nt.`type_id` = 11 )
                    AND n.is_highlighted = 1
                    AND (n.`publication_start_date` IS NULL OR n.`publication_start_date`='0000-00-00 00:00:00' OR n.`publication_start_date` <= NOW())
                    AND (n.`publication_end_date` IS NULL OR n.`publication_end_date`='0000-00-00 00:00:00' OR n.`publication_end_date` > NOW())
                group by 
                    n.`id`
                ORDER BY
                    n.`start_date` DESC, nt.type_id ASC
            ";
            $tblNewsData = $objDb->getAll($strNewsQuery);
                        
            if(!empty($tblNewsData)){
                foreach ($tblNewsData as $numIdx => $rowNewsData) {
                    if(!empty($rowNewsData['title'])){
                        if (!empty($rowNewsData['image']) && is_file($CONF['pub_dir']."cikkek/".$rowNewsData['id']."/original/".$rowNewsData['image'])) {
                            $tblNewsData[$numIdx]['image'] = $CONF['base_url']."media/pub/cikkek/".$rowNewsData['id']."/original/".$rowNewsData['image'];
                        } else {
                            $tblNewsData[$numIdx]['image'] = $CONF['web_url']."images/news-placeholder.jpg";
                        }
                    }
                }
            }
            $this->_tblNewsData = $tblNewsData;
            
            $strPartnerQuery = "
                SELECT 
                    `id`
                    ,`name".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS name
                    ,`ext_url".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS ext_url
                    ,`target_value`
                    ,`image`
                FROM 
                    `partner`
                WHERE 
                    `delete_date` IS NULL 
                    AND `is_active` = 1
                ORDER BY
                    `priority`, `name".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` ASC
            ";
            
            $tblPartnerData = $objDb->getAll($strPartnerQuery);
            
            foreach ($tblPartnerData as $numIdx => $rowPartnerData) {
                if (!empty($rowPartnerData['image']) && is_file($CONF['pub_dir']."partnerek/".$rowPartnerData['id']."/small/".$rowPartnerData['image'])) {
                    $tblPartnerData[$numIdx]['image'] = $CONF['base_url']."media/pub/partnerek/".$rowPartnerData['id']."/small/".$rowPartnerData['image'];
                } else {
                    $tblPartnerData[$numIdx]['image'] = $CONF['web_url']."images/180x120.png";
                }
            }
            
            $this->_tblPartnerData = $tblPartnerData;
            $this->_rowType = $rowType;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblNewsData", $this->_tblNewsData);
                $objSmarty->assign("tblPartnerData", $this->_tblPartnerData);
                $objSmarty->assign("rowType", $this->_rowType);
            }

            $objSmarty->display($strTplPagePath);
        }
    }
    