<?php 
    class Kereses{
        private $_tblData;
        private $_numSearchLength;
        private $_rowType;
        
        function __construct(){
            $this->_tblData = array();
            $this->_numSearchLength = 0;
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $strLanguage, $rowLabel;
            $strQuery = '';
            $tblData = '';
            $strSearch = '';
            $strSearchInputName = $rowLabel['keresett_szoveg'];
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    t.id = 1
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            if(isset($_GET[$strSearchInputName]) && !empty($_GET[$strSearchInputName])){
                $strSearch = addslashes(trim(htmlspecialchars_decode($_GET[$strSearchInputName], ENT_QUOTES)));

                $numSearchLength = mb_strlen($strSearch, 'UTF-8');
                
                if($numSearchLength >= 3){       
                    $strQuery = "
                        SELECT 
                            n.`id`
                            ,n.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                            ,n.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                            ,n.`start_date`
                            ,n.`end_date`
                            ,n.lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                            ,n.description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                            ,n.`image`
                        FROM 
                            `news` as n
                        JOIN
                            `news_type` as nt ON n.id = nt.news_id
                        WHERE 
                            n.`delete_date` IS NULL 
                            AND n.`is_active` = 1
                            AND nt.`type_id` = 1
                            AND (n.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." LIKE '%" . $strSearch . "%' OR n.lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")." LIKE '%" . $strSearch . "%' OR n.description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." LIKE '%" . $strSearch . "%')
                            AND (n.`publication_start_date` IS NULL OR n.`publication_start_date`='0000-00-00 00:00:00' OR n.`publication_start_date` <= NOW())
                            AND (n.`publication_end_date` IS NULL OR n.`publication_end_date`='0000-00-00 00:00:00' OR n.`publication_end_date` > NOW())
                        ORDER BY
                            n.`start_date` DESC
                    ";
                            
                    $tblData = $objDb->getAll($strQuery);

                    if(!empty($tblData)){
                      foreach ($tblData as $numIdx=>$rowData) {
                          if (!empty($rowData['image']) && is_file($CONF['pub_dir']."cikkek/".$rowData['id']."/big/".$rowData['image'])) {
                              $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/cikkek/".$rowData['id']."/big/".$rowData['image'];
                          } else {
                              $tblData[$numIdx]['image'] = $CONF['web_url']."images/news-placeholder.jpg";
                          }
                      }
                    }
                }
                $this->_tblData = $tblData;
                $this->_numSearchLength = $numSearchLength;
                $this->_rowType = $rowType;
                return true;
            }
        }
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("numSearchLength", $this->_numSearchLength);
                $objSmarty->assign("rowType", $this->_rowType);  
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>