<?php 
    class Kreativ_csoport{
        private $_tblData;
        private $_strPageType;
        private $_tblCityData;
        private $_rowType;
        
        function __construct(){
            $this->_tblData = array();
            $this->_strPageType = "lista";
            $this->_tblCityData = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strQuery = "";
            $tblCityData = "";
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // vég oldal
                $this->_strPageType = "veg";
                $strQuery = "
                    SELECT 
                         id
                        ,name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                        ,title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,email
                        ,image
                        ,description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                        ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = staff.city_id) AS city_name
                    FROM 
                        staff
                    WHERE 
                        delete_date IS NULL 
                        AND is_active = 1
                        AND type_id = 16
                        AND url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$rowUrl[1]}'
                    ORDER BY
                        id
                    LIMIT 1
                ";
                $tblData = $objDb->getRow($strQuery);
                
                if (!empty($tblData['image']) && is_file($CONF['pub_dir']."szerzok/".$tblData['id']."/big/".$tblData['image'])) {
                    $tblData['image'] = $CONF['base_url']."media/pub/szerzok/".$tblData['id']."/big/".$tblData['image'];
                } else {
                    $tblData['image'] = $CONF['web_url']."images/user-placeholder.jpg";
                }
            } else {
                // lista oldal
                $this->_strPageType = "lista";
                $strQuery = "
                    SELECT 
                         id
                        ,name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                        ,url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        ,title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,email
                        ,image
                        ,description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                        ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = staff.city_id) AS city_name
                    FROM 
                        staff
                    WHERE 
                        delete_date IS NULL 
                        AND is_active = 1
                        AND type_id = 16
                    ORDER BY
                        priority
                ";
                $tblData = $objDb->getAll($strQuery);

                foreach ($tblData as $numIdx=>$rowData) {
                    if (!empty($rowData['image']) && is_file($CONF['pub_dir']."szerzok/".$rowData['id']."/big/".$rowData['image'])) {
                        $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/szerzok/".$rowData['id']."/big/".$rowData['image'];
                    } else {
                        $tblData[$numIdx]['image'] = $CONF['web_url']."images/user-placeholder.jpg";
                    }
                    $tblData[$numIdx]['description_strip'] =  strip_tags($rowData['description']);
                }
                
                $tblCityData = $objDb->getAll("
                    SELECT 
                        c.id
                        ,c.name
                    FROM
                        city as c
                    WHERE
                        c.delete_date IS NULL
                        AND c.is_active = 1
                    ORDER BY
                        c.name
                ");
            }
 
            $this->_tblData = $tblData;
            $this->_tblCityData = $tblCityData;
            $this->_rowType = $rowType;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblCityData", $this->_tblCityData);
                $objSmarty->assign("rowType", $this->_rowType);
            }
            
            if ($this->_strPageType == "veg") {
                // vég oldal
                $objSmarty->display("page".DIRECTORY_SEPARATOR."Kreativ_csoport_veg.tpl");
            } else {
                // lista oldal
                $objSmarty->display($strTplPagePath);
            }
        }
    }
?>    