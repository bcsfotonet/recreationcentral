<?php 
    class Magazinok{      
        private $_tblData;
        private $_strPageType;
        private $_tblStudy;
        private $_rowType;
        private $_rowStudyType;
        private $_rowStaffType;
        private $_tblStaff;
        private $_numCustomerId;
        
        function __construct(){   
            $this->_tblData = array();
            $this->_tblStudy = array();
            $this->_strPageType = "lista";
            $this->_rowType = array();
            $this->_rowStudyType = array();
            $this->_rowStaffType = array();
            $this->_tblStaff = array();
            $this->_numCustomerId = 0;
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage, $rowLabel;
            
            $numCustomerId = getCustomerId();
            
            $tblData = array();
            $strQuery = "";
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // vég oldal
                $this->_strPageType = "veg";
                $strQuery = "
                    SELECT
                        m.`id`
                        ,yai.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,yai.`url`
                        ,m.`main_editor".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS main_editor
                        ,m.`lang".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS lang
                        ,m.foundation_year
                        ,m.paper_issn
                        ,m.online_issn
                        ,m.`editorial_address".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS editorial_address
                        ,m.publication_year
                        ,m.volume
                        ,m.`description".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS description
                        ,m.`orderable`
                        ,m.`image`
                        ,m.`filename`
                    FROM 
                        `magazine` as m
                    JOIN
                        `year_and_issue` as yai ON yai.id = m.year_and_issue_id
                    WHERE 
                        m.`delete_date` IS NULL
                        AND m.`is_active` = 1
                        AND yai.`delete_date` IS NULL
                        AND yai.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$rowUrl[1]}'
                    LIMIT 1
                ";
                        
                $tblData = $objDb->getRow($strQuery);
                
                if(!empty($tblData)){
//                
                    if (!empty($tblData['image']) && is_file($CONF['pub_dir']."magazinok/".$tblData['id']."/big/".$tblData['image'])) {
                        $tblData['image'] = $CONF['base_url']."media/pub/magazinok/".$tblData['id']."/big/".$tblData['image'];
                    } else {
                        $tblData['image'] = $CONF['web_url']."images/16decbor.png";
                    }
                    
                    if (!empty($tblData['filename']) && is_file($CONF['pub_dir']."magazinok/".$tblData['id']."/".$tblData['filename'])) {
                        $tblData['filename'] = $CONF['base_url']."media/pub/magazinok/".$tblData['id']."/".$tblData['filename'];
                    } else {
                        $tblData['filename'] = '';
                    }
                    
                    // szerkesztobizottsagi tagok
                    $strStaffQuery = "
                        SELECT 
                            staff.id
                           ,name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                           ,url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                           ,title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                           ,email
                           ,image
                           ,description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                        FROM 
                           staff
                        JOIN
                            magazine_staff ON magazine_staff.staff_id = staff.id
                        WHERE 
                            staff.delete_date IS NULL 
                            AND staff.is_active = 1
                            AND staff.type_id = 6
                            AND magazine_staff.magazine_id = ".$tblData['id']."
                        ORDER BY
                            priority
                    ";
//                    var_dump($strStaffQuery);die;
                    $tblStaff = $objDb->getAll($strStaffQuery);
                    $this->_tblStaff = $tblStaff;
                    
                    $strStaffTypeQuery = "
                        SELECT
                            t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                            ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        FROM 
                            type as t
                        WHERE
                            t.id = 6
                            -- t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                            AND t.delete_date IS NULL
                    ";
                    $rowStaffType = $objDb->getRow($strStaffTypeQuery);
                    $this->_rowStaffType = $rowStaffType;
                    
                    $strStudyQuery = "
                        SELECT
                            s.id
                            ,coalesce(NULLIF(s.title,''),NULLIF(s.title_en,''),s.title_ro) AS title
                            ,coalesce(NULLIF(s.url,''),NULLIF(s.url_en,''),url_ro) AS url
                        FROM
                            study as s
                        JOIN
                            magazine_study as ms ON s.id = ms.study_id
                        WHERE
                            s.delete_date IS NULL
                            AND s.is_active = 1
                            AND ms.magazine_id = ".$tblData['id']."
                    ";
//                    var_dump($strStudyQuery);
                    $tblStudy = $objDb->getAll($strStudyQuery);
                    $this->_tblStudy = $tblStudy;
                    
                    // ha csak kulfoldi nyelven van mentve a tanulmany a linkje nem lesz jo..
                    // TODO eldonteni, milyen nyelvu a tanulmany es a linkjet az szerint modositani
                    $strStudyTypeQuery = "
                        SELECT
                            t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                            ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        FROM 
                            type as t
                        WHERE
                            t.id = 23
                            -- t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                            AND t.delete_date IS NULL
                    ";
                    $rowStudyType = $objDb->getRow($strStudyTypeQuery);
                    $this->_rowStudyType = $rowStudyType;
                }else{
                    header("Location: {$CONF['base_url']}404");
                }
                
            } else {
                // lista oldal
                $this->_strPageType = "lista";
                $strQuery = "
                    SELECT 
                        m.`id`
                        ,m.year_and_issue_id
                        ,yai.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,yai.year_title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS year_title
                        ,yai.issue_title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS issue_title
                        ,yai.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        ,m.`orderable`
                        ,m.`image`
                    FROM 
                        `magazine` as m
                    JOIN
                        `year_and_issue` as yai ON yai.id = m.year_and_issue_id
                    WHERE 
                        m.`delete_date` IS NULL 
                        AND m.`is_active` = 1
                        AND yai.`delete_date` IS NULL
                    ORDER BY
                        yai.priority DESC, yai.`issue_title` ASC
                ";
                        
//                var_dump($strQuery);die;
                $tblPreData = $objDb->getAll($strQuery);
        
                if(!empty($tblPreData)){
                    foreach ($tblPreData as $key=>$item) {
                        $tblData[$item['year_title']][$key] = $item;
                    }
                }

                if(!empty($tblData)){
                    
                    foreach ($tblData as $strIdx => $tblDataTmp) {
                        
                        foreach ($tblDataTmp as $numIdx => $rowData) {
                            if (!empty($rowData['image']) && is_file($CONF['pub_dir']."magazinok/".$rowData['id']."/big/".$rowData['image'])) {
                                $tblData[$strIdx][$numIdx]['image'] = $CONF['base_url']."media/pub/magazinok/".$rowData['id']."/big/".$rowData['image'];
                            } else {
                                $tblData[$strIdx][$numIdx]['image'] = $CONF['web_url']."images/16decbor.png";
                            }
                        }
                        
                    }
                }
//                print_r($tblData);die;
            }
            $this->_tblData = $tblData;
            $this->_rowType = $rowType;
            $this->_numCustomerId = $numCustomerId;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;

            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("rowType", $this->_rowType);
                $objSmarty->assign("numCustomerId", $this->_numCustomerId);
            }
            
            if ($this->_strPageType == "veg") {
                //vég oldal
                $objSmarty->assign("tblStaff", $this->_tblStaff);
                $objSmarty->assign("tblStudy", $this->_tblStudy);
                $objSmarty->assign("rowStudyType", $this->_rowStudyType);
                $objSmarty->assign("rowStaffType", $this->_rowStaffType);
                $objSmarty->display("page".DIRECTORY_SEPARATOR."Magazinok_veg.tpl");
            } else {
                //lista oldal
                $objSmarty->display($strTplPagePath);
            }

        }
    }
    