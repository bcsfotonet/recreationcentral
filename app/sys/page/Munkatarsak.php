<?php 
    class Munkatarsak{
        private $_tblData;
        private $_strPageType;
        private $_rowType;
        
        function __construct(){
            $this->_tblData = array();
            $this->_strPageType = "lista";
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strQuery = "";
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // vég oldal
                $this->_strPageType = "veg";
                $strQuery = "
                    SELECT 
                         id
                        ,name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                        ,title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,email
                        ,image
                        ,description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                    FROM 
                        staff
                    WHERE 
                        delete_date IS NULL 
                        AND is_active = 1
                        AND type_id = 4
                        AND url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$rowUrl[1]}'
                    ORDER BY
                        id
                    LIMIT 1
                ";
                $tblData = $objDb->getRow($strQuery);
                
                if (!empty($tblData['image']) && is_file($CONF['pub_dir']."munkatarsak/".$tblData['id']."/big/".$tblData['image'])) {
                    $tblData['image'] = $CONF['base_url']."media/pub/munkatarsak/".$tblData['id']."/big/".$tblData['image'];
                } else {
                    $tblData['image'] = $CONF['web_url']."images/user-placeholder.jpg";
                }
            } else {
                // lista oldal
                $this->_strPageType = "lista";
                $strQuery = "
                    SELECT 
                         id
                        ,name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                        ,url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        ,title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,email
                        ,image
                        ,description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                    FROM 
                        staff
                    WHERE 
                        delete_date IS NULL 
                        AND is_active = 1
                        AND type_id = 4
                    ORDER BY
                        priority
                ";
                $tblData = $objDb->getAll($strQuery);

                foreach ($tblData as $numIdx=>$rowData) {
                    if (!empty($rowData['image']) && is_file($CONF['pub_dir']."munkatarsak/".$rowData['id']."/big/".$rowData['image'])) {
                        $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/munkatarsak/".$rowData['id']."/big/".$rowData['image'];
                    } else {
                        $tblData[$numIdx]['image'] = $CONF['web_url']."images/user-placeholder.jpg";
                    }
                    $tblData[$numIdx]['description_strip'] =  strip_tags($rowData['description']);
                }
                
            }
 
            $this->_tblData = $tblData;
            $this->_rowType = $rowType;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("rowType", $this->_rowType);
            }
            
            if ($this->_strPageType == "veg") {
                //vég oldal
                $objSmarty->display("page".DIRECTORY_SEPARATOR."Munkatarsak_veg.tpl");
            } else {
                //lista oldal
                $objSmarty->display($strTplPagePath);
            }

        }
    }
?>    