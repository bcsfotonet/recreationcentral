<?php 
    class Plakatok{      
        private $_tblData;
        private $_strPageType;
        private $_tblCityData;

        function __construct(){    
            $this->_tblData = array();
            $this->_strPageType = "lista";
            $this->_tblCityData = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strQuery = "";
            $tblCityData = "";
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // ilyen meg nincs 
                // vég oldal
                /*
                $this->_strPageType = "veg";
                $strQuery = "
                    SELECT 
                        p.`id`
                        ,p.`title`
                        ,p.`url`
                        ,p.`start_date`
                        ,p.`name`
                        ,p.`city_id`
                    FROM 
                        `presentation` as p
                    WHERE 
                        p.`delete_date` IS NULL 
                        AND p.`is_active` = 1
                        AND p.url = '{$rowUrl[1]}'
                    ORDER BY
                        p.`start_date` DESC
                    LIMIT 1
                ";
                $tblData = $objDb->getRow($strQuery);
                */
            } else {
                // lista oldal
                $this->_strPageType = "lista";
                $strQuery = "
                    SELECT 
                        p.`id`
                        ,p.`title`
                        ,p.`image`
                        ,(SELECT name FROM city WHERE delete_date IS NULL AND is_active = 1 AND id = p.city_id) AS city_name
                    FROM 
                        `poster` as p
                    WHERE 
                        p.`delete_date` IS NULL 
                        AND p.`is_active` = 1
                    ORDER BY
                        p.`priority`
                ";
                
                $tblData = $objDb->getAll($strQuery);
                
                if(!empty($tblData)){
                    foreach ($tblData as $numIdx=>$rowData) {
                        if (!empty($rowData['image']) && is_file($CONF['pub_dir']."plakatok/".$rowData['id']."/big/".$rowData['image'])) {
                            $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/plakatok/".$rowData['id']."/big/".$rowData['image'];
                            $tblData[$numIdx]['image_original_url'] = $CONF['base_url']."media/pub/plakatok/".$rowData['id']."/original/".$rowData['image'];
                            $originalImgDir = $CONF['pub_dir']."plakatok/".$rowData['id']."/original/".$rowData['image'];                          
			    $tblImageSize = getimagesize($originalImgDir);
                            $rowImgWidth = $tblImageSize[0];
                            $rowImgHeight = $tblImageSize[1];
                            $tblData[$numIdx]['image_size'] = $rowImgWidth.'x'.$rowImgHeight;
                        } else {
                            $tblData[$numIdx]['image'] = $CONF['web_url']."images/news-placeholder.jpg";
                            $tblData[$numIdx]['image_original_url'] = $CONF['web_url']."images/news-placeholder.jpg";
                            $tblData[$numIdx]['image_size'] = '600x315';
                        }
                    }
                }
                
                $tblCityData = $objDb->getAll("
                    SELECT 
                        c.id
                        ,c.name
                    FROM
                        city as c
                    WHERE
                        c.delete_date IS NULL
                        AND c.is_active = 1
                    ORDER BY
                        c.name
                ");
               
            }
            $this->_tblData = $tblData;
            $this->_tblCityData = $tblCityData;  
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblCityData", $this->_tblCityData);
            }
            
//            if ($this->_strPageType == "veg") {
//                //vég oldal
//                $objSmarty->display("page".DIRECTORY_SEPARATOR."Plakatok_veg.tpl");
//            } else {
                //lista oldal
                $objSmarty->display($strTplPagePath);
//            }

        }
    }
    