<?php 
    class Profil{
        private $_tblData;
        private $_tblFormData;
        private $_tblFormError;
        private $_rowType;
        
        function __construct(){
            $this->_tblData = array();
            $this->_tblFormData = array();
            $this->_tblFormError = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
           
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
                    
            $rowType = $objDb->getRow($strTypeQuery);
            
            $tblData = $objDb->getRow("
                SELECT 
                     t.id
                    ,t.name
                    ,s.title AS static_title 
                    ,s.description AS static_description   
                FROM 
                    type AS t
                LEFT JOIN
                    static AS s ON (s.id = t.static_id)
                WHERE 
                    t.url = '{$rowUrl[0]}'
                    AND t.delete_date IS NULL 
                    AND t.is_active = 1
                    AND 
                    (
                        s.id IS NULL
                        OR    
                        (s.delete_date IS NULL 
                        AND s.is_active = 1)
                    )
           ");
                
            if ($tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
                    
            if(isset($_SESSION['customer_id']) && is_numeric($_SESSION['customer_id'])){
                $tblUserData = $objDb->getRow("
                    SELECT
                        id
                        , is_person
                        , is_company
                        , last_name
                        , first_name
                        , company_name
                        , email
                        , tel
                        , zip
                        , city
                        , street
                        , house_number
                        , tax_number
                    FROM
                        customer
                    WHERE
                        delete_date IS NULL
                        AND is_active = 1
                        AND id = '".addslashes($_SESSION['customer_id'])."'
               ");        

                $tblFormData = array(
                    'personRadio' => (isset($tblUserData['is_person']) && !empty($tblUserData['is_person']) ? $tblUserData['is_person'] : '')
                    , 'companyRadio' => (isset($tblUserData['is_company']) && !empty($tblUserData['is_company']) ? $tblUserData['is_company'] : '')
                    , 'lastName' => (isset($_POST['lastName']) && !empty($_POST['lastName']) ? stripslashes(trim($_POST['lastName'])) : (isset($tblUserData['last_name']) && !empty($tblUserData['last_name']) ? $tblUserData['last_name'] : ''))
                    , 'firstName' => (isset($_POST['firstName']) && !empty($_POST['firstName']) ? stripslashes(trim($_POST['firstName'])) : (isset($tblUserData['first_name']) && !empty($tblUserData['first_name']) ? $tblUserData['first_name'] : ''))
                    , 'company' => (isset($_POST['company']) && !empty($_POST['company']) ? stripslashes(trim($_POST['company'])) : (isset($tblUserData['company_name']) && !empty($tblUserData['company_name']) ? $tblUserData['company_name'] : ''))
                    , 'email' => (isset($tblUserData['email']) && !empty($tblUserData['email']) ? $tblUserData['email'] : '')
                    , 'tel' => (isset($_POST['tel']) && !empty($_POST['tel']) ? stripslashes(trim($_POST['tel'])) : (isset($tblUserData['tel']) && !empty($tblUserData['tel']) ? $tblUserData['tel'] : ''))
                    , 'zip' => (isset($_POST['zip']) && !empty($_POST['zip']) ? stripslashes(trim($_POST['zip'])) : (isset($tblUserData['zip']) && !empty($tblUserData['zip']) ? $tblUserData['zip'] : ''))
                    , 'city' => (isset($_POST['city']) && !empty($_POST['city']) ? stripslashes(trim($_POST['city'])) : (isset($tblUserData['city']) && !empty($tblUserData['city']) ? $tblUserData['city'] : ''))
                    , 'street' => (isset($_POST['street']) && !empty($_POST['street']) ? stripslashes(trim($_POST['street'])) : (isset($tblUserData['street']) && !empty($tblUserData['street']) ? $tblUserData['street'] : ''))
                    , 'houseNum' => (isset($_POST['houseNum']) && !empty($_POST['houseNum']) ? stripslashes(trim($_POST['houseNum'])) : (isset($tblUserData['house_number']) && !empty($tblUserData['house_number']) ? $tblUserData['house_number'] : ''))
                    , 'taxNumber' => (isset($_POST['taxNumber']) && !empty($_POST['taxNumber']) ? stripslashes(trim($_POST['taxNumber'])) : (isset($tblUserData['tax_number']) && !empty($tblUserData['tax_number']) ? $tblUserData['tax_number'] : ''))
                    , 'pass' => (isset($_POST['pass']) && !empty($_POST['pass']) ? stripslashes(trim($_POST['pass'])) : '')
                    , 'passAgain' => (isset($_POST['passAgain']) && !empty($_POST['passAgain']) ? stripslashes(trim($_POST['passAgain'])) : '')
                );        
            
                // var_dump($tblFormData); die;
                $this->_tblFormData = $tblFormData;
            }  
            
            $this->_rowType = $rowType;
            return true;
        }
        
        private function validateForm($tblFormData){
            global $objDb;
            
            $tblFormError = array(
                'lastName' => (isset($_POST['lastName']) && empty($_POST['lastName']) ? 'A mező kitöltése kötelező!' : '')
                , 'firstName' => (isset($_POST['firstName']) && empty($_POST['firstName']) ? 'A mező kitöltése kötelező!' : '')
                , 'company' => (isset($_POST['company']) && empty($_POST['company']) ? 'A mező kitöltése kötelező!' : '')
                , 'email' => (isset($tblFormData['email']) && empty($tblFormData['email']) ? 'A mező kitöltése kötelező!' : (emailCheck($tblFormData['email'])==false ? 'Hibás e-mail cím!' : ''))
                , 'tel' => (isset($_POST['tel']) && empty($_POST['tel']) ? 'A mező kitöltése kötelező!' : '')
                , 'zip' => (isset($_POST['zip']) && empty($_POST['zip']) ? 'A mező kitöltése kötelező!' : '')
                , 'city' => (isset($_POST['city']) && empty($_POST['city']) ? 'A mező kitöltése kötelező!' : '')
                , 'street' => (isset($_POST['street']) && empty($_POST['street']) ? 'A mező kitöltése kötelező!' : '')
                , 'houseNum' => (isset($_POST['houseNum']) && empty($_POST['houseNum']) ? 'A mező kitöltése kötelező!' : '')
                , 'taxNumber' => (isset($_POST['taxNumber']) && empty($_POST['taxNumber']) ? 'A mező kitöltése kötelező!' : '')
                , 'pass' => (isset($_POST['pass']) && !empty($_POST['pass']) && empty($_POST['passAgain']) ? 'Nem adta meg a jelszót kétszer!' : (!empty($_POST['passAgain']) && $_POST['passAgain']!=$_POST['pass'] ? 'A beírt jelszavak nem egyeznek' : ''))
                , 'passAgain' => (isset($_POST['passAgain']) && !empty($_POST['passAgain']) && empty($_POST['pass']) ? 'Nem adta meg a jelszót kétszer!' : '')
            );
            
            if(empty(array_filter($tblFormError))){
                // nincs hiba
                
                $isAlreadyCustomer = $objDb->getRow("
                    SELECT
                        id
                    FROM
                        customer
                    WHERE
                        delete_date IS NULL
                        AND is_active = 1
                        AND email = '".$tblFormData['email']."'
                        AND id <> '".addslashes($_SESSION['customer_id'])."'
                ");

                if(empty($isAlreadyCustomer)){
                    return true;
                }else{
                    $this->_tblFormError = array('isAlreadyCustomer' => 'Ezzel az e-mail címmel már létezik regisztrált felhasználó!');
                    return false;
                }
            }else{                
                $this->_tblFormError = $tblFormError;
                return false;
            }
        }
        
        private function saveForm($tblFormData){
            // mentes
            global $objDb;
            $dbres = '';
            
            if(!empty($tblFormData)){
                
                $strQueryUpdate = "
                    UPDATE
                        customer
                    SET
                        modify_date = now()
                        ".(isset($tblFormData['lastName']) && !empty($tblFormData['lastName']) ? ",last_name = '{$tblFormData['lastName']}'" : "")."   
                        ".(isset($tblFormData['firstName']) && !empty($tblFormData['firstName']) ? ",first_name = '{$tblFormData['firstName']}'" : "")."
                        ".(isset($tblFormData['company']) && !empty($tblFormData['company']) ? ",company_name = '{$tblFormData['company']}'" : "")."
                        ".(isset($tblFormData['tel']) && !empty($tblFormData['tel']) ? ",tel = '{$tblFormData['tel']}'" : "")."
                        ".(isset($tblFormData['zip']) && !empty($tblFormData['zip']) ? ",zip = '{$tblFormData['zip']}'" : "")."
                        ".(isset($tblFormData['city']) && !empty($tblFormData['city']) ? ",city = '{$tblFormData['city']}'" : "")."
                        ".(isset($tblFormData['street']) && !empty($tblFormData['street']) ? ",street = '{$tblFormData['street']}'" : "")."
                        ".(isset($tblFormData['house_number']) && !empty($tblFormData['house_number']) ? ",house_number = '{$tblFormData['house_number']}'" : "")."
                        ".(isset($tblFormData['taxNumber']) && !empty($tblFormData['taxNumber']) ? ",tax_number = '{$tblFormData['taxNumber']}'" : "")."
                        ".(isset($tblFormData['pass']) && !empty($tblFormData['pass']) ? ",passwd = '".sha1($tblFormData['pass'])."'" : "")."
                    WHERE
                        id = '".addslashes($_SESSION['customer_id'])."'
                ";
//              print $strQueryUpdate;
                $dbres = $objDb->query($strQueryUpdate);             
            }
            
            if ($dbres !== FALSE) {
                return true;             
            }else{
                return false;
            }
            
        }
        
        public function run($strTplPagePath){
            global $objSmarty,$CONF,$rowUrl;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
//                $objSmarty->assign("tblFormData", $this->_tblFormData);
                $objSmarty->assign("rowType", $this->_rowType);
                // ha van post, akkor volt kuldes
                if(!empty($_POST)){
                    // form validalas
                    $isFormValid = $this->validateForm($this->_tblFormData);
                    if($isFormValid == true){
                        // form adatok mentese
                        $isFormSaveSuccessfull = $this->saveForm($this->_tblFormData);
                        if($isFormSaveSuccessfull == true){
                            header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'profil/sikeres' : ''));
                            exit;
                        }else{
                            header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'profil/sikertelen' : ''));
                            exit;
                        }
                    }else{
                        $objSmarty->assign("tblFormData", $this->_tblFormData);
                        $objSmarty->assign("tblFormError", $this->_tblFormError);
                    }
                }elseif(isset($rowUrl[1]) && $rowUrl[1]=='sikeres'){                  
                    $strSuccessText = 'Sikeres mentés!';
                    $objSmarty->assign("strSuccessText", $strSuccessText);
                }elseif(isset($rowUrl[1]) && $rowUrl[1]=='sikertelen'){
                    $strSuccessText = 'A mentés közben hiba történt! <br> Kérjük próbálja meg később!';
                    $objSmarty->assign("strSuccessText", $strSuccessText);
                }else{
                    $objSmarty->assign("tblFormData", $this->_tblFormData);
                }
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>