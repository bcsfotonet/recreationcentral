<?php 
    class Prospektus{
        private $_tblData;
        private $_rowType;
        
        function __construct(){
            $this->_tblData = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
           
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            $tblData = $objDb->getRow("
                SELECT 
                     t.id
                    ,t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,s.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS static_title 
                    ,s.description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS static_description   
                FROM 
                    type AS t
                LEFT JOIN
                    static AS s ON(s.id = t.static_id)
                WHERE 
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$rowUrl[0]}'
                    AND t.delete_date IS NULL 
                    AND t.is_active = 1
                    AND 
                    (
                        s.id IS NULL
                        OR    
                        (s.delete_date IS NULL 
                        AND s.is_active = 1)
                    )
           ");
            if ($tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            $this->_rowType = $rowType;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("rowType", $this->_rowType);
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>