<?php
     class Referenciak{
        private $_tblData;
        private $_strPageType;
        private $_tblGallery;
        private $_tblVideo;
        private $_tblDocument;
        private $_rowType;
        
        function __construct(){    
            $this->_tblData = array();
            $this->_strPageType = "lista";
            $this->_tblGallery = array();
            $this->_tblVideo = array();
            $this->_tblDocument = array();
            $this->_rowType = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strQuery = "";
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // vég oldal
                $this->_strPageType = "veg";
                $strQuery = "
                    SELECT 
                        n.id
                        ,n.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,n.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        ,n.start_date
                        ,n.end_date
                        ,n.lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                        ,n.description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                        ,n.image
                        ,n.`gallery_id`
                        ,n.`video_id`
                    FROM 
                        `news` as n
                    JOIN
                        `news_type` as nt ON n.id = nt.news_id
                    WHERE 
                        n.`delete_date` IS NULL 
                        AND n.`is_active` = 1
                        AND nt.`type_id` = 11
                        AND (n.`publication_start_date` IS NULL OR n.`publication_start_date`='0000-00-00 00:00:00' OR n.`publication_start_date` <= NOW())
                        AND (n.`publication_end_date` IS NULL OR n.`publication_end_date`='0000-00-00 00:00:00' OR n.`publication_end_date` > NOW())
                        AND n.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$rowUrl[1]}'
                    ORDER BY
                        n.`start_date` DESC
                    LIMIT 1
                ";
//                        print $strQuery;
                $tblData = $objDb->getRow($strQuery);
                
                if(!empty($tblData)){
                
                    if (!empty($tblData['image']) && is_file($CONF['pub_dir']."cikkek/".$tblData['id']."/big/".$tblData['image'])) {
                        $tblData['image'] = $CONF['base_url']."media/pub/cikkek/".$tblData['id']."/big/".$tblData['image'];
                    } else {
                        $tblData['image'] = $CONF['web_url']."images/news-placeholder.jpg";
                    }
                    //TODO + join video + galéria + doksik

                    // csatolt galeria es a benne levo kepek
                    $strGalQuery = "";
                    if (!empty($tblData['gallery_id'])){
                        $strGalQuery = "
                            SELECT
                                g.id as gallery_id
                                ,g.title as gallery_title
                                ,gi.image
                            FROM
                                gallery_image as gi
                            JOIN
                                gallery as g ON gi.gallery_id = g.id                     
                            WHERE
                                g.id = ".$tblData['gallery_id']."
                                AND g.delete_date IS NULL
                                AND g.is_active = 1
                                AND gi.delete_date IS NULL
                                AND gi.is_active = 1
                            ORDER BY
                                gi.priority
                        ";

                        $_tblGallery = $objDb->getAll($strGalQuery);

                        if (!empty($_tblGallery)){
                            foreach ($_tblGallery as $numIdx=>$rowGallery) {
                                if (!empty($rowGallery['image']) && is_file($CONF['pub_dir']."kozponti-galeria/".$rowGallery['gallery_id']."/original/".$rowGallery['image'])) {
                                    $_tblGallery[$numIdx]['original_image'] = $CONF['base_url']."media/pub/kozponti-galeria/".$rowGallery['gallery_id']."/original/".$rowGallery['image'];

                                    $tblOriginalImgSize = getimagesize($CONF['pub_dir']."kozponti-galeria/".$rowGallery['gallery_id']."/original/".$rowGallery['image']);

                                    if(!empty($tblOriginalImgSize)){
                                        $_tblGallery[$numIdx]['original_size'] = $tblOriginalImgSize[0] . 'x' . $tblOriginalImgSize[1];
                                    }else{
                                        $_tblGallery[$numIdx]['original_size'] = '600x315';
                                    }

                                }
                                if (!empty($rowGallery['image']) && is_file($CONF['pub_dir']."kozponti-galeria/".$rowGallery['gallery_id']."/big/".$rowGallery['image'])) {
                                    $_tblGallery[$numIdx]['big_image'] = $CONF['base_url']."media/pub/kozponti-galeria/".$rowGallery['gallery_id']."/big/".$rowGallery['image'];
                                }
                            }
                        }

                        $this->_tblGallery = $_tblGallery;

                    }

                    // csatolt video
                    $strVidQuery = "";
                    if (!empty($tblData['video_id'])){
                        $strVidQuery = "
                            SELECT
                                v.id
                                ,v.title
                                ,v.code
                                ,v.description
                                ,v.image
                            FROM
                                video as v
                            WHERE
                                v.id = ".$tblData['video_id']."
                                AND v.delete_date IS NULL
                                AND v.is_active = 1 
                        ";
                        $this->_tblVideo = $objDb->getRow($strVidQuery);
                    }

                    // csatolt dokumentumok
                    $strDocQuery = "";
                    if (!empty($tblData['id'])){
                        $strDocQuery = "
                            SELECT
                                d.id
                                ,d.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                                ,d.lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                                ,d.filename".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS filename
                            FROM
                                document as d
                            JOIN
                                news_document as nd ON nd.document_id = d.id
                            WHERE
                                nd.news_id = ".$tblData['id']."
                                AND d.delete_date IS NULL
                                AND d.is_active = 1 
                        ";

                        $_tblDocument = $objDb->getAll($strDocQuery);

                        if (!empty($_tblDocument)){
                            foreach ($_tblDocument as $numIdx=>$rowDocument) {
                                if (!empty($rowDocument['filename']) && is_file($CONF['pub_dir']."dokumentumtar/".$rowDocument['id'].(!empty($strLanguage) ? "/{$strLanguage}" : "")."/".$rowDocument['filename'])) {
                                    $_tblDocument[$numIdx]['filename'] = $CONF['base_url']."media/pub/dokumentumtar/".$rowDocument['id'].(!empty($strLanguage) ? "/{$strLanguage}" : "")."/".$rowDocument['filename'];
                                }
                            }
                        }

                        $this->_tblDocument = $_tblDocument;
                    }
                }else{
                    header("Location: {$CONF['base_url']}404");
                }
                
            } else {
                // lista oldal
                $this->_strPageType = "lista";
                $strQuery = "
                    SELECT 
                        n.`id`
                        ,n.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        ,n.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        ,n.`start_date`
                        ,n.`end_date`
                        ,n.lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                        ,n.description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                        ,n.`image`
                    FROM 
                        `news` as n
                    JOIN
                        `news_type` as nt ON n.id = nt.news_id
                    WHERE 
                        n.`delete_date` IS NULL 
                        AND n.`is_active` = 1
                        AND nt.`type_id` = 11
                        AND (n.`publication_start_date` IS NULL OR n.`publication_start_date`='0000-00-00 00:00:00' OR n.`publication_start_date` <= NOW())
                        AND (n.`publication_end_date` IS NULL OR n.`publication_end_date`='0000-00-00 00:00:00' OR n.`publication_end_date` > NOW())
                    ORDER BY
                        n.`start_date` DESC
                ";
                $tblData = $objDb->getAll($strQuery);
                
                foreach ($tblData as $numIdx=>$rowData) {
                    if (!empty($rowData['image']) && is_file($CONF['pub_dir']."cikkek/".$rowData['id']."/big/".$rowData['image'])) {
                        $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/cikkek/".$rowData['id']."/big/".$rowData['image'];
                    } else {
                        $tblData[$numIdx]['image'] = $CONF['web_url']."images/news-placeholder.jpg";
                    }
                }
            }
            $this->_tblData = $tblData;
            $this->_rowType = $rowType;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblGallery", $this->_tblGallery);
                $objSmarty->assign("tblVideo", $this->_tblVideo);
                $objSmarty->assign("tblDocument", $this->_tblDocument);
                $objSmarty->assign("rowType", $this->_rowType);
            }
//            var_dump($this->_tblData);
           
            if ($this->_strPageType == "veg") {
                //vég oldal
                $objSmarty->display("page".DIRECTORY_SEPARATOR."Referenciak_veg.tpl");
            } else {
                //lista oldal
                $objSmarty->display($strTplPagePath);
            }

        }
    }
    