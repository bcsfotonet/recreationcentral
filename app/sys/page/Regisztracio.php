<?php 
    require_once "Email.php";

    /*
    Form adatok
       personRadio
       companyRadio
       lastName
       firstName
       company
       email
       tel
       country
       zip
       city
       street
       houseNum
       taxNumber
       pass
       passAgain
    */
    
    class Regisztracio{
        private $_tblData;
        private $_tblFormData;
        private $_tblFormError;
        private $_objEmail;
        function __construct(){
            $this->_tblData = array();
            $this->_tblFormData = array();
            $this->_tblFormError = array();
            $this->_objEmail = new Email();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
           
            $tblData = $objDb->getRow("
                SELECT 
                     t.id
                    ,t.name
                    ,s.title AS static_title 
                    ,s.description AS static_description   
                FROM 
                    type AS t
                LEFT JOIN
                    static AS s ON (s.id = t.static_id)
                WHERE 
                    t.url = '{$rowUrl[0]}'
                    AND t.delete_date IS NULL 
                    AND t.is_active = 1
                    AND 
                    (
                        s.id IS NULL
                        OR    
                        (s.delete_date IS NULL 
                        AND s.is_active = 1)
                    )
           ");
            
            $tblFormData = array(
                'personRadio' => (isset($_POST['personRadio']) && !empty($_POST['personRadio']) ? stripslashes(trim($_POST['personRadio'])) : '')
                , 'companyRadio' => (isset($_POST['companyRadio']) && !empty($_POST['companyRadio']) ? stripslashes(trim($_POST['companyRadio'])) : '')
                , 'lastName' => (isset($_POST['lastName']) && !empty($_POST['lastName']) ? stripslashes(trim($_POST['lastName'])) : '')
                , 'firstName' => (isset($_POST['firstName']) && !empty($_POST['firstName']) ? stripslashes(trim($_POST['firstName'])) : '')
                , 'company' => (isset($_POST['company']) && !empty($_POST['company']) ? stripslashes(trim($_POST['company'])) : '')
                , 'email' => (isset($_POST['email']) && !empty($_POST['email']) ? stripslashes(trim($_POST['email'])) : '')
                , 'tel' => (isset($_POST['tel']) && !empty($_POST['tel']) ? stripslashes(trim($_POST['tel'])) : '')
                , 'country' => (isset($_POST['country']) && !empty($_POST['country']) ? stripslashes(trim($_POST['country'])) : '')
                , 'zip' => (isset($_POST['zip']) && !empty($_POST['zip']) ? stripslashes(trim($_POST['zip'])) : '')
                , 'city' => (isset($_POST['city']) && !empty($_POST['city']) ? stripslashes(trim($_POST['city'])) : '')
                , 'street' => (isset($_POST['street']) && !empty($_POST['street']) ? stripslashes(trim($_POST['street'])) : '')
                , 'houseNum' => (isset($_POST['houseNum']) && !empty($_POST['houseNum']) ? stripslashes(trim($_POST['houseNum'])) : '')
                , 'taxNumber' => (isset($_POST['taxNumber']) && !empty($_POST['taxNumber']) ? stripslashes(trim($_POST['taxNumber'])) : '')
                , 'pass' => (isset($_POST['pass']) && !empty($_POST['pass']) ? stripslashes(trim($_POST['pass'])) : '')
                , 'passAgain' => (isset($_POST['passAgain']) && !empty($_POST['passAgain']) ? stripslashes(trim($_POST['passAgain'])) : '')
            );
                    
            if ($tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
            
            $this->_tblFormData = $tblFormData;  
            
            return true;
        }
        
        private function validateForm($tblFormData){
            global $objDb;
            
            $tblFormError = array(
                'lastName' => (isset($_POST['lastName']) && empty($_POST['lastName']) ? 'A mező kitöltése kötelező!' : '')
                , 'firstName' => (isset($_POST['firstName']) && empty($_POST['firstName']) ? 'A mező kitöltése kötelező!' : '')
                , 'company' => (isset($_POST['company']) && empty($_POST['company']) ? 'A mező kitöltése kötelező!' : '')
                , 'email' => (isset($_POST['email']) && empty($_POST['email']) ? 'A mező kitöltése kötelező!' : (emailCheck($_POST['email'])==false ? 'Hibás e-mail cím!' : ''))
                , 'tel' => (isset($_POST['tel']) && empty($_POST['tel']) ? 'A mező kitöltése kötelező!' : '')
                , 'country' => (isset($_POST['country']) && empty($_POST['country']) ? 'A mező kitöltése kötelező!' : '')
                , 'zip' => (isset($_POST['zip']) && empty($_POST['zip']) ? 'A mező kitöltése kötelező!' : '')
                , 'city' => (isset($_POST['city']) && empty($_POST['city']) ? 'A mező kitöltése kötelező!' : '')
                , 'street' => (isset($_POST['street']) && empty($_POST['street']) ? 'A mező kitöltése kötelező!' : '')
                , 'houseNum' => (isset($_POST['houseNum']) && empty($_POST['houseNum']) ? 'A mező kitöltése kötelező!' : '')
                , 'taxNumber' => (isset($_POST['taxNumber']) && empty($_POST['taxNumber']) ? 'A mező kitöltése kötelező!' : '')
                , 'pass' => (isset($_POST['pass']) && empty($_POST['pass']) ? 'A mező kitöltése kötelező!' : (!empty($_POST['passAgain']) && $_POST['passAgain']!=$_POST['pass'] ? 'A beírt jelszavak nem egyeznek' : ''))
                , 'passAgain' => (isset($_POST['passAgain']) && empty($_POST['passAgain']) ? 'A mező kitöltése kötelező!' : '')
            );

            if(empty(array_filter($tblFormError))){
                // nincs hiba
                
                $isAlreadyCustomer = $objDb->getRow("
                    SELECT
                        id
                    FROM
                        customer
                    WHERE
                        delete_date IS NULL
                        AND is_active = 1
                        AND email = '".$tblFormData['email']."'
                ");
                   
                if(empty($isAlreadyCustomer)){
                    return true;
                }else{
                    $this->_tblFormError = array('isAlreadyCustomer' => 'Ezzel az e-mail címmel már létezik regisztrált felhasználó!');
                    return false;
                }
            }else{                
                $this->_tblFormError = $tblFormError;
                return false;
            }
        }
        
        private function saveForm($tblFormData){
            // mentes
            global $objDb;
            $dbres = '';
            
            if(!empty($tblFormData)){
                $strQueryInsert = "
                    INSERT INTO
                        customer
                    (
                        is_person
                        ,is_company
                        ,last_name
                        ,first_name
                        ,company_name
                        ,email
                        ,tel
                        ,country
                        ,zip
                        ,city
                        ,street
                        ,house_number
                        ,tax_number
                        ,passwd
                    ) VALUES (
                        '".(!empty($tblFormData['personRadio']) ? $tblFormData['personRadio'] : 0)."'
                        ,'".(!empty($tblFormData['companyRadio']) ? $tblFormData['companyRadio'] : 0)."'
                        ,".(!empty($tblFormData['lastName']) ? "'".$tblFormData['lastName']."'" : "NULL")."
                        ,".(!empty($tblFormData['firstName']) ? "'".$tblFormData['firstName']."'" : "NULL")."
                        ,".(!empty($tblFormData['company']) ? "'".$tblFormData['company']."'" : "NULL")."
                        ,'".$tblFormData['email']."'
                        ,'".$tblFormData['tel']."'
                        ,'".$tblFormData['country']."'
                        ,'".$tblFormData['zip']."'
                        ,'".$tblFormData['city']."'
                        ,'".$tblFormData['street']."'
                        ,'".$tblFormData['houseNum']."'
                        ,".(!empty($tblFormData['taxNumber']) ? "'".$tblFormData['taxNumber']."'" : "NULL")."
                        ,'".sha1($tblFormData['pass'])."' 
                    )
                ";

                $dbres = $objDb->insert($strQueryInsert); // visszaadja az insertelt id-t
            }
            
            if (is_numeric($dbres)) {
                return true;             
            }else{
                return false;
            }
            
        }
            
        private function sendForm($tblFormData){
            // kuldes
            global $CONF, $objDb;
            
            $isSendSuccessFull = false;
            $receiverMail = $tblFormData['email'];
            
            if(!empty($receiverMail)){
                $rowCustomer = $objDb->getRow("
                    SELECT
                        id
                        , last_name
                        , first_name
                    FROM
                        customer
                    WHERE
                        delete_date IS NULL
                        AND is_active = 1
                        AND email = '".$receiverMail."'
                ");
                if(!empty($rowCustomer)){
                    $strOriginal = file_get_contents_utf8($CONF['project_dir'] . 'email_templates/regisztracio.html');
                    $strNewName = replaceStringBetweenTags('$','~',$strOriginal,$rowCustomer['last_name'].' '.$rowCustomer['first_name']);

                    if($this->_objEmail->sendMessage('',$receiverMail,'Regisztráció',$strNewName)){
                       $isSendSuccessFull = true;
                    }
                }
            }
            return $isSendSuccessFull;
        }  
        
        public function run($strTplPagePath){
            global $objSmarty,$CONF,$rowUrl;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                // ha van post, akkor volt kuldes
                if(!empty($_POST)){
                    // form validalas
                    $isFormValid = $this->validateForm($this->_tblFormData);
                    if($isFormValid == true){
                        // form adatok mentese
                        $isFormSaveSuccessfull = $this->saveForm($this->_tblFormData);
                        if($isFormSaveSuccessfull == true){
                            // form adatok kuldese
                            $isFormSendSuccessfull = $this->sendForm($this->_tblFormData);
                            if($isFormSendSuccessfull == true){
                                header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'regisztracio/sikeres' : ''));
                                exit;
                            }else{
                                header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'regisztracio/sikertelen' : ''));
                                exit;
                            }
                        }else{
                            header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'regisztracio/sikertelen' : ''));
                            exit;
                        }
                    }else{
                        $objSmarty->assign("tblFormData", $this->_tblFormData);
                        $objSmarty->assign("tblFormError", $this->_tblFormError);
                    }
                }elseif(isset($rowUrl[1]) && $rowUrl[1]=='sikeres'){                  
                    $strSuccessText = 'Sikeres küldés!';
                    $objSmarty->assign("strSuccessText", $strSuccessText);
                }elseif(isset($rowUrl[1]) && $rowUrl[1]=='sikertelen'){
                    $strSuccessText = 'A küldés közben hiba történt! <br> Kérjük próbálja meg később!';
                    $objSmarty->assign("strSuccessText", $strSuccessText);
                }else{
                    $objSmarty->assign("tblFormData", $this->_tblFormData);
                }
            }
            $objSmarty->display($strTplPagePath);
        }
    }
?>