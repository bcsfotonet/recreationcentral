<?php 
//    print_r($_POST);
    require_once "Email.php";
    class Rendeles{
        private $_tblData;
        private $_tblFormData;
        private $_tblFormError;
        private $_rowType;
        private $_tblUserData;
        private $_tblOrderData;
        private $_numMagazinePrice;
        private $_numPostPrice;
        private $_numMagazinePiece;
        private $_numTotal;
        private $_objEmail;
        
        function __construct(){
            $this->_tblData = array();
            $this->_tblFormData = array();
            $this->_tblFormError = array();
            $this->_rowType = array();
            $this->_tblUserData = array();
            $this->_tblOrderData = array();
            $this->_numMagazinePrice = 0;
            $this->_numPostPrice = 0;
            $this->_numMagazinePiece = 0;
            $this->_numTotal = 0;
            $this->_objEmail = new Email();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            $numMagazinePrice = 750;
            $numPostPrice = 300;
            $numTotal = 0;
            $numMagazinePiece = 0;
                    
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
                    
            $rowType = $objDb->getRow($strTypeQuery);
            
            $tblData = $objDb->getRow("
                SELECT 
                     t.id
                    ,t.name
                    ,s.title AS static_title 
                    ,s.description AS static_description   
                FROM 
                    type AS t
                LEFT JOIN
                    static AS s ON (s.id = t.static_id)
                WHERE 
                    t.url = '{$rowUrl[0]}'
                    AND t.delete_date IS NULL 
                    AND t.is_active = 1
                    AND 
                    (
                        s.id IS NULL
                        OR    
                        (s.delete_date IS NULL 
                        AND s.is_active = 1)
                    )
           ");
                
            if ($tblData !== FALSE) {
                $this->_tblData = $tblData;
            }
                    
            if(isset($_SESSION['customer_id']) && is_numeric($_SESSION['customer_id'])){
                $tblUserData = $objDb->getRow("
                    SELECT
                        id
                        , is_person
                        , is_company
                        , last_name
                        , first_name
                        , company_name
                        , email
                        , tel
                        , country
                        , zip
                        , city
                        , street
                        , house_number
                        , tax_number
                    FROM
                        customer
                    WHERE
                        delete_date IS NULL
                        AND is_active = 1
                        AND id = '".addslashes($_SESSION['customer_id'])."'
               ");
                
                // TODO lekerni a mentett adatokat
                $tblOrderData = $objDb->getAll("
                    SELECT
                        o.id
                        ,o.customer_id
                        ,o.receive_mode
                        ,o.post_different
                        ,o.post_city
                        ,o.post_name
                        ,o.post_address
                        ,o.privacy_policy
                        ,o.post_price
                        ,o.total_price
                        ,o.currency
                        ,yai.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS magazine_title
                        ,mo.magazine_id
                        ,mo.num_magazine
                    FROM
                        `order` as o
                    JOIN
                        magazine_order as mo ON mo.order_id = o.id
                    JOIN
                        magazine as m ON m.id = mo.magazine_id
                    JOIN 
                        year_and_issue as yai ON m.year_and_issue_id = yai.id
                    WHERE
                        o.delete_date IS NULL
                        AND o.order_date IS NULL
                        AND o.customer_id = '".addslashes($_SESSION['customer_id'])."'
                    ORDER BY
                        magazine_title DESC
               ");
                        
                if(!empty($tblOrderData)){
                    foreach ($tblOrderData as $numIdx=>$rowData) {
                        if (!empty($rowData['num_magazine'])){       
                            if($rowData['receive_mode']==0){
                                $numTotal += ($rowData['num_magazine']*$numMagazinePrice)+($rowData['num_magazine']*$numPostPrice);
                            }else{
                                $numTotal += ($rowData['num_magazine']*$numMagazinePrice);
                            }
                            $numMagazinePiece += $rowData['num_magazine'];
                        }
                    }
                }
                $tblFormData = array(
                    'customer_id' => (isset($tblOrderData[0]['customer_id']) && !empty($tblOrderData[0]['customer_id']) ? $tblOrderData[0]['customer_id'] : '')
                    , 'receive_mode' => (isset($_POST['receive_mode']) && !empty($_POST['receive_mode']) ? stripslashes(trim($_POST['receive_mode'])) : (isset($tblOrderData[0]['receive_mode']) && !empty($tblOrderData[0]['receive_mode']) ? $tblOrderData[0]['receive_mode'] : ''))
                    , 'post_different' => (isset($_POST['post_different']) && !empty($_POST['post_different']) ? stripslashes(trim($_POST['post_different'])) : (isset($tblOrderData[0]['post_different']) && !empty($tblOrderData[0]['post_different']) ? $tblOrderData['post_different'] : ''))
                    , 'post_city' => (isset($_POST['post_city']) && !empty($_POST['post_city']) ? stripslashes(trim($_POST['post_city'])) : (isset($tblOrderData[0]['post_city']) && !empty($tblOrderData[0]['post_city']) ? $tblOrderData[0]['post_city'] : ''))
                    , 'post_name' => (isset($_POST['post_name']) && !empty($_POST['post_name']) ? stripslashes(trim($_POST['post_name'])) : (isset($tblOrderData[0]['post_name']) && !empty($tblOrderData[0]['post_name']) ? $tblOrderData[0]['post_name'] : ''))
                    , 'post_address' => (isset($_POST['post_address']) && !empty($_POST['post_address']) ? stripslashes(trim($_POST['post_address'])) : (isset($tblOrderData[0]['post_address']) && !empty($tblOrderData[0]['post_name']) ? $tblOrderData[0]['post_address'] : ''))
                    , 'privacy_policy' => (isset($_POST['privacy_policy']) && !empty($_POST['privacy_policy']) ? stripslashes(trim($_POST['privacy_policy'])) : (isset($tblOrderData[0]['privacy_policy']) && !empty($tblOrderData[0]['privacy_policy']) ? $tblOrderData[0]['privacy_policy'] : ''))
                    , 'total_price' => (isset($numTotal) && !empty($numTotal) ? stripslashes(trim($numTotal)) : '')
                );     
//                 var_dump($_POST);die;
//                var_dump($tblOrderData);die;
   
                //  , 'total_price' => (isset($_POST['total_price']) && !empty($_POST['total_price']) ? stripslashes(trim($_POST['total_price'])) : '')
              
                $this->_tblFormData = $tblFormData;
                $this->_tblUserData = $tblUserData;
                $this->_tblOrderData = $tblOrderData;
                $this->_numMagazinePrice = $numMagazinePrice;
                $this->_numPostPrice = $numPostPrice;
                $this->_numMagazinePiece = $numMagazinePiece;
                $this->_numTotal = $numTotal;
            }  
       
            $this->_rowType = $rowType;
            return true;
        }
        
        private function validateForm($tblFormData){
            global $objDb;
            
            $tblFormError = array(
                'receive_mode' => (isset($_POST['total_price']) && !is_numeric($_POST['receive_mode']) ? 'A mező kitöltése kötelező!' : '')
                , 'post_different' => (isset($_POST['total_price']) && isset($_POST['post_different']) && !is_numeric($_POST['post_different']) ? 'A mező kitöltése kötelező!' : '')
                , 'post_city' => (isset($_POST['receive_mode']) && $_POST['receive_mode']==1 && (!isset($_POST['post_city']) || empty($_POST['post_city'])) ? 'A mező kitöltése kötelező!' : '')
                , 'post_name' => (isset($_POST['post_different']) && isset($_POST['post_name']) && empty($_POST['post_name']) ? 'A mező kitöltése kötelező!' : '')
                , 'post_address' => (isset($_POST['post_different']) && isset($_POST['post_address']) && empty($_POST['post_address']) ? 'A mező kitöltése kötelező!' : '')
                , 'privacy_policy' => (isset($_POST['total_price']) && empty($_POST['privacy_policy']) ? 'Az adatkezelési nyilatkozat és az Á.SZ.F. elfogadása kötelező!' : '')
                , 'total_price' => (!isset($_POST['total_price']) || empty($_POST['total_price']) ? 'A rendelés leadásához, legalább egy magazint tegyen a kosárba!' : '')
            );
            
            if(empty(array_filter($tblFormError))){
                // nincs hiba
                return true;
            }else{
                $this->_tblFormError = $tblFormError;
                return false;
            }
        }
        
        private function saveForm($tblFormData){
            // mentes
            global $objDb;
            $dbres = '';
            
            if(!empty($tblFormData)){
                
                $strQueryUpdate = "
                    UPDATE
                        `order`
                    SET
                        modify_date = NOW()
                        ".(isset($tblFormData['receive_mode']) && !empty($tblFormData['receive_mode']) ? ",receive_mode = '{$tblFormData['receive_mode']}'" : "")."   
                        ".(isset($tblFormData['post_different']) && !empty($tblFormData['post_different']) ? ",post_different = '{$tblFormData['post_different']}'" : "")."
                        ".(isset($tblFormData['post_city']) && !empty($tblFormData['post_city']) ? ",post_city = '{$tblFormData['post_city']}'" : "")."
                        ".(isset($tblFormData['post_name']) && !empty($tblFormData['post_name']) ? ",post_name = '{$tblFormData['post_name']}'" : "")."
                        ".(isset($tblFormData['post_address']) && !empty($tblFormData['post_address']) ? ",post_address = '{$tblFormData['post_address']}'" : "")."
                        ".(isset($tblFormData['privacy_policy']) && !empty($tblFormData['privacy_policy']) ? ",privacy_policy = '{$tblFormData['privacy_policy']}'" : "")."
                        ".(isset($tblFormData['total_price']) && !empty($tblFormData['total_price']) ? ",total_price = '{$tblFormData['total_price']}'" : "")."
                        ,order_date = NOW()
                    WHERE
                        customer_id = '".addslashes($_SESSION['customer_id'])."'
                        AND order_date IS NULL
                ";
                        
//              print $strQueryUpdate;
                $dbres = $objDb->query($strQueryUpdate);             
            }
            
            if ($dbres !== FALSE) {
                return true;             
            }else{
                return false;
            }
            
        }
        
        private function sendForm($tblUserData,$tblFormData){
            // kuldes
            global $CONF, $objDb;
            
            $isSendSuccessFull = false;
            $receiverMail = $tblUserData['email'];
            
            if(!empty($receiverMail)){
                $rowCustomer = $objDb->getRow("
                    SELECT
                        id
                        ,CASE WHEN is_person = 1 THEN CONCAT(last_name, ' ', first_name) WHEN is_company = 1 THEN company_name ELSE '' END AS name 
                        -- , last_name
                        -- , first_name
                    FROM
                        customer
                    WHERE
                        delete_date IS NULL
                        AND is_active = 1
                        AND email = '".$receiverMail."'
                ");
                if(!empty($rowCustomer)){
                    if($tblFormData['receive_mode']==0){
                        $strOriginal = file_get_contents_utf8($CONF['project_dir'] . 'email_templates/rendeles-posta.html');
                    }else{
                        $strOriginal = file_get_contents_utf8($CONF['project_dir'] . 'email_templates/rendeles-szemelyes.html'); 
                    }
                    
                    //$strNewName = replaceStringBetweenTags('$','~',$strOriginal,$rowCustomer['last_name'].' '.$rowCustomer['first_name']);
                    $strNewName = replaceStringBetweenTags('$','~',$strOriginal,$rowCustomer['name']);

                    if($this->_objEmail->sendMessage('',$receiverMail,'Rendelés',$strNewName,'marketing@recreationcentral.eu','info@bcsfotonet.hu')){  
                        $isSendSuccessFull = true;
                    }
                }
            }
            return $isSendSuccessFull;
        }  
        
        public function run($strTplPagePath){
            global $objSmarty,$CONF,$rowUrl;

            if( $this->getData() === true ){
                $objSmarty->assign("tblData", $this->_tblData);
//                $objSmarty->assign("tblFormData", $this->_tblFormData);
                $objSmarty->assign("tblUserData", $this->_tblUserData);
                $objSmarty->assign("tblOrderData", $this->_tblOrderData);
                $objSmarty->assign("rowType", $this->_rowType);
                $objSmarty->assign("numMagazinePrice", $this->_numMagazinePrice);
                $objSmarty->assign("numPostPrice", $this->_numPostPrice);
                $objSmarty->assign("numMagazinePiece", $this->_numMagazinePiece);
                $objSmarty->assign("numTotal", $this->_numTotal);
                
                if(!empty($_POST)){
                     // form validalas
                    $isFormValid = $this->validateForm($this->_tblFormData);
                    if($isFormValid == true){
                        // form adatok mentese
                        $isFormSaveSuccessfull = $this->saveForm($this->_tblFormData);
                        if($isFormSaveSuccessfull == true){
                            // form adatok kuldese
                            $isFormSendSuccessfull = $this->sendForm($this->_tblUserData,$this->_tblFormData);
                            if($isFormSendSuccessfull == true){
                                header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'rendeles/sikeres' : ''));
                                exit;
                            }else{
                                header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'rendeles/sikertelen' : ''));
                                exit;
                            }
                        }else{
                            header('Location:'.(!empty($CONF['base_url']) ? $CONF['base_url'].'rendeles/sikertelen' : ''));
                            exit;
                        }
                    }else{
                        $objSmarty->assign("tblFormData", $this->_tblFormData);
                        $objSmarty->assign("tblFormError", $this->_tblFormError);
                    }
                }elseif(isset($rowUrl[1]) && $rowUrl[1]=='sikeres'){                  
                    $strSuccessText = 'Sikeres küldés!';
                    $objSmarty->assign("strSuccessText", $strSuccessText);
                }elseif(isset($rowUrl[1]) && $rowUrl[1]=='sikertelen'){
                    $strSuccessText = 'A küldés közben hiba történt! <br> Kérjük próbálja meg később!';
                    $objSmarty->assign("strSuccessText", $strSuccessText);
                }else{
                    $objSmarty->assign("tblFormData", $this->_tblFormData);
                }
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>