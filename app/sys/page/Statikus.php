<?php 
    class Statikus{
        private $_tblData;
        private $_tblGallery;
        private $_tblVideo;
        private $_tblDocument;
        
        function __construct(){
            $this->_tblData = array();
            $this->_tblGallery = array();
            $this->_tblVideo = array();
            $this->_tblDocument = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;

            if(!empty($rowUrl[0])){
                $strQuery = "
                    SELECT 
                          s.id
                        , s.description".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS description
                        , s.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                        , s.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                        , s.gallery_id
                        , s.video_id
                    FROM 
                        static as s
                    WHERE 
                        s.delete_date IS NULL 
                        AND s.is_active = 1
                        AND s.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '".$rowUrl[0]."'
                    LIMIT 1
                ";
                $tblData = $objDb->getRow($strQuery);
                
                if(!empty($tblData)){
                    // csatolt galeria es a benne levo kepek
                    $strGalQuery = "";
                    if (!empty($tblData['gallery_id'])){
                        $strGalQuery = "
                            SELECT
                                g.id as gallery_id
                                ,g.title as gallery_title
                                ,gi.image
                            FROM
                                gallery_image as gi
                            JOIN
                                gallery as g ON gi.gallery_id = g.id                     
                            WHERE
                                g.id = ".$tblData['gallery_id']."
                                AND g.delete_date IS NULL
                                AND g.is_active = 1
                                AND gi.delete_date IS NULL
                                AND gi.is_active = 1
                            ORDER BY
                                gi.priority
                        ";

                        $_tblGallery = $objDb->getAll($strGalQuery);

                        if (!empty($_tblGallery)){
                            foreach ($_tblGallery as $numIdx=>$rowGallery) {
                                if (!empty($rowGallery['image']) && is_file($CONF['pub_dir']."kozponti-galeria/".$rowGallery['gallery_id']."/original/".$rowGallery['image'])) {
                                    $_tblGallery[$numIdx]['original_image'] = $CONF['base_url']."media/pub/kozponti-galeria/".$rowGallery['gallery_id']."/original/".$rowGallery['image'];

                                    $tblOriginalImgSize = getimagesize($CONF['pub_dir']."kozponti-galeria/".$rowGallery['gallery_id']."/original/".$rowGallery['image']);

                                    if(!empty($tblOriginalImgSize)){
                                        $_tblGallery[$numIdx]['original_size'] = $tblOriginalImgSize[0] . 'x' . $tblOriginalImgSize[1];
                                    }else{
                                        $_tblGallery[$numIdx]['original_size'] = '600x315';
                                    }

                                }
                                if (!empty($rowGallery['image']) && is_file($CONF['pub_dir']."kozponti-galeria/".$rowGallery['gallery_id']."/big/".$rowGallery['image'])) {
                                    $_tblGallery[$numIdx]['big_image'] = $CONF['base_url']."media/pub/kozponti-galeria/".$rowGallery['gallery_id']."/big/".$rowGallery['image'];
                                }
                            }
                        }

                        $this->_tblGallery = $_tblGallery;
                    }
                    
                    // csatolt video
                    $strVidQuery = "";
                    if (!empty($tblData['video_id'])){
                        $strVidQuery = "
                            SELECT
                                v.id
                                ,v.title
                                ,v.code
                                ,v.description
                                ,v.image
                            FROM
                                video as v
                            WHERE
                                v.id = ".$tblData['video_id']."
                                AND v.delete_date IS NULL
                                AND v.is_active = 1 
                        ";
                        $this->_tblVideo = $objDb->getRow($strVidQuery);
                    }
                    
                    // csatolt dokumentumok
                    $strDocQuery = "";
                    if (!empty($tblData['id'])){
                        $strDocQuery = "
                            SELECT
                                d.id
                                ,d.title".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS title
                                ,d.lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS lead
                                ,d.filename".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS filename
                            FROM
                                document as d
                            JOIN
                                static_document as sd ON sd.document_id = d.id
                            WHERE
                                sd.static_id = ".$tblData['id']."
                                AND d.delete_date IS NULL
                                AND d.is_active = 1 
                        ";

                        $_tblDocument = $objDb->getAll($strDocQuery);

                        if (!empty($_tblDocument)){
                            foreach ($_tblDocument as $numIdx=>$rowDocument) {
                                if (!empty($rowDocument['filename']) && is_file($CONF['pub_dir']."dokumentumtar/".$rowDocument['id'].(!empty($strLanguage) ? "/{$strLanguage}" : "")."/".$rowDocument['filename'])) {
                                    $_tblDocument[$numIdx]['filename'] = $CONF['base_url']."media/pub/dokumentumtar/".$rowDocument['id'].(!empty($strLanguage) ? "/{$strLanguage}" : "")."/".$rowDocument['filename'];
                                }
                            }
                        }

                        $this->_tblDocument = $_tblDocument;
                    }
                }
            }
            $this->_tblData = $tblData;
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("tblGallery", $this->_tblGallery);
                $objSmarty->assign("tblVideo", $this->_tblVideo);
                $objSmarty->assign("tblDocument", $this->_tblDocument);
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>