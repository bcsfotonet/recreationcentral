<?php 
    class Szerkesztobizottsag{
        // TODO legyen author ne staff vagy valamivel kulon valasztva
        private $_tblData;
        
        function __construct(){
            $this->_tblData = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl;
            
            $strQuery = "
                SELECT 
                     id
                    ,name
                    ,title
                    ,email
                    ,image
                    ,description
                FROM
                    staff
                WHERE
                    delete_date IS NULL
                    AND is_active = 1
                    AND type_id = 6
                ORDER BY
                    name
            ";
                
            $tblData = $objDb->getAll($strQuery);
            
            foreach ($tblData as $numIdx=>$rowData) {
                if (!empty($rowData['image']) && is_file($CONF['pub_dir']."szerzok/".$rowData['id']."/big/".$rowData['image'])) {
                    $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/szerzok/".$rowData['id']."/big/".$rowData['image'];
                } else {
                    $tblData[$numIdx]['image'] = $CONF['web_url']."images/user-placeholder.jpg";
                }
            }
            
            $this->_tblData = $tblData;
            
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>    