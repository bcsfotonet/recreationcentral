<?php 
    class Tanulmanyok{      
        private $_tblData;
        private $_strPageType;
        private $_tblStudyEditorStaff;
        private $_rowType;
        private $_rowTypeNew;
        private $_strYear;
        private $_tblYear;
        
        function __construct(){    
            $this->_tblData = array();
            $this->_tblStudyEditorStaff = array();
            $this->_strPageType = "lista";
            $this->_rowType = array();
            $this->_rowTypeNew = array();
            $this->_strYear = '';
            $this->_tblYear = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strQuery = "";
            $strYear = '';
            $strTypeQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowType = $objDb->getRow($strTypeQuery);
            
            $strTypeNewQuery = "
                SELECT
                    t.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name
                    ,t.url
                    ,t.url_en
                FROM 
                    type as t
                WHERE
                    -- t.id = 1
                    t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '$rowUrl[0]'
                    AND t.delete_date IS NULL
            ";
            $rowTypeNew = $objDb->getRow($strTypeNewQuery);
            if (!empty($rowUrl) && isset($rowUrl[1]) && !empty($rowUrl[1])) {
                // vég oldal
                $this->_strPageType = "veg";
                $strQuery = "
                    SELECT
                        s.`id`
                        -- ,(SELECT name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name FROM staff WHERE id = author_staff_id AND delete_date IS NULL AND is_active = 1) as author_name
                        -- ,(SELECT name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name FROM staff WHERE id = co_editor_staff_id AND delete_date IS NULL AND is_active = 1) as co_editor_name
                        -- ,(SELECT url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url FROM staff WHERE id = author_staff_id AND delete_date IS NULL AND is_active = 1) as author_url
                        -- ,(SELECT url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url FROM staff WHERE id = co_editor_staff_id AND delete_date IS NULL AND is_active = 1) as co_editor_url
                        ,s.`title`
                        ,s.`title_en`
                        ,s.`url`
                        ,s.`url_en`
                        ,s.`magazine".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS magazine
                        ,s.`authors".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS authors
                        ,s.`keywords".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS keywords
                        ,s.`doi`
                        ,s.`doi_url`
                        ,s.`abstract".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS abstract
                        -- ,s.`bibliography".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS bibliography
                        ,s.`lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS lead
                        ,s.`description".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS description
                        ,s.`image`
                    FROM 
                        `study` as s
                    WHERE 
                        s.`delete_date` IS NULL 
                        AND s.`is_active` = 1
                        AND s.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." = '{$rowUrl[1]}'
                    LIMIT 1
                ";
                        
                $tblData = $objDb->getRow($strQuery);
                
                if(!empty($tblData)){
                
                    if (!empty($tblData['image']) && is_file($CONF['pub_dir']."tanulmanyok/".$tblData['id']."/big/".$tblData['image'])) {
                        $tblData['image'] = $CONF['base_url']."media/pub/tanulmanyok/".$tblData['id']."/big/".$tblData['image'];
                    } else {
                        $tblData['image'] = $CONF['web_url']."images/study-placeholder.jpg";
                    }
                    $strStudyEditorQuery = "
                        SELECT
                            s.id
                            ,s.name
                            ,s.url
                        FROM
                            staff as s
                        JOIN
                            study_editor_staff ON staff_id = s.id
                        WHERE
                            s.delete_date IS NULL
                            AND s.is_active = 1
                            AND study_editor_staff.study_id = ".$tblData['id']
                    ;
                    
                    $tblStudyEditorStaff = $objDb->getAll($strStudyEditorQuery);
                    
                    $this->_tblStudyEditorStaff = $tblStudyEditorStaff;
                }else{
                    header("Location: {$CONF['base_url']}404");
                }
                
            } else {
                // lista oldal
                $strYearSql = '';
                
                $strYearQuery = "
                    SELECT DISTINCT
                        s.`year`
                    FROM 
                        `study` as s
                    WHERE 
                        s.`delete_date` IS NULL 
                        AND s.`is_active` = 1
                    ORDER BY
                        s.`year` DESC
                ";
                
                $tblYear = $objDb->getAll($strYearQuery);
//                $isYearConnected = $objDb->getOne($strYearQuery);
                
                if(isset($_GET['y']) && !empty($_GET['y'])){
                    $strYear = addslashes(trim(htmlspecialchars_decode($_GET['y'], ENT_QUOTES)));
                    if(!empty($strYear)){

                        $strYearSql = "AND year='$strYear'";
                    }
                }
//                    $numSearchLength = mb_strlen($strSearch, 'UTF-8');
//
//                    if($numSearchLength >= 3){
                $this->_strPageType = "lista";
                $strQuery = "
                    SELECT 
                        s.`id`
               		-- ,(SELECT name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name FROM staff WHERE id = author_staff_id AND delete_date IS NULL AND is_active = 1) as author_name
                        -- ,(SELECT url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url FROM staff WHERE id = author_staff_id AND delete_date IS NULL AND is_active = 1) as author_url
                        ,s.`title`
                        ,s.`title_en`
                        ,s.`url`
                        ,s.`url_en`
                        ,s.`magazine".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS magazine
                        ,s.`authors".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS authors
                        ,s.`doi`
                        ,s.`doi_url`
                        -- ,s.`abstract".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS abstract
                        -- ,s.`bibliography".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS bibliography
                        --  ,s.`lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS lead
                        -- ,s.`description".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS description
                        ,s.`image`
                    FROM 
                        `study` as s
                    WHERE 
                        s.`delete_date` IS NULL 
                        AND s.`is_active` = 1
                        $strYearSql
                    ORDER BY
                        s.`priority` ASC
                ";
                        
                $tblData = $objDb->getAll($strQuery);
                
                if(!empty($tblData)){
                    foreach ($tblData as $numIdx=>$rowData) {
                        if (!empty($rowData['image']) && is_file($CONF['pub_dir']."tanulmanyok/".$rowData['id']."/big/".$rowData['image'])) {
                            $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/tanulmanyok/".$rowData['id']."/big/".$rowData['image'];
                        } else {
                            $tblData[$numIdx]['image'] = $CONF['web_url']."images/study-placeholder.jpg";
                        }
                    }
                }
            }
            $this->_tblData = $tblData;
            $this->_rowType = $rowType;
            $this->_rowTypeNew = $rowTypeNew;
            $this->_strYear = $strYear;
            $this->_tblYear = $tblYear;
            
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;

            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
                $objSmarty->assign("rowType", $this->_rowType);
                $objSmarty->assign("rowTypeNew", $this->_rowTypeNew); 
            }
            
            if ($this->_strPageType == "veg") {
                //vég oldal
                $objSmarty->assign("tblStudyEditorStaff", $this->_tblStudyEditorStaff);
                $objSmarty->display("page".DIRECTORY_SEPARATOR."Tanulmanyok_veg.tpl");
            } else {
                //lista oldal
                $objSmarty->assign("strYear", $this->_strYear);
                $objSmarty->assign("tblYear", $this->_tblYear);
                $objSmarty->display($strTplPagePath);
            }

        }
    }
    