<?php 
    class Videok{
        private $_tblData;
        
        function __construct(){
            $this->_tblData = array();
        }
        
        private function getData(){
            global $CONF, $objDb, $rowUrl, $strLanguage;
            
            $strQuery = "
                SELECT 
                     v.id
                    ,v.title
                    ,v.code
                    ,v.description
                    ,v.image
                FROM
                    video as v
                JOIN
                    video_type as vt ON vt.video_id = v.id
                WHERE
                    v.delete_date IS NULL
                    AND v.is_active = 1
                    AND vt.type_id = 5
                ORDER BY
                    v.title
            ";
            
            $tblData = $objDb->getAll($strQuery);
            
            foreach ($tblData as $numIdx=>$rowData) {
                if (!empty($rowData['image']) && is_file($CONF['pub_dir']."video/".$rowData['id']."/big/".$rowData['image'])) {
                    $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/video/".$rowData['id']."/big/".$rowData['image'];
                } else {
                    $tblData[$numIdx]['image'] = 'http://img.youtube.com/vi/'.$rowData['code'].'/0.jpg';
                }
            }
            
            $this->_tblData = $tblData;
            
            return true;
        }
        
        public function run($strTplPagePath){
            global $objSmarty;
            
            if( $this->getData() === true ){
//                $objSmarty->assign("tblLoggedUserData", $this->_tblData);
                $objSmarty->assign("tblData", $this->_tblData);
            }

            $objSmarty->display($strTplPagePath);
        }
    }
?>