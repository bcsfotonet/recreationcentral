<?php
    function smarty_insert_end ($params, &$smarty) {
        global $CONF, $objDb, $objSmarty, $strLanguage;
        
        $tblFooterMenu = array();
        $tblFooterMenuTmp = $objDb->getAll("
            (
                SELECT 
                     m.id AS menu_id, m.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name, m.parent_menu_id, m.ext_url, null AS url, m.target_value, m.priority
                FROM 
                    menu AS m
                WHERE
                    m.delete_date IS NULL
                    AND m.is_active = 1
                    AND m.is_visible_footer_menu = 1
                    AND m.content_table IS NULL 
                    AND m.content_id IS NULL
                    AND (m.ext_url IS NOT NULL OR m.parent_menu_id IS NULL)
            ) 
            UNION 
            (
                SELECT 
                     m.id AS menu_id, m.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name, m.parent_menu_id, m.ext_url, t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url, m.target_value, m.priority
                FROM 
                    menu AS m 
                JOIN 
                    type AS t ON (m.content_table = 'type' AND m.content_id = t.id)
                WHERE
                    m.delete_date IS NULL
                    AND m.is_active = 1
                    AND m.is_visible_footer_menu = 1
                    AND m.ext_url IS NULL
                    AND t.delete_date IS NULL
                    AND t.is_active = 1
            )
            UNION
            (
                SELECT 
                     m.id AS menu_id, m.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name, m.parent_menu_id, m.ext_url, s.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url, m.target_value, m.priority
                FROM 
                    menu AS m 
                JOIN 
                    static AS s ON (m.content_table = 'static' AND m.content_id = s.id)
                WHERE
                    m.delete_date IS NULL
                    AND m.is_active = 1
                    AND m.is_visible_footer_menu = 1
                    AND m.ext_url IS NULL
                    AND s.delete_date IS NULL
                    AND s.is_active = 1
            )
            ORDER BY	
            	parent_menu_id, priority
        ");
        
//        var_dump($tblFooterMenu);die;
        
        if ($tblFooterMenuTmp !== FALSE) {
            foreach ($tblFooterMenuTmp as $rowFooterMenuTmp) {
                if (!is_numeric($rowFooterMenuTmp['parent_menu_id'])) {
                    $tblFooterMenu[$rowFooterMenuTmp['menu_id']]['id'] = $rowFooterMenuTmp['menu_id'];
                    $tblFooterMenu[$rowFooterMenuTmp['menu_id']]['name'] = $rowFooterMenuTmp['name'];
                    $tblFooterMenu[$rowFooterMenuTmp['menu_id']]['url'] = (!empty($rowFooterMenuTmp['url']) ? $CONF['base_url_lang'].$rowFooterMenuTmp['url'] : $rowFooterMenuTmp['ext_url']);
                    $tblFooterMenu[$rowFooterMenuTmp['menu_id']]['target_value'] = $rowFooterMenuTmp['target_value'];
                } else {
                    $tblFooterMenu[$rowFooterMenuTmp['parent_menu_id']]['child'][$rowFooterMenuTmp['menu_id']]['id'] = $rowFooterMenuTmp['menu_id'];
                    $tblFooterMenu[$rowFooterMenuTmp['parent_menu_id']]['child'][$rowFooterMenuTmp['menu_id']]['name'] = $rowFooterMenuTmp['name'];
                    $tblFooterMenu[$rowFooterMenuTmp['parent_menu_id']]['child'][$rowFooterMenuTmp['menu_id']]['url'] = (!empty($rowFooterMenuTmp['url']) ? $CONF['base_url_lang'].$rowFooterMenuTmp['url'] : $rowFooterMenuTmp['ext_url']);
                    $tblFooterMenu[$rowFooterMenuTmp['parent_menu_id']]['child'][$rowFooterMenuTmp['menu_id']]['target_value'] = $rowFooterMenuTmp['target_value'];
                }
            }
        }
        
        $objSmarty->assign("tblFooterMenu", $tblFooterMenu);
        
        $strTpl = $smarty->fetch('content/end.tpl');
        return $strTpl;
    }
