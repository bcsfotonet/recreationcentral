<?php
    function smarty_insert_header ($params, &$smarty) {
        global $CONF,$objSmarty,$rowLabel,$strLanguage;
        
        $searchLink = $CONF['base_url_lang'] . $rowLabel['kereses_url'];
        $forgottenPassLink = $CONF['base_url'] . 'elfelejtett-jelszo';

        if(isset($_SESSION['customer_id']) && !empty($_SESSION['customer_id'])){
            $loginText = 'Kijelentkezés';
            $loginLink = $CONF['base_url'] . '?kijelentkezes';
            $registerText = 'Profil';
            $registerLink = $CONF['base_url'] . 'profil';
            $orderLink = $CONF['base_url'] . 'rendeles';
        }else{
            $loginText = $rowLabel['bejelentkezes'];
            $loginLink = $CONF['base_url_lang'] . $rowLabel['bejelentkezes_url'] ;
            $registerText = $rowLabel['regisztracio'];
            $registerLink = $CONF['base_url_lang'] . $rowLabel['regisztracio_url'];
            $orderLink = $CONF['base_url_lang'] . $rowLabel['regisztracio_url'];
        }
        
        if (isset($_GET['kijelentkezes']) && customerIsLoggedIn()==true) {
            customerLogout();
        }
        
        $objSmarty->assign("searchLink", $searchLink);
        $objSmarty->assign("loginLink", $loginLink);
        $objSmarty->assign("loginText", $loginText);
        $objSmarty->assign("registerText", $registerText);
        $objSmarty->assign("registerLink", $registerLink);
        $objSmarty->assign("forgottenPassLink", $forgottenPassLink);
        $objSmarty->assign("customerIsLoggedIn", customerIsLoggedIn());
        $objSmarty->assign("rowLabel", $rowLabel);
        $objSmarty->assign("strLanguage", $strLanguage);
        $objSmarty->assign("orderLink", $orderLink);
        $strTpl = $smarty->fetch('content/header.tpl');
        return $strTpl;
    }
