<?php
    function smarty_insert_headline ($params, &$smarty) {
        
        if(!empty($params['title'])){
            $smarty->assign('strTitle', $params['title']);
        } else {
            $smarty->assign('strTitle', "");
        }
        if(!empty($params['firstli'])){
            $smarty->assign('strFirstLi', $params['firstli']);
        } else {
            $smarty->assign('strFirstLi', "");
        }
        if(!empty($params['firstliurl'])){
            $smarty->assign('strFirstLiUrl', $params['firstliurl']);
        } else {
            $smarty->assign('strFirstLiUrl', "");
        }
        if(!empty($params['secondli'])){
            $smarty->assign('strSecondLi', $params['secondli']);
        } else {
            $smarty->assign('strSecondLi', "");
        }
        if (!empty($params['secondliurl'])) {
            $smarty->assign('strSecondLiUrl', $params['secondliurl']);
        } else {
            $smarty->assign('strSecondLiUrl', "");
        }
        if(!empty($params['thirdli'])){
            $smarty->assign('strThirdLi', $params['thirdli']);
        } else {
            $smarty->assign('strThirdLi', "");
        }
        if (!empty($params['thirdliurl'])) {
            $smarty->assign('strThirdLiUrl', $params['thirdliurl']);
        } else {
            $smarty->assign('strThirdLiUrl', "");
        }
        if(!empty($params['selected'])){
            $smarty->assign('strSelected', $params['selected']);
        } else {
            $smarty->assign('strSelected', "");
        }
        
        $strTpl = $smarty->fetch('content/headline.tpl');
        return $strTpl;
    }
