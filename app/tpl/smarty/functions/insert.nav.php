<?php
    function smarty_insert_nav ($params, &$smarty) {
        global $CONF, $objDb, $objSmarty, $strLanguage;
        
        $tblMainMenu = array();
        $tblMainMenuTmp = $objDb->getAll("
            (
                SELECT 
                     m.id AS menu_id, m.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name, m.parent_menu_id, m.ext_url, null AS url, m.target_value, m.priority
                FROM 
                    menu AS m
                WHERE
                    m.delete_date IS NULL
                    AND m.is_active = 1
                    AND m.is_visible_main_menu = 1
                    -- AND m.is_visible_footer_menu = 1
                    AND m.content_table IS NULL 
                    AND m.content_id IS NULL
                    AND (m.ext_url IS NOT NULL OR m.parent_menu_id IS NULL)
            ) 
            UNION 
            (
                SELECT 
                     m.id AS menu_id, m.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name, m.parent_menu_id, m.ext_url, t.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url, m.target_value, m.priority
                FROM 
                    menu AS m 
                JOIN 
                    type AS t ON (m.content_table = 'type' AND m.content_id = t.id)
                WHERE
                    m.delete_date IS NULL
                    AND m.is_active = 1
                    AND m.is_visible_main_menu = 1
                    -- AND m.is_visible_footer_menu = 1
                    AND m.ext_url IS NULL
                    AND t.delete_date IS NULL
                    AND t.is_active = 1
            )
            UNION
            (
                SELECT 
                     m.id AS menu_id, m.name".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS name, m.parent_menu_id, m.ext_url, s.url".(!empty($strLanguage) ? "_{$strLanguage}" : "")." AS url, m.target_value, m.priority
                FROM 
                    menu AS m 
                JOIN 
                    static AS s ON (m.content_table = 'static' AND m.content_id = s.id)
                WHERE
                    m.delete_date IS NULL
                    AND m.is_active = 1
                    AND m.is_visible_main_menu = 1
                    -- AND m.is_visible_footer_menu = 1
                    AND m.ext_url IS NULL
                    AND s.delete_date IS NULL
                    AND s.is_active = 1
            )
            ORDER BY	
            	parent_menu_id, priority
        ");
        if ($tblMainMenuTmp !== FALSE) {
            foreach ($tblMainMenuTmp as $rowMainMenuTmp) {
                if (!is_numeric($rowMainMenuTmp['parent_menu_id']) && !empty($rowMainMenuTmp['name'])) {
                    $tblMainMenu[$rowMainMenuTmp['menu_id']]['id'] = $rowMainMenuTmp['menu_id'];
                    $tblMainMenu[$rowMainMenuTmp['menu_id']]['name'] = $rowMainMenuTmp['name'];
                    $tblMainMenu[$rowMainMenuTmp['menu_id']]['url'] = (!empty($rowMainMenuTmp['url']) ? $CONF['base_url_lang'].$rowMainMenuTmp['url'] : $rowMainMenuTmp['ext_url']);
                    $tblMainMenu[$rowMainMenuTmp['menu_id']]['target_value'] = $rowMainMenuTmp['target_value'];
                } elseif(!empty($rowMainMenuTmp['name'])) {
                    $tblMainMenu[$rowMainMenuTmp['parent_menu_id']]['child'][$rowMainMenuTmp['menu_id']]['id'] = $rowMainMenuTmp['menu_id'];
                    $tblMainMenu[$rowMainMenuTmp['parent_menu_id']]['child'][$rowMainMenuTmp['menu_id']]['name'] = $rowMainMenuTmp['name'];
                    $tblMainMenu[$rowMainMenuTmp['parent_menu_id']]['child'][$rowMainMenuTmp['menu_id']]['url'] = (!empty($rowMainMenuTmp['url']) ? $CONF['base_url_lang'].$rowMainMenuTmp['url'] : $rowMainMenuTmp['ext_url']);
                    $tblMainMenu[$rowMainMenuTmp['parent_menu_id']]['child'][$rowMainMenuTmp['menu_id']]['target_value'] = $rowMainMenuTmp['target_value'];
                }
            }
        }
//         print_r($tblMainMenu);
        $objSmarty->assign("tblMainMenu", $tblMainMenu);
        
        $strTpl = $smarty->fetch('content/nav.tpl');
        return $strTpl;
    }
