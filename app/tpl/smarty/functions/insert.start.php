<?php
    function smarty_insert_start ($params, &$smarty) {
        if(!empty($params['title'])){
            $smarty->assign('strTitle', $params['title']);
        } else {
            $smarty->assign('strTitle', "");
        }

        if(!empty($params['description'])){
            $smarty->assign('strDescription', $params['description']);
        } else {
            $smarty->assign('strDescription', "");
        }

        if(!empty($params['ogTitle'])){
            $smarty->assign('strOgTitle', $params['ogTitle']);
        } else {
            $smarty->assign('strOgTitle', "");
        }

        if(!empty($params['ogDescription'])){
            $smarty->assign('strOgDescription', $params['ogDescription']);
        } else {
            $smarty->assign('strOgDescription', "");
        }

        if(!empty($params['ogImage'])){
            $smarty->assign('strOgImage', $params['ogImage']);
        } else {
            $smarty->assign('strOgImage', "");
        }


        $strTpl = $smarty->fetch('content/start.tpl');
        return $strTpl;
    }
