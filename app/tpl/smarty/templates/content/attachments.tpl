<div class="container">
    <div class="row attachment justify-content-center">
        {if !empty($tblGallery)}
            {insert name="photoswipe"}
            <div class="col-12 col-md-6">
                <div class="attachment__title">{$rowLabel.galeria}</div>
                <div class="attachment__gallery gallery" itemscope itemtype="https://schema.org/ImageGallery">
                    {foreach from=$tblGallery key=numIdx item=rowGallery}
                        <figure itemprop="associatedMedia" itemscope class="gallery__item" itemtype="https://schema.org/ImageObject">
                            <a href="{$rowGallery.original_image}" itemprop="contentUrl" id="gallery-{$rowGallery.gallery_id}" data-size="{$rowGallery.original_size}">
                                <img src="{$rowGallery.big_image}" itemprop="thumbnail" alt="" />
                            </a>
                        </figure>
                    {/foreach}
                </div>
            </div>
        {/if}
        {*if !empty($tblData.gallery_title)}
            <div class="attachment__caption">{$tblData.gallery_title}</div>
        {/if*}
        {if !empty($tblVideo.code)}
            <div class="col-12 col-md-6">
                <div class="attachment__title">{$rowLabel.video}</div>
                <div class="attachment__video">
                    <iframe src="https://www.youtube.com/embed/{$tblVideo.code}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    {*
                    <iframe width="100%" height="315px" src="https://www.youtube.com/embed/{$tblVideo.code}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    *}
                </div>
            </div>
        {/if}
        {if !empty($tblDocument)}
            <div class="col-12">
                <div class="attachment__title">{$rowLabel.dokumentumok}</div>
                {foreach from=$tblDocument item=rowDocument}
                    {if !empty($rowDocument.filename)}
                        <div class="document">
                            {if !empty($rowDocument.title)}
                                <span class="document__title">{$rowDocument.title}</span>
                            {/if}
                            {if !empty($rowDocument.lead)}
                                <p class="document__desc">
                                    {$rowDocument.lead}
                                </p>
                            {/if}
                            <a class="document__download" href="{$rowDocument.filename}">{$rowLabel.letoltes}</a>
                        </div>
                    {/if}
                {/foreach}
            </div>
        {/if}
    </div>
</div>