			
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 pb20">
                            <div class="footer__title">{$rowLabel.elerhetosegi_adatok}:</div>
                            <div class="footer__text">
                                <p>Közép-Kelet-Európai Rekreációs Társaság</p>
                                <p>{$rowLabel.roviditett_nev}: KERT</p>
                                <p>{$rowLabel.szekhely}: 6723 Szeged, Csaba utca 48/A</p>
                                <p>{$rowLabel.levelezesi_cim}: 6723 Szeged, Csaba utca 48/A</p>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-5 pb20">
                            <div class="footer__title">{$rowLabel.tovabbi_tartalom}</div>
                            {if !empty($tblFooterMenu)}
                                <ul class="footer__list">
                                    {foreach from=$tblFooterMenu item=rowFooterMenu}
                                        <li class="footer__li"><a {if !empty($rowFooterMenu.url)}href="{$rowFooterMenu.url}" target="{if !empty($rowFooterMenu.target_value)}{$rowFooterMenu.target_value}{else}_self{/if}"{/if}>{$rowFooterMenu.name}</a></li>
                                    {/foreach}
                                </ul>
                            {/if}
                        </div>
                        <div class="col-12 col-md-3 pb20">
                            <div class="footer__title">{$rowLabel.kozossegi_media}</div>
                            <a href="https://www.youtube.com/channel/UCghtAX1H_Wsyd7hEqxJ_yQg?feature=watch" target="_blank" class="footer__icon">
                                <img src="{$CONF.web_url}images/youtube.svg" alt="Youtube" >
                            </a>
                            <a href="https://www.facebook.com/rekreacio" target="_blank" class="footer__icon">
                                <img src="{$CONF.web_url}images/facebook.svg" alt="Facebook" >
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 footer__made-by">
                            <a href="https://bcsfotonet.hu" target="_blank">
                                <img src="{$CONF.web_url}images/bcs-logo.svg" alt="BCs FotoNet">
                            </a>
                        </div>
                    </div>
                </div>
            </footer>

        </div>
        <div id="goToTop" class="goToTop"></div>
        {literal}
            <script>
                var strBaseUrl = '{/literal}{$CONF.base_url}{literal}';
            	var strWebUrl = '{/literal}{$CONF.web_url}{literal}';
                var strXhrUrl = '{/literal}{$CONF.base_url}xhr/{literal}';
                var strBaseLang = '{/literal}{$strLanguage}{literal}';
            </script>
        {/literal}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="{$CONF.web_url}vendor/vendor.js"></script>
        <script src="{$CONF.web_url}js/main.js"></script>
    </body>
</html>