<!-- Global site tag (gtag.js) - Google Analytics -->

{literal}
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136971372-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-136971372-1');
    </script>
{/literal}