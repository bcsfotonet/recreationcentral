<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-4">
                <a href="{$CONF.base_url_lang}">
                    <img src="{$CONF.web_url}images/logo.png" alt="Közép-Kelet-Európai Rekreációs Társaság" class="header__logo">
                </a>
                <div class="header__slogen">
                    {$rowLabel.idezet}
                </div>
                <div class="mobil-nav">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="col-md-8">
                {*
                    A masodik utemben lesznek
                *}
                <div class="row mt10 mb10 justify-content-center justify-content-md-end"> 
                    {if empty($strLanguage)}
                        <a href="{if !empty($loginLink)} {$loginLink} {/if}" class="header__link"> 
                            <img src="{$CONF.web_url}images/login.svg" alt="{if !empty($loginText)}{$loginText}{else}Bejelentkezés{/if}" class="header__icon">
                            {if !empty($loginText)}{$loginText}{else}{$rowLabel.bejelentkezes}{/if}
                        </a>
                        <a href="{if !empty($registerLink)} {$registerLink} {/if}" class="header__link">
                            <img src="{$CONF.web_url}images/reg.svg" alt="{if !empty($registerText)}{$registerText}{else}Regisztráció{/if}">
                            {if !empty($registerText)}{$registerText}{else}{$rowLabel.regisztracio}{/if}
                        </a>
                    {/if}
                    <div class="language-selector">
                        <a href="{$CONF.base_url}"><img src="{$CONF.web_url}images/hungary.svg" alt="Magyar nyelv"></a>
                        <a href="{$CONF.base_url}en"><img src="{$CONF.web_url}images/angol.svg" alt="Angol nyelv"></a>
                        <a href="{$CONF.base_url}ro"><img src="{$CONF.web_url}images/roman.svg" alt="Román nyelv"></a>
                    </div>
                </div>      
                <div class="row justify-content-center justify-content-md-end">
                    <div class="form__item">
                        <form class="form__group" method="get" action="{if !empty($searchLink)} {$searchLink} {/if}">
                            <input type="text" name="{$rowLabel.keresett_szoveg}" placeholder="{$rowLabel.kereses}..." />
                        </form>
                    </div>
                {if $customerIsLoggedIn==true && empty($strLanguage)}  
                    <a href="{$orderLink}" class="button">
                        <img src="{$CONF.web_url}images/basket.svg" alt="Rendelés">
                        Rendelés
                    </a>
                {/if}
                </div>
            </div>
        </div>
    </div>
</header>