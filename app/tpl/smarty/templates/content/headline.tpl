<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="headline">
                <div class="headline__title">{$strTitle}</div>
                <ul class="breadcrumb">
                	{if !empty($strFirstLi)}
                    <li class="breadcrumb__li">
                        <a {if !empty($strFirstLiUrl)}href="{$strFirstLiUrl}"{/if}>{$strFirstLi}</a>
                    </li>
                    {/if}
                    {if !empty($strSecondLi)}
                    <li class="breadcrumb__li">
                        <a {if !empty($strSecondLiUrl)}href="{$strSecondLiUrl}"{/if}>{$strSecondLi}</a>
                    </li>
                    {/if}
                    {if !empty($strThirdLi)}
                    <li class="breadcrumb__li">
                        <a {if !empty($strThirdLiUrl)}href="{$strThirdLiUrl}"{/if}>{$strThirdLi}</a>
                    </li>
                    {/if}
                    <li class="breadcrumb__li breadcrumb__li--selected">{$strSelected}</li>
                </ul>
            </div>
        </div>
    </div>
</div>