<div class="container">
    <div class="row justify-content-center align-items-center">
        <div class="col-6 col-sm-4">
            <div class="kert-logos">
                <a href="javascript:void(false);" target="_blank" style="cursor: default">
                    <img src="{$CONF.web_url}images/yourrecreation.png" alt="Yourrecreation">
                </a>
            </div>
        </div>
        <div class="col-6 col-sm-4">
            <div class="kert-logos">
                <a href="https://egeszsegfejlesztes.eu/" target="_blank">
                    <img src="{$CONF.web_url}images/egfejl.svg" alt="Egeszségfejlesztés">
                </a>
            </div>
        </div>
        <div class="col-8 col-sm-4">
            <div class="kert-logos">
                <a href="https://www.szechenyi2020.hu" target="_blank">
                    <img src="{$CONF.web_url}images/eu-esba.png" alt="Széchenyi" class="kert-logos__esba">
                </a>
                <a href="https://www.szechenyi2020.hu/" target="_blank">
                    <img src="{$CONF.web_url}images/eu-esba-mobile.png" alt="Széchenyi" class="kert-logos__esba-mobil">
                </a>
            </div>
        </div>
    </div>
</div>