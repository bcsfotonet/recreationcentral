<div class="nav">
    {if !empty($tblMainMenu)}
        <ul class="nav__list">
            {foreach from=$tblMainMenu item=rowMainMenu}
                {if !empty($rowMainMenu.name)}
                    <li class="nav__li {if !empty($rowMainMenu.child)}nav__parent-nav js-sub-nav{/if}">
                        <a {if !empty($rowMainMenu.url)}href="{$rowMainMenu.url}" target="{if !empty($rowMainMenu.target_value)}{$rowMainMenu.target_value}{else}_self{/if}"{/if}>{$rowMainMenu.name}</a>
                        {if !empty($rowMainMenu.child)}
                            <ul class="nav__sub-nav js-sub-list">
                                {foreach from=$rowMainMenu.child item=rowChildMenu}
                                    {if !empty($rowChildMenu.name)}
                                        <li class="nav__li">
                                            <a {if !empty($rowChildMenu.url)}href="{$rowChildMenu.url}" target="{if !empty($rowChildMenu.target_value)}{$rowChildMenu.target_value}{else}_self{/if}"{/if}>{$rowChildMenu.name}</a>
                                        </li>
                                    {/if}
                                {/foreach}
                            </ul>
                        {/if}
                    </li>
                {/if}
            {/foreach}
        </ul>
    {/if}
</div>