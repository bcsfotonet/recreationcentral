<hr>
{if !empty($tblPartnerData)}
    <div class="container">
        <div class="partners">
            <div class="row">               
                <div class="col-12 col-md-3">
                    <h3 class="partners__title">{$rowLabel.partnereink}</h3>
                </div>
                {*
                    <div class="col-12 col-md-9">
                        <div class="partners__desc">
                            <p>Partner stat szöveg</p>
                            <hr>
                        </div>
                    </div>
                *}
            </div>
            <div class="row justify-content-center align-items-center mt40">
                <div class="col-12">
                    <div class="partners__wrapper">
                        {foreach from=$tblPartnerData item=rowPartnerData}
                            <a {if !empty($rowPartnerData.ext_url)} href="{$rowPartnerData.ext_url}" target="{if !empty($rowPartnerData.target_value)}{$rowPartnerData.target_value}{else}_blank{/if}" {/if} class="partners__item" title="{$rowPartnerData.name}">
                                <img src="{$rowPartnerData.image}" alt="{$rowPartnerData.name}">
                            </a> 
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>                
{/if}


