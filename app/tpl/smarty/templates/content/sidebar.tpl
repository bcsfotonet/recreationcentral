<div class="two-part__sidebar">
    <div class="sidebar-box">
        <a href="{$CONF.base_url_lang}{$rowLabel.esemenynaptar_url}" class="sidebar-box__title"><i class="fas fa-calendar"></i>{$rowLabel.esemenynaptar}</a>
    </div>
    <div class="sidebar-box">
        <a href="{$CONF.base_url_lang}{$rowLabel.kozlesi_feltetelek_url}" class="sidebar-box__title"><i class="far fa-file-alt"></i>{$rowLabel.kozlesi_feltetelek}</a> 
    </div>

    {*
        <div class="sidebar-box">
            <div class="sidebar-box__title">Eseménynaptár</div>
            <div id="my-calendar"></div>
        </div>
        <div class="sidebar-box">
            <div class="sidebar-box__title">Magazin</div>
            <div class="sidebar-box__magazin">
                <img src="{$CONF.web_url}images/magazin.png" alt="">
            </div>
        </div>
        <div class="sidebar-box">
            <div class="sidebar-box__title">Közlési feltételek</div>
        </div>
    *}
</div>