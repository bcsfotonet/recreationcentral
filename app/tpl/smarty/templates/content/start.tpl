<!DOCTYPE html>
<html lang="hu">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="format-detection" content="telephone=no">
        <meta name="description" content="{$strDescription}">

        {* Open graph meta data *}
        <meta property="og:image" content="{$strOgImage}" />
        <meta property="og:title" content="{$strOgTitle}" />
        <meta property="og:description" content="{$strOgDescription}" />

        {* Style import *}
        <link rel="stylesheet" href="{$CONF.web_url}vendor/vendor.css">
        <link rel="stylesheet" href="{$CONF.web_url}css/main.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/default-skin/default-skin.min.css">

        {* Apple icons *}
        <link rel="apple-touch-icon" sizes="76x76" href="{$CONF.web_url}images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="{$CONF.web_url}images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="{$CONF.web_url}images/favicon/favicon-16x16.png">
        <link rel="manifest" href="{$CONF.web_url}images/favicon/site.webmanifest">
        <link rel="mask-icon" href="{$CONF.web_url}images/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">


        <title>{$strTitle}</title>
        {insert name="hotjar"}
        {insert name="analytics"}
    </head>
    <body>
        <div class="wrapper">
    