
{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Alapdokumentumok"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content">
            {insert name="headline" 
            title="`$rowType.name`"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.tarsasag`"
            secondliurl=""
            selected="`$rowType.name`"}
            <div class="container">
                <div class="row">
                    {if !empty($tblData)}
                        {foreach from=$tblData item=rowData}
                            {if !empty($rowData.filename)}
                                <div class="col-12 col-md-6">
                                    <div class="document">
                                        {if !empty($rowData.title)}
                                            <span class="document__title">{$rowData.title}</span>
                                        {/if}
                                        {if !empty($rowData.lead)}
                                            <p class="document__desc">
                                                {$rowData.lead}
                                            </p>
                                        {/if}
                                        {if !empty($rowData.filename)}
                                            <a class="document__download" href="{$rowData.filename}">Letöltés</a>
                                        {/if}
                                    </div>
                                </div>
                            {/if}
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
        
    {insert name="end"}