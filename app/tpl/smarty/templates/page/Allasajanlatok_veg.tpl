
{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description="`$tblData.lead`"
    ogTitle="`$tblData.title`"
    ogDescription="`$tblData.lead`"
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
        {if !empty($tblData.gallery_id)}
            {insert name="photoswipe"}
        {/if}

    <div class="content">
        {if !empty($tblData.title)}
            {assign var="selected_title" value=$tblData.title|truncate:30}        
        {else}
            {assign var="selected_title" value="Hír"}
        {/if}    
        {insert 
            name = "headline" 
            title = "`$rowType.name`"
            firstli = "`$rowLabel.nyitooldal`"
            firstliurl = $CONF.base_url_lang
            secondli = "`$rowLabel.tarsasag`"
            secondliurl = ""
            thirdli = "`$rowType.name`"
            thirdliurl = "`$CONF.base_url_lang``$rowType.url`"
            selected = $selected_title
        }   
        {if !empty($tblData)}
            <div class="container">         
                <div class="row">
                    <div class="col-12">
                        <div class="carrier">                   
                            <div class="carrier__title">{$tblData.title}</div>
                            {if !empty($tblData.end_date)}
                                <div class="carrier__time"><b>Jelentkezési határidő:</b> {if !empty($tblData.end_date)}{$tblData.end_date|date_format:"%Y/%m/%d %H:%M"}{/if}</div>
                            {/if}
                            {if !empty($tblData.lead)}
                                <div class="carrier__lead">
                                    {$tblData.lead}
                                </div>
                            {/if}
                            {if !empty($tblData.description)}
                                <div class="editor-text stat-text">
                                    {$tblData.description}
                                </div>
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
        {/if}
    </div>

{insert name="end"}