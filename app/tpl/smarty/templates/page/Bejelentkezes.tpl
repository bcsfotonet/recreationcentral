
{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Bejelentkezés"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
    {insert name="header"}
    {insert name="nav"}
                        
    <div class="content">
        {insert name="headline" 
        title="`$tblData.name`"
        firstli="`$rowLabel.nyitooldal`"
        firstliurl=$CONF.base_url_lang
        secondli=""
        secondliurl=""
        selected="`$tblData.name`"}

        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    {if !empty($tblData.static_description)}
                        <div class="stat-text">
                            {$tblData.static_description}
                        </div>
                    {/if}
                </div>
                <div class="col-12 col-md-6">
                    <form class="form" method="post" action="">
                        {if !empty($tblFormError.loginError)}
                            <div class="form__item">
                                <div class="form__alert">{$tblFormError.loginError}</div>
                            </div>
                        {/if}
                        <div class="form__row">
                            <div class="form__item mx-width300">
                                <label>E-mail cím *</label>
                                <div class="form__group">
                                    <input type="text" name="email" maxlength="250" placeholder="E-mail cím" {if !empty($tblFormData.email)}value="{$tblFormData.email}"{/if} />
                                </div>
                                <div class="form__alert">{if !empty($tblFormError.email)}{$tblFormError.email}{/if}</div>
                            </div>
                        </div>
                        <div class="form__row">
                            <div class="form__item mx-width300">
                                <label>Jelszó *</label>
                                <div class="form__group">
                                    <input type="password" name="pass" maxlength="50" placeholder="Jelszó" {if !empty($tblFormData.pass)}value="{$tblFormData.pass}"{/if} />
                                </div>
                                <div class="form__alert">{if !empty($tblFormError.pass)}{$tblFormError.pass}{/if}</div>
                            </div>
                        </div>
                        <div class="form__row">
                            <div class="form__item">
                                <input type="submit" value="Belépés" class="button">
                            </div>
                        </div>
                    </form>
                    {*{if !empty($forgottenPassLink)}
                        <a href="{$forgottenPassLink}" class="link">Elfelejtett jelszó</a>
                    {/if}*}
                </div>
            </div>
        </div>
    </div>
        
    {insert name="end"}