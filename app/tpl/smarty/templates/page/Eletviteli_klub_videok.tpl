
{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Életviteli klub videók"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content">
            {insert name="headline"
            title="`$rowType.name`"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.media`"
            secondliurl=""
            selected="`$rowType.name`"}
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <form action="">
                            <div class="form__item mx-width300">
                                <div class="form__group" data-type="14">
                                    <select name="" id="" class="js-example-basic-single">
                                        <option value="0">{$rowLabel.minden_varos}</option>
                                        {if !empty($tblCityData)}
                                            {foreach from=$tblCityData item=rowCityData}
                                                <option value="{$rowCityData.id}">{$rowCityData.name}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="changingDataContainerId" class="row">
                    {if !empty($tblData)}
                        {foreach from=$tblData item=rowData}
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="video">
                                    {if !empty($rowData.code)}
                                        <div class="video__frame">
                                            <iframe width="100%" height="200" src="https://www.youtube.com/embed/{$rowData.code}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        </div>
                                    {/if}
                                    {if !empty($rowData.title)}
                                        <div class="video__title">{$rowData.title}</div>
                                    {/if}
                                    {if !empty($rowData.description)}
                                        <div class="video__description">{$rowData.description}</div>
                                    {/if}
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
        
    {insert name="end"}