
{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Elfelejtett jelszó"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
    {insert name="header"}
    {insert name="nav"}
                        
    <div class="content">
        {insert name="headline" 
        title="`$tblData.name`"
        firstli="`$rowLabel.nyitooldal`"
        firstliurl=$CONF.base_url_lang
        secondli=""
        secondliurl=""
        selected="`$tblData.name`"}

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    {if !empty($tblFormData)}
                        {if !empty($tblData.static_description)}
                            <div class="stat-text">
                                {$tblData.static_description}
                            </div>
                        {/if}              
                        <form class="form" method="post" action="">
                            <div class="form__row">
                                <div class="form__item mx-width300" style="margin: 0 auto 15px;">
                                    <label>E-mail cím</label>
                                    <div class="form__group">
                                        <input type="text" name="email" maxlength="250" placeholder="E-mail cím" {if !empty($tblFormData.email)}value="{$tblFormData.email}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.email)}{$tblFormError.email}{/if}</div>
                                </div>
                            </div>
                            <div class="form__row">
                                <div class="form__item">
                                    <input type="submit" value="Elküldés" class="button" style="margin: 0 auto;">
                                </div>
                            </div>
                        </form>
                    {else}
                        <div class="successfull-text" style="font-size:24px; color:green; text-align:center; margin:2em auto; line-height:1.6;">
                            {if !empty($strSuccessText)}{$strSuccessText}{else}Sikeres küldés!{/if}
                        </div>
                    {/if}
                    {if !empty($loginLink)}
                        <a href="{$loginLink}" class="link">Vissza a bejelentkezéshez</a>                     
                    {/if}                  
                </div>
            </div>
        </div>
    </div>
        
    {insert name="end"}