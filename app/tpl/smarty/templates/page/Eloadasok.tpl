{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Előadások"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content">
            {insert name="headline" 
            title="`$rowType.name`"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.tarsasag`"
            secondliurl=""
            selected="`$rowType.name`"}
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <form action="">
                            <div class="form__item">
                                <div class="form__group" data-type="20">
                                    <select class="js-example-basic-single">
                                        <option value="0">Minden város</option>
                                        {if !empty($tblCityData)}
                                            {foreach from=$tblCityData item=rowCityData}
                                                <option value="{$rowCityData.id}">{$rowCityData.name}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="changingDataContainerId" class="row">
                    {if !empty($tblData)}
                        {foreach from=$tblData item=rowData}
                            <div class="col-12 col-md-6">
                                <div class="lecture">
                                    <div class="lecture__title">{$rowData.name}{if !empty($rowData.name) && !empty($rowData.title)} - {/if}{$rowData.title}</div>
                                    <hr>
                                    <div class="lecture__date">{if !empty($rowData.city_name)}<b>{$rowData.city_name}</b>{/if}{if !empty($rowData.start_date)}{$rowData.start_date|date_format:"%Y/%m/%d %H:%M"}{/if}</div>
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
        
    {insert name="end"}