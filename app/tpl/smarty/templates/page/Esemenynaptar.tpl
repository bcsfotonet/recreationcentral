{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Eseménynaptár"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
    {insert name="header"}
    {insert name="nav"}

    <div class="content">
        {insert name="headline"
        title="`$rowType.name`"
        firstli="`$rowLabel.nyitooldal`"
        firstliurl=$CONF.base_url_lang
        secondli=""
        secondliurl=""
        selected="`$rowType.name`"}
        <div class="container">
            <div class="row">
                <div class="col-12 calendar js-calendar">
                    <div class="calendar__header">
                        <div class="calendar__prev js-calendar__prev">
                            <img src="{$CONF.web_url}images/prev.svg" alt="Előző">
                        </div>
                        <div class="calendar__next js-calendar__next">
                            <img src="{$CONF.web_url}images/next.svg" alt="Következő">
                        </div>
                        <div class="calendar__month js-calendar__title">&nbsp;</div>
                    </div>
                    <div id="js-calendar__view">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    {insert name="end"}