{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description="`$firstImageData.description`"
    ogTitle="`$firstImageData.title`"
    ogDescription="`$firstImageData.description`"
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
        {insert name="photoswipe"}

    <div class="content">
        {if !empty($firstImageData.title)}
            {assign var="selected_title" value=$firstImageData.title|truncate:30} 
        {else}
            {assign var="selected_title" value="Galéria"}
        {/if}   
        
        {insert 
            name = "headline" 
            title = "`$rowType.name`"
            firstli = "`$rowLabel.nyitooldal`"
            firstliurl = $CONF.base_url_lang
            secondli = "`$rowLabel.media`"
            secondliurl = ""
            thirdli = "`$rowType.name`"
            thirdliurl = "`$CONF.base_url_lang``$rowType.url`"
            selected = $selected_title
        }   
        {if !empty($tblData)}
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12">                       
                        <div class="gallery text-center">
                            {if !empty($firstImageData.title)}
                                <div class="gallery__title">{$firstImageData.title}</div>
                            {/if}
                            {if !empty($firstImageData.description)}
                                <div class="gallery__desc">{$firstImageData.description}</div>
                            {/if}
                        </div>                            
                        <div class="gallery text-center" itemscope itemtype="https://schema.org/ImageGallery">
                            {foreach from=$tblData key=i item=rowData}
                                {if !empty($rowData.image)}
                                    <figure itemprop="associatedMedia" itemscope  class="gallery__img" itemtype="https://schema.org/ImageObject">
                                        <a {if !empty($rowData.image_original_url)}href="{$rowData.image_original_url}"{/if} itemprop="contentUrl" data-size="{$rowData.image_size}">
                                            <img {if !empty($rowData.image_url)}src="{$rowData.image_url}"{/if} itemprop="thumbnail" alt="{$rowData.title}-{$i}" />
                                        </a>
                                    </figure>
                                {/if}
                            {/foreach}
                        </div>                     
                    </div>
                </div>
            </div>
        {/if}
    </div>

{insert name="end"}