{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Gyakran ismételt kérdések"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content">
            {insert 
                name="headline" 
                title="`$rowStaticData.name`"
                firstli="`$rowLabel.nyitooldal`"
                firstliurl=$CONF.base_url_lang
                secondli=""
                secondliurl=""
                selected="`$rowStaticData.name`"
            }
            {if !empty($tblData)}
                <div class="container">
                    <div class="row justify-content-center">
                        {*
                        regi sitebuild
                        if !empty($rowStaticData.static_description)}
                            <div class="col-12 col-md-5 stat-text">
                                {$rowStaticData.static_description}
                            </div>
                        {/if
                        <div class="col-12 col-md-7 faq">
                        *}
                        <div class="col-12 col-md-8 faq">                   
                            {foreach from=$tblData item=rowData}
                                {if !empty($rowData.question)}
                                    <div class="faq__question js-question">
                                        {$rowData.question}
                                    </div>
                                    <div class="faq__answer editor-text">
                                        {$rowData.answer}
                                    </div>
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                </div>
            {/if}
        </div>
        
    {insert name="end"}