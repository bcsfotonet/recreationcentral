{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Főoldal"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}                       
        
    <div class="content">
        {insert name="headline"
            title="`$rowLabel.kiemelt_hirek`"
        }
        <div class="two-part">
            <div class="two-part__main">
                {if !empty($tblNewsData)}
                    {foreach from=$tblNewsData item=rowNewsData}
                        {if !empty($rowNewsData.title)}
                            <div class="col-12 col-sm-6">
                                 <div class="news-item">
                                     <div class="news-item__image" style="background-image: url('{$rowNewsData.image}')"></div>
                                    <span class="news-item__title">{$rowNewsData.title}</span>
                                    {if !empty($rowNewsData.start_date)}
                                        <span class="news-item__date">{$rowNewsData.start_date|date_format:"%Y/%m/%d %H:%M"}{if !empty($rowNewsData.end_date)} - {$rowNewsData.end_date|date_format:"%Y/%m/%d %H:%M"}{/if}</span>
                                    {/if}
                                    {if !empty($rowNewsData.lead)}
                                        <p class="news-item__lead">
                                            {$rowNewsData.lead}
                                        </p>
                                    {/if}
                                    {if !empty($rowNewsData.url)}
                                        <a href="{$CONF.base_url_lang}{$rowNewsData.type_url}/{$rowNewsData.url}" class="news-item__more">{$rowLabel.reszletek}</a>
                                    {/if}
                                 </div>
                            </div>
                        {/if}
                    {/foreach}
                {/if}
            </div>
            {insert name="sidebar"}
        </div>
        {insert name="partners"}
    </div>
        {insert name="kert"}
        {insert name="end"}