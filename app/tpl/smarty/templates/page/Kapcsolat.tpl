{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Kapcsolat"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
    {insert name="header"}
    {insert name="nav"}
                        
    <div class="content">
        {insert name="headline" 
        title="`$tblData.name`"
        firstli="`$rowLabel.nyitooldal`"
        firstliurl=$CONF.base_url_lang
        secondli=""
        secondliurl=""
        selected="`$tblData.name`"}

        <div class="container">
            <div class="row justify-content-center">
                {if !empty($tblData.static_description)}
                    <div class="col-12 col-md-5">
                        <div class="editor-text stat-text">
                            {$tblData.static_description}
                        </div>
                    </div>
                {/if}
                <div class="col-12 col-md-7 map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2758.7979296237268!2d20.1502987155841!3d46.25424477911818!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4744886d6554bcb5%3A0x838b37c2d396c341!2sSzeged%2C+D%C3%B3zsa+u.+2%2C+6720!5e0!3m2!1shu!2shu!4v1542447484974" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
        
    {insert name="end"}