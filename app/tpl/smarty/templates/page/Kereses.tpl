{insert
    name = "start"
    title = "Közép-Kelet-Európai Rekreációs Társaság"
    description = ""
    ogTitle="Keresés"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
    {insert name = "header"}
    {insert name = "nav"}
                        
    <div class="content">
        {insert name = "headline" 
        title = "`$rowLabel.a_kereses_eredmenye`"
        firstli = "`$rowLabel.nyitooldal`"
        firstliurl = $CONF.base_url_lang
        secondli = ""
        secondliurl = ""
        selected = "`$rowLabel.a_kereses_eredmenye`"}

        <div class="container">
            <div class="row">
                {if !empty($tblData) && $numSearchLength >= 3}
                    {foreach from=$tblData item=rowData}
                        <div class="col-12 col-sm-6 col-lg-4">
                            <div class="news-item">
                                <img class="news-item__image" src="{$rowData.image}" alt="{$rowData.title}">
                                <span class="news-item__title">{$rowData.title}</span>
                                {if !empty($rowData.start_date)}
                                    <span class="news-item__date">{$rowData.start_date|date_format:"%Y/%m/%d %H:%M"}{if !empty($rowData.end_date)} - {$rowData.end_date|date_format:"%Y/%m/%d %H:%M"}{/if}</span>
                                {/if}
                                {if !empty($rowData.lead)}
                                    <p class="news-item__lead">
                                        {$rowData.lead}
                                    </p>
                                {/if}
                                {if !empty($rowData.url)}
                                    <a href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}" class="news-item__more">{$rowLabel.reszletek}</a>
                                {/if}
                            </div>
                        </div>
                    {/foreach}
                {elseif $numSearchLength < 3} 
                    <div class="col-12 col-sm-6 col-lg-4">
                        {$rowLabel.minimum_3_karakter}
                    </div>
                {else}
                    <div class="col-12 col-sm-6 col-lg-4">
                        {$rowLabel.nem_talalhato}
                    </div>
                {/if}
            </div>
        </div>
    </div>
        
    {insert name = "end"}