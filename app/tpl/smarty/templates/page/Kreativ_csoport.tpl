{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Kreatív csoport"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content">
            {insert name="headline" 
            title="`$rowType.name`"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.tarsasag`"
            secondliurl=""
            selected="`$rowType.name`"}
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <form action="">
                            <div class="form__item mx-width300">
                                <div class="form__group" data-type="16">
                                    <select class="js-example-basic-single">
                                        <option value="0">{$rowLabel.minden_varos}</option>
                                        {if !empty($tblCityData)}
                                            {foreach from=$tblCityData item=rowCityData}
                                                <option value="{$rowCityData.id}">{$rowCityData.name}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="changingDataContainerId" class="row">
                    {if !empty($tblData)}
                        {foreach from=$tblData item=rowData}
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="staff">
                                    <a class="staff__img" style="cursor:default;">
                                        {* 
                                            staff/{$rowData.id}/{$rowData.image} - ha majd fel lesz töltve a kép a megfelelő mappába
                                            <img src="{$CONF.web_url}images/staff.png" alt="">
                                        *}
                                        <img src="{$rowData.image}" alt="{$rowData.name}">
                                    </a>
                                    <span class="staff__name">{$rowData.name}</span>
                                    <span class="staff__titulus">{$rowData.title}</span>
                                    {if !empty($rowData.city_name)}
                                        <span class="staff__city">{$rowData.city_name}</span>
                                    {/if}
                                    <span class="staff__mail">
                                        <a href="mailto:{$rowData.email}">{$rowData.email}</a>
                                    </span>
                                    {if !empty($rowData.url) && !empty($rowData.description_strip)}
                                        <a href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}" class="staff__more">{$rowLabel.reszletek}</a>
                                    {/if}
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
        
    {insert name="end"}