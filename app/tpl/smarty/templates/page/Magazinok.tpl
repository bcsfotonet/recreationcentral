{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Magazinok"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}

{insert name="header"}
{insert name="nav"}

<div class="content">
    {insert name="headline" 
    title="`$rowType.name`"
    firstli="`$rowLabel.nyitooldal`"
    firstliurl=$CONF.base_url_lang
    secondli="`$rowLabel.recreation_tudomanyos_magazin`"
    secondliurl=""
    selected="`$rowType.name`"}
    <div class="two-part">
        <div class="two-part__main">
            <div class="container">
                {if !empty($tblData)}
                    {foreach from=$tblData item=tblDataTmp key=strIdx} 
                        <div class="row">
                            <div class="col-12">
                                <div class="magazin-year">{$strIdx}</div>
                            </div>
                        </div>
                        <div class="row">
                            {foreach from=$tblDataTmp item=rowData}
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <div class="magazin">
                                        <a href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}" class="magazin__title">{$rowData.issue_title}</a>
                                        <div class="magazin__img">
                                            {if !empty($rowData.orderable) && $rowData.orderable==1 && empty($strLanguage)}
                                                <div class="flag">{$rowLabel.rendelheto}</div>
                                            {/if}
                                            <img src="{$rowData.image}" alt="{$rowData.title}">
                                            <a href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}" class="magazin__link" style="margin-bottom: 15px;">{$rowLabel.adatlap}</a>
                                            {if !empty($rowData.orderable) && empty($strLanguage)}
                                                <a data-href="{if $customerIsLoggedIn==true}{$orderLink}{else}{$loginLink}{/if}" data-mid="{$rowData.id}" data-num="1" data-cid="{$numCustomerId}" class="magazin__btn add-to-basket"><i class="fas fa-shopping-cart"></i>{$rowLabel.kosarba}</a>
                                            {/if}
                                        </div>
                                    </div>
                                </div>
                            {/foreach}
                        </div>
                    {/foreach}
                {/if}
            </div>
        </div>
        {insert name="sidebar"}
    </div>
</div>

{insert name="end"}