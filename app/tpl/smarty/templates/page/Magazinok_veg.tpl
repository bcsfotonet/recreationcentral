{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description="`$tblData.description`"
    ogTitle="`$tblData.title`"
    ogDescription="`$tblData.description`"
    ogImage="`$tblData.image`"
}
{insert name="header"}
{insert name="nav"}

<div class="content magazin-end">
    {if !empty($tblData.title)}
        {assign var="selected_title" value=$tblData.title|truncate:30}        
    {else}
        {assign var="selected_title" value="Magazin"}
    {/if}    
    {insert 
        name = "headline" 
        title = "`$rowType.name`"
        firstli = "`$rowLabel.nyitooldal`"
        firstliurl = $CONF.base_url_lang
        secondli = "`$rowLabel.recreation_tudomanyos_magazin`"
        secondliurl = ""
        thirdli = "`$rowType.name`"
        thirdliurl = "`$CONF.base_url_lang``$rowType.url`"
        selected = $selected_title
    }   
    {if !empty($tblData)}
        <div class="row">
            <div class="col-12 col-md-4 col-lg-3 mb-5">
                <div class="sidebar-box">
                    <div class="sidebar-box__magazin" style="background-image: url('{$tblData.image}')"></div>
                </div>
                {if !empty($tblData.filename)}
                    <hr>
                    <a href="{$tblData.filename}" class="magazin__link" target="_blank"><i class="fas fa-eye"></i>Elolvasom</a>
                    <hr>
                {/if}
                <div class="price"><b>Ár: 750 Ft</b></div>
                {if !empty($tblData.orderable) && empty($strLanguage)}
                    <div class="text-center">
                        <a data-href="{if $customerIsLoggedIn==true}{$orderLink}{else}{$loginLink}{/if}" data-mid="{$tblData.id}" data-num="1" data-cid="{$numCustomerId}" class="magazin__btn add-to-basket" ><i class="fas fa-shopping-cart"></i>{$rowLabel.kosarba}</a>
                    </div>
                {/if}
            </div>
            <div class="col-12 col-md-8 col-lg-9">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="magazin-year">
                                {if !empty($tblData.title)}
                                    <span>
                                    {$tblData.title}
                                </span>
                                {/if}
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="magazin-data">
                                {if !empty($tblData.main_editor)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Főszerkesztő</div>
                                        <div class="magazin-data__content">
                                            {$tblData.main_editor}
                                        </div>
                                    </div>
                                {/if}
                                {if !empty($tblStaff)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Szerkesztőbizottság
                                            <hr>
                                            <div class="magazin-data__info">
                                                A szerkesztőre rákattintva megtekinthető az adatlapja
                                            </div>
                                        </div>
                                        <div class="magazin-data__content">
                                            {foreach from=$tblStaff item=rowStaff}
                                                <a href="{$CONF.base_url_lang}{$rowStaffType.url}/{$rowStaff.url}" target="_blank">{$rowStaff.name}</a>
                                            {/foreach}
                                        </div>
                                    </div>
                                {/if}
                                {if !empty($tblData.lang)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Nyelv</div>
                                        <div class="magazin-data__content">
                                            {$tblData.lang}
                                        </div>
                                    </div>
                                {/if}
                                {if !empty($tblData.foundation_year)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Alapítva</div>
                                        <div class="magazin-data__content">
                                            {$tblData.foundation_year}
                                        </div>
                                    </div>
                                {/if}
                                {if !empty($tblData.paper_issn)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Papír ISSN</div>
                                        <div class="magazin-data__content">
                                            {$tblData.paper_issn}
                                        </div>
                                    </div>
                                {/if}
                                {if !empty($tblData.online_issn)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Online ISSN</div>
                                        <div class="magazin-data__content">
                                            {$tblData.online_issn}
                                        </div>
                                    </div>
                                {/if}
                                {if !empty($tblData.editorial_address)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Szerkesztőség címe</div>
                                        <div class="magazin-data__content">
                                            {$tblData.editorial_address}
                                        </div>
                                    </div>
                                {/if}
                                {if !empty($tblData.publication_year)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Megjelenés éve</div>
                                        <div class="magazin-data__content">
                                            {$tblData.publication_year}
                                        </div>
                                    </div>
                                {/if}
                                {if !empty($tblData.volume)}
                                    <div class="magazin-data__row">
                                        <div class="magazin-data__title">Tervezett kötetek</div>
                                        <div class="magazin-data__content">
                                            {$tblData.volume}
                                        </div>
                                    </div>
                                {/if}
                            </div>
                        </div>
                        {if !empty($tblData.description|strip_tags:false)}
                            <div class="col-12">
                                <div class="magazin-year">Tartalomjegyzék</div>
                            </div>
                            <div class="col-12">
                                <div class="magazin-data">
                                    <div class="magazin-data__abstract editor-text">
                                        {$tblData.description}
                                    </div>
                                </div>
                            </div>
                        {/if}
                        {if !empty($tblStudy)}
                            <div class="col-12">
                                <div class="magazin-year">Tanulmányok</div>
                            </div>
                            <div class="col-12">
                                <div class="magazin-data">
                                    {foreach from=$tblStudy item=rowStudy}
                                        <div class="magazin-data__row">
                                            <div class="magazin-data__title">
                                                <a href="{$CONF.base_url_lang}{$rowStudyType.url}/{$rowStudy.url}"><i class="fas fa-sign-in-alt"></i>Részletek</a>
                                            </div>
                                            <div class="magazin-data__content">
                                                {*
                                                <a href="{$CONF.base_url_lang}{$rowStudyType.url}/{$rowStudy.url}" class="study-image">
                                                    <img src="{$tblData.image}" alt="{$rowStudy.title}">
                                                </a>
                                                *}
                                                {$rowStudy.title}
                                            </div>
                                        </div>
                                    {/foreach}
                                </div>
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    {/if}
</div>
{insert name="end"}