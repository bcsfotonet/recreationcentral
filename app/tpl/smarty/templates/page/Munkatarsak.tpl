{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Munkatársak"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content">
            {insert name="headline" 
            title="`$rowType.name`"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.tarsasag`"
            secondliurl=""
            selected="`$rowType.name`"}
            <div class="container">
                <div class="row">
                    {if !empty($tblData)}
                        {foreach from=$tblData item=rowData}
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="staff">
                                    {if !empty($rowData.url) && !empty($rowData.description_strip)}
                                        <a class="staff__img" href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}">
                                            <img src="{$rowData.image}" alt="{$rowData.name}">
                                        </a>
                                        {else}
                                        <a class="staff__img" style="cursor:default;">
                                            <img src="{$rowData.image}" alt="{$rowData.name}">
                                        </a>
                                    {/if}
                                    <span class="staff__name">{$rowData.name}</span>
                                    {if !empty($rowData.title)}
                                        <span class="staff__titulus">{$rowData.title}</span>
                                    {/if}
                                    {if !empty($rowData.email)}
                                        <span class="staff__mail">
                                            <a href="mailto:{$rowData.email}">{$rowData.email}</a>
                                        </span>
                                    {/if}
                                    {if !empty($rowData.url) && !empty($rowData.description_strip)}
                                        <a href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}" class="staff__more">{$rowLabel.reszletek}</a>
                                    {/if}
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
        
    {insert name="end"}