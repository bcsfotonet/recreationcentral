{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Plakátok"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
        {insert name="photoswipe"}
                        
        <div class="content">
            {insert name="headline" 
            title="Plakátok"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.media`"
            secondliurl=""
            selected="Plakátok"}
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <form action="">
                            <div class="form__item mx-width300">
                                <div class="form__group" data-type="21">
                                    <select class="js-example-basic-single">
                                        <option value="0">Minden város</option>
                                        {if !empty($tblCityData)}
                                            {foreach from=$tblCityData item=rowCityData}
                                                <option value="{$rowCityData.id}">{$rowCityData.name}</option>
                                            {/foreach}
                                        {/if}
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="changingDataContainerId" class="row">
                    {if !empty($tblData)}
                        {foreach from=$tblData item=rowData}
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="poster">
                                    <div class="poster__img gallery">
                                        <figure itemprop="associatedMedia" itemscope itemtype="https://schema.org/ImageObject">
                                            <a href="{$rowData.image_original_url}" itemprop="contentUrl" id="gallery-{$rowData.id}" data-size="{$rowData.image_size}">
                                                <img src="{$rowData.image}" alt="{$rowData.title}" itemprop="thumbnail">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="poster__title">{$rowData.title}</div>
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
        
    {insert name="end"}