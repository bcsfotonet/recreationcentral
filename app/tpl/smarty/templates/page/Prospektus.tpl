{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Prospektus"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
    {insert name="header"}
    {insert name="nav"}
                        
    <div class="content">
        {insert name="headline" 
        title="`$tblData.name`"
        firstli="`$rowLabel.nyitooldal`"
        firstliurl=$CONF.base_url_lang
        secondli=""
        secondliurl=""
        selected="`$tblData.name`"}

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="bb-custom-wrapper">	
                        <div id="bb-bookblock" class="bb-bookblock">
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page1.jpg" class="bb-item-img" alt="image01"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page2.jpg" class="bb-item-img" alt="image02"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page3.jpg" class="bb-item-img" alt="image03"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page4.jpg" class="bb-item-img" alt="image04"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page5.jpg" class="bb-item-img" alt="image05"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page6.jpg" class="bb-item-img" alt="image06"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page7.jpg" class="bb-item-img" alt="image07"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page8.jpg" class="bb-item-img" alt="image08"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page9.jpg" class="bb-item-img" alt="image09"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page10.jpg" class="bb-item-img" alt="image10"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page11.jpg" class="bb-item-img" alt="image11"/>
                            </div>
                            <div class="bb-item">
                                <img src="{$CONF.web_url}images/prospektus/page12.jpg" class="bb-item-img" alt="image12"/>
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center mt-4">
                        <div class="col-12">
                            <nav class="text-center">
                                <a id="bb-nav-prev" href="#" class="bb-custom-icon bb-custom-icon-arrow-left"><i class="fas fa-chevron-left"></i>Előző</a>
                                <a id="bb-nav-next" href="#" class="bb-custom-icon bb-custom-icon-arrow-right">Következő<i class="fas fa-chevron-right"></i></a>
                            </nav>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
        
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="{$CONF.web_url}js/modernizr.custom.js"></script>
    <script src="{$CONF.web_url}js/jquerypp.custom.js"></script>
    <script src="{$CONF.web_url}js/jquery.bookblock.js"></script>
    {literal}
        <script>
            var Page = (function() {

                var config = {
                        $bookBlock : $( '#bb-bookblock' ),
                        $navNext : $( '#bb-nav-next' ),
                        $navPrev : $( '#bb-nav-prev' ),
                    },
                    init = function() {
                        config.$bookBlock.bookblock( {
                            speed : 1000,
                            shadowSides : 0.8,
                            shadowFlip : 0.4
                        } );
                        initEvents();
                    },
                    initEvents = function() {

                        var $slides = config.$bookBlock.children();

                        // add navigation events
                        config.$navNext.on( 'click touchstart', function() {
                            config.$bookBlock.bookblock( 'next' );
                            return false;
                        } );

                        config.$navPrev.on( 'click touchstart', function() {
                            config.$bookBlock.bookblock( 'prev' );
                            return false;
                        } );

                        // add swipe events
                        $slides.on( {
                            'swipeleft' : function( event ) {
                                config.$bookBlock.bookblock( 'next' );
                                return false;
                            },
                            'swiperight' : function( event ) {
                                config.$bookBlock.bookblock( 'prev' );
                                return false;
                            }
                        } );

                        // add keyboard events
                        $( document ).keydown( function(e) {
                            var keyCode = e.keyCode || e.which,
                                arrow = {
                                    left : 37,
                                    up : 38,
                                    right : 39,
                                    down : 40
                                };

                            switch (keyCode) {
                                case arrow.left:
                                    config.$bookBlock.bookblock( 'prev' );
                                    break;
                                case arrow.right:
                                    config.$bookBlock.bookblock( 'next' );
                                    break;
                            }
                        } );
                    };

                    return { init : init };

            })();

            Page.init();
        </script>
    {/literal}

    {insert name="end"}