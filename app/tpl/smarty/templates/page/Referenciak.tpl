{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Referenciák"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content">
            {insert name="headline" 
            title="`$rowType.name`"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.tarsasag`"
            secondliurl=""
            selected="`$rowType.name`"}
            <div class="container">
                <div class="row">
                    {if !empty($tblData)}
                        {foreach from=$tblData item=rowData}
                            <div class="col-12 col-sm-6 col-lg-4">
                                <div class="news-item">
                                    <div class="news-item__image" style="background-image: url('{$rowData.image}')"></div>
                                    <span class="news-item__title">{$rowData.title}</span>
                                    {* 
                                        date('Y.m.d H:i', strtotime({$rowData.start_date}))
                                        $rowData.start_date|date_format:"%Y. %m. %d %H:%M"
                                    *}
                                    {if !empty($rowData.start_date)}
                                        <span class="news-item__date">{$rowData.start_date|date_format:"%Y/%m/%d %H:%M"}{if !empty($rowData.end_date)} - {$rowData.end_date|date_format:"%Y/%m/%d %H:%M"}{/if}</span>
                                    {/if}
                                    {if !empty($rowData.lead)}
                                        <p class="news-item__lead">
                                            {$rowData.lead}
                                        </p>
                                    {/if}
                                    {if !empty($rowData.url)}
                                        <a href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}" class="news-item__more">{$rowLabel.reszletek}</a>
                                    {/if}
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
            {*<!-- Lapozó -->
            <div class="row">
                <div class="col-12">
                    <div class="pagination justify-content-center">
                        <div class="pagination__wrapper">
                            <a class="pagination__btn-wrapper">1</a>
                            <span class="pagination__btn-wrapper pagination__btn-wrapper__more">...</span>			   
                            <a class="pagination__btn-wrapper pagination__btn-wrapper__prev"></a>
                                <a class="pagination__btn-wrapper">1.</a>

                                <a href="" class="pagination__btn-wrapper pagination__btn-wrapper__current">2.</a>

                            <a href="" class="pagination__btn-wrapper pagination__btn-wrapper__next"></a>
                            <span class="pagination__btn-wrapper pagination__btn-wrapper__more">...</span>
                            <a href="" class="pagination__btn-wrapper">2</a>
                            <form class="pagination__form" action="" method="GET">	    
                                <input type="number" name="o">
                            </form>
                        </div>
                    </div>
                </div>
            </div>*}
        </div>
        
    {insert name="end"}