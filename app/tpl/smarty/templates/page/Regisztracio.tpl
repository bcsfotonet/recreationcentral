{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Regisztráció"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
    {insert name="header"}
    {insert name="nav"}
                        
    <div class="content">
        {insert name="headline" 
        title="`$tblData.name`"
        firstli="`$rowLabel.nyitooldal`"
        firstliurl=$CONF.base_url_lang
        secondli=""
        secondliurl=""
        selected="`$tblData.name`"}

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-lg-8">
                    {if !empty($tblFormData)}
                        {if !empty($tblData.static_description)}
                            <div class="stat-text">
                                {$tblData.static_description}
                            </div>
                        {/if}               
                        <form id="formRegistration" name="formRegistration" class="form" method="post" action="" >
                            <div class="form__row">
                                <div class="form__item" style="max-width: 150px; display: inline-block; line-height: 20px; padding-right: 10px;">
                                    <div class="form__alert">{if isset($tblFormError.isAlreadyCustomer) && !empty($tblFormError.isAlreadyCustomer)}{$tblFormError.isAlreadyCustomer}{/if}</div>
                                    <div class="form__group">
                                        <label>
                                            <input type="radio" id="personRadio" name="personRadio" value="1" {if !empty($tblFormData.personRadio)}checked{elseif (empty($tblFormData.personRadio) && empty($tblFormData.companyRadio))}checked{/if}>
                                            Magánszemély
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form__item" style="max-width: 150px; display: inline-block; line-height: 20px;">
                                    <div class="form__group">
                                        <label>Cég
                                            <input type="radio" id="companyRadio" name="companyRadio" value="1" {if !empty($tblFormData.companyRadio)}checked{/if}>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form__row">
                                <div class="form__item mx-width300" style="display:inline-block;">
                                    <label>Vezetéknév *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationLastName" name="lastName" maxlength="50" placeholder="Vezetéknév" {if !empty($tblFormData.lastName)}value="{$tblFormData.lastName}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.lastName)}{$tblFormError.lastName}{/if}</div>
                                </div>
                                <div class="form__item mx-width300" style="display:inline-block;">
                                    <label>Keresztnév *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationFirstName" name="firstName" maxlength="50" placeholder="Keresztnév" {if !empty($tblFormData.firstName)}value="{$tblFormData.firstName}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.firstName)}{$tblFormError.firstName}{/if}</div>
                                </div>
                            </div>
                            <div class="form__row">
                                <div class="form__item mx-width300">
                                    <label>Cégnév *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationCompany" name="company" maxlength="50" placeholder="Cégnév" {if !empty($tblFormData.company)}value="{$tblFormData.company}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.company)}{$tblFormError.company}{/if}</div>
                                </div>
                                <div class="form__item mx-width300">
                                    <label>Adószám *</label>
                                    <div class="form__group">
                                        <input type="text" id="taxNumber" name="taxNumber" maxlength="30" placeholder="Adószám" {if !empty($tblFormData.taxNumber)}value="{$tblFormData.taxNumber}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.taxNumber)}{$tblFormError.taxNumber}{/if}</div>
                                </div>
                            </div>
                            <div class="form__row">
                                <div class="form__item mx-width300">
                                    <label>E-mail cím *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationEmail" name="email" maxlength="250" placeholder="E-mail cím" {if !empty($tblFormData.email)}value="{$tblFormData.email}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.email)}{$tblFormError.email}{/if}</div>
                                </div>
                                <div class="form__item mx-width300">
                                    <label>Telefonszám *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationTel" name="tel" maxlength="50" placeholder="Telefonszám" {if !empty($tblFormData.tel)}value="{$tblFormData.tel}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.tel)}{$tblFormError.tel}{/if}</div>
                                </div>
                            </div>
                            <hr>
                            <div class="form__row">
                                <div class="form__item" style="max-width: 165px;">
                                    <label>Ország *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationCountry" name="country" maxlength="50" value="Magyarország" readonly />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.country)}{$tblFormError.country}{/if}</div>
                                </div>
                                <div class="form__item" style="max-width: 110px;">
                                    <label>Irányítószám *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationZip" name="zip" maxlength="10" placeholder="Irányító szám" {if !empty($tblFormData.zip)}value="{$tblFormData.zip}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.zip)}{$tblFormError.zip}{/if}</div>
                                </div>
                            </div>
                            <div class="form__row">
                                <div class="form__item" style="max-width: 165px;">
                                    <label>Város *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationCity" name="city" maxlength="50" placeholder="Város" {if !empty($tblFormData.city)}value="{$tblFormData.city}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.city)}{$tblFormError.city}{/if}</div>
                                </div>
                                <div class="form__item" style="max-width: 200px;">
                                    <label>Utca *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationStreet" name="street" maxlength="100" placeholder="Utca" {if !empty($tblFormData.street)}value="{$tblFormData.street}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.street)}{$tblFormError.street}{/if}</div>
                                </div>
                                <div class="form__item" style="max-width: 100px;">
                                    <label>Házszám *</label>
                                    <div class="form__group">
                                        <input type="text" id="formRegistrationHouseNum" name="houseNum" maxlength="10" placeholder="Házszám" {if !empty($tblFormData.houseNum)}value="{$tblFormData.houseNum}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.houseNum)}{$tblFormError.houseNum}{/if}</div>
                                </div>
                            </div>
                            <hr>
                            <div class="form__row">
                                <div class="form__item" style="max-width: 165px">
                                    <label>Jelszó *</label>
                                    <div class="form__group">
                                        <input type="password" id="formRegistrationPass" name="pass" maxlength="50" placeholder="Jelszó" {if !empty($tblFormData.pass)}value="{$tblFormData.pass}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.pass)}{$tblFormError.pass}{/if}</div>
                                </div>
                                <div class="form__item" style="max-width: 165px">
                                    <label>Jelszó megerősítése *</label>
                                    <div class="form__group">
                                        <input type="password" id="formRegistrationPassAgain" name="passAgain" maxlength="50" placeholder="Jelszó megerősítése" {if !empty($tblFormData.passAgain)}value="{$tblFormData.passAgain}"{/if} />
                                    </div>
                                    <div class="form__alert">{if !empty($tblFormError.passAgain)}{$tblFormError.passAgain}{/if}</div>
                                </div>
                            </div>
                            <div class="form__row">
                                <div class="form__item">
                                    <input type="submit" value="Regisztráció" class="button" />
                                </div>
                            </div>
                        </form>
                    {else}
                        <div class="successfull-text" style="font-size:24px; color:green; text-align:center; margin:2em auto; line-height:1.6;">
                            {if !empty($strSuccessText)}{$strSuccessText}{else}Sikeres küldés!{/if}
                        </div>
                    {/if}
                    {*{if !empty($loginLink)}
                        <a href="{$loginLink}" class="link">Vissza a bejelentkezéshez</a>                     
                    {/if}  *}
                </div>
            </div>
        </div>
    </div>
        
    {insert name="end"}