{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Rendelés"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
{insert name="header"}
{insert name="nav"}

<div class="content">
    {insert name="headline" 
    title="`$rowType.name`"
    firstli="`$rowLabel.nyitooldal`"
    firstliurl=$CONF.base_url_lang
    secondli=""
    secondliurl=""
    selected="`$rowType.name`"}

    <div class="container">
        {if $customerIsLoggedIn==true}
            {if !empty($tblFormData)}
                {if !empty($tblOrderData)}
                    <div class="row d-flex justify-content-center">
                        <div class="col-12 col-md-10 col-xl-8">
                            {if !empty($tblData.static_description)}
                                <div class="stat-text">
                                    {$tblData.static_description}
                                </div>
                            {/if}
                        </div>
                    </div>
                    <form action="" class="form" method="post">   
                        <div class="row mb-5 d-flex justify-content-center">
                            <div class="col-12 col-md-10 col-xl-8">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="card-title"><i class="fas fa-book"></i><b>Magazinok</b> (kosár)</div>
                                        <a href="{$CONF.base_url_lang}magazinok" class="link text-left"><i class="fas fa-angle-double-left"></i> vissza a magazinokhoz</a>
                                        <hr>
                                        {foreach from=$tblOrderData item=rowOrderData}
                                            <div class="order-page__row">
                                                <div>
                                                    <div class="order-page__title">{$rowOrderData.magazine_title}</div>
                                                    <a data-href="{if $customerIsLoggedIn==true}{$orderLink}{else}{$loginLink}{/if}" data-mid="{$rowOrderData.magazine_id}" data-num="{$rowOrderData.num_magazine}" data-cid="{$rowOrderData.customer_id}" class="order-page__link remove-from-basket"><i class="fas fa-times"></i>Törlöm a kosárból</a>  
                                                </div>
                                                <div class="order-page__quantity">
                                                    <div class="form__row" style="width: 50px;">
                                                        <div class="form__item" style="width:55px;">
                                                            <div class="form__group">
                                                                <input type="number" min="0" max="999" step="1" class="basket-numbox" style="width:55px;" data-mid="{$rowOrderData.magazine_id}" data-cid="{$rowOrderData.customer_id}" value="{$rowOrderData.num_magazine}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="order-page__price">
                                                        {$numMagazinePrice} Ft
                                                    </div>
                                                </div>
                                            </div>
                                        {/foreach}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-5 d-flex justify-content-center">
                            <div class="col-12 col-md-10 col-xl-8">
                                <div class="card">
                                    <div class="card-body">
                                        {if !empty($tblUserData)}
                                            <div class="card-title"><i class="fas fa-user-alt"></i> Általános adatok</div>
                                            <hr>
                                            <div class="form__row">
                                                <div class="form__item" style="max-width: 150px; display: inline-block; line-height: 20px; padding-right: 10px;">
                                                    <label>
                                                        {if !empty($tblUserData.is_person)}Magánszemély{/if}
                                                        {if !empty($tblUserData.is_company)}Cég{/if}
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form__row">
                                                {if !empty($tblUserData.last_name)}
                                                    <div class="form__item mx-width300">
                                                        <label>Vezetéknév</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.last_name}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                                {if !empty($tblUserData.first_name)}
                                                    <div class="form__item mx-width300">
                                                        <label>Keresztnév</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.first_name}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                            </div>
                                            <div class="form__row">
                                                {if !empty($tblUserData.company_name)}
                                                    <div class="form__item mx-width300">
                                                        <label>Cégnév</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.company_name}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                                {if !empty($tblUserData.tax_number)}
                                                    <div class="form__item mx-width300">
                                                        <label>Adószám</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.tax_number}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                            </div>
                                            <div class="form__row">
                                                {if !empty($tblUserData.email)}
                                                    <div class="form__item mx-width300">
                                                        <label>E-mail cím</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.email}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                                {if !empty($tblUserData.tel)}
                                                    <div class="form__item mx-width300">
                                                        <label>Telefonszám</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.tel}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                            </div>
                                            <div class="form__row">
                                                {if !empty($tblUserData.country)}
                                                    <div class="form__item" style="max-width: 165px;">
                                                        <label>Ország</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.country}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                                {if !empty($tblUserData.zip)}
                                                    <div class="form__item" style="max-width: 110px;">
                                                        <label>Irányítószám</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.zip}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                            </div>
                                            <div class="form__row">
                                                {if !empty($tblUserData.city)}
                                                    <div class="form__item" style="max-width: 165px;">
                                                        <label>Város</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.city}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                                {if !empty($tblUserData.street)}
                                                    <div class="form__item" style="max-width: 200px;">
                                                        <label>Utca</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.street}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                                {if !empty($tblUserData.house_number)}
                                                    <div class="form__item" style="max-width: 100px;">
                                                        <label>Házszám</label>
                                                        <div class="form__group">
                                                            <input type="text" placeholder="{$tblUserData.house_number}" disabled />
                                                        </div>
                                                    </div>
                                                {/if}
                                            </div>

                                        {/if}
                                            <div class="card-title mt-5"><i class="fas fa-hands"></i>Átvételi mód</div>
                                            <hr>
                                            <div class="form__alert">{if !empty($tblFormError.receive_mode)}{$tblFormError.receive_mode}{/if}</div>
                                            <div class="form__row">
                                                <div class="form__item">
                                                    <div class="form__group">
                                                        <label>
                                                            Posta
                                                            <input type="radio" name="receive_mode" value="0" {if isset($tblFormData.receive_mode) && $tblFormData.receive_mode==0}checked{/if} data-cid="{$rowOrderData.customer_id}" />
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form__row">
                                                <div class="form__item">
                                                    <div class="form__group">
                                                        <label>
                                                            Személyes átvétel
                                                            <input type="radio" name="receive_mode" value="1" {if isset($tblFormData.receive_mode) && $tblFormData.receive_mode==1}checked{/if} data-cid="{$rowOrderData.customer_id}" />
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="differentAddress">
                                                <div class="form__row">
                                                    <div class="form__item mt-3">
                                                        <div class="form__group">
                                                            <div class="form__alert">{if !empty($tblFormError.post_different)}{$tblFormError.post_different}{/if}</div>
                                                            <label>
                                                                A postázási adatok különböznek a számlázási adatoktól.
                                                                <input type="checkbox" name="post_different" value="1" {if !empty($tblFormData.post_different)}checked{/if} />
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="personalReceipt" style="display:none;">
                                                <div class="card-title mt-5">Személyes átvétel - Városok</div>
                                                <hr>
                                                <div class="form__alert">{if !empty($tblFormError.post_city)}{$tblFormError.post_city}{/if}</div>
                                                <div class="form__row">
                                                    <div class="form__item">
                                                        <div class="form__group">
                                                            <label>
                                                                <input type="radio" name="post_city" value="Budapest" {if !empty($tblFormData.post_city) && $tblFormData.post_city=='Budapest'}checked{/if} />
                                                                Budapest
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <div class="form__item">
                                                        <div class="form__group">
                                                            <label>
                                                                <input type="radio" name="post_city" value="Szeged" {if !empty($tblFormData.post_city) && $tblFormData.post_city=='Szeged'}checked{/if} />
                                                                Szeged
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <div class="form__item">
                                                        <div class="form__group">
                                                            <label>
                                                                <input type="radio" name="post_city" value="Szombathely" {if !empty($tblFormData.post_city) && $tblFormData.post_city=='Szombathely'}checked{/if} />
                                                                Szombathely
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <div class="form__item">
                                                        <div class="form__group">
                                                            <label>
                                                                <input type="radio" name="post_city" value="Miskolc" {if !empty($tblFormData.post_city) && $tblFormData.post_city=='Miskolc'}checked{/if} />
                                                                Miskolc
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <div class="form__item">
                                                        <div class="form__group">
                                                            <label>
                                                                <input type="radio" name="post_city" value="Pécs" {if !empty($tblFormData.post_city) && $tblFormData.post_city=='Pécs'}checked{/if} />
                                                                Pécs
                                                                <span class="checkmark"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="addressData" style="display:none;">
                                                <div class="card-title mt-5">Postázási adatok</div>
                                                <hr>
                                                <div class="form__row">
                                                    <div class="form__item mx-width300">
                                                        <label>Postázási név</label>
                                                        <div class="form__group">
                                                            <input type="text" name="post_name" placeholder="Postázási név" {if !empty($tblFormData.post_name)}value="{$tblFormData.post_name}"{/if} />
                                                        </div>
                                                        <div class="form__alert">{if !empty($tblFormError.post_name)}{$tblFormError.post_name}{/if}</div>
                                                    </div>
                                                </div>
                                                <div class="form__row">
                                                    <div class="form__item mx-width300">
                                                        <label>Postázási cím</label>
                                                        <div class="form__group">
                                                            <input type="text" name="post_address" placeholder="Postázási cím" {if !empty($tblFormData.post_address)}value="{$tblFormData.post_address}"{/if} />
                                                        </div>
                                                        <div class="form__alert">{if !empty($tblFormError.post_address)}{$tblFormError.post_address}{/if}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="deliveryCost" class="order-page__delivery mt-3">
                                                <span>Postázási költség:</span>
                                                <span>{$numMagazinePiece} x {$numPostPrice} Ft</span>
                                            </div>
                                            <hr>
                                            <div class="order-page__summary">
                                                <span>Összesen fizetendő:</span>
                                                <div class="form__alert">{if !empty($tblFormError.total_price)}{$tblFormError.total_price}{/if}</div>
                                                <span>{$numTotal} Ft</span>
                                                <input type="hidden" name="total_price" value="{$numTotal}" />
                                            </div>
                                            <div class="form__row">
                                                <div class="form__item mt-3 mb-3">
                                                    <div class="form__group">
                                                        <label>
                                                            Az <a href="{$CONF.base_url_lang}adatvedelem" class="link d-inline-block" target="_blank">adatvédelmi nyilatkozat</a>-ot és az
                                                            <a href="{$CONF.base_url_lang}aszf" class="link d-inline-block" target="_blank">Á.SZ.F.</a>-t elfogadom
                                                            <input type="checkbox" name="privacy_policy" value="1" {if !empty($tblFormData.privacy_policy)}checked{/if} />
                                                            <span class="checkmark"></span>
                                                        </label>
                                                        <div class="form__alert">{if !empty($tblFormError.privacy_policy)}{$tblFormError.privacy_policy}{/if}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="submit" class="button">Megrendelem</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                {else}
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-6">
                            <div class="stat-text" style="text-align:center; margin:1em auto;">Még nem adott magazint a megrendeléshez.</div>
                            {*if !empty($orderLink)*}
                                <a href="{$CONF.base_url_lang}magazinok" class="link">Vissza a magazinokhoz</a>                     
                            {*/if*}   
                        </div> 
                    </div>
                {/if}
            {else}
                <div class="successfull-text" style="font-size:24px; color:green; text-align:center; margin:2em auto; line-height:1.6;">
                    {if !empty($strSuccessText)}{$strSuccessText}{else}Sikeres küldés!{/if}
                </div>
            {/if}
        {else}
            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <div class="stat-text" style="text-align:center; margin:1em auto;">Az oldal megtekintéséhez be kell jelentkeznie.</div>
                    {if !empty($loginLink)}
                        <a href="{$loginLink}" class="link">Vissza a bejelentkezéshez</a>                     
                    {/if}   
                </div>
            </div>
        {/if}
    </div>
</div>

{insert name="end"}


