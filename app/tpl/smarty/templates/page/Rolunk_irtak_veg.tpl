{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description="`$tblData.lead`"
    ogTitle="`$tblData.title`"
    ogDescription="`$tblData.lead`"
    ogImage="`$tblData.image`"
}
        {insert name="header"}
        {insert name="nav"}
        {if !empty($tblData.gallery_id)}
            {insert name="photoswipe"}
        {/if}

    <div class="content">
        {if !empty($tblData.title)}
            {assign var="selected_title" value=$tblData.title|truncate:30}        
        {else}
            {assign var="selected_title" value="Cikk"}
        {/if}
        {insert 
            name = "headline" 
            title = "`$rowType.name`"
            firstli = "`$rowLabel.nyitooldal`"
            firstliurl = $CONF.base_url_lang
            secondli = "`$rowLabel.media`"
            secondliurl = ""
            thirdli = "`$rowType.name`"
            thirdliurl = "`$CONF.base_url_lang``$rowType.url`"
            selected = $selected_title
        }
        {if !empty($tblData)}
            <div class="container">         
                <div class="row">
                    {if !empty($tblData.image)}
                        <div class="col-12 col-lg-4 text-center">
                            <div class="news-item__image" style="background-image: url('{$tblData.image}')"></div>
                        </div>
                    {/if}
                    <div class="col-12 col-lg-8">
                        <span class="news-item__title">{$tblData.title}</span>
                        {if !empty($tblData.start_date)}
                            <span class="news-item__date">{$tblData.start_date|date_format:"%Y/%m/%d %H:%M"}{if !empty($tblData.end_date)} - {$tblData.end_date|date_format:"%Y/%m/%d %H:%M"}{/if}</span>
                        {/if}
                        {if !empty($tblData.lead)}
                            <p class="news-item__lead">
                                {$tblData.lead}
                            </p>
                        {/if}
                        {if !empty($tblData.description)}
                            <div class="editor-text stat-text">
                                {$tblData.description}
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
            {insert name="attachments"}
        {/if}
    </div>

{insert name="end"}