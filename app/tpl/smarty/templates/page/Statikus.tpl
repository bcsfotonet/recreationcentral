{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="`$tblData.title`"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content stat-page">
            {insert
                name = "headline" 
                title = $tblData.title
                firstli = "`$rowLabel.nyitooldal`"
                firstliurl = $CONF.base_url_lang
                secondli = ""
                secondliurl = ""
                selected = $tblData.title
            }
            {*
            <div class="two-part">
                    <div class="two-part__main">
            *}
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-8">
                                    <div class="editor-text stat-text">
                                        {if !empty($tblData.description)}
                                            {$tblData.description}
                                        {/if}
                                    </div>
                                </div>
                                {insert name="attachments"}
                            </div>
                        </div>
            {*
                    </div>    
                {insert name="sidebar"}
            </div>
            *}
        </div>
        
    {insert name="end"}