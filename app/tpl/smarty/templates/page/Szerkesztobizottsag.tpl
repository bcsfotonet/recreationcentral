{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Szerkesztőbizottság"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}
        {insert name="header"}
        {insert name="nav"}
                        
        <div class="content">
            {insert name="headline" 
            title="`$rowType.name`"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.recreation_tudomanyos_magazin`"
            secondliurl=""
            selected="`$rowType.name`"}
            <div class="container">
                <div class="row">
                    {if !empty($tblData)}
                        {foreach from=$tblData item=rowData}
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="staff">
                                    {if !empty($rowData.url) && !empty($rowData.description_strip)}
                                        <a class="staff__img" href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}">
                                            <img src="{$rowData.image}" alt="{$rowData.name}">
                                        </a>
                                    {else}
                                        <a class="staff__img" href="javacript:void(false);" style="cursor: default">
                                            {*
                                                staff/{$rowData.id}/{$rowData.image} - ha majd fel lesz töltve a kép a megfelelő mappába
                                                <img src="{$CONF.web_url}images/staff.png" alt="">
                                            *}
                                            <img src="{$rowData.image}" alt="{$rowData.name}">
                                        </a>
                                    {/if}
                                    <span class="staff__name">{$rowData.name}</span>
                                    <span class="staff__titulus">{$rowData.title}</span>
                                    <span class="staff__mail">
                                        <a href="mailto:{$rowData.email}">{$rowData.email}</a>
                                    </span>
                                    {if !empty($rowData.url) && !empty($rowData.description_strip)}
                                        <a href="{$CONF.base_url_lang}{$rowType.url}/{$rowData.url}" class="staff__more">{$rowLabel.reszletek}</a>
                                    {/if}
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
        
    {insert name="end"}