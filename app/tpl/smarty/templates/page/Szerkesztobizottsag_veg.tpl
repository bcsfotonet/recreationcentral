{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description="`$tblData.description`"
    ogTitle="`$tblData.name`"
    ogDescription="`$tblData.description`"
    ogImage="`$tblData.image`"
}
        {insert name="header"}
        {insert name="nav"}
                        
    <div class="content">
        {if !empty($tblData.name)}
            {assign var="selected_name" value=$tblData.name}
        {else}
            {assign var="selected_name" value="Munkatárs"}
        {/if}
        {insert 
            name="headline" 
            title="`$rowType.name`"
            firstli="`$rowLabel.nyitooldal`"
            firstliurl=$CONF.base_url_lang
            secondli="`$rowLabel.recreation_tudomanyos_magazin`"
            secondliurl=""
            thirdli = "`$rowType.name`"
            thirdliurl="`$CONF.base_url_lang``$rowType.url`"
            selected=$selected_name
        }
        <div class="container">
            {if !empty($tblData)}
                <div class="row justify-content-sm-center">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="staff">
                            {if !empty($tblData.image)}
                                <a class="staff__img" style="cursor:default;">
                                    <img src="{$tblData.image}" alt="{$tblData.name}">
                                </a>
                            {/if}
                            <span class="staff__name">{$tblData.name}</span>
                            {if !empty($tblData.title)}
                                <span class="staff__titulus">{$tblData.title}</span>
                            {/if}
                            {if !empty($tblData.email)}
                                <span class="staff__mail">
                                    <a href="mailto:{$tblData.email}">{$tblData.email}</a>
                                </span>
                            {/if}
                        </div>
                    </div>
                    {if !empty($tblData.description)}
                        <div class="col-12 col-md-8 col-lg-9">
                            <div class="editor-text stat-text">
                                {$tblData.description}
                            </div>
                        </div>
                    {/if}
                </div>
            {/if}
        </div>
    </div>
        
    {insert name="end"}