{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description=""
    ogTitle="Tanulmányok"
    ogDescription=""
    ogImage="`$CONF.web_url`images/logo.png"
}

    {insert name="header"}
    {insert name="nav"}

    <div class="content">
        {insert name="headline" 
        title="`$rowType.name`"
        firstli="`$rowLabel.nyitooldal`"
        firstliurl=$CONF.base_url_lang
        secondli="`$rowLabel.recreation_tudomanyos_magazin`"
        secondliurl=""
        selected="`$rowType.name`"}
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form id="year_filter_id" method="get" action="">
                        <div class="form__item" style="width: 80px;" data-function="year">
                            <select name="y" class="js-example-basic-single">
                                <option value="0">{$rowLabel.osszes}</option>
                                {if !empty($tblYear)}
                                    {foreach from=$tblYear item=rowYear}
                                        <option value="{$rowYear.year}" {if !empty($strYear) && $strYear==$rowYear.year}selected{/if}>{$rowYear.year}</option>
                                    {/foreach}
                                {/if}
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                {if !empty($tblData)}
                    {foreach from=$tblData item=rowData}
                        {if ((!empty($rowData.title) || !empty($rowData.title_en)) && empty($strLanguage)) ||  !empty($rowData.title_en)}
                            <div class="col-12 col-md-6">
                                <div class="study">
                                    {if !empty($rowData.authors)}
                                        <div class="study__head">
                                            <i class="fas fa-users"></i>
                                            {$rowData.authors}
                                        </div>
                                    {/if}
                                    {if !empty($rowData.title) && empty($strLanguage)}
                                        <a {if !empty($rowData.url)}href="{$CONF.base_url}{$rowTypeNew.url}/{$rowData.url}"{/if} class="study__title">
                                           <img src="{$CONF.web_url}/images/huflag.png" alt="Magyar zászló" style="max-height: 14px;line-height: 15px; margin-right: 10px;">{$rowData.title}
                                           {* 
                                           &#x1F1ED;&#x1F1FA; {$rowData.title}
                                           *}
                                        </a>
                                        <hr>
                                    {/if}                 
                                    {if !empty($rowData.title_en)}
                                        <a {if !empty($rowData.url_en)}href="{$CONF.base_url}en/{$rowTypeNew.url_en}/{$rowData.url_en}"{/if} class="study__title">
                                            <img src="{$CONF.web_url}/images/enflag.png" alt="Angol zászló" style="max-height: 14px;line-height: 15px; margin-right: 10px;">{$rowData.title_en}
                                            {*
                                            &#x1F1EC;&#x1F1E7; {$rowData.title_en}
                                            *}
                                        </a>
                                        {if !empty($strLanguage)}
                                            <hr>
                                        {/if}

                                    {/if}

                                {if !empty($rowData.doi)}

                                    <span class="study__doi">

                                            <a {if !empty($rowData.doi_url)}href="{$rowData.doi_url}" target="_blank"{/if} class="link">

                                                {$rowData.doi}

                                            </a>

                                        </span>

                                {/if}

                                {if !empty($rowData.url) && empty($strLanguage)}

                                    <a href="{$CONF.base_url}{$rowTypeNew.url}/{$rowData.url}" class="study__more">{$rowLabel.bovebben}</a>

                                {elseif !empty($rowData.url_en)}

                                    <a href="{$CONF.base_url}en/{$rowTypeNew.url_en}/{$rowData.url_en}" class="study__more">{$rowLabel.bovebben}</a>

                                {/if}

                            </div>

                        </div>

                    {/if}

                {/foreach}

            {else}
                <div class="col-12 col-sm-6 col-lg-4">
                    {$rowLabel.nem_talalhato}
                </div>
            {/if}

        </div>

    </div>

</div>



{insert name="end"}