{insert
    name="start"
    title="Közép-Kelet-Európai Rekreációs Társaság"
    description="`$tblData.description`"
    ogTitle="`$tblData.title`"
    ogDescription="`$tblData.description`"
    ogImage="`$CONF.web_url`images/logo.png"
}
{insert name="header"}
{insert name="nav"}

    <div class="content">
        {if !empty($tblData.title) && empty($strLanguage)}
            {assign var="selected_title" value=$tblData.title|truncate:30}
        {elseif !empty($tblData.title_en)}
            {assign var="selected_title" value=$tblData.title_en|truncate:30}
        {else}
            {assign var="selected_title" value="`$rowLabel.tanulmany`"}
        {/if}    
        {insert 
            name = "headline" 
            title = "`$rowType.name`"
            firstli = "`$rowLabel.nyitooldal`"
            firstliurl = $CONF.base_url_lang
            secondli = "`$rowLabel.recreation_tudomanyos_magazin`"
            secondliurl = ""
            thirdli = "`$rowType.name`"
            thirdliurl = "`$CONF.base_url_lang``$rowType.url`"
            selected = $selected_title
        }   
        {if !empty($tblData)}
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-10">
                        <div class="study">
                            {if !empty($tblData.magazine)}
                                <div class="study__head study__head--light">
                                    {$tblData.magazine}
                                </div>
                            {/if}
                            <div class="study__label">{$rowLabel.cim}</div>
                            {if !empty($tblData.title) && empty($strLanguage)}
                                <div class="study__title">
                                    <img src="{$CONF.web_url}/images/huflag.png" alt="Magyar zászló" style="max-height: 14px;line-height: 15px; margin-right: 10px;">{$tblData.title}
                                </div>
                                <hr>
                            {/if}                 
                            {if !empty($tblData.title_en)}
                                <div class="study__title">
                                    <img src="{$CONF.web_url}/images/enflag.png" alt="Angol zászló" style="max-height: 14px;line-height: 15px; margin-right: 10px;">{$tblData.title_en}
                                </div>
                                {if !empty($strLanguage)}
                                    <hr>
                                {/if}
								
                            {/if}

                        {if !empty($tblData.authors)}
                            <span class="study__author"><div class="study__label">{$rowLabel.szerzok}:</div>
                                {$tblData.authors}
                                </span>
                        {/if}
                        {if !empty($tblData.keywords)}
                            <span class="study__keywords"><div class="study__label">{$rowLabel.kulcsszavak}:</div>
                                {$tblData.keywords}
                                </span>
                        {/if}
                        {if !empty($tblData.doi)}
                            <span class="study__doi">
                                    <div class="study__label">DOI:</div>
                                    <a {if !empty($tblData.doi_url)}href="{$tblData.doi_url}" target="_blank"{/if} class="link">
                                        {$tblData.doi}
                                    </a>
                                </span>
                        {/if}

                        {if !empty($tblData.abstract)}
                            <div class="study__label">{$rowLabel.absztrakt}</div>
                            <div class="stat-text">
                                {$tblData.abstract}
                            </div>
                        {/if}

                        {if !empty($tblData.description|strip_tags:false)}
                            <div class="study__label">{$rowLabel.tanulmany}</div>
                            <div class="stat-text">
                                {$tblData.description}
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
    {/if}
</div>
{insert name="end"}