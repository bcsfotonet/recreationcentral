<?php
require_once(__DIR__ . '/../config/app.php');
require_once(__DIR__ . '/../config/db.php');
require_once(__DIR__ . '/../sys/DB.php');
require_once(__DIR__ . '/../sys/function.php');

$response = new stdClass();
$objDb = new DB();
$objDb->connect();
$data = array();
$html = '';
$isOrderOk = false;
$customer_id = isset($_POST['customer_id']) && is_numeric($_POST['customer_id']) ? trim(addslashes($_POST['customer_id'])) : 0;
$order_id = orderExistHandle($customer_id);
$magazine_id = isset($_POST['magazine_id']) && is_numeric($_POST['magazine_id']) ? trim(addslashes($_POST['magazine_id'])) : 0;
$num_magazine = isset($_POST['num_magazine']) && is_numeric($_POST['num_magazine']) ? trim(addslashes($_POST['num_magazine'])) : 0;
$receive_mode = isset($_POST['receive_mode']) && is_numeric($_POST['receive_mode']) ? trim(addslashes($_POST['receive_mode'])) : 0;
$action = isset($_POST['action']) && !empty($_POST['action']) ? $_POST['action'] : '';

if(!empty($order_id) && (!empty($magazine_id) || $action=='update_total')){
    // kosarba rakas
    $isOrderOk = magazineOrder($order_id, $magazine_id, $num_magazine, $action, $receive_mode);
}

if($isOrderOk==true){
    $response->status = 'ok';
    $response->order_id = $order_id;
}else{
    $response->status = 'empty';
    $response->order_id = 0;
//    $response->html = $rowLabel['nem_talalhato'];
}

/*
 * 
mondjuk insert/delete/update a magazine_order tablaba egy sor
 * magazine_id
 * order_id kell
 * + mennyiseg/magazin 
$city_id = is_numeric($_POST['city_id']) ? trim(addslashes($_POST['city_id'])) : 0;
$type_id = is_numeric($_POST['type_id']) ? trim(addslashes($_POST['type_id'])) : 0;

if(!empty($city_id) && !empty($type_id) && $type_id == 14){
    // kosarba rakas
    $html = videoFilterView($city_id, $type_id);
}
*/

//if(!empty($html)){
//    $response->status = 'ok';
//    $response->html = $html;
//}else{
//    $response->status = 'empty';
//    $response->html = $rowLabel['nem_talalhato'];
//}

echo json_encode($response);

?>