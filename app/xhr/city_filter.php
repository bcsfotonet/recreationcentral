<?php
require_once(__DIR__ . '/../config/app.php');
require_once(__DIR__ . '/../config/db.php');
require_once(__DIR__ . '/../sys/DB.php');
require_once(__DIR__ . '/../sys/function.php');

$response = new stdClass();
$objDb = new DB();
$objDb->connect();
$data = array();
$city_id = is_numeric($_POST['city_id']) ? trim(addslashes($_POST['city_id'])) : 0;
$type_id = is_numeric($_POST['type_id']) ? trim(addslashes($_POST['type_id'])) : 0;
$html = '';

if(!empty($city_id) && !empty($type_id) && $type_id == 14){
    // eletviteli videok varos kivalasztva
    $html = videoFilterView($city_id, $type_id);
}elseif($city_id==0 && !empty($type_id) && $type_id == 14){
    // eletviteli videok, az osszes varos 
    $html = videoFilterView($city_id, $type_id);
}elseif(!empty($city_id) && !empty($type_id) && $type_id == 15){
    // galeria, varos kivalasztva
    $html = galleryFilterView($city_id,$type_id);
}elseif($city_id==0 && !empty($type_id) && $type_id == 15){
    // galeria, az osszes varos
    $html = galleryFilterView($city_id,$type_id);
}elseif(!empty($city_id) && !empty($type_id) && $type_id == 16){
    // kreativ csoport, varos kivalasztva
   $html = creativeTeamFilterView($city_id,$type_id);
}elseif($city_id==0 && !empty($type_id) && $type_id == 16){
    // kreativ csoport, az osszes varos
    $html = creativeTeamFilterView($city_id,$type_id);
}elseif(!empty($city_id) && !empty($type_id) && $type_id == 20){
    // Eloadasok, varos kivalasztva
   $html = presentationFilterView($city_id,$type_id);
}elseif($city_id==0 && !empty($type_id) && $type_id == 20){
    // Eloadasok, az osszes varos
    $html = presentationFilterView($city_id,$type_id);
}elseif(!empty($city_id) && !empty($type_id) && $type_id == 21){
    // Plakatok, varos kivalasztva
   $html = posterFilterView($city_id,$type_id);
}elseif($city_id==0 && !empty($type_id) && $type_id == 21){
    // Plakatok, az osszes varos
    $html = posterFilterView($city_id,$type_id);
}

if(!empty($html)){
    $response->status = 'ok';
    $response->html = $html;
}else{
    $response->status = 'empty';
    $response->html = $rowLabel['nem_talalhato'];
}

echo json_encode($response);

?>