<?php
require_once(__DIR__ . '/../config/app.php');
require_once(__DIR__ . '/../config/db.php');
require_once(__DIR__ . '/../sys/DB.php');

$objDb = new DB();
$objDb->connect();
$data = array();
$strQuery = "
    SELECT 
        n.`id`
        ,n.`title".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS title
        ,n.`url".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS url
        ,n.`start_date`
        ,n.`end_date`
        ,n.`lead".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS lead
        ,n.`description".(!empty($strLanguage) ? "_{$strLanguage}" : "")."` AS description
        ,n.`image`
        ,c.name as city_name
    FROM 
        `news` as n
    JOIN
        `news_type` as nt ON n.id = nt.news_id
    LEFT JOIN
        city as c ON c.id = n.city_id AND c.is_active = 1 AND c.delete_date IS NULL 
    WHERE
        n.`delete_date` IS NULL 
        AND n.`is_active` = 1
        AND nt.`type_id` = 2
        AND (n.`publication_start_date` IS NULL OR n.`publication_start_date`='0000-00-00 00:00:00' OR n.`publication_start_date` <= NOW())
        AND (n.`publication_end_date` IS NULL OR n.`publication_end_date`='0000-00-00 00:00:00' OR n.`publication_end_date` > NOW())
    ORDER BY
        n.`start_date` DESC
";

$tblData = $objDb->getAll($strQuery);

foreach ($tblData as $numIdx=>$rowData) {
    
    if (!empty($rowData['image']) && is_file($CONF['pub_dir']."cikkek/".$rowData['id']."/small/".$rowData['image'])) {
        $tblData[$numIdx]['image'] = $CONF['base_url']."media/pub/cikkek/".$rowData['id']."/small/".$rowData['image'];
    } else {
        $tblData[$numIdx]['image'] = $CONF['web_url']."images/news-placeholder.jpg";
    }
    if(!empty($rowData['title'])){
        $data[] = array(
            'imgSource' => $tblData[$numIdx]['image'],
            'title' => $rowData['title'],
            'city' => (!empty($rowData['city_name']) ? $rowData['city_name'] : ''),
            'start' => $rowData['start_date'],
            'lead' => (!empty($rowData['lead']) ? $rowData['lead'] : ''),
            'moreSrc' => $CONF['base_url_lang'].$rowLabel['esemenyek_url'].'/'.$rowData['url'],
            'backgroundColor' => '#738d40',
            'strMore' => $rowLabel['reszletek']
        );
    }
}

echo json_encode($data);

?>