'use strict';

var util = require('./util');
util.checkIfEnvIsValid();

var gulp = require('gulp');
var $    = require('gulp-load-plugins')();
var del  = require('del');

// ----------------------
// Front taskok
// ----------------------

gulp.task('scripts', function() {

    var browserify = require('browserify');
    var source     = require('vinyl-source-stream');
    var buffer     = require('vinyl-buffer');
    var glob       = require('glob');
    var path       = require('path');
    var Q          = require('q');

    del.sync(util.dest(util.jsDestDir));

    var promises = [];
    glob.sync(util.src('js/*.js')).forEach(function(scriptPath) {
        var deferred = Q.defer();
        promises.push(deferred.promise);

        browserify({
            entries: scriptPath
        })
            .bundle()
            .pipe(source(path.basename(scriptPath)))
            .pipe(buffer())
            .pipe($.if(util.isEnv('prod'), $.uglify({ mangle: false }).on('error', function(e){
                console.log(e);
            })))
            .pipe(gulp.dest(util.jsDestDir))
            .on('finish', function() { deferred.resolve() });
    });

    return Q.all(promises);
});

gulp.task('html', function () {
    return gulp.src([util.src('/**/*.html'), '!' + util.src('/includes/*')])
        .pipe($.fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .on('error', util.handleError)
        .pipe($.rename({dirname: ''}))
        .pipe(gulp.dest(util.dest('')));
});

gulp.task('vendors', function() {

    var Q = require('q');
    // Promises are used here, so that the task finished console log would be
    // accurate.
    var promises = [];

    del.sync("app/web/vendor");

    (function () {
        // Concatenate files into vendor.js
        var deferred = Q.defer();
        promises.push(deferred.promise);

        console.log('Creating "vendor.js"...');
        gulp
            .src([
                __dirname + '/node_modules/jquery/dist/jquery.min.js',
                __dirname + '/node_modules/bootstrap/dist/js/bootstrap.min.js',
                __dirname + '/node_modules/select2/dist/js/select2.min.js',
                __dirname + '/node_modules/slick-carousel/slick/slick.min.js',
                __dirname + '/node_modules/simple-jscalendar/source/jsCalendar.min.js',
                __dirname + '/node_modules/simple-jscalendar/source/jsCalendar.lang.template.js',
                __dirname + '/node_modules/moment/min/moment.min.js',
                __dirname + '/node_modules/moment/locale/hu.js',
                __dirname + '/node_modules/fullcalendar/dist/fullcalendar.min.js',
                __dirname + '/node_modules/fullcalendar/dist/local/hu.js',
                __dirname + '/node_modules/photoswipe/dist/photoswipe.js',
                __dirname + '/node_modules/photoswipe/dist/photoswipe-ui-default.js',
                __dirname + '/node_modules/jquery-cookiebar/jquery.cookiebar.js',
            ])
            .pipe($.concat('vendor.js'))
            // Careful with this, sometimes it mangle's too aggressively. In
            // those cases, try `$.uglify({ mangle: false })` or change the
            // library entirely if you want
            //.pipe($.uglify())
            .pipe(gulp.dest("app/web/vendor"))
            .on('finish', function() { deferred.resolve() });
    })();

    (function () {
        // Concatenate files into vendor.css
        var deferred = Q.defer();
        promises.push(deferred.promise);

        console.log('Creating "vendor.css"...');
        gulp
            .src([
                __dirname + '/node_modules/sweetalert2/dist/sweetalert2.min.css',
                __dirname + '/node_modules/slick-carousel/slick/slick-theme.css',
                __dirname + '/node_modules/slick-carousel/slick/slick.css',
                __dirname + '/node_modules/simple-jscalendar/source/jsCalendar.min.css',
                __dirname + '/node_modules/fullcalendar/dist/fullcalendar.min.css',
                __dirname + '/node_modules/photoswipe/dist/photoswipe.css',
                __dirname + '/node_modules/photoswipe/dist/default-skin/default-skin.css',
                __dirname + '/node_modules/jquery-cookiebar/jquery.cookiebar.css'
            ])
            .pipe($.cssmin())
            .pipe($.concat('vendor.css'))
            .pipe(gulp.dest("app/web/vendor"))
            .on('finish', function() { deferred.resolve() });
    })();

    return Q.all(promises);
});

gulp.task('styles', function () {
    var path  = require('path');
    del.sync(util.dest(util.cssDestDir));

    return gulp.src(util.src('scss/*.scss'))
        .pipe($.if(util.isEnv('dev'), $.sourcemaps.init()))
        .pipe($.sass({
            includePaths: [
                path.join(__dirname, '/node_modules')
            ],
        })
            .on('error', util.handleError))
        .pipe($.autoprefixer({browsers: ['> 1%']}))
        .pipe($.if(util.isEnv('dev'), $.sourcemaps.write()))
        .pipe($.if(util.isEnv('prod'), $.cssmin()))
        .pipe(gulp.dest(util.cssDestDir));
});

// ----------------------
// Admin taskok
// ----------------------

gulp.task('admin-vendors', function() {

    var Q = require('q');
    // Promises are used here, so that the task finished console log would be
    // accurate.
    var promises = [];

    del.sync("app/admin/web/vendor");

    (function () {
        // Concatenate files into vendor.js
        var deferred = Q.defer();
        promises.push(deferred.promise);

        console.log('Creating "vendor.js"...');
        gulp
            .src([
                __dirname + '/node_modules/jquery/dist/jquery.min.js',
                __dirname + '/node_modules/bootstrap/dist/js/bootstrap.min.js',
                __dirname + '/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
                __dirname + '/node_modules/quill/dist/quill.min.js',
                __dirname + '/node_modules/select2/dist/js/select2.min.js',
                __dirname + '/node_modules/sweetalert2/dist/sweetalert2.min.js',
                __dirname + '/node_modules/flatpickr/dist/flatpickr.min.js',
                __dirname + '/node_modules/flatpickr/dist/l10n/hu.js',

            ])
            .pipe($.concat('vendor.js'))
            // Careful with this, sometimes it mangle's too aggressively. In
            // those cases, try `$.uglify({ mangle: false })` or change the
            // library entirely if you want
            //.pipe($.uglify())
            .pipe(gulp.dest("app/admin/web/vendor"))
            .on('finish', function() { deferred.resolve() });
    })();

    (function () {
        // Concatenate files into vendor.css
        var deferred = Q.defer();
        promises.push(deferred.promise);

        console.log('Creating "vendor.css"...');
        gulp
            .src([
                __dirname + '/node_modules/sweetalert2/dist/sweetalert2.min.css',
                __dirname + '/node_modules/quill/dist/quill.snow.css',
                __dirname + '/node_modules/quill/dist/quill.bubble.css',
                __dirname + '/node_modules/flatpickr/dist/flatpickr.min.css'
            ])
            .pipe($.cssmin())
            .pipe($.concat('vendor.css'))
            .pipe(gulp.dest("app/admin/web/vendor"))
            .on('finish', function() { deferred.resolve() });
    })();

    return Q.all(promises);
});

gulp.task('admin-html', function () {
    return gulp.src([util.adminSrc('/**/*.html'), '!' + util.src('/includes/*')])
        .pipe($.fileInclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .on('error', util.handleError)
        .pipe($.rename({dirname: ''}))
        .pipe(gulp.dest(util.adminDest('')));
});

gulp.task('admin-styles', function () {
    var path  = require('path');
    del.sync(util.adminDest(util.adminCssDestDir));

    return gulp.src(util.adminSrc('scss/*.scss'))
        .pipe($.if(util.isEnv('dev'), $.sourcemaps.init()))
        .pipe($.sass({
            includePaths: [
                path.join(__dirname, '/node_modules')
            ],
        })
            .on('error', util.handleError))
        .pipe($.autoprefixer({browsers: ['> 1%']}))
        .pipe($.if(util.isEnv('dev'), $.sourcemaps.write()))
        .pipe($.if(util.isEnv('prod'), $.cssmin()))
        .pipe(gulp.dest(util.adminDest('css')));
});

gulp.task('admin-scripts', function() {
   
    var browserify = require('browserify');
    var source     = require('vinyl-source-stream');
    var buffer     = require('vinyl-buffer');
    var glob       = require('glob');
    var path       = require('path');
    var Q          = require('q');
    
    del.sync(util.adminDest(util.adminJsDestDir));

    var promises = [];
    glob.sync(util.adminSrc('js/*.js')).forEach(function(scriptPath) {
        var deferred = Q.defer();
        promises.push(deferred.promise);

        browserify({
            entries: scriptPath
        })
        .bundle()
        .pipe(source(path.basename(scriptPath)))
        .pipe(buffer())
        .pipe($.if(util.isEnv('prod'), $.uglify({ mangle: false }).on('error', function(e){
            console.log(e);
        })))
        .pipe(gulp.dest(util.adminDest(util.adminJsDestDir)))
        .on('finish', function() { deferred.resolve() });
    });

    return Q.all(promises);
});



gulp.task('watch', function() {
    if (!util.isEnv('dev')) {
        return;
    }

    var notifier = require('node-notifier');

    $.watch(util.src('scss/**/*.scss'), function () {
        gulp.start('styles');
    });

    $.watch(util.src('js/**/*.js'), function () {
        gulp.start('scripts');
    });

    $.watch(util.src('/**/*.html'), function () {
        gulp.start('html');
    });
});

gulp.task('adminWatch', function() {
    if (!util.isEnv('dev')) {
        return;
    }

    var notifier = require('node-notifier');

    $.watch(util.adminSrc('scss/**/*.scss'), function () {
        gulp.start('styles');
    });

    $.watch(util.adminSrc('js/**/*.js'), function () {
        gulp.start('scripts');
    });

    $.watch(util.adminSrc('/**/*.html'), function () {
        gulp.start('html');
    });
});

gulp.task('front', ['html', 'scripts', 'styles', 'watch']);

gulp.task('admin', ['admin-html', 'admin-scripts', 'admin-styles', 'adminWatch']);