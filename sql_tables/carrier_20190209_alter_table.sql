ALTER TABLE `carrier` ADD `title_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title`, ADD `title_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title_en`;

ALTER TABLE `carrier` ADD `url_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `url`;
ALTER TABLE `carrier` ADD `url_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `url_en`;

ALTER TABLE `carrier` ADD `lead_en` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `lead`;
ALTER TABLE `carrier` ADD `lead_ro` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `lead_en`;

ALTER TABLE `carrier` ADD `description_en` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `carrier` ADD `description_ro` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `description_en`;
