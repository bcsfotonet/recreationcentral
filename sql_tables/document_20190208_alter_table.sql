ALTER TABLE `document` ADD `title_en` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title`;
ALTER TABLE `document` ADD `title_ro` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title_en`;

ALTER TABLE `document` ADD `lead_en` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `lead`;
ALTER TABLE `document` ADD `lead_ro` VARCHAR(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `lead_en`;

ALTER TABLE `document` ADD `filename_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `filename`;
ALTER TABLE `document` ADD `filename_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `filename_en`;