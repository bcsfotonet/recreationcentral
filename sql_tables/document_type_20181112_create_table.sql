
CREATE TABLE `document_type` (
  `document_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `document_type`
--
ALTER TABLE `document_type`
  ADD UNIQUE KEY `document_id` (`document_id`,`type_id`);
COMMIT;
