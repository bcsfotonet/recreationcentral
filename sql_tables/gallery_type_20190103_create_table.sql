
CREATE TABLE `gallery_type` (
  `gallery_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `gallery_type`
--
ALTER TABLE `gallery_type`
  ADD UNIQUE KEY `gallery_id` (`gallery_id`,`type_id`);
COMMIT;
