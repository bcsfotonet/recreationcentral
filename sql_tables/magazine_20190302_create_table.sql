-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `magazine`
--

CREATE TABLE IF NOT EXISTS `magazine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year_and_issue_id` int(11) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `url_en` varchar(128) DEFAULT NULL,
  `url_ro` varchar(128) DEFAULT NULL,
  `main_editor` varchar(128) DEFAULT NULL,
  `main_editor_en` varchar(128) DEFAULT NULL,
  `main_editor_ro` varchar(128) DEFAULT NULL,
  `orderable` tinyint(1) NOT NULL DEFAULT '1',
  `lang` varchar(128) DEFAULT NULL,
  `lang_en` varchar(128) DEFAULT NULL,
  `lang_ro` varchar(128) DEFAULT NULL,
  `foundation_year` varchar(4) DEFAULT NULL,
  `paper_issn` varchar(128) DEFAULT NULL,
  `online_issn` varchar(128) DEFAULT NULL,
  `editorial_address` varchar(128) DEFAULT NULL,
  `editorial_address_en` varchar(128) DEFAULT NULL,
  `editorial_address_ro` varchar(128) DEFAULT NULL,
  `publication_year` varchar(4) DEFAULT NULL,
  `volume` varchar(4) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `description_en` longtext DEFAULT NULL,
  `description_ro` longtext DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `filename` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
