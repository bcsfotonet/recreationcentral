-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `magazine_order`
--

CREATE TABLE IF NOT EXISTS `magazine_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `magazine_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `num_magazine` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
