-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `magazine_staff`
--

CREATE TABLE IF NOT EXISTS `magazine_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `magazine_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
