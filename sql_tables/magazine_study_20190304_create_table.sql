-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `magazine_study`
--

CREATE TABLE IF NOT EXISTS `magazine_study` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `magazine_id` int(11) NOT NULL,
  `study_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
