-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2018. Okt 01. 00:48
-- Kiszolgáló verziója: 10.1.35-MariaDB
-- PHP verzió: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `recreation`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL,
  `parent_menu_id` int(11) DEFAULT NULL,
  `is_visible_main_menu` tinyint(1) NOT NULL DEFAULT '1',
  `is_visible_footer_menu` tinyint(1) NOT NULL DEFAULT '1',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '0',
  `content_table` varchar(32) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `ext_url` varchar(128) DEFAULT NULL,
  `target_value` varchar(32) DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `menu`
--

INSERT INTO `menu` (`id`, `name`, `parent_menu_id`, `is_visible_main_menu`, `is_visible_footer_menu`, `is_active`, `priority`, `content_table`, `content_id`, `ext_url`, `target_value`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES
(1, 'Média', NULL, 1, 0, 1, 1, NULL, NULL, NULL, NULL, 1, '2018-09-24 20:03:17', 1, '2018-09-30 09:43:01', NULL, NULL),
(2, 'Hírek', 1, 1, 0, 1, 1, 'type', 1, NULL, NULL, 1, '2018-09-24 20:04:27', 1, '2018-09-30 09:43:22', NULL, NULL),
(3, 'Események', 1, 1, 0, 1, 2, 'type', 2, NULL, NULL, 1, '2018-09-24 20:05:35', NULL, NULL, NULL, NULL),
(4, 'Videók', 1, 1, 0, 1, 3, 'type', 5, NULL, NULL, 1, '2018-09-24 20:06:13', NULL, NULL, NULL, NULL),
(5, 'Rólunk írták', 1, 1, 0, 1, 4, 'type', 3, NULL, NULL, 1, '2018-09-24 20:06:58', NULL, NULL, NULL, NULL),
(6, 'Társaság', NULL, 1, 0, 1, 2, NULL, NULL, NULL, NULL, 1, '2018-09-25 19:52:39', 1, '2018-09-30 22:23:22', NULL, NULL),
(7, 'Munkatársak', 6, 0, 0, 1, 0, 'type', 4, NULL, NULL, 1, '2018-09-29 10:33:15', NULL, NULL, NULL, NULL),
(8, 'Bemutatkozás', 6, 0, 0, 1, 0, 'static', 1, NULL, NULL, 1, '2018-09-29 22:44:19', NULL, NULL, NULL, NULL),
(12, 'Szerkesztőbizottság', 6, 0, 0, 1, 0, 'type', 6, NULL, NULL, 1, '2018-09-29 23:09:33', NULL, NULL, NULL, NULL),
(13, 'Alapdokumentumok', 6, 0, 0, 1, 0, 'type', 7, NULL, NULL, 1, '2018-09-29 23:13:15', NULL, NULL, NULL, NULL);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
