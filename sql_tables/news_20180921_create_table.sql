-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `lead` text,
  `description` longtext,
  `publication_start_date` timestamp NULL DEFAULT NULL,
  `publication_end_date` timestamp NULL DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;

--
-- A tábla adatainak kiíratása `news`
--

INSERT INTO `news` (`id`, `title`, `url`, `start_date`, `end_date`, `lead`, `description`, `publication_start_date`, `publication_end_date`, `image`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES
(1, 'A globalizáció és a változás hatása az emberi erőforrás menedzsment funkciókra', 'a-globalizacio-es-a-valtozas-hatasa-az-emberi-eroforras-menedzsment-funkciokra', '2018-10-11 12:20:00', '2018-10-22 12:00:00', 'Kovács Tamás Attila számos hazai publikáció szerzője a rekreáció, sport, edzéstan, rekreációs szakmai oktatás témáiban', 'Hosszú szöveg', NULL, NULL, '333x194.png', 1, 1, '2018-09-16 22:17:31', NULL, NULL, NULL, NULL);