ALTER TABLE `news` 
	ADD `type_id` INT(10) UNSIGNED NOT NULL AFTER `id`,
	ADD `is_highlighted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `is_active`;