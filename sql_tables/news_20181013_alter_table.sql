ALTER TABLE `news` CHANGE `type_id` `city_id` INT(10) UNSIGNED NULL DEFAULT NULL;

ALTER TABLE `news` ADD `gallery_id` INT NULL DEFAULT NULL AFTER `is_highlighted`, ADD `video_id` INT NULL DEFAULT NULL AFTER `gallery_id`;