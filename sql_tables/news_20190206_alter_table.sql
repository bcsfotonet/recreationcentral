ALTER TABLE `news` ADD `title_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title`, ADD `title_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title_en`;

ALTER TABLE `news` ADD `url_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `url`;
ALTER TABLE `news` ADD `url_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `url_en`;

ALTER TABLE `news` ADD `lead_en` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `lead`;
ALTER TABLE `news` ADD `lead_ro` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `lead_en`;

ALTER TABLE `news` ADD `description_en` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `news` ADD `description_ro` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `description_en`;

/* teszt adatok */
update news set title_en = concat('__en__ ', title), title_ro = concat('__ro__ ', title);
update news set url_en = concat('en-', url), url_ro = concat('ro-', url);
update news set lead_en = concat('__en__ ', lead), lead_ro = concat('__ro__ ', lead);
update news set description_en = concat('__en__ ', description), description_ro = concat('__ro__ ', description);
