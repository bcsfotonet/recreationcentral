-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL, 
  `receive_mode` tinyint(1) NOT NULL DEFAULT '0',
  `post_different` tinyint(1) NOT NULL DEFAULT '0',
  `post_city` varchar(128) DEFAULT NULL,
  `post_name` varchar(128) DEFAULT NULL,
  `post_address` varchar(128) DEFAULT NULL,
  `privacy_policy` tinyint(1) NOT NULL DEFAULT '0',
  `post_price` int(11) NOT NULL DEFAULT '0',
  `total_price` int(11) NOT NULL DEFAULT '0',
  `currency` varchar(10) DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
