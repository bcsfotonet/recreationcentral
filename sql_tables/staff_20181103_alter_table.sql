ALTER TABLE `staff` CHANGE `title` `title` VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;

ALTER TABLE `staff` ADD `priority` INT NOT NULL DEFAULT '1' AFTER `is_active`;