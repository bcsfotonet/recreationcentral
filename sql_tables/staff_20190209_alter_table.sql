ALTER TABLE `staff` 
ADD `name_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `name`
, ADD `name_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `name_en`;

ALTER TABLE `staff` 
ADD `title_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title`
, ADD `title_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title_en`;

ALTER TABLE `staff` ADD `url_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `url`;
ALTER TABLE `staff` ADD `url_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `url_en`;

ALTER TABLE `staff` ADD `description_en` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `staff` ADD `description_ro` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `description_en`;
