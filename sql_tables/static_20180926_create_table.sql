--
-- Tábla szerkezet ehhez a táblához `static`
--

CREATE TABLE `static` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `lead` text,
  `description` longtext,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla adatainak kiíratása `static`
--

INSERT INTO `static` (`id`, `title`, `url`, `lead`, `description`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES
(1, 'Bemutakozás', 'bemutakozas', 'Bemutakozás rövid szöveg', 'Bemutatkozás hosszú szöveg', 1, 1, '2018-09-26 16:46:50', NULL, NULL, NULL, NULL),
(2, 'Közérdekű információk', 'kozerdeku-informaciok', NULL, 'Közérdekű információk szöveg', 1, 1, '2018-09-26 16:47:42', NULL, NULL, NULL, NULL),
(3, 'Pályázatok', 'Palyazatok', NULL, 'Pályázatok szöveg', 1, 1, '2018-09-26 16:48:21', NULL, NULL, NULL, NULL),
(4, 'Adó 1%', 'ado-1', NULL, 'Adó 1%', 1, 1, '2018-09-26 16:49:10', NULL, NULL, NULL, NULL),
(5, 'ÁSZF', 'aszf', NULL, 'ÁSZF szöveg', 1, 1, '2018-09-26 16:49:36', NULL, NULL, NULL, NULL),
(6, 'Adatvédelem', 'adatvedelem', NULL, 'Adatvédelem szöveg', 1, 1, '2018-09-26 16:50:05', NULL, NULL, NULL, NULL);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `static`
--
ALTER TABLE `static`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `static`
--
ALTER TABLE `static`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;
