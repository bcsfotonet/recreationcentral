
CREATE TABLE `static_document` (
  `static_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- A tábla indexei `static_document`
--
ALTER TABLE `static_document`
  ADD UNIQUE KEY `static_id` (`static_id`,`document_id`);
COMMIT;
