-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `study`
--

CREATE TABLE IF NOT EXISTS `study` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_staff_id` int(11) DEFAULT NULL,
  `co_editor_staff_id` int(11) DEFAULT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `magazine` varchar(128) NOT NULL,
  `doi` varchar(128) NOT NULL,
  `doi_url` varchar(128) NOT NULL,
  `abstract` text DEFAULT NULL,
  `bibliography` text DEFAULT NULL,
  `lead` text DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `image` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `priority` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
