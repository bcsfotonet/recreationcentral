ALTER TABLE `study` ADD `title_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title`
, ADD `title_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `title_en`;

ALTER TABLE `study` ADD `url_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `url`;
ALTER TABLE `study` ADD `url_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `url_en`;

ALTER TABLE `study` ADD `magazine_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `magazine`;
ALTER TABLE `study` ADD `magazine_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `magazine_en`;

ALTER TABLE `study` ADD `abstract_en` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `abstract`;
ALTER TABLE `study` ADD `abstract_ro` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `abstract_en`;

ALTER TABLE `study` ADD `bibliography_en` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `bibliography`;
ALTER TABLE `study` ADD `bibliography_ro` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `bibliography_en`;

ALTER TABLE `study` ADD `lead_en` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `lead`;
ALTER TABLE `study` ADD `lead_ro` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `lead_en`;

ALTER TABLE `study` ADD `description_en` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `description`;
ALTER TABLE `study` ADD `description_ro` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `description_en`;
