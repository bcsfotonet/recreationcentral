ALTER TABLE `study` 
ADD `authors` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `magazine_ro`
, ADD `authors_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `authors`
, ADD `authors_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `authors_en`
, ADD `keywords` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `authors_ro`
, ADD `keywords_en` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `keywords`
, ADD `keywords_ro` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL AFTER `keywords_en`;