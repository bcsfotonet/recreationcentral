-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `study_editor_staff`
--

CREATE TABLE IF NOT EXISTS `study_editor_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `study_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
