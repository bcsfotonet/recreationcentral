-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sys_module`
--

CREATE TABLE IF NOT EXISTS `sys_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_group_id` int(11) DEFAULT NULL,
  `name` varchar(128) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_new_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `is_edit_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `icon_class` varchar(64) DEFAULT NULL,
  `type` varchar(16) NOT NULL DEFAULT 'list',
  `url` varchar(64) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modifiy_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=19;

--
-- A tábla adatainak kiíratása `sys_module`
--

INSERT INTO `sys_module` (`id`, `module_group_id`, `name`, `priority`, `is_active`, `is_new_allowed`, `is_edit_allowed`, `is_delete_allowed`, `icon_class`, `type`, `url`, `create_user_id`, `create_date`, `modify_user_id`, `modifiy_date`, `delete_user_id`, `delete_date`) VALUES
(1, NULL, 'Adminisztrátorok', 10, 1, 1, 1, 1, 'fa-users', 'grid', 'adminisztratorok', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(2, NULL, 'Dokumentumtár', 20, 1, 1, 1, 1, 'fa-users', 'list', 'dokumentumtar', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(3, 1, 'Slider', 30, 1, 1, 1, 1, 'fa-caret-square-right', 'list', 'slider', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(4, 1, 'Központi galéria', 40, 1, 1, 1, 1, 'fa-images', 'list', 'kozponti-galeria', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(5, 1, 'Videó', 50, 1, 1, 1, 1, 'fa-youtube', 'list', 'video', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(6, 2, 'Statikus', 60, 1, 1, 1, 1, 'fa-align-left', 'grid', 'statikus', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(7, 2, 'Cikkek', 70, 1, 1, 1, 1, 'fa-align-left', 'list', 'cikkek', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(8, 2, 'Előadások', 80, 1, 1, 1, 1, 'fa-align-left', 'list', 'eloadasok', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(9, 2, 'GY.I.K.', 90, 1, 1, 1, 1, 'fa-align-left', 'list', 'gyik', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(10, 2, 'Munkatársak', 100, 1, 1, 1, 1, 'fa-align-left', 'list', 'munkatarsak', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(11, 2, 'Szerzők', 110, 1, 1, 1, 1, 'fa-align-left', 'list', 'szerzok', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(12, 2, 'Referenciák', 120, 1, 1, 1, 1, 'fa-align-left', 'list', 'referenciak', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(13, 2, 'Szponzorok', 130, 1, 1, 1, 1, 'fa-align-left', 'list', 'szponzorok', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(14, NULL, 'Menükezelés', 140, 1, 1, 1, 1, 'fa-bars', 'list', 'menukezeles', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(15, NULL, 'Rendelés', 150, 1, 1, 1, 1, 'fa-envelope', 'list', 'rendeles', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(16, NULL, 'Felhasználók', 160, 1, 1, 1, 1, 'fa-envelope', 'list', 'felhasznalok', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(17, NULL, 'Magazinok', 170, 1, 1, 1, 1, 'fa-align-left', 'list', 'magazinok', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL),
(18, NULL, 'Tanulmányok', 180, 1, 1, 1, 1, 'fa-align-left', 'list', 'tanulmanyok', 1, '2018-08-12 20:51:33', NULL, NULL, NULL, NULL);