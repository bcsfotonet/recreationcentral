-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sys_module_group`
--

CREATE TABLE IF NOT EXISTS `sys_module_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `icon_class` varchar(64) DEFAULT NULL,
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

--
-- A tábla adatainak kiíratása `sys_module_group`
--

INSERT INTO `sys_module_group` (`id`, `name`, `priority`, `is_active`, `icon_class`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES
(1, 'Média', 25, 1, 'fa-file-image', 1, '2018-08-12 20:45:04', NULL, NULL, NULL, NULL),
(2, 'Tartalom', 75, 1, 'fa-align-left', 1, '2018-08-12 20:45:04', NULL, NULL, NULL, NULL);