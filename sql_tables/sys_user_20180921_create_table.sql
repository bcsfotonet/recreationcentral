-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sys_user`
--

CREATE TABLE IF NOT EXISTS `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `login` varchar(32) NOT NULL,
  `passwd` varchar(72) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(255) NOT NULL,
  `user_group_id` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=3;

--
-- A tábla adatainak kiíratása `sys_user`
--

INSERT INTO `sys_user` (`id`, `name`, `login`, `passwd`, `is_active`, `email`, `user_group_id`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES
(1, 'Admin', 'admin', '$2y$10$Qo0RwFRSLw/kv/A/iKUFQugQIsaH7LGfjJsRdLCzRS95rhIIDXOIC', 1, 'horvath.tamas@bcsfotonet.hu', 0, 1, '2018-08-12 20:41:57', 1, '2018-09-16 08:48:07', NULL, NULL),
(2, 'Kert user', 'kert', '$2y$10$Qo0RwFRSLw/kv/A/iKUFQugQIsaH7LGfjJsRdLCzRS95rhIIDXOIC', 1, 'horvath.tamas@bcsfotonet.hu', 1, 1, '2018-08-12 20:41:57', 1, '2018-09-13 22:07:50', NULL, NULL);
