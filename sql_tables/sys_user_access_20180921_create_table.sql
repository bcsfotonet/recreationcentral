-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sys_user_access`
--

CREATE TABLE IF NOT EXISTS `sys_user_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `is_new_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `is_edit_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `is_delete_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modifiy_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=6;

--
-- A tábla adatainak kiíratása `sys_user_access`
--

INSERT INTO `sys_user_access` (`id`, `user_id`, `module_id`, `is_new_allowed`, `is_edit_allowed`, `is_delete_allowed`, `create_user_id`, `create_date`, `modify_user_id`, `modifiy_date`, `delete_user_id`, `delete_date`) VALUES
(1, 2, 1, 1, 1, 1, 0, '2018-09-13 21:55:45', NULL, NULL, NULL, NULL),
(2, 2, 2, 1, 1, 1, 0, '2018-09-13 21:55:45', NULL, NULL, NULL, NULL),
(3, 2, 3, 1, 1, 1, 0, '2018-09-13 21:55:45', NULL, NULL, NULL, NULL),
(4, 2, 4, 1, 1, 1, 0, '2018-09-13 22:04:26', NULL, NULL, NULL, NULL),
(5, 2, 5, 1, 1, 1, 0, '2018-09-13 22:07:50', NULL, NULL, NULL, NULL);