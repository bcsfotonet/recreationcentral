-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `sys_user_group`
--

CREATE TABLE IF NOT EXISTS `sys_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modify_user_id` int(11) DEFAULT NULL,
  `modifiy_date` timestamp NULL DEFAULT NULL,
  `delete_user_id` int(11) DEFAULT NULL,
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=2;

--
-- A tábla adatainak kiíratása `sys_user_group`
--

INSERT INTO `sys_user_group` (`id`, `name`, `create_user_id`, `create_date`, `modify_user_id`, `modifiy_date`, `delete_user_id`, `delete_date`) VALUES
(0, 'Super admin', 1, '2018-08-12 20:37:09', NULL, NULL, NULL, NULL),
(1, 'Kert', 1, '2018-08-12 20:43:12', NULL, NULL, NULL, NULL);