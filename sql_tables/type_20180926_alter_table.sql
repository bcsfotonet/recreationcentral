ALTER TABLE `type` ADD `url` VARCHAR(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `type_group`;

UPDATE `type` SET `url` = 'esemenyek', `modify_date` = NULL, `delete_date` = NULL WHERE `type`.`id` = 2;
UPDATE `type` SET `url` = 'hirek', `modify_date` = NULL, `delete_date` = NULL WHERE `type`.`id` = 1;
UPDATE `type` SET `url` = 'rolunk-irtak', `modify_date` = NULL, `delete_date` = NULL WHERE `type`.`id` = 3;

INSERT INTO `type` (`id`, `name`, `type_group`, `url`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES (NULL, 'Munkatársak', 'staff', 'munkatarsak', '1', '1', CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);

INSERT INTO `type` (`id`, `name`, `type_group`, `url`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES (NULL, 'Videók', 'video', 'videok', '1', '1', CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);
INSERT INTO `type` (`id`, `name`, `type_group`, `url`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES (NULL, 'Szerkesztőbizottság', 'author', 'szerkesztobizottsag', '1', '1', CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);
INSERT INTO `type` (`id`, `name`, `type_group`, `url`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES (NULL, 'Alapdokumentumok', 'document', 'alapdokumentumok', '1', '1', CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);
INSERT INTO `type` (`id`, `name`, `type_group`, `url`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES (NULL, 'Eseménynaptár', 'event', 'esemenynaptar', '1', '1', CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);
INSERT INTO `type` (`id`, `name`, `type_group`, `url`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES (NULL, 'Kapcsolat', 'contact', 'kapcsolat', '1', '1', CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);
INSERT INTO `type` (`id`, `name`, `type_group`, `url`, `is_active`, `create_user_id`, `create_date`, `modify_user_id`, `modify_date`, `delete_user_id`, `delete_date`) VALUES (NULL, 'GY.I.K.', 'faq', 'gyik', '1', '1', CURRENT_TIMESTAMP, NULL, NULL, NULL, NULL);

UPDATE `type` SET `name` = 'Hírek', `modify_date` = NULL, `delete_date` = NULL WHERE `type`.`id` = 1;
UPDATE `type` SET `name` = 'Események', `modify_date` = NULL, `delete_date` = NULL WHERE `type`.`id` = 2;
