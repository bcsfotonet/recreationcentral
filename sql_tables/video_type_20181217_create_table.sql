
CREATE TABLE `video_type` (
  `video_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `video_type`
--
ALTER TABLE `video_type`
  ADD UNIQUE KEY `video_id` (`video_id`,`type_id`);
COMMIT;
