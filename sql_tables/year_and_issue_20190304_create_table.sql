-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `year_and_issue`
--

CREATE TABLE IF NOT EXISTS `year_and_issue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `title_en` varchar(128) DEFAULT NULL,
  `title_ro` varchar(128) DEFAULT NULL,
  `year_title` varchar(128) DEFAULT NULL,
  `year_title_en` varchar(128) DEFAULT NULL,
  `year_title_ro` varchar(128) DEFAULT NULL,
  `issue_title` varchar(128) DEFAULT NULL,
  `issue_title_en` varchar(128) DEFAULT NULL,
  `issue_title_ro` varchar(128) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `url_en` varchar(128) DEFAULT NULL,
  `url_ro` varchar(128) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '1',
  `delete_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
