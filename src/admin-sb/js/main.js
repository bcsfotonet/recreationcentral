//var editor_js = require('./moduls/editor');
var select2 = require('./moduls/select2');
var filter = require('./moduls/filter');
var sweetalert = require('./moduls/sweetalert');
//var status = require('./moduls/status');
var tooltip = require('./moduls/tooltip');
var menu = require('./moduls/menu');
var flatpickr = require('./moduls/flatpickr');

//editor_js.init();
select2.init();
filter.init();
sweetalert.init();
//status.init();
tooltip.init();
menu.init();
flatpickr.init();

var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],

    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    ['link'], //, 'image'
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],

    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

    [{ 'align': [] }],

    ['clean'] 
];

$("document").ready(function(){

    var EditorContainer = document.getElementById('hu_editor');
    
    if(EditorContainer !== null) {
        var hu_editor = new Quill(EditorContainer, {
            modules: { toolbar: toolbarOptions },
            theme: 'snow',
            bounds: '#scrolling-container',
            scrollingContainer: '#scrolling-container'
        });
    } else {
		
    }
    
    var EnEditorContainer = document.getElementById('en_editor');
    
    if(EnEditorContainer !== null) {
        var en_editor = new Quill(EnEditorContainer, {
            modules: { toolbar: toolbarOptions },
            theme: 'snow',
            bounds: '#scrolling-container',
            scrollingContainer: '#scrolling-container'
        });
    } else {
		
    }
    
    var RoEditorContainer = document.getElementById('ro_editor');
    
    if(RoEditorContainer !== null) {
        var ro_editor = new Quill(RoEditorContainer, {
            modules: { toolbar: toolbarOptions },
            theme: 'snow',
            bounds: '#scrolling-container',
            scrollingContainer: '#scrolling-container'
        });
    } else {
		
    }
    
    $(".url_parent_input").on("keyup", function(){
//    	console.log($("div.url_input_div").data( "url_field" ));
    	var strInputNameHu = $("div#url_to_hu").data( "url_field" );
        var rowInputNameHu = strInputNameHu.split(";");
        var strUrlTmpHu = "";
        
        $.each( rowInputNameHu, function( index, value ) {
        	strUrlTmpHu += $("input[name='"+value+"']").val()+"-";
        });
        if (strUrlTmpHu != "") {
    		var strUrl = urlGen(strUrlTmpHu);
    		$("div#url_to_hu input.url_input").val(strUrl);
    		$("div#url_to_hu span.url_span").html(strUrl);
    	}
        
        var strInputNameEn = $("div#url_to_en").data( "url_field" );
        var rowInputNameEn = strInputNameEn.split(";");
        var strUrlTmpEn = "";
        
        $.each( rowInputNameEn, function( index, value ) {
        	strUrlTmpEn += $("input[name='"+value+"']").val()+"-";
        });
        if (strUrlTmpEn != "") {
    		var strUrl = urlGen(strUrlTmpEn);
    		$("div#url_to_en input.url_input").val(strUrl);
    		$("div#url_to_en span.url_span").html(strUrl);
    	}
        
        var strInputNameRo = $("div#url_to_ro").data( "url_field" );
        var rowInputNameRo = strInputNameRo.split(";");
        var strUrlTmpRo = "";
        
        $.each( rowInputNameRo, function( index, value ) {
        	strUrlTmpRo += $("input[name='"+value+"']").val()+"-";
        });
        if (strUrlTmpRo != "") {
    		var strUrl = urlGen(strUrlTmpRo);
    		$("div#url_to_ro input.url_input").val(strUrl);
    		$("div#url_to_ro span.url_span").html(strUrl);
    	}
        
//    	if($("div.url_input_div").data( "url_field" ) === $(this).attr("name")){
//    		var strUrl = urlGen($(this).val());
//    		$("div.url_input_div input.url_input").val(strUrl);
//    		$("div.url_input_div span.url_span").html(strUrl);
//    	}
    });

	$(".module-save-form").on("submit", function() {
		
		var postData = new FormData(this);
//    	var fileData = new FormData();

		if(typeof(hu_editor) != "undefined" && hu_editor !== null){
//			postData += "&"+$("#hu_editor").data("input_name")+"="+hu_editor.root.innerHTML;
			postData.append($("#hu_editor").data("input_name"), hu_editor.root.innerHTML);
		}
                if(typeof(en_editor) != "undefined" && en_editor !== null){
//			postData += "&"+$("#hu_editor").data("input_name")+"="+hu_editor.root.innerHTML;
			postData.append($("#en_editor").data("input_name"), en_editor.root.innerHTML);
		}
                if(typeof(ro_editor) != "undefined" && ro_editor !== null){
//			postData += "&"+$("#hu_editor").data("input_name")+"="+hu_editor.root.innerHTML;
			postData.append($("#ro_editor").data("input_name"), ro_editor.root.innerHTML);
		}
//		if ($(".file_upload_input").length == 1) {
//		    if ($(".file_upload_input").prop('files').length > 0) {
		    	
//		    	$.each( $(".file_upload_input").prop('files'), function( index, value ) {
//			    	console.log( index + ": " + value['name'] );
//			    	postData.append($(".file_upload_input").prop('name'), value, value['name']);		    	
//	    		});

//		    	console.log($(".file_upload_input").prop('name'));
//		    	console.log($(".file_upload_input").prop('files')[0]['name']);
//		    	console.log("sdsd");
//		    	postData.append($(".file_upload_input").prop('name'), $(".file_upload_input").prop('files')[0], $(".file_upload_input").prop('files')[0]['name']);		    	

//		    }
//		}
		
//		console.log(postData);
		saveAjax(postData);
		return false;
	});
	
	$(".module-row-save-field").on("change", function() {
		var postData = new FormData();
		
		if ($(this).getType() == "checkbox") {
//			var postData = this.name+"="+this.checked; //{$(this).attr("name")= $(this).prop("checked")};
			postData.append(this.name, this.checked);
			
			if($(this).hasClass("is_active_main_field") === true){
//				$(this).closest(".list-view__row").toggleClass("inactive");
				if(this.checked == false){
					$(this).closest(".list-view__row").addClass("inactive"); //főmenü inaktiválásra szürkíti a saját és a gyerekei sorát
					$(this).closest(".list-view__row").find(".eyeicon i").addClass("fa-eye-slash"); //áthúzottra állítja a szem ikont a saját és a gyerekei sorában
					$(this).closest(".list-view__row").find(".list-view__sub").addClass("inactive"); //inaktiválásra a gyerekei sorát szürkére állítja külön, hogy ha saját magát aktiválja, attól még a gyerekek inaktívak maradjanak
					$(this).closest(".list-view__row").find(".is_active_sub_field").prop('checked', false); //gyerekek checkbox-át is false-ra állítja
					$(this).closest(".grid-view__row").addClass("inactive");
					$(this).closest(".grid-view__row").find(".eyeicon i").addClass("fa-eye-slash");
				}else{
					$(this).closest(".list-view__row").removeClass("inactive"); //aktiválásra csak saját soráról veszi le a szürkítést
					$(this).find("~ .eyeicon i").removeClass("fa-eye-slash"); //aktiválásra csak saját sorában lévő szem ikont módosítja
					$(this).closest(".grid-view__row").removeClass("inactive");
				}
			}
			if($(this).hasClass("is_active_sub_field") === true){
//				$(this).closest(".list-view__sub").toggleClass("inactive");
				if(this.checked == false){
					$(this).closest(".list-view__sub").addClass("inactive"); //almenü inaktiválásra szürkíti a saját sorát
					$(this).find("~ .eyeicon i").addClass("fa-eye-slash"); //csak saját sorában állítja be az áthúzott szemet
				}else{
					$(this).closest(".list-view__sub").removeClass("inactive"); //almenü aktiválásra leveszi szürkítést a saját soráról
					$(this).closest(".list-view__row").removeClass("inactive"); //és a szülő menü soráról is
					$(this).find("~ .eyeicon i").removeClass("fa-eye-slash"); //eltávolítja az áthúzott szemet magáról
					$(this).closest(".list-view__row").find(".is_active_main_field").find("~ .eyeicon i").removeClass("fa-eye-slash"); //és a szülő menü soráról is
					$(this).closest(".list-view__row").find(".is_active_main_field").prop('checked', true); //szülő checkboxát is true-ra állítja
				}
			}
		} else if ($(this).getType() == "select") {
//			var postData = this.name+"="+$(this).val();
			postData.append(this.name, $(this).val());
		}
		
		saveAjax(postData);
	});
	
	// Aktív/inaktív jelölés 
	$(".eye_checkbox").on("change", function(){
		$(this).closest(".tiled-view").toggleClass("inactive");
		$(this).find("~ .eyeicon i").toggleClass("fa-eye-slash");
		$(this).val($(this).prop('checked'));
	});
	
	// Töröltnek jelölés
	$(".del_checkbox").on("change", function(){
		$(this).closest(".tiled-view").toggleClass("deleted");
		$(this).val($(this).prop('checked'));
	});
	
	// Fájlfeltöltés után előnézet vagy feltöltendő fájlok nevének listázása
	$(".file_upload_input").on("change", function(){
		readURL(this);
	});
	
	$(".search_input").on("change", function(){
//		setTimeout(function() {
        	console.log($(this).val());
//	    }, 1000);
	});
	
		
});

// Mentés
function saveAjax(postData){
//	console.log(postData);
//	console.log(fileData);
	strModuleUrl = $("input[name='module_url']").val();
//	console.log(strModuleUrl);
	if(strModuleUrl != ""){
//		postData = postData + "&module_url="+strModuleUrl;
		postData.append("module_url", strModuleUrl);
	}
//	console.log(postData);
	$.ajax({
		 type: "POST"
		,dataType: "json"
		,url: strAdminBaseUrl+"xhr_controller.php?p=save_module_form"
		,data: postData
		,processData: false
		,contentType: false
		,cache: false
		,success: function(data) {
//			console.log(data['type']);

			if(data['type'] == "ok"){ 
				if($("input[name='"+strModuleUrl+"_field[id]']").val() == "" && typeof(data["new_id"]) != "undefined" && data["new_id"] !== null){
//					console.log('insert');
					$("input[name='"+strModuleUrl+"_field[id]']").val(data["new_id"]);
				}else{
//					console.log('update');
//					console.log(data['refresh']);
					if ($(".return-msg-div").length == 0 && data['refresh'] == 'true') {
//						document.location.href = document.location.href;						
						window.location.reload();
					}
				}
//				console.log(data["msg"]);
//				console.log($(".return-msg-div").length);
				if (data["msg"].length > 0) {
					$(".return-msg-div").html(data["msg"]);
					$(".return-msg-div").removeClass("return-msg-div--warning");
					$(".return-msg-div").addClass("return-msg-div--success");
					$(".return-msg-div").fadeTo("fast" , 1);
					$(".return-msg-div").animate({
						opacity:0
					}, data["msg"].length*50, function() {
						$(".return-msg-div").html("");
						if (typeof(data["new_id"]) != "undefined" && data["new_id"] !== null) {
							document.location.href = strAdminBaseUrl+strModuleUrl+"/edit/"+data["new_id"];
						} else {
							document.location.href = document.location.href;
						}
					});	
				}
				
				$(".deleted").remove();
			} else {

//				inputonkénti errror
//				class="form__item form__item--error"
//				<span class="form__error">A mező kitöltése kötelező!</span>
				
				if (data["msg"].length > 0) {
					$(".return-msg-div").html(data["msg"]);
					$(".return-msg-div").removeClass("return-msg-div--success");
					$(".return-msg-div").addClass("return-msg-div--warning");
					$(".return-msg-div").fadeTo("fast" , 1);
//					hibánál nem tűnik el a szöveg
//					$(".return-msg-div").animate({
//						opacity:0
//					}, data["msg"].length*100, function() {
//						$(".return-msg-div").html("");
//					});	
				}
			}
			
		}
	});
} 

$.fn.getType = function(){ return this[0].tagName == "INPUT" ? this[0].type.toLowerCase() : this[0].tagName.toLowerCase(); }

function urlGen( strText ){
	strText = strText.toLowerCase();

	strText = strText.replace(/á/gi, "a");
	strText = strText.replace(/é/gi, "e");
	strText = strText.replace(/í/gi, "i");
	strText = strText.replace(/ó/gi, "o");
	strText = strText.replace(/ö/gi, "o");
	strText = strText.replace(/ő/gi, "o");
	strText = strText.replace(/ú/gi, "u");
	strText = strText.replace(/ü/gi, "u");
	strText = strText.replace(/ű/gi, "u");
	strText = strText.replace(/ß/gi, "ss");
	strText = strText.replace(/ä/gi, "ae");
	strText = strText.replace(/ë/gi, "e");

	strText = strText.replace(/[^a-z0-9_]/gi, "_");
	strText = strText.replace(/[_]+/gi, "-");
	strText = strText.replace(/^-+/gi, "");
	strText = strText.replace(/-+$/gi, "");
	return strText;
}


function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var inputId = $(input).prop('name');
        var column = $(input).data('column_name');
        
        reader.onload = function (e) {
        	
        	if (input.files.length == 1 && $("input[name='"+inputId+"']").prop("multiple") == false && $("input[name='"+inputId+"']").hasClass("image_upload_input") == true) {
        		// Egy darab kép feltölthetősége esetén előnézetet jelenítünk meg
//console.log(input.files);
	//        	$.each( input.files, function( index, value ) {
	//		    	console.log( index + ": " + value['name'] );
	//		    	postData.append($(".file_upload_input").prop('name'), value, value['name']);
	//		    	if (index > 0) {
	//		    		$('img[data-input_name="'+inputId+'"]').clone().insertAfter('img[data-input_name="'+inputId+'"]').attr('src', e.target.result);
	//		    	} else {
			    		$('img[data-input_name="'+inputId+'"]').attr('src', e.target.result);
	//		    	}
	//    		});
        	} else {
        		// Több darab kép vagy egyéb fájl feltölthetősége esetén a feltöltendő fájlok neveit jelenítjük meg
        		var strPreviewList = "";
        		$.each( input.files, function( index, value ) {
    		    	strPreviewList += value['name']+"<br/>";
        		});
        		$('.preview_'+column+' .preview-list').html(strPreviewList);
        	}
        	$('.preview_'+column).removeClass("d-none");
        	
        };

        reader.readAsDataURL(input.files[0]);
    }
}
