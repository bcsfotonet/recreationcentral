"use strict"


module.exports = {

    EditorContainer: null,
    EnEditorContainer: null,
    RoEditorContainer: null,

    init: function(){

        var EditorContainer = document.getElementById('hu_editor');
        var EnEditorContainer = document.getElementById('en_editor');
        var RoEditorContainer = document.getElementById('ro_editor');

        if (EditorContainer != null) {
            
            // Normál tartalom editorja
            var hu_editor = new Quill(EditorContainer, {
                modules: { toolbar: toolbarOptions },
                theme: 'snow'
            });

//            var delta = editor.getContents();
//            console.log(delta);
//            var length = editor.getLength();
//            console.log(length);
        }

        if (EnEditorContainer != null) {
            var en_editor = new Quill(EnEditorContainer, {
                modules: { toolbar: toolbarOptions },
                theme: 'snow'
            });
        }

        if (RoEditorContainer != null) {
            var ro_editor = new Quill(RoEditorContainer, {
                modules: { toolbar: toolbarOptions },
                theme: 'snow'
            });
        }
    
        var toolbarOptions = [
            ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
            ['blockquote', 'code-block'],
    
            [{ 'header': 1 }, { 'header': 2 }],               // custom button values
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    
            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    
            [{ 'align': [] }],
    
            ['clean'] 
        ];
    
        /* var seotoolbarOptions = [
            ['bold', 'italic', 'underline'],        // toggled buttons
    
            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
            [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
            [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    
            [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    
            [{ 'align': [] }],
    
            ['clean'] 
        ]; */
    }
}