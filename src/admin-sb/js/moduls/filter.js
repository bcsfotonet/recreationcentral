"use strict";

module.exports = {

    init: function(){

        // Tartalom szűrés
        $("#tartalomInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".tiled-view__data").filter(function() {
                $(this).parents(".tiled-view").toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });

        $("#news-filter").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".js-data").filter(function() {
                $(this).parents(".tiled-view").parent().toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    }

  /*   myFunction: function() {
        var input = document.getElementById('news-filter');
        var filter = input.value.toLowerCase();
        var data = document.querySelectorAll('.tiled-view__data');

        console.log(filter);

        // Loop through all list items, and hide those who don't match the search query
        for (var i = 0; i < data.length; i++) {
            var teszt = data[i];

            if (data[i].toLowerCase().indexOf(filter) > -1) {
                teszt.parentNode.parentNode.style.display = "";
                console.log(teszt.innerText);
                console.log(teszt.innerText.toUpperCase().indexOf(filter));
                //console.log(data[i].innerText.toLowerCase());
            } else {
                data[i].parentNode.parentNode.style.display = "none";
            }
        }
    } */
}