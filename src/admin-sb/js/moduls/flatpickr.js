"use strict";

var $ = require('jquery');

module.exports = {

    init: function(){
        var defaults = {
            allowInput: true,
            enableTime: true,
            time_24hr: true,
            locale: 'hu',
            onOpen: function(selectedDates, dateStr, instance) {
                var $elem = $(instance.calendarContainer);
                $elem.addClass('showTimeInput');

                if($elem.hasClass('custom-inited')) { return; }
                $elem.addClass('custom-inited');

                $elem.on('keydown', function(e) {
                    if (e.which === 13) {
                        instance.close();
                    }
                });
            }
        };

        $('.js-datetime').each(function() {
            var options = $.extend(true, {}, defaults);
            var customConfig = $(this).data('js-datetime');

            if(typeof customConfig !== 'undefined') {
                $.extend(true, options, customConfig.flatpickr || {});
            }

            if($(this).hasClass('flatpickr-input') === false) {
                new flatpickr($(this).get(0), options);
            }
        });
    }
}