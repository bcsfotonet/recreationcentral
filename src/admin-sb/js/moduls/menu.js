"use strict";

module.exports = {

    init: function(){
        var ls = window.localStorage;

        var menu = document.querySelectorAll('.js-dropdown');

        for(var i=0; i < menu.length; i++) {

            var menu_li= menu[i];

            menu_li.addEventListener("click", function(){

                var sub_li = this.children[1];

                if(ls.getItem("data") == "block") {
                    sub_li.style.display = "block";
                    /* console.log(sub_li); */
                }

                if(this.className === "nav__li nav__dropdown js-dropdown parent-menu-opened") {
                    sub_li.style.display = "none";
                    sub_li.style.transition = "all 2s";
                } else {
                    sub_li.style.display = "block";
                    sub_li.style.transition = "all 2s";
                    ls.setItem("data", sub_li);
                    console.log(ls.getItem('data'));
                }

                this.classList.toggle('parent-menu-opened');
                
            });

        }

        /* $('.js-dropdown').on('click',function(e){
            e.stopPropagation();
            $(this).toggleClass('parent-menu-opened');
            $('.parent-menu-opened').toggleClass('parent-menu-opened:after');
            $(this).children('.nav__subnav').stop().slideToggle();
            
        }); */
    
        //mobilmenu
        $('.hamburger-menu').on('click',function(e){
            e.stopPropagation();
            $('.hamburger-menu').toggleClass('hamburger-menu-opened');
            $('.sidebar').toggleClass('sidebar--transform');
        });
    
        // Menükezelő
    
        $(".js-list-view-dropdown").click(function(){
            $(this).toggleClass('list-view__btn--active');
            $(this).parents(".list-view__row").find(".list-view__sub").stop().slideToggle(300);
        });
    },

    localStorage: function(){

    }
}