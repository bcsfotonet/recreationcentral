"use strict";

module.exports = {

    init: function(){
        this.rowarchive();
        this.tiledarchive();
        this.menuarchive();
    },

    menuarchive: function(){
        
        var input = document.querySelectorAll('.js-menu-checkbox');

        for(var x=0; x < input.length; x++) {

            var selecteditem = input[x].children[0];

            selecteditem.addEventListener("click", function(e){
                    
                e.preventDefault();
            
                this.parentElement.parentElement.parentElement.parentElement.parentElement.classList.toggle('inactive');
                console.log(this.parentElement.children[1].children[0]);
                this.parentElement.children[1].children[0].classList.toggle('fa-eye-slash');
                
            });
        }
    },

    rowarchive: function(){
        var btn = document.querySelectorAll('.js-row-archive');
        

        for(var x=0; x < btn.length; x++) {
            
            btn[x].addEventListener("click", function(){
                this.parentElement.parentElement.parentElement.classList.toggle('inactive');
                this.childNodes[1].classList.toggle('fa-eye-slash');
            });
        }
    },

    tiledarchive: function(){

        var btn = document.querySelectorAll('.js-status-changer');
        
        for(var x=0; x < btn.length; x++) {

            var element = btn[x];
            
            btn[x].addEventListener("click", function(){
                this.parentElement.parentElement.parentElement.childNodes[5].classList.toggle('inactive');
                this.childNodes[1].classList.toggle('fa-eye-slash');
            });
        }
    }
}