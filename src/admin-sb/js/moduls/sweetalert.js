"use strict";

module.exports = {

    init: function(){
        $('.js-remove').click(function(e){
        	/*
            swal({
                title: 'Törlés',
                text: 'Biztosan törölni szeretné?',
                type: 'error',
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonText: 'Igen',
                confirmButtonColor: '#f27474',
                cancelButtonText: 'Mégse'
              });
              */
            var numRowId = $(this).data("row_id");
//            var isRowChild = $(this).data("row_child");
            var objRow = $(this).closest(".list_row_"+numRowId);
            if(objRow.length == 0){
            	var objRow = $(this).closest(".grid_row_"+numRowId);            	
            }
            
//            alert(".list_row_"+numRowId);
//            $(this).closest(".grid-view__row").remove
            if(objRow.length != 0){
	        	swal({
	        		title: 'Törlés',
	                text: 'Biztosan törölni szeretné az elemet?',
	                type: 'error',
	                showCloseButton: true,
	                showCancelButton: true,
	                cancelButtonText: 'Mégse',
	                confirmButtonText: 'Igen',
	                confirmButtonColor: '#f27474'
	    		}).then(value => {
	//    			console.log(value);
	    			if (value.value === true) {
	    				
		//    			var postData = "do=del&module_url=gyik&id=1";
		    			var postData = "do=del&id="+numRowId; //+"&is_child="+isRowChild;
		//    			console.log(postData);
		    			
		    			$.ajax({
		    				 type: "POST"
		    				,dataType: "json"
		    				,url: strAdminBaseUrl+"xhr_controller.php?p=save_module_form"
		    				,data: postData
		    				,success: function(data) {
//		    					console.log(data["type"]);
		    					if(data["type"] == 'ok'){
		    						objRow.remove();
		    					}
		    				    swal({
		    				    	title: (data["type"] == "ok" ? "" : "Hiba"),
		    		                text: data["msg"],
		    		                type: (data["type"] == "ok" ? "success" : data["type"]),
		    				    	confirmButtonColor: '#f27474'
		    				    }).then(value => {
		    				    	console.log(data["refresh"]);
		    				    	console.log($(".return-msg-div").length);
		    				    	if ($(".return-msg-div").length == 0 && data["refresh"] == "true") {
		    				    		console.log(document.location.href);
//		    							document.location.href = document.location.href;
		    				    		window.location.reload();
		    						}
		    				    });
		    				}
		    			});
	    			}
				});
            }
    		
        });
    }
}
