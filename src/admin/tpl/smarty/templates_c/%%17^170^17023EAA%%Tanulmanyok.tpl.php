<?php /* Smarty version 2.6.31, created on 2019-02-10 23:25:28
         compiled from page%5Cmodule%5CTanulmanyok.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Tanulmanyok.tpl', 1, false),array('insert', 'nav', 'page\\module\\Tanulmanyok.tpl', 2, false),array('insert', 'headline', 'page\\module\\Tanulmanyok.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Tanulmanyok.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Tanulmanyok.tpl', 23, false),array('insert', 'selectitem', 'page\\module\\Tanulmanyok.tpl', 26, false),array('insert', 'imageupload', 'page\\module\\Tanulmanyok.tpl', 32, false),array('insert', 'submitcancel', 'page\\module\\Tanulmanyok.tpl', 105, false),array('insert', 'end', 'page\\module\\Tanulmanyok.tpl', 110, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => 'Magazin', 'type' => 'text', 'placeholder' => "Add meg a magazint!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[magazine]", 'value' => ($this->_tpl_vars['tblEditedRowData']['magazine']))), $this); ?>
                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Szerző", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[author_staff_id]", 'options' => $this->_tpl_vars['tblAuthor'])), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Társszerkesztő", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[co_editor_staff_id]", 'options' => $this->_tpl_vars['tblCoEditor'])), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Szerkesztői bizottsági tag", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[staff_id][]", 'options' => $this->_tpl_vars['tblEditorStaffID'], 'multiple' => '1')), $this); ?>
  
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "DOI szám", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a DOI számot!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[doi]", 'value' => ($this->_tpl_vars['tblEditedRowData']['doi']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => 'DOI url', 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a DOI url-t!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[doi_url]", 'value' => ($this->_tpl_vars['tblEditedRowData']['doi_url']))), $this); ?>

                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'imageupload', 'label' => "Tanulmány képe (690x460px)", 'btn_title' => "Kép feltöltése", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[image]", 'is_img' => '1', 'file_url' => ($this->_tpl_vars['tblEditedRowData']['image']))), $this); ?>

                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tanulmány adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "Magyar cím", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a tanulmány címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title']))), $this); ?>

                            	<div id="url_to_hu" class="url_input_div" data-url_field="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[title]">
                                    <div class="form__item">
                                        <label>Magyar url</label><br />
                                        <span class="url_span pl-3"><?php echo $this->_tpl_vars['tblEditedRowData']['url']; ?>
</span>
                                    </div>
                                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_input', 'label' => 'Magyar url', 'type' => 'hidden', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[url]", 'value' => ($this->_tpl_vars['tblEditedRowData']['url']))), $this); ?>

                                </div>
                                <div class="form__item">
                                    <label>Bevezetés</label>
                                    <textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[lead]" ><?php echo $this->_tpl_vars['tblEditedRowData']['lead']; ?>
</textarea>
                            	</div>
                                <div class="form__item">
                                    <label>Abstract</label>
                                    <textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[abstract]" ><?php echo $this->_tpl_vars['tblEditedRowData']['abstract']; ?>
</textarea>
                            	</div>                                
                                <div class="form__item">
                                    <label>Irodalomjegyzék</label>
                                    <textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[bibliography]" ><?php echo $this->_tpl_vars['tblEditedRowData']['bibliography']; ?>
</textarea>
                            	</div>
                            	<div class="editor">
                                    <label>Tanulmány leírása</label>
                                    <div id="hu_editor" data-input_name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[description]"><?php echo $this->_tpl_vars['tblEditedRowData']['description']; ?>
</div>
                                </div>

                        </div>
                            
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Tartalom angol adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "Angol cím", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a tanulmány címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title_en']))), $this); ?>

                            <div id="url_to_en" class="url_input_div" data-url_field="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[title_en]">
                                <div class="form__item">
                                    <label>Angol url</label><br />
                                    <span class="url_span pl-3"><?php echo $this->_tpl_vars['tblEditedRowData']['url_en']; ?>
</span>
                            	</div>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_input', 'label' => 'Angol url', 'type' => 'hidden', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[url_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['url_en']))), $this); ?>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Tartalom román adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "Román cím", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a tanulmány címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title_ro']))), $this); ?>

                            <div id="url_to_ro" class="url_input_div" data-url_field="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[title_ro]">
                                <div class="form__item">
                                    <label>Román url</label><br />
                                    <span class="url_span pl-3"><?php echo $this->_tpl_vars['tblEditedRowData']['url_ro']; ?>
</span>
                            	</div>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_input', 'label' => "Román url", 'type' => 'hidden', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[url_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['url_ro']))), $this); ?>

                            </div>
                        </div>       
                    </div>
                </div>
    	    </div>
    	    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>