<?php /* Smarty version 2.6.31, created on 2019-02-11 02:32:33
         compiled from content/inputitem.tpl */ ?>
<?php if ($this->_tpl_vars['strInputType'] != 'hidden'): ?>
<div <?php if (strpos ( $this->_tpl_vars['strInputClass'] , 'search_input' ) === false): ?> class="form__item" <?php endif; ?>>
    <label><?php echo $this->_tpl_vars['strInputLabel']; ?>
<?php if (isset ( $this->_tpl_vars['numInputFill'] ) && $this->_tpl_vars['numInputFill'] == 1): ?>*<?php endif; ?></label>
<?php endif; ?>
    <input 
    	class="<?php if (isset ( $this->_tpl_vars['numInputFill'] ) && $this->_tpl_vars['numInputFill'] == 1): ?>fill<?php endif; ?> <?php if (isset ( $this->_tpl_vars['strInputClass'] )): ?><?php echo $this->_tpl_vars['strInputClass']; ?>
<?php endif; ?>" 
    	type="<?php echo $this->_tpl_vars['strInputType']; ?>
" 
    	placeholder="<?php echo $this->_tpl_vars['strInputPlaceholder']; ?>
" 
    	name="<?php echo $this->_tpl_vars['strInputName']; ?>
" 
    	value="<?php echo $this->_tpl_vars['strInputValue']; ?>
"
    	<?php if (! empty ( $this->_tpl_vars['numMaxlength'] )): ?>maxlength="<?php echo $this->_tpl_vars['numMaxlength']; ?>
"<?php endif; ?>
	>
    <span class="form__error"></span>
<?php if ($this->_tpl_vars['strInputType'] != 'hidden'): ?>
</div>
<?php endif; ?>