<?php /* Smarty version 2.6.31, created on 2018-11-17 00:08:27
         compiled from page%5Cmodule%5CAdminisztratorok.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Adminisztratorok.tpl', 1, false),array('insert', 'nav', 'page\\module\\Adminisztratorok.tpl', 2, false),array('insert', 'headline', 'page\\module\\Adminisztratorok.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Adminisztratorok.tpl', 17, false),array('insert', 'permissions', 'page\\module\\Adminisztratorok.tpl', 20, false),array('insert', 'inputitem', 'page\\module\\Adminisztratorok.tpl', 25, false),array('insert', 'submitcancel', 'page\\module\\Adminisztratorok.tpl', 39, false),array('insert', 'end', 'page\\module\\Adminisztratorok.tpl', 44, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', "Új adminisztrátor létrehozása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', "Adminisztrátor szerkesztése"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>


            <div class="row">
                <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'permissions', 'permissions' => ($this->_tpl_vars['tblPermission']))), $this); ?>

    
                <div class="col-12 col-lg-8">
                    <div class="form__title">Adminisztrátor adatai</div>
                    <hr>
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => 'Id', 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field_id", 'value' => ($this->_tpl_vars['rowEditedUserData']['id']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Név", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a neved!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field_name", 'value' => ($this->_tpl_vars['rowEditedUserData']['name']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Azonosító", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg az azonosítót!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field_login", 'value' => ($this->_tpl_vars['rowEditedUserData']['login']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "E-mail cím", 'type' => 'email', 'fill' => '1', 'placeholder' => "Add meg az e-mail címet!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field_email", 'value' => ($this->_tpl_vars['rowEditedUserData']['email']))), $this); ?>

                    
                                        <?php if ($this->_tpl_vars['rowEditedUserData']['id'] == $this->_tpl_vars['tblLoggedUserData']['id']): ?>
                        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Jelszó", 'type' => 'password', 'placeholder' => "Add meg a jelszót!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field_password")), $this); ?>

                    <?php endif; ?>
                    <div class="form__item">
                        <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
password" class="button">Jelszó küldése</a>
                    </div>
                </div>
            </div>
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>


        </form>
    </div>
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>