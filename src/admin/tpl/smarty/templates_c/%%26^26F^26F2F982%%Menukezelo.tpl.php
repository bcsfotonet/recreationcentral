<?php /* Smarty version 2.6.31, created on 2019-02-11 02:30:19
         compiled from page%5Cmodule%5CMenukezelo.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Menukezelo.tpl', 1, false),array('insert', 'nav', 'page\\module\\Menukezelo.tpl', 2, false),array('insert', 'headline', 'page\\module\\Menukezelo.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Menukezelo.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Menukezelo.tpl', 23, false),array('insert', 'selectitem', 'page\\module\\Menukezelo.tpl', 26, false),array('insert', 'submitcancel', 'page\\module\\Menukezelo.tpl', 66, false),array('insert', 'end', 'page\\module\\Menukezelo.tpl', 71, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                                        
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Mely menüpont alá tartozik?", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[parent_menu_id]", 'options' => $this->_tpl_vars['tblParentMenu'])), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Mely tartalom jelenjen meg?", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[content_url]", 'options' => $this->_tpl_vars['tblContentUrl'])), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Külső hivatkozás megnyitása", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[target_value]", 'options' => $this->_tpl_vars['tblTargetValue'])), $this); ?>

                </div>
                <div id="tabs" class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#tabs_hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>                    
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#tabs_en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#tabs_ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="tabs_hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Menüpont elnevezése", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a menüpont nevét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name']))), $this); ?>

                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Külső tartalomra hivatkozás", 'type' => 'text', 'fill' => '0', 'placeholder' => "Add meg a URL-t!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[ext_url]", 'value' => ($this->_tpl_vars['tblEditedRowData']['ext_url']))), $this); ?>

                        </div>
                                               
                        <div class="tab-pane fade" id="tabs_en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Angol tartalom adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Menüpont elnevezése", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a menüpont nevét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name_en']))), $this); ?>

                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Külső tartalomra hivatkozás", 'type' => 'text', 'fill' => '0', 'placeholder' => "Add meg a URL-t!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[ext_url_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['ext_url_en']))), $this); ?>

                        </div>
                        <div class="tab-pane fade" id="tabs_ro" role="tabpanel" aria-labelledby="ro-tab">
                               <div class="form__title">Román tartalom adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Menüpont elnevezése", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a menüpont nevét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name_ro']))), $this); ?>

                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Külső tartalomra hivatkozás", 'type' => 'text', 'fill' => '0', 'placeholder' => "Add meg a URL-t!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[ext_url_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['ext_url_ro']))), $this); ?>

                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>