<?php /* Smarty version 2.6.31, created on 2019-02-11 02:32:26
         compiled from content/selectitem.tpl */ ?>
<?php if (! empty ( $this->_tpl_vars['tblOtion'] )): ?>
<!--     <div class="form__item"> -->
    <div <?php if (strpos ( $this->_tpl_vars['strInputClass'] , 'search_input' ) === false): ?> class="form__item" <?php endif; ?>>
        <label><?php echo $this->_tpl_vars['strLabel']; ?>
<?php if (isset ( $this->_tpl_vars['numInputFill'] ) && $this->_tpl_vars['numInputFill'] == 1): ?>*<?php endif; ?></label>
        <div class="form__group">
            <select 
            	name="<?php echo $this->_tpl_vars['strSelectName']; ?>
" 
            	id="" 
            	class="<?php if (isset ( $this->_tpl_vars['numInputFill'] ) && $this->_tpl_vars['numInputFill'] == 1): ?>fill<?php endif; ?> <?php if (isset ( $this->_tpl_vars['strInputClass'] )): ?><?php echo $this->_tpl_vars['strInputClass']; ?>
<?php endif; ?> js-example-basic-single"  
            	<?php if ($this->_tpl_vars['isMultiple'] == 1): ?>multiple="multiple"<?php endif; ?>
            >
            	<?php $_from = $this->_tpl_vars['tblOtion']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowOtion']):
?>
                <option value="<?php echo $this->_tpl_vars['rowOtion']['value']; ?>
" <?php echo $this->_tpl_vars['rowOtion']['selected']; ?>
><?php echo $this->_tpl_vars['rowOtion']['text']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
            </select>
            <div class="form__error"></div>
        </div>
	</div>
<?php endif; ?>