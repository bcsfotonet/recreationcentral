<?php /* Smarty version 2.6.31, created on 2019-02-11 02:29:20
         compiled from page%5Cmodule%5CKreativ_csoport.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Kreativ_csoport.tpl', 1, false),array('insert', 'nav', 'page\\module\\Kreativ_csoport.tpl', 2, false),array('insert', 'headline', 'page\\module\\Kreativ_csoport.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Kreativ_csoport.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Kreativ_csoport.tpl', 23, false),array('insert', 'selectitem', 'page\\module\\Kreativ_csoport.tpl', 36, false),array('insert', 'imageupload', 'page\\module\\Kreativ_csoport.tpl', 37, false),array('insert', 'submitcancel', 'page\\module\\Kreativ_csoport.tpl', 77, false),array('insert', 'end', 'page\\module\\Kreativ_csoport.tpl', 82, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "Név", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a nevet!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name']))), $this); ?>

                	<div id="url_to_hu" class="url_input_div" data-url_field="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[id];<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[name]">
						<div class="form__item">
							<label>Url</label><br />
                    		<span class="url_span pl-3"><?php echo $this->_tpl_vars['tblEditedRowData']['url']; ?>
</span>
                    	</div>
                    	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_input', 'label' => 'Url', 'type' => 'hidden', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[url]", 'value' => ($this->_tpl_vars['tblEditedRowData']['url']))), $this); ?>

                    </div>
                    
					<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "E-mail cím", 'type' => 'text', 'placeholder' => "Add meg az e-mail címet!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[email]", 'value' => ($this->_tpl_vars['tblEditedRowData']['email']))), $this); ?>

					<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Város", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[city_id]", 'options' => $this->_tpl_vars['tblCity'])), $this); ?>
                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'imageupload', 'label' => "Kép (475x475px)", 'btn_title' => "Kép feltöltése", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[image]", 'is_img' => '1', 'file_url' => ($this->_tpl_vars['tblEditedRowData']['image']))), $this); ?>

                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Alapadatok</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => 'Titulus', 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a titulust!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title']))), $this); ?>

                            	
                            	<div class="editor">
                                    <div id="hu_editor" data-input_name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[description]"><?php echo $this->_tpl_vars['tblEditedRowData']['description']; ?>
</div>
                                </div>

                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>