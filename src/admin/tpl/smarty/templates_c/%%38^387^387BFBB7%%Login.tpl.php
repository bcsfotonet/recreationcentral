<?php /* Smarty version 2.6.31, created on 2019-02-11 01:23:58
         compiled from page%5CLogin.tpl */ ?>
<!DOCTYPE html>
<html lang="hu">
    <head>
        <title>Adminisztrációs felület</title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['CONF']['admin_web_url']; ?>
css/main.css">

    </head>
    <body class="login-page">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <h1>recreationcentral.eu</h1>
                    <div class="login-page__header">
                        <p>Kérjük adja meg a felhasználónevét és jelszavát!</p>
                    </div>
                </div>
                <?php if ($this->_tpl_vars['strMsg'] != ""): ?>
                <div class="col-12 col-md-5">
                    <div class="login-page__alert">
                        <span><?php echo $this->_tpl_vars['strMsg']; ?>
</span>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="row justify-content-center">
                <div class="col-12 col-sm-6 col-lg-4">
                    <form action="" method="post" class="form">
                        <div class="form__row">
                            <div class="form__item">
                                <div class="form__group">
                                    <input name="username_input" placeholder="Felhasználónév" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form__row">
                            <div class="form__item">
                                <div class="form__group">
                                    <input name="passwd_input" placeholder="Jelszó" type="password" id="password">
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="button" value="Bejelentkezés">
                    </form>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="login-page__poweredby">
                    <span>Powered by</span>
                    <a href="https://bcsfotonet.hu" target="blank"><img src="<?php echo $this->_tpl_vars['CONF']['admin_web_url']; ?>
images/logo.svg"></a>
                </div>
            </div>
        </div>
    </body>
</html>