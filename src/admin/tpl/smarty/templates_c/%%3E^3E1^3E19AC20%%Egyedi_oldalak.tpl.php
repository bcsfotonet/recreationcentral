<?php /* Smarty version 2.6.31, created on 2019-02-11 01:44:12
         compiled from page%5Cmodule%5CEgyedi_oldalak.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Egyedi_oldalak.tpl', 1, false),array('insert', 'nav', 'page\\module\\Egyedi_oldalak.tpl', 2, false),array('insert', 'headline', 'page\\module\\Egyedi_oldalak.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Egyedi_oldalak.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Egyedi_oldalak.tpl', 21, false),array('insert', 'selectitem', 'page\\module\\Egyedi_oldalak.tpl', 25, false),array('insert', 'submitcancel', 'page\\module\\Egyedi_oldalak.tpl', 79, false),array('insert', 'end', 'page\\module\\Egyedi_oldalak.tpl', 84, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>


            <div class="row justify-content-center">
                <div class="col-12 col-lx-8">
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Oldal típusa", 'fill' => '1', 'type' => 'text', 'placeholder' => "Add meg az oldal típusát", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[type_group]", 'value' => ($this->_tpl_vars['tblEditedRowData']['type_group']))), $this); ?>
                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Mely tartalom jelenjen meg?", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[static_id]", 'options' => $this->_tpl_vars['tblStaticContent'])), $this); ?>

                    
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Oldal magyar adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "Magyar cím", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg az oldal címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name']))), $this); ?>

                            <div id="url_to_hu" class="url_input_div" data-url_field="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[name]">
                                <div class="form__item">
                                    <label>Magyar url</label><br />
                                    <span class="url_span pl-3"><?php echo $this->_tpl_vars['tblEditedRowData']['url']; ?>
</span>
                            	</div>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_input', 'label' => 'Magyar url', 'type' => 'hidden', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[url]", 'value' => ($this->_tpl_vars['tblEditedRowData']['url']))), $this); ?>

                            </div>
                        </div>
                            
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Tartalom angol adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "Angol cím", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg az oldal címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name_en']))), $this); ?>

                            <div id="url_to_en" class="url_input_div" data-url_field="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[name_en]">
                                <div class="form__item">
                                    <label>Angol url</label><br />
                                    <span class="url_span pl-3"><?php echo $this->_tpl_vars['tblEditedRowData']['url_en']; ?>
</span>
                            	</div>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_input', 'label' => 'Angol url', 'type' => 'hidden', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[url_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['url_en']))), $this); ?>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Tartalom román adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "Román cím", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg az oldal címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name_ro']))), $this); ?>

                            <div id="url_to_ro" class="url_input_div" data-url_field="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[name_ro]">
                                <div class="form__item">
                                    <label>Román url</label><br />
                                    <span class="url_span pl-3"><?php echo $this->_tpl_vars['tblEditedRowData']['url_ro']; ?>
</span>
                            	</div>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_input', 'label' => "Román url", 'type' => 'hidden', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[url_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['url_ro']))), $this); ?>

                            </div>
                        </div>
                    </div>
                </div>
    	    </div>    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>

            