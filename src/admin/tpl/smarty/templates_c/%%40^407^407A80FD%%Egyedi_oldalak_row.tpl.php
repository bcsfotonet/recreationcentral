<?php /* Smarty version 2.6.30, created on 2018-10-28 00:52:21
         compiled from content/module/Egyedi_oldalak_row.tpl */ ?>
<?php $_from = $this->_tpl_vars['tblData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['numRowIdx'] => $this->_tpl_vars['rowData']):
?> 
	<div class="list-view__row list_row_<?php echo $this->_tpl_vars['rowData']['id']; ?>
 <?php if ($this->_tpl_vars['rowData']['is_active'] == 0): ?>inactive<?php endif; ?>">
        <div class="row align-items-center">
            <div class="col-12 col-sm-10">
                <div class="list-view__name"><?php echo $this->_tpl_vars['rowData']['name']; ?>
</div>
            </div>
            <div class="col-12 col-sm-2 flex flex--end">
                <div class="form__item js-row-menu-status">
                    <div class="form__group">
                        <label>
                            <input class="module-row-save-field is_active_main_field" type="checkbox" name="row_field_data[<?php echo $this->_tpl_vars['rowData']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][is_active]" value="" <?php if ($this->_tpl_vars['rowData']['is_active'] == 1): ?>checked<?php endif; ?>>
                            <span class="eyeicon "><i class="far fa-eye <?php if ($this->_tpl_vars['rowData']['is_active'] == 0): ?>fa-eye-slash<?php endif; ?>"></i></span>
                        </label>
                    </div>
                </div>
                <?php if ($this->_tpl_vars['rowModuleData']['is_edit_allowed'] == 1): ?>
                    <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
/edit/<?php echo $this->_tpl_vars['rowData']['id']; ?>
" class="edit" data-toggle="tooltip" data-placement="top" title="Szerkesztés">
                        <i class="far fa-edit"></i>
                    </a>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['rowModuleData']['is_delete_allowed'] == 1): ?>
                    <a href="#" class="remove js-remove" data-row_id="<?php echo $this->_tpl_vars['rowData']['id']; ?>
" data-toggle="tooltip" data-placement="top" title="Törlés">
                        <i class="fas fa-trash"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endforeach; endif; unset($_from); ?>