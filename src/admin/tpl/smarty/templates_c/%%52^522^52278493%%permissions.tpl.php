<?php /* Smarty version 2.6.31, created on 2018-11-17 00:08:27
         compiled from content/permissions.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'implode', 'content/permissions.tpl', 12, false),)), $this); ?>
<div class="col-12 col-lg-4">
    <div class="permissions">
        <div class="permissions__title">Jogosultságok</div>
        <hr>
        <div class="row">
            <?php $_from = $this->_tpl_vars['tblPermission']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowPermission']):
?>
                <div class="col-12 col-sm-6 col-lg-12 form__item">
                    <div class="form__group">
                        <label>
                            <i class="fas <?php echo $this->_tpl_vars['rowPermission']['icon_class']; ?>
"></i>
                            <?php echo $this->_tpl_vars['rowPermission']['name']; ?>

                            <input name="adminisztratorok_field_permission[<?php echo ((is_array($_tmp=',')) ? $this->_run_mod_handler('implode', true, $_tmp, $this->_tpl_vars['rowPermission']['modules']) : implode($_tmp, $this->_tpl_vars['rowPermission']['modules'])); ?>
]" type="checkbox" <?php if ($this->_tpl_vars['rowEditedUserData']['is_super_user'] == true): ?>disabled<?php endif; ?> <?php if ($this->_tpl_vars['rowPermission']['access_user'] == true): ?>checked<?php endif; ?>>
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            <?php endforeach; endif; unset($_from); ?>
        </div>
    </div>
</div>