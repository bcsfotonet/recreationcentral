<?php /* Smarty version 2.6.31, created on 2019-02-11 02:29:45
         compiled from page%5Cmodule%5CPartnerek.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Partnerek.tpl', 1, false),array('insert', 'nav', 'page\\module\\Partnerek.tpl', 2, false),array('insert', 'headline', 'page\\module\\Partnerek.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Partnerek.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Partnerek.tpl', 23, false),array('insert', 'selectitem', 'page\\module\\Partnerek.tpl', 27, false),array('insert', 'imageupload', 'page\\module\\Partnerek.tpl', 28, false),array('insert', 'submitcancel', 'page\\module\\Partnerek.tpl', 68, false),array('insert', 'end', 'page\\module\\Partnerek.tpl', 73, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Név", 'type' => 'text', 'maxlength' => ($this->_tpl_vars['tblValidate']['name']['maxlength']), 'fill' => ($this->_tpl_vars['tblValidate']['name']['is_fill']), 'placeholder' => "Add meg a partner nevét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name']))), $this); ?>

					<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Külső hivatkozás megnyitása", 'fill' => ($this->_tpl_vars['tblValidate']['target_value']['is_fill']), 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[target_value]", 'options' => $this->_tpl_vars['tblTargetValue'])), $this); ?>
                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'imageupload', 'label' => "Partner képe (180x120px)", 'btn_title' => "Kép feltöltése", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[image]", 'is_img' => '1', 'file_url' => ($this->_tpl_vars['tblEditedRowData']['image']))), $this); ?>

                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Partner adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Külső tartalomra hivatkozás", 'maxlength' => ($this->_tpl_vars['tblValidate']['ext_url']['maxlength']), 'fill' => ($this->_tpl_vars['tblValidate']['ext_url']['is_fill']), 'type' => 'text', 'placeholder' => "Add meg a URL-t!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[ext_url]", 'value' => ($this->_tpl_vars['tblEditedRowData']['ext_url']))), $this); ?>

                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>