<?php /* Smarty version 2.6.31, created on 2019-02-11 02:30:16
         compiled from content/module/menukezelo_row.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'content/module/menukezelo_row.tpl', 10, false),)), $this); ?>

<?php $_from = $this->_tpl_vars['tblData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['numRowIdx'] => $this->_tpl_vars['tblMainData']):
?> 
	<div class="list-view__row list_row_<?php echo $this->_tpl_vars['tblMainData']['main']['id']; ?>
 <?php if ($this->_tpl_vars['tblMainData']['main']['is_active'] == 0): ?>inactive<?php endif; ?>">
        <div class="row align-items-center">
            <div class="col-12 col-md-2">
                <div class="list-view__order-edit">
                    <div class="<?php if (! empty ( $this->_tpl_vars['tblMainData']['child'] )): ?>list-view__btn<?php endif; ?> js-list-view-dropdown" <?php if (empty ( $this->_tpl_vars['tblMainData']['child'] )): ?>style="width: 40px;"<?php endif; ?>></div>
                    <div class="form__item">
                        <div class="form__group">
                        	<?php $this->assign('numMaxPriority', count($this->_tpl_vars['tblData'])); ?>
                            <select name="row_field_data[<?php echo $this->_tpl_vars['tblMainData']['main']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][priority]" id="" class="js-example-basic-single module-row-save-field">    
    							<?php unset($this->_sections['numOrder']);
$this->_sections['numOrder']['name'] = 'numOrder';
$this->_sections['numOrder']['start'] = (int)1;
$this->_sections['numOrder']['loop'] = is_array($_loop=$this->_tpl_vars['numMaxPriority']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['numOrder']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['numOrder']['show'] = true;
$this->_sections['numOrder']['max'] = $this->_sections['numOrder']['loop'];
if ($this->_sections['numOrder']['start'] < 0)
    $this->_sections['numOrder']['start'] = max($this->_sections['numOrder']['step'] > 0 ? 0 : -1, $this->_sections['numOrder']['loop'] + $this->_sections['numOrder']['start']);
else
    $this->_sections['numOrder']['start'] = min($this->_sections['numOrder']['start'], $this->_sections['numOrder']['step'] > 0 ? $this->_sections['numOrder']['loop'] : $this->_sections['numOrder']['loop']-1);
if ($this->_sections['numOrder']['show']) {
    $this->_sections['numOrder']['total'] = min(ceil(($this->_sections['numOrder']['step'] > 0 ? $this->_sections['numOrder']['loop'] - $this->_sections['numOrder']['start'] : $this->_sections['numOrder']['start']+1)/abs($this->_sections['numOrder']['step'])), $this->_sections['numOrder']['max']);
    if ($this->_sections['numOrder']['total'] == 0)
        $this->_sections['numOrder']['show'] = false;
} else
    $this->_sections['numOrder']['total'] = 0;
if ($this->_sections['numOrder']['show']):

            for ($this->_sections['numOrder']['index'] = $this->_sections['numOrder']['start'], $this->_sections['numOrder']['iteration'] = 1;
                 $this->_sections['numOrder']['iteration'] <= $this->_sections['numOrder']['total'];
                 $this->_sections['numOrder']['index'] += $this->_sections['numOrder']['step'], $this->_sections['numOrder']['iteration']++):
$this->_sections['numOrder']['rownum'] = $this->_sections['numOrder']['iteration'];
$this->_sections['numOrder']['index_prev'] = $this->_sections['numOrder']['index'] - $this->_sections['numOrder']['step'];
$this->_sections['numOrder']['index_next'] = $this->_sections['numOrder']['index'] + $this->_sections['numOrder']['step'];
$this->_sections['numOrder']['first']      = ($this->_sections['numOrder']['iteration'] == 1);
$this->_sections['numOrder']['last']       = ($this->_sections['numOrder']['iteration'] == $this->_sections['numOrder']['total']);
?>
    								<option 
    									value="<?php echo $this->_sections['numOrder']['index']; ?>
" 
    									<?php if ($this->_sections['numOrder']['index'] == $this->_tpl_vars['tblMainData']['main']['priority']): ?>selected="selected"<?php endif; ?>
									>
										<?php echo $this->_sections['numOrder']['index']; ?>

									</option>
    							<?php endfor; endif; ?>
    						</select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-10 col-xl-4">
                <div class="list-view__name"><?php echo $this->_tpl_vars['tblMainData']['main']['name']; ?>
</div>
            </div>
            <div class="col-12 col-sm-10 col-xl-4 flex flex--end">
                <div class="list-view__settings">
                    <div class="form__item">
                        <div class="form__group">
                            <label>
                                Főmenü
                                <input class="module-row-save-field" type="checkbox" name="row_field_data[<?php echo $this->_tpl_vars['tblMainData']['main']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][is_visible_main_menu]" value="" <?php if ($this->_tpl_vars['tblMainData']['main']['is_visible_main_menu'] == 1): ?>checked<?php endif; ?> >
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form__item">
                        <div class="form__group">
                            <label>
                                Footer menü
                                <input class="module-row-save-field" type="checkbox" name="row_field_data[<?php echo $this->_tpl_vars['tblMainData']['main']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][is_visible_footer_menu]" value="" <?php if ($this->_tpl_vars['tblMainData']['main']['is_visible_footer_menu'] == 1): ?>checked<?php endif; ?> >
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-2 col-xl-2 flex flex--end">
                <div class="form__item js-row-menu-status">
                    <div class="form__group">
                        <label>
                            <input class="module-row-save-field is_active_main_field" type="checkbox" name="row_field_data[<?php echo $this->_tpl_vars['tblMainData']['main']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][is_active]" value="" <?php if ($this->_tpl_vars['tblMainData']['main']['is_active'] == 1): ?>checked<?php endif; ?>>
                            <span class="eyeicon "><i class="far fa-eye <?php if ($this->_tpl_vars['tblMainData']['main']['is_active'] == 0): ?>fa-eye-slash<?php endif; ?>"></i></span>
                        </label>
                    </div>
                </div>
                <?php if ($this->_tpl_vars['rowModuleData']['is_edit_allowed'] == 1): ?>
                	                    <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
/edit/<?php echo $this->_tpl_vars['tblMainData']['main']['id']; ?>
" class="edit" data-toggle="tooltip" data-placement="top" title="Szerkesztés">
                        <i class="far fa-edit"></i>
                    </a>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['rowModuleData']['is_delete_allowed'] == 1): ?>
                    <a href="#" class="remove js-remove" data-row_id="<?php echo $this->_tpl_vars['tblMainData']['main']['id']; ?>
" data-toggle="tooltip" data-placement="top" title="Törlés">
                        <i class="fas fa-trash"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>
        
                <?php $_from = $this->_tpl_vars['tblMainData']['child']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['numRowChildIdx'] => $this->_tpl_vars['tblChildData']):
?> 
            <div class="list-view__sub list_row_<?php echo $this->_tpl_vars['tblChildData']['id']; ?>
 <?php if ($this->_tpl_vars['tblChildData']['is_active'] == 0): ?>inactive<?php endif; ?>">
                <hr>
<!--                 <div class="container-fluid"> -->
                    <div class="row align-items-center pl-5">
                        <div class="col-12 col-md-2">
                            <div class="list-view__order-edit">
                                <div class="list-view__btn" style="opacity:0; cursor:unset;"></div>
                                <div class="form__item">
                                    <div class="form__group">
                                    	<?php $this->assign('numMaxChildPriority', count($this->_tpl_vars['tblMainData']['child'])); ?>
                                    	
                                        <select name="row_field_data[<?php echo $this->_tpl_vars['tblChildData']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][priority]" id="" class="js-example-basic-single module-row-save-field">
                                            <?php unset($this->_sections['numOrder']);
$this->_sections['numOrder']['name'] = 'numOrder';
$this->_sections['numOrder']['start'] = (int)1;
$this->_sections['numOrder']['loop'] = is_array($_loop=$this->_tpl_vars['numMaxChildPriority']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['numOrder']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['numOrder']['show'] = true;
$this->_sections['numOrder']['max'] = $this->_sections['numOrder']['loop'];
if ($this->_sections['numOrder']['start'] < 0)
    $this->_sections['numOrder']['start'] = max($this->_sections['numOrder']['step'] > 0 ? 0 : -1, $this->_sections['numOrder']['loop'] + $this->_sections['numOrder']['start']);
else
    $this->_sections['numOrder']['start'] = min($this->_sections['numOrder']['start'], $this->_sections['numOrder']['step'] > 0 ? $this->_sections['numOrder']['loop'] : $this->_sections['numOrder']['loop']-1);
if ($this->_sections['numOrder']['show']) {
    $this->_sections['numOrder']['total'] = min(ceil(($this->_sections['numOrder']['step'] > 0 ? $this->_sections['numOrder']['loop'] - $this->_sections['numOrder']['start'] : $this->_sections['numOrder']['start']+1)/abs($this->_sections['numOrder']['step'])), $this->_sections['numOrder']['max']);
    if ($this->_sections['numOrder']['total'] == 0)
        $this->_sections['numOrder']['show'] = false;
} else
    $this->_sections['numOrder']['total'] = 0;
if ($this->_sections['numOrder']['show']):

            for ($this->_sections['numOrder']['index'] = $this->_sections['numOrder']['start'], $this->_sections['numOrder']['iteration'] = 1;
                 $this->_sections['numOrder']['iteration'] <= $this->_sections['numOrder']['total'];
                 $this->_sections['numOrder']['index'] += $this->_sections['numOrder']['step'], $this->_sections['numOrder']['iteration']++):
$this->_sections['numOrder']['rownum'] = $this->_sections['numOrder']['iteration'];
$this->_sections['numOrder']['index_prev'] = $this->_sections['numOrder']['index'] - $this->_sections['numOrder']['step'];
$this->_sections['numOrder']['index_next'] = $this->_sections['numOrder']['index'] + $this->_sections['numOrder']['step'];
$this->_sections['numOrder']['first']      = ($this->_sections['numOrder']['iteration'] == 1);
$this->_sections['numOrder']['last']       = ($this->_sections['numOrder']['iteration'] == $this->_sections['numOrder']['total']);
?> 
                								<option 
                									value="<?php echo $this->_sections['numOrder']['index']; ?>
" 
                									<?php if ($this->_sections['numOrder']['index'] == $this->_tpl_vars['tblChildData']['priority']): ?>selected="selected"<?php endif; ?>
            									>
            										<?php echo $this->_sections['numOrder']['index']; ?>

        										</option>
                							<?php endfor; endif; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-10 col-xl-4">
                            <div class="list-view__name"><?php echo $this->_tpl_vars['tblChildData']['name']; ?>
</div>
                        </div>
                        <div class="col-12 col-sm-10 col-xl-4 flex flex--end">
                            <div class="list-view__settings">
                                <div class="form__item">
                                    <div class="form__group">
                                        <label>
                                            Főmenü
                                            <input class="module-row-save-field" type="checkbox" name="row_field_data[<?php echo $this->_tpl_vars['tblChildData']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][is_visible_main_menu]" value="" <?php if ($this->_tpl_vars['tblChildData']['is_visible_main_menu'] == 1): ?>checked<?php endif; ?> >
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form__item">
                                    <div class="form__group">
                                        <label>
                                            Footer menü
                                            <input class="module-row-save-field" type="checkbox" name="row_field_data[<?php echo $this->_tpl_vars['tblChildData']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][is_visible_footer_menu]" value="" <?php if ($this->_tpl_vars['tblChildData']['is_visible_footer_menu'] == 1): ?>checked<?php endif; ?> >
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-2 col-xl-2 flex flex--end">
                            <div class="form__item js-row-menu-status">
                                <div class="form__group">
                                    <label>
                                        <input class="module-row-save-field is_active_sub_field" type="checkbox" name="row_field_data[<?php echo $this->_tpl_vars['tblChildData']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][is_active]" value="" <?php if ($this->_tpl_vars['tblChildData']['is_active'] == 1): ?>checked<?php endif; ?>>
                                        <span class="eyeicon "><i class="far fa-eye <?php if ($this->_tpl_vars['tblChildData']['is_active'] == 0): ?>fa-eye-slash<?php endif; ?>"></i></span>
                                    </label>
                                </div>
                            </div>
                            <?php if ($this->_tpl_vars['rowModuleData']['is_edit_allowed'] == 1): ?>
                                <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
/edit/<?php echo $this->_tpl_vars['tblChildData']['id']; ?>
" class="edit" data-toggle="tooltip" data-placement="top" title="Szerkesztés">
                                    <i class="far fa-edit"></i>
                                </a>
                            <?php endif; ?>
                            <?php if ($this->_tpl_vars['rowModuleData']['is_delete_allowed'] == 1): ?>
                                <a href="#" class="remove js-remove" data-row_id="<?php echo $this->_tpl_vars['tblChildData']['id']; ?>
" data-toggle="tooltip" data-placement="top" title="Törlés">
                                    <i class="fas fa-trash"></i>
                                </a>
                            <?php endif; ?>
                            
                        </div>
                        
                    </div>
<!--                 </div> -->
            </div>
        <?php endforeach; endif; unset($_from); ?>
	</div>
<?php endforeach; endif; unset($_from); ?>