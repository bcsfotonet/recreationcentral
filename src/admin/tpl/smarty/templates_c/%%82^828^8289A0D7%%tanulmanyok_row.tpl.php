<?php /* Smarty version 2.6.31, created on 2019-02-10 23:25:25
         compiled from content/module/tanulmanyok_row.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'content/module/tanulmanyok_row.tpl', 40, false),)), $this); ?>

<?php $_from = $this->_tpl_vars['tblData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['numRowIdx'] => $this->_tpl_vars['rowData']):
?>
    <div class="col-12 col-sm-6 col-xl-3 grid-view__row grid_row_<?php echo $this->_tpl_vars['rowData']['id']; ?>
 <?php if ($this->_tpl_vars['rowData']['is_active'] == 0): ?>inactive<?php endif; ?>">
        <div class="tiled-view">
            <div class="tiled-view__header">
            	<?php if ($this->_tpl_vars['rowModuleData']['is_edit_allowed'] == 1): ?>
                    <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
/edit/<?php echo $this->_tpl_vars['rowData']['id']; ?>
" class="button button--edit">
                        <i class="far fa-edit"></i>Szerkesztés
                    </a>
                <?php endif; ?>
                <div class="tiled-view__icon-wrapper">
    				<div class="form__item js-row-menu-status">
                        <div class="form__group">
                            <label>
                                <input class="module-row-save-field is_active_main_field" type="checkbox" name="row_field_data[<?php echo $this->_tpl_vars['rowData']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][is_active]" value="" <?php if ($this->_tpl_vars['rowData']['is_active'] == 1): ?>checked<?php endif; ?>>
                                <span class="eyeicon "><i class="far fa-eye <?php if ($this->_tpl_vars['rowData']['is_active'] == 0): ?>fa-eye-slash<?php endif; ?>"></i></span>
                            </label>
                        </div>
                    </div>
                    <?php if ($this->_tpl_vars['rowModuleData']['is_delete_allowed'] == 1): ?>
                        <a href="#" class="remove js-remove" data-row_id="<?php echo $this->_tpl_vars['rowData']['id']; ?>
" data-toggle="tooltip" data-placement="top" title="Törlés">
                            <i class="fas fa-trash"></i>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
            <hr>
            <div class="tiled-view__content">
            	<?php if (! empty ( $this->_tpl_vars['rowData']['image'] )): ?>
            	<div class="tiled-view__img">
                    <img src="<?php echo $this->_tpl_vars['rowData']['image']; ?>
" alt="<?php echo $this->_tpl_vars['rowData']['title']; ?>
">
                </div>
                <?php endif; ?>
                <div class="tiled-view__data">
                    <b>Cím:</b><?php echo $this->_tpl_vars['rowData']['title']; ?>

                </div>
            </div>
            <div class="form__item">
                <div class="form__group">
                    <?php $this->assign('numMaxPriority', count($this->_tpl_vars['tblData'])); ?>
                    <select name="row_field_data[<?php echo $this->_tpl_vars['rowData']['id']; ?>
][<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field][priority]" id="" class="js-example-basic-single module-row-save-field">    
						<?php unset($this->_sections['numOrder']);
$this->_sections['numOrder']['name'] = 'numOrder';
$this->_sections['numOrder']['start'] = (int)1;
$this->_sections['numOrder']['loop'] = is_array($_loop=$this->_tpl_vars['numMaxPriority']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['numOrder']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['numOrder']['show'] = true;
$this->_sections['numOrder']['max'] = $this->_sections['numOrder']['loop'];
if ($this->_sections['numOrder']['start'] < 0)
    $this->_sections['numOrder']['start'] = max($this->_sections['numOrder']['step'] > 0 ? 0 : -1, $this->_sections['numOrder']['loop'] + $this->_sections['numOrder']['start']);
else
    $this->_sections['numOrder']['start'] = min($this->_sections['numOrder']['start'], $this->_sections['numOrder']['step'] > 0 ? $this->_sections['numOrder']['loop'] : $this->_sections['numOrder']['loop']-1);
if ($this->_sections['numOrder']['show']) {
    $this->_sections['numOrder']['total'] = min(ceil(($this->_sections['numOrder']['step'] > 0 ? $this->_sections['numOrder']['loop'] - $this->_sections['numOrder']['start'] : $this->_sections['numOrder']['start']+1)/abs($this->_sections['numOrder']['step'])), $this->_sections['numOrder']['max']);
    if ($this->_sections['numOrder']['total'] == 0)
        $this->_sections['numOrder']['show'] = false;
} else
    $this->_sections['numOrder']['total'] = 0;
if ($this->_sections['numOrder']['show']):

            for ($this->_sections['numOrder']['index'] = $this->_sections['numOrder']['start'], $this->_sections['numOrder']['iteration'] = 1;
                 $this->_sections['numOrder']['iteration'] <= $this->_sections['numOrder']['total'];
                 $this->_sections['numOrder']['index'] += $this->_sections['numOrder']['step'], $this->_sections['numOrder']['iteration']++):
$this->_sections['numOrder']['rownum'] = $this->_sections['numOrder']['iteration'];
$this->_sections['numOrder']['index_prev'] = $this->_sections['numOrder']['index'] - $this->_sections['numOrder']['step'];
$this->_sections['numOrder']['index_next'] = $this->_sections['numOrder']['index'] + $this->_sections['numOrder']['step'];
$this->_sections['numOrder']['first']      = ($this->_sections['numOrder']['iteration'] == 1);
$this->_sections['numOrder']['last']       = ($this->_sections['numOrder']['iteration'] == $this->_sections['numOrder']['total']);
?>
							<option 
								value="<?php echo $this->_sections['numOrder']['index']; ?>
" 
								<?php if ($this->_sections['numOrder']['index'] == $this->_tpl_vars['rowData']['priority']): ?>selected="selected"<?php endif; ?>
							>
								<?php echo $this->_sections['numOrder']['index']; ?>

							</option>
						<?php endfor; endif; ?>
					</select>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; endif; unset($_from); ?>                