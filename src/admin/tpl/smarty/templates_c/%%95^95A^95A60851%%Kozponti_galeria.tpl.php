<?php /* Smarty version 2.6.31, created on 2019-02-11 01:24:19
         compiled from page%5Cmodule%5CKozponti_galeria.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Kozponti_galeria.tpl', 1, false),array('insert', 'nav', 'page\\module\\Kozponti_galeria.tpl', 2, false),array('insert', 'headline', 'page\\module\\Kozponti_galeria.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Kozponti_galeria.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Kozponti_galeria.tpl', 23, false),array('insert', 'selectitem', 'page\\module\\Kozponti_galeria.tpl', 26, false),array('insert', 'imageupload', 'page\\module\\Kozponti_galeria.tpl', 75, false),array('insert', 'submitcancel', 'page\\module\\Kozponti_galeria.tpl', 140, false),array('insert', 'end', 'page\\module\\Kozponti_galeria.tpl', 145, false),array('modifier', 'count', 'page\\module\\Kozponti_galeria.tpl', 120, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>


			<div class="row mtb-30">
				<div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Melyik galériatárban jelenjen meg?", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[type_id][]", 'options' => $this->_tpl_vars['tblType'], 'multiple' => '1')), $this); ?>

					<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Város", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[city_id]", 'options' => $this->_tpl_vars['tblCity'])), $this); ?>

                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu"
                                aria-selected="true">Magyar</a>
                        </li>
                        
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                        	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_parent_input', 'label' => "Magyar cím", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a galéria címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title']))), $this); ?>

                        	<div id="url_to_hu" class="url_input_div" data-url_field="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[title]">
    							<div class="form__item">
    								<label>Magyar url</label><br />
                            		<span class="url_span pl-3"><?php echo $this->_tpl_vars['tblEditedRowData']['url']; ?>
</span>
                            	</div>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'url_input', 'label' => 'Magyar url', 'type' => 'hidden', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[url]", 'value' => ($this->_tpl_vars['tblEditedRowData']['url']))), $this); ?>

                            </div>
                        	
                        	
                        	<div class="form__item">
								<label>Rövid leírás</label>
                        		<textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[description]" ><?php echo $this->_tpl_vars['tblEditedRowData']['description']; ?>
</textarea>
							</div>
                        </div>
  
                    </div>
                </div>
            </div>

            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'imageupload', 'is_img' => '1', 'label' => "", 'btn_title' => "Képek feltöltése", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[gallery_image][]", 'file_url' => "", 'is_multiple' => '1')), $this); ?>


            <div class="row" id="sortable">
            	<?php if (! empty ( $this->_tpl_vars['tblEditedRowData']['images'] )): ?>
                 	<?php $_from = $this->_tpl_vars['tblEditedRowData']['images']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowImage']):
?>
                 		<?php if (! empty ( $this->_tpl_vars['rowImage']['image'] )): ?>
                            <div class="col-12 col-sm-6 col-xl-3 grid-view__row grid_row_<?php echo $this->_tpl_vars['rowImage']['id']; ?>
">
                                <div class="tiled-view <?php if ($this->_tpl_vars['rowImage']['is_active'] == 0): ?>inactive<?php endif; ?>">
            	                                                        <div class="tiled-view__header">
                                    	                                        <div class="tiled-view__icon-wrapper">
                                            <div class="form__item js-row-menu-status">
                                                <div class="form__group">
                                                    <label>
                                                        <input class="eye_checkbox" type="checkbox" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[images][<?php echo $this->_tpl_vars['rowImage']['id']; ?>
][is_active]" value="" <?php if ($this->_tpl_vars['rowImage']['is_active'] == 1): ?>checked<?php endif; ?>>
                                                        <span class="eyeicon "><i class="far fa-eye <?php if ($this->_tpl_vars['rowImage']['is_active'] == 0): ?>fa-eye-slash<?php endif; ?>"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <?php if ($this->_tpl_vars['rowModuleData']['is_delete_allowed'] == 1): ?>
                                                <div class="form__group">
                                                    <label class="pl-0 mb-0 mt-1">
                                                        <input class="del_checkbox" type="checkbox" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[images][<?php echo $this->_tpl_vars['rowImage']['id']; ?>
][delete_date]" value="" >
                                                        <span class="delicon remove"><i class="fas fa-trash"></i></span>
                                                    </label>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="tiled-view__content">
                                        <div class="tiled-view__img">
                                            <img src="<?php echo $this->_tpl_vars['rowImage']['image_url']; ?>
" alt="<?php echo $this->_tpl_vars['rowImage']['alt']; ?>
">
                                        </div>
                                    </div>
                                    <div class="form__item">
                                        <label>&nbsp;</label>
                                        <div class="form__group">
                                            <?php $this->assign('numMaxPriority', count($this->_tpl_vars['tblEditedRowData']['images'])); ?>
                                            <select name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[images][<?php echo $this->_tpl_vars['rowImage']['id']; ?>
][priority]" id="" class="js-example-basic-single">    
                    							<?php unset($this->_sections['numOrder']);
$this->_sections['numOrder']['name'] = 'numOrder';
$this->_sections['numOrder']['start'] = (int)1;
$this->_sections['numOrder']['loop'] = is_array($_loop=$this->_tpl_vars['numMaxPriority']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['numOrder']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['numOrder']['show'] = true;
$this->_sections['numOrder']['max'] = $this->_sections['numOrder']['loop'];
if ($this->_sections['numOrder']['start'] < 0)
    $this->_sections['numOrder']['start'] = max($this->_sections['numOrder']['step'] > 0 ? 0 : -1, $this->_sections['numOrder']['loop'] + $this->_sections['numOrder']['start']);
else
    $this->_sections['numOrder']['start'] = min($this->_sections['numOrder']['start'], $this->_sections['numOrder']['step'] > 0 ? $this->_sections['numOrder']['loop'] : $this->_sections['numOrder']['loop']-1);
if ($this->_sections['numOrder']['show']) {
    $this->_sections['numOrder']['total'] = min(ceil(($this->_sections['numOrder']['step'] > 0 ? $this->_sections['numOrder']['loop'] - $this->_sections['numOrder']['start'] : $this->_sections['numOrder']['start']+1)/abs($this->_sections['numOrder']['step'])), $this->_sections['numOrder']['max']);
    if ($this->_sections['numOrder']['total'] == 0)
        $this->_sections['numOrder']['show'] = false;
} else
    $this->_sections['numOrder']['total'] = 0;
if ($this->_sections['numOrder']['show']):

            for ($this->_sections['numOrder']['index'] = $this->_sections['numOrder']['start'], $this->_sections['numOrder']['iteration'] = 1;
                 $this->_sections['numOrder']['iteration'] <= $this->_sections['numOrder']['total'];
                 $this->_sections['numOrder']['index'] += $this->_sections['numOrder']['step'], $this->_sections['numOrder']['iteration']++):
$this->_sections['numOrder']['rownum'] = $this->_sections['numOrder']['iteration'];
$this->_sections['numOrder']['index_prev'] = $this->_sections['numOrder']['index'] - $this->_sections['numOrder']['step'];
$this->_sections['numOrder']['index_next'] = $this->_sections['numOrder']['index'] + $this->_sections['numOrder']['step'];
$this->_sections['numOrder']['first']      = ($this->_sections['numOrder']['iteration'] == 1);
$this->_sections['numOrder']['last']       = ($this->_sections['numOrder']['iteration'] == $this->_sections['numOrder']['total']);
?>
                    								<option 
                    									value="<?php echo $this->_sections['numOrder']['index']; ?>
" 
                    									<?php if ($this->_sections['numOrder']['index'] == $this->_tpl_vars['rowImage']['priority']): ?>selected="selected"<?php endif; ?>
                									>
                										<?php echo $this->_sections['numOrder']['index']; ?>

                									</option>
                    							<?php endfor; endif; ?>
                    						</select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
    				<?php endforeach; endif; unset($_from); ?>
    			<?php endif; ?>
            </div>
            
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>

            