<?php /* Smarty version 2.6.31, created on 2019-02-11 02:15:28
         compiled from page%5Cmodule%5CGyik.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Gyik.tpl', 1, false),array('insert', 'nav', 'page\\module\\Gyik.tpl', 2, false),array('insert', 'headline', 'page\\module\\Gyik.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Gyik.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Gyik.tpl', 64, false),array('insert', 'submitcancel', 'page\\module\\Gyik.tpl', 96, false),array('insert', 'end', 'page\\module\\Gyik.tpl', 101, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

 			<div class="row justify-content-center">
                <div class="col-12 col-lx-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom magyar adatai</div>
                            <hr>
							<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

							<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                            
                          	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Kérdés", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a kérdést!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[question]", 'value' => ($this->_tpl_vars['tblEditedRowData']['question']))), $this); ?>

                            <div class="editor">
                                <div id="hu_editor" data-input_name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[answer]"><?php echo $this->_tpl_vars['tblEditedRowData']['answer']; ?>
</div>
                            </div>
<!--                             <?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[answer] -->
<!--                             <div class="editor"> -->
<!--                                 <textarea class="w-100" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[answer]" id="editor"><?php echo $this->_tpl_vars['tblEditedRowData']['answer']; ?>
</textarea> -->
<!--                             </div> -->
                        </div>

                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Tartalom angol adatai</div>
                            <hr>
                              	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Kérdés", 'type' => 'text', 'fill' => '0', 'placeholder' => "Add meg a kérdést!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[question_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['question_en']))), $this); ?>

    							<div class="editor">
                                    <div id="en_editor" data-input_name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[answer_en]"><?php echo $this->_tpl_vars['tblEditedRowData']['answer_en']; ?>
</div>
                                </div>
                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Tartalom román adatai</div>
                            <hr>
                              	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Kérdés", 'type' => 'text', 'fill' => '0', 'placeholder' => "Add meg a kérdést!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[question_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['question_ro']))), $this); ?>

    							<div class="editor">
                                    <div id="ro_editor" data-input_name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[answer_ro]"><?php echo $this->_tpl_vars['tblEditedRowData']['answer_ro']; ?>
</div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>   	    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>

            