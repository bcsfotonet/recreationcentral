<?php /* Smarty version 2.6.31, created on 2019-02-11 02:30:15
         compiled from page%5CModule_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Module_list.tpl', 1, false),array('insert', 'nav', 'page\\Module_list.tpl', 2, false),array('insert', 'headline', 'page\\Module_list.tpl', 5, false),array('insert', 'newbtn', 'page\\Module_list.tpl', 10, false),array('insert', 'selectitem', 'page\\Module_list.tpl', 22, false),array('insert', 'inputitem', 'page\\Module_list.tpl', 26, false),array('insert', 'module_list_row', 'page\\Module_list.tpl', 37, false),array('insert', 'end', 'page\\Module_list.tpl', 45, false),array('function', 'math', 'page\\Module_list.tpl', 20, false),array('modifier', 'count', 'page\\Module_list.tpl', 20, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Adminisztrációs felület")), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline')), $this); ?>


    <div class="container-fluid">
        <?php if ($this->_tpl_vars['rowModuleData']['is_new_allowed'] == 1): ?>
            <div class="row">
                <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'newbtn', 'title' => ($this->_tpl_vars['rowModuleData']['module_name'])." - Új elem hozzáadása", 'class' => "col-12 col-md-6 col-lg-6", 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url'])."/edit")), $this); ?>

            </div>
        <?php endif; ?>
        <?php if (! empty ( $this->_tpl_vars['tblList']['search'] )): ?>
        	<div class="row justify-content-center">
            	<?php $_from = $this->_tpl_vars['tblList']['search']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['strColumn'] => $this->_tpl_vars['rowSearchField']):
?>
                	<div class='col-12 col-md-<?php echo smarty_function_math(array('equation' => "min(6, floor( x / y ))",'x' => 12,'y' => count($this->_tpl_vars['tblList']['search'])), $this);?>
'>
                		<?php if ($this->_tpl_vars['rowSearchField']['type'] == 'select' && $this->_tpl_vars['rowSearchField']['multiple'] == 1): ?>
                			<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'class' => 'search_input', 'label' => $this->_tpl_vars['rowSearchField']['text'], 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_searchfield[".($this->_tpl_vars['strColumn'])."][]", 'options' => $this->_tpl_vars['rowSearchField']['options'], 'multiple' => $this->_tpl_vars['rowSearchField']['multiple'])), $this); ?>

            			<?php elseif ($this->_tpl_vars['rowSearchField']['type'] == 'select' && $this->_tpl_vars['rowSearchField']['multiple'] != 1): ?>
                			<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'class' => 'search_input', 'label' => $this->_tpl_vars['rowSearchField']['text'], 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_searchfield[".($this->_tpl_vars['strColumn'])."]", 'options' => $this->_tpl_vars['rowSearchField']['options'], 'multiple' => 0)), $this); ?>

                		<?php else: ?>
                			<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'class' => 'search_input filterInput', 'label' => $this->_tpl_vars['rowSearchField']['text'], 'placeholder' => "", 'type' => 'text', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_searchfield[".($this->_tpl_vars['strColumn'])."]")), $this); ?>

                		<?php endif; ?>
                    </div>
            	<?php endforeach; endif; unset($_from); ?>
        	</div>
        <?php endif; ?>
        <div class="row">
            <div class="col-12">
                <div class="list-view">
				<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

				
           		<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'module_list_row', 'module_name' => ($this->_tpl_vars['rowModuleData']['file_name']))), $this); ?>

                
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>