<?php /* Smarty version 2.6.31, created on 2019-02-11 01:44:32
         compiled from content/checkboxitem.tpl */ ?>
<div class="form__item">
    <div class="form__group">
        <label>
            <?php echo $this->_tpl_vars['strLabel']; ?>

            <input type="checkbox" name="<?php echo $this->_tpl_vars['strInputName']; ?>
" value="true" <?php if ($this->_tpl_vars['strValue'] == 1): ?>checked<?php endif; ?>>
            <span class="checkmark"></span>
        </label>
    </div>
</div>