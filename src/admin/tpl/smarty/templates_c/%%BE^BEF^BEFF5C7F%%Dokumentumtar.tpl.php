<?php /* Smarty version 2.6.31, created on 2019-02-11 01:24:06
         compiled from page%5Cmodule%5CDokumentumtar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Dokumentumtar.tpl', 1, false),array('insert', 'nav', 'page\\module\\Dokumentumtar.tpl', 2, false),array('insert', 'headline', 'page\\module\\Dokumentumtar.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Dokumentumtar.tpl', 17, false),array('insert', 'selectitem', 'page\\module\\Dokumentumtar.tpl', 23, false),array('insert', 'inputitem', 'page\\module\\Dokumentumtar.tpl', 26, false),array('insert', 'imageupload', 'page\\module\\Dokumentumtar.tpl', 53, false),array('insert', 'submitcancel', 'page\\module\\Dokumentumtar.tpl', 84, false),array('insert', 'end', 'page\\module\\Dokumentumtar.tpl', 89, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>


            <div class="row justify-content-center">
            	<div class="col-12 col-lx-8">
            		<div class="form__title">Általános beállítások</div>
                    <hr>
					<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Melyik dokumentumtárban jelenjen meg?", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[type_id][]", 'options' => $this->_tpl_vars['tblType'], 'multiple' => '1')), $this); ?>

            	</div>
                <div class="col-12 col-lx-8">
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                                        
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>
                        
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Dokumentum magyar adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Megnevezés", 'type' => 'text', 'placeholder' => "Add meg a dokumentum címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title']))), $this); ?>


                            <div class="form__item">
								<label>Rövid leírás</label>
                        		<textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[lead]" ><?php echo $this->_tpl_vars['tblEditedRowData']['lead']; ?>
</textarea>
                        	</div>
                        	
                        	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'imageupload', 'label' => "", 'btn_title' => "Fájl feltöltése", 'column' => 'filename', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[filename]", 'is_img' => '0', 'extList' => ".doc, .docx, .pdf, .xls, .xlsx, .jpg, .jpeg, .png, .gif", 'file_url' => ($this->_tpl_vars['tblEditedRowData']['filename']))), $this); ?>

                        </div>
                        
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Dokumentum angol adatai</div>
                            <hr>
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Megnevezés", 'type' => 'text', 'placeholder' => "Add meg a dokumentum címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title_en']))), $this); ?>

                            
							<div class="form__item">
								<label>Rövid leírás</label>
                        		<textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[lead_en]" ><?php echo $this->_tpl_vars['tblEditedRowData']['lead_en']; ?>
</textarea>
                        	</div>
                        	
                        	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'imageupload', 'label' => "", 'btn_title' => "Fájl feltöltése", 'column' => 'filename_en', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[filename_en]", 'is_img' => '0', 'extList' => ".doc, .docx, .pdf, .xls, .xlsx, .jpg, .jpeg, .png, .gif", 'file_url' => ($this->_tpl_vars['tblEditedRowData']['filename_en']))), $this); ?>

                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Dokumentum román adatai</div>
                            <hr>
    
                            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Megnevezés", 'type' => 'text', 'placeholder' => "Add meg a dokumentum címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title_ro']))), $this); ?>

                            
							<div class="form__item">
								<label>Rövid leírás</label>
                        		<textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[lead_ro]" ><?php echo $this->_tpl_vars['tblEditedRowData']['lead_ro']; ?>
</textarea>
                        	</div>
                        	
                        	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'imageupload', 'label' => "", 'btn_title' => "Fájl feltöltése", 'column' => 'filename_ro', 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[filename_ro]", 'is_img' => '0', 'extList' => ".doc, .docx, .pdf, .xls, .xlsx, .jpg, .jpeg, .png, .gif", 'file_url' => ($this->_tpl_vars['tblEditedRowData']['filename_ro']))), $this); ?>

                        </div>
                    </div>
                </div>
    	    </div>    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>

            