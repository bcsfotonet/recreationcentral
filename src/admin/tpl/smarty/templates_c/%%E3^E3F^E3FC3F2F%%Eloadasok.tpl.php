<?php /* Smarty version 2.6.31, created on 2019-02-11 01:53:44
         compiled from page%5Cmodule%5CEloadasok.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Eloadasok.tpl', 1, false),array('insert', 'nav', 'page\\module\\Eloadasok.tpl', 2, false),array('insert', 'headline', 'page\\module\\Eloadasok.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Eloadasok.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Eloadasok.tpl', 23, false),array('insert', 'selectitem', 'page\\module\\Eloadasok.tpl', 26, false),array('insert', 'submitcancel', 'page\\module\\Eloadasok.tpl', 63, false),array('insert', 'end', 'page\\module\\Eloadasok.tpl', 68, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                                        
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Város", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[city_id]", 'options' => $this->_tpl_vars['tblCity'])), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Előadó", 'type' => 'text', 'placeholder' => "Add meg az előadó nevét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[name]", 'value' => ($this->_tpl_vars['tblEditedRowData']['name']))), $this); ?>
                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Dátum", 'class' => "js-datetime", 'type' => 'text', 'placeholder' => "Add meg a kezdés dátumát!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[start_date]", 'value' => ($this->_tpl_vars['tblEditedRowData']['start_date']))), $this); ?>

                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Magyar cím", 'type' => 'text', 'fill' => '1', 'placeholder' => "Add meg a tartalom címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title']))), $this); ?>

                        </div>
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Angol cím", 'type' => 'text', 'fill' => '0', 'placeholder' => "Add meg a tartalom címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title_en']))), $this); ?>

                        </div>
                        <div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Tartalom adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Román cím", 'type' => 'text', 'fill' => '0', 'placeholder' => "Add meg a tartalom címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title_ro']))), $this); ?>

                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>