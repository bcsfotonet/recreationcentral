<?php /* Smarty version 2.6.31, created on 2019-02-11 02:32:26
         compiled from content/imageupload.tpl */ ?>
<div class="form__item">
	<?php if ($this->_tpl_vars['strLabel'] != ""): ?> 
    	<label><?php echo $this->_tpl_vars['strLabel']; ?>
</label>
    <?php endif; ?>
    <div class="form__group">
        <button class="button button--upload"><?php echo $this->_tpl_vars['strBtnTitle']; ?>
</button>
        <input 
        	class="file_upload_input <?php if ($this->_tpl_vars['isImage'] == 1): ?>image_upload_input<?php endif; ?>" 
        	type="file" 
        	<?php echo $this->_tpl_vars['strMultiple']; ?>
 
        	name="<?php echo $this->_tpl_vars['strInputName']; ?>
"
        	data-column_name="<?php echo $this->_tpl_vars['strColumn']; ?>
"
        	<?php if (! empty ( $this->_tpl_vars['strExtList'] )): ?> accept="<?php echo $this->_tpl_vars['strExtList']; ?>
" <?php else: ?> accept="image/*" <?php endif; ?>
    	>
    </div>
</div>
        
        <?php if ($this->_tpl_vars['isImage'] == '1'): ?>
        	<?php if (empty ( $this->_tpl_vars['strMultiple'] )): ?>
            	<div class="form__item preview d-none">
            		<label>Feltöltésre kiválasztott kép előnézet</label>
        			<img data-input_name="<?php echo $this->_tpl_vars['strInputName']; ?>
" src="" class="image-upload-preview w-auto img-thumbnail mt-2 mx-auto d-block" />
    			</div>
    			
    			<div class="form__item <?php if (empty ( $this->_tpl_vars['strFileUrl'] )): ?>d-none<?php endif; ?>">
    				<label>Mentett kép</label>
                	<div class="col-12 mt-2 grid-view__row  ">
                        <div class="tiled-view">
                            <div class="tiled-view__header">
                                <div class="tiled-view__icon-wrapper">
                                    <?php if ($this->_tpl_vars['rowModuleData']['is_edit_allowed'] == 1): ?>
                                        <div class="form__group">
                                            <label class="pl-0 mb-0 mt-1">
                                                <input class="del_checkbox" type="checkbox" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[image_delete]" value="" >
                                                <span class="delicon remove"><i class="fas fa-trash"></i></span>
                                            </label>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <hr>
                            <div class="tiled-view__content">
                                <div class="tiled-view__img">
                                    <img  src="<?php echo $this->_tpl_vars['strFileUrl']; ?>
" class="image-upload-preview w-auto  img-fluid mt-2 mx-auto d-block" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
            	<div class="form__item preview d-none preview_<?php echo $this->_tpl_vars['strColumn']; ?>
">
            		<label>Feltöltésre kiválasztott képek</label>
        			<div class="preview-list"></div>
    			</div>
            <?php endif; ?>
    	<?php else: ?>
    		<div class="form__item preview d-none preview_<?php echo $this->_tpl_vars['strColumn']; ?>
">
        		<label>Feltöltésre kiválasztott dokumentum</label>
    			<div class="preview-list"></div>
			</div>
			<div class="form__item <?php if (empty ( $this->_tpl_vars['strFileUrl'] )): ?>d-none<?php endif; ?>">
				<label>Mentett dokumentum</label>
        		<div class="col-12 mt-2 ">
        			<div class="tiled-view p-2">
        				<div class="tiled-view__icon-wrapper">
                            <?php if ($this->_tpl_vars['rowModuleData']['is_edit_allowed'] == 1): ?>
                                <div class="form__group">
                                    <label class="pl-0 mb-0 mr-3">
                                        <input class="del_checkbox" type="checkbox" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[<?php echo $this->_tpl_vars['strColumn']; ?>
_delete]" value="" >
                                        <span class="delicon remove"><i class="fas fa-trash"></i></span>
                                    </label>
                                </div>
                            <?php endif; ?>
                            <div class="form__item">
        	    				<a class="document__download" href="<?php echo $this->_tpl_vars['strFileUrl']; ?>
" target="_blank">Letöltés</a>
        	    			</div>
                        </div>
                    </div>
        		</div>
    		</div>
        <?php endif; ?>
    
<!-- </div> -->