<?php /* Smarty version 2.6.31, created on 2019-02-11 01:44:30
         compiled from content/pagination.tpl */ ?>
<div class="col-12">
    <nav aria-label="Page navigation">
        <ul class="pagination">
          	<li class="page-item <?php if ($this->_tpl_vars['objPagination']->getNumCurrPage() == 1): ?>disabled<?php endif; ?>">
        		<a class="page-link" href="<?php echo $this->_tpl_vars['strUrl']; ?>
<?php echo $this->_tpl_vars['objPagination']->getNumCurrPage()-1; ?>
" aria-label="Previous">
              		<span aria-hidden="true">&laquo;</span>
              		<span class="sr-only">Previous</span>
            	</a>
          	</li>
          	<?php $this->assign('numMaxPage', $this->_tpl_vars['objPagination']->getMaxPage()); ?>
			<?php unset($this->_sections['numPagination']);
$this->_sections['numPagination']['name'] = 'numPagination';
$this->_sections['numPagination']['start'] = (int)1;
$this->_sections['numPagination']['loop'] = is_array($_loop=$this->_tpl_vars['numMaxPage']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['numPagination']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['numPagination']['show'] = true;
$this->_sections['numPagination']['max'] = $this->_sections['numPagination']['loop'];
if ($this->_sections['numPagination']['start'] < 0)
    $this->_sections['numPagination']['start'] = max($this->_sections['numPagination']['step'] > 0 ? 0 : -1, $this->_sections['numPagination']['loop'] + $this->_sections['numPagination']['start']);
else
    $this->_sections['numPagination']['start'] = min($this->_sections['numPagination']['start'], $this->_sections['numPagination']['step'] > 0 ? $this->_sections['numPagination']['loop'] : $this->_sections['numPagination']['loop']-1);
if ($this->_sections['numPagination']['show']) {
    $this->_sections['numPagination']['total'] = min(ceil(($this->_sections['numPagination']['step'] > 0 ? $this->_sections['numPagination']['loop'] - $this->_sections['numPagination']['start'] : $this->_sections['numPagination']['start']+1)/abs($this->_sections['numPagination']['step'])), $this->_sections['numPagination']['max']);
    if ($this->_sections['numPagination']['total'] == 0)
        $this->_sections['numPagination']['show'] = false;
} else
    $this->_sections['numPagination']['total'] = 0;
if ($this->_sections['numPagination']['show']):

            for ($this->_sections['numPagination']['index'] = $this->_sections['numPagination']['start'], $this->_sections['numPagination']['iteration'] = 1;
                 $this->_sections['numPagination']['iteration'] <= $this->_sections['numPagination']['total'];
                 $this->_sections['numPagination']['index'] += $this->_sections['numPagination']['step'], $this->_sections['numPagination']['iteration']++):
$this->_sections['numPagination']['rownum'] = $this->_sections['numPagination']['iteration'];
$this->_sections['numPagination']['index_prev'] = $this->_sections['numPagination']['index'] - $this->_sections['numPagination']['step'];
$this->_sections['numPagination']['index_next'] = $this->_sections['numPagination']['index'] + $this->_sections['numPagination']['step'];
$this->_sections['numPagination']['first']      = ($this->_sections['numPagination']['iteration'] == 1);
$this->_sections['numPagination']['last']       = ($this->_sections['numPagination']['iteration'] == $this->_sections['numPagination']['total']);
?>
				<li class="page-item <?php if ($this->_tpl_vars['objPagination']->getNumCurrPage() == $this->_sections['numPagination']['index']): ?>active<?php endif; ?>">
					<a class="page-link" href="<?php echo $this->_tpl_vars['strUrl']; ?>
<?php echo $this->_sections['numPagination']['index']; ?>
"><?php echo $this->_sections['numPagination']['index']; ?>
</a>
				</li>
			<?php endfor; endif; ?>
<!--           <li class="page-item active"><a class="page-link" href="#">2</a></li> -->
<!--           <li class="page-item"><a class="page-link" href="#">3</a></li> -->
      		<li class="page-item <?php if ($this->_tpl_vars['objPagination']->getNumCurrPage() == $this->_tpl_vars['numMaxPage']): ?>disabled<?php endif; ?>">
            	<a class="page-link" href="<?php echo $this->_tpl_vars['strUrl']; ?>
<?php echo $this->_tpl_vars['objPagination']->getNumCurrPage()+1; ?>
" aria-label="Next">
              		<span aria-hidden="true">&raquo;</span>
              		<span class="sr-only">Next</span>
           		</a>
          	</li>
        </ul>
    </nav>
</div>