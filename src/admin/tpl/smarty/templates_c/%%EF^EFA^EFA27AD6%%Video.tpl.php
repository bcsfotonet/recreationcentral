<?php /* Smarty version 2.6.31, created on 2019-02-11 01:43:25
         compiled from page%5Cmodule%5CVideo.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\module\\Video.tpl', 1, false),array('insert', 'nav', 'page\\module\\Video.tpl', 2, false),array('insert', 'headline', 'page\\module\\Video.tpl', 13, false),array('insert', 'backbtn', 'page\\module\\Video.tpl', 17, false),array('insert', 'inputitem', 'page\\module\\Video.tpl', 23, false),array('insert', 'selectitem', 'page\\module\\Video.tpl', 27, false),array('insert', 'imageupload', 'page\\module\\Video.tpl', 29, false),array('insert', 'submitcancel', 'page\\module\\Video.tpl', 82, false),array('insert', 'end', 'page\\module\\Video.tpl', 87, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


<div class="content">

    <?php if ($this->_tpl_vars['strTplPage'] == 'new'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Új hozzáadása"); ?>
    <?php elseif ($this->_tpl_vars['strTplPage'] == 'edit'): ?>
        <?php $this->assign('strTitle', ($this->_tpl_vars['rowModuleData']['module_name'])." - Szerkesztés"); ?>
    <?php else: ?>
        <?php $this->assign('strTitle', ""); ?>
    <?php endif; ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => $this->_tpl_vars['strTitle'])), $this); ?>


    <div class="container-fluid">
        <form action="" method="post" class="form module-save-form">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'backbtn', 'url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

    
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="form__title">Általános beállítások</div>
                    <hr>
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => 'module_url', 'value' => ($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "", 'type' => 'hidden', 'placeholder' => "", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[id]", 'value' => ($this->_tpl_vars['tblEditedRowData']['id']))), $this); ?>

                    
					<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Videó kódja", 'type' => 'text', 'placeholder' => "Add meg a videó youtube kódját!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[code]", 'value' => ($this->_tpl_vars['tblEditedRowData']['code']))), $this); ?>
                    
                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Melyik videótárban jelenjen meg?", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[type_id][]", 'options' => $this->_tpl_vars['tblType'], 'multiple' => '1')), $this); ?>

					<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'selectitem', 'label' => "Város", 'select_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[city_id]", 'options' => $this->_tpl_vars['tblCity'])), $this); ?>

                    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'imageupload', 'label' => "Videó kezdőképe (600x315px)", 'btn_title' => "Kép feltöltése", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[image]", 'is_img' => '1', 'file_url' => ($this->_tpl_vars['tblEditedRowData']['image']))), $this); ?>

                    
                </div>
                <div class="col-12 col-lg-8">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="hu-tab" data-toggle="tab" href="#hu" role="tab" aria-controls="hu" aria-selected="true">Magyar</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="en-tab" data-toggle="tab" href="#en" role="tab" aria-controls="en" aria-selected="false">Angol</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="ro-tab" data-toggle="tab" href="#ro" role="tab" aria-controls="ro" aria-selected="false">Román</a>
                        </li>

                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="hu" role="tabpanel" aria-labelledby="hu-tab">
                            <div class="form__title">Videó adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Cím", 'type' => 'text', 'placeholder' => "Add meg a videó címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title']))), $this); ?>

                            	<div class="form__item">
    								<label>Videó rövid leírása</label>
                            		<textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[description]" ><?php echo $this->_tpl_vars['tblEditedRowData']['description']; ?>
</textarea>
								</div>
                        </div>                       
                        <div class="tab-pane fade" id="en" role="tabpanel" aria-labelledby="en-tab">
                            <div class="form__title">Videó adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Cím", 'type' => 'text', 'placeholder' => "Add meg a videó címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title_en]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title_en']))), $this); ?>

                            	<div class="form__item">
    								<label>Videó rövid leírása</label>
                            		<textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[description_en]" ><?php echo $this->_tpl_vars['tblEditedRowData']['description_en']; ?>
</textarea>
								</div>
                        </div>
						<div class="tab-pane fade" id="ro" role="tabpanel" aria-labelledby="ro-tab">
                            <div class="form__title">Videó adatai</div>
                            <hr>
                            	<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'inputitem', 'label' => "Cím", 'type' => 'text', 'placeholder' => "Add meg a videó címét!", 'input_name' => ($this->_tpl_vars['rowModuleData']['url'])."_field[title_ro]", 'value' => ($this->_tpl_vars['tblEditedRowData']['title_ro']))), $this); ?>

                            	<div class="form__item">
    								<label>Videó rövid leírása</label>
                            		<textarea rows="5" name="<?php echo $this->_tpl_vars['rowModuleData']['url']; ?>
_field[description_ro]" ><?php echo $this->_tpl_vars['tblEditedRowData']['description_ro']; ?>
</textarea>
								</div>
                        </div>
                    </div>
                </div>
    	    </div>
    	    
	        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'submitcancel', 'cancel_url' => ($this->_tpl_vars['CONF']['admin_base_url']).($this->_tpl_vars['rowModuleData']['url']))), $this); ?>

        </form>
    </div>
    
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>