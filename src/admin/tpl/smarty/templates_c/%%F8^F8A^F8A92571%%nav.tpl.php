<?php /* Smarty version 2.6.31, created on 2019-02-11 02:32:33
         compiled from content/nav.tpl */ ?>
<body>
    <div class="sidebar">
        <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
" class="sidebar__title">recreationcentral.eu </a>
        <div class="sidebar__logout">
            <i class="fas fa-sign-out-alt"></i>
            <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
Logout">Kijelentkezés</a>
        </div>
        
        <div class="nav">
            <ul class="nav__list">        
                <?php $_from = $this->_tpl_vars['tblModule']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tblModuleTmp']):
?>
                    <?php if (! empty ( $this->_tpl_vars['tblModuleTmp']['group'] )): ?>
                        <li class="nav__li nav__dropdown js-dropdown">
                                <a class="parent-li" href="#">
                                    <i class="fa <?php echo $this->_tpl_vars['tblModuleTmp']['group']['icon_class']; ?>
"></i>
                                    <span><?php echo $this->_tpl_vars['tblModuleTmp']['group']['name']; ?>
</span>
                                </a>
                            <ul class="nav__subnav">
                    <?php endif; ?>
                            <?php $_from = $this->_tpl_vars['tblModuleTmp']['submodule']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tblSubmoduleTmp']):
?>
                                <li class="nav__li">
                                    <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
<?php echo $this->_tpl_vars['tblSubmoduleTmp']['module']['url']; ?>
">
                                        <i class="fa <?php echo $this->_tpl_vars['tblSubmoduleTmp']['module']['icon_class']; ?>
"></i>
                                        <span><?php echo $this->_tpl_vars['tblSubmoduleTmp']['module']['name']; ?>
</span>
                                    </a>
                                </li>
                            <?php endforeach; else: ?>
                                <li class="nav__li">
                                    <a href="<?php echo $this->_tpl_vars['CONF']['admin_base_url']; ?>
<?php echo $this->_tpl_vars['tblModuleTmp']['module']['url']; ?>
">
                                        <i class="fa <?php echo $this->_tpl_vars['tblModuleTmp']['module']['icon_class']; ?>
"></i>
                                        <span><?php echo $this->_tpl_vars['tblModuleTmp']['module']['name']; ?>
</span>
                                    </a>
                                </li>
                            <?php endif; unset($_from); ?>
                    <?php if (! empty ( $this->_tpl_vars['tblModuleTmp']['group'] )): ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                <?php endforeach; endif; unset($_from); ?>
            </ul>
        </div>       
    </div>
      