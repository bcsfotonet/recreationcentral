var select2             = require('./moduls/select2');
var slick               = require('./moduls/slick');
var nav                 = require('./moduls/nav');
var faq                 = require('./moduls/faq');
var fullcalendar        = require('./moduls/fullcalendar');
var gototop             = require('./moduls/gototop');
if($('.pswp').length>0){
    var photoswipe      = require('./moduls/photoswipe');
}
var youtubevideo        = require('./moduls/youtube-video');
var cookiebar           = require('./moduls/cookiebar');
var registration        = require('./moduls/registration');
var order               = require('./moduls/order');

select2.init();
slick.init();
nav.init();
faq.init();
fullcalendar.init();
gototop.init();
if($('.pswp').length>0){
    photoswipe.init();
}
youtubevideo.init();
cookiebar.init();
registration.init();
order.init();