"use strict";

module.exports = {

    init:function(){

        $(document).ready(function() {
            
            $.cookieBar({
                message:'Oldalunk cookie-kat (sütiket) használ.Ezen fájlok információkat szolgáltatnak számunkra a felhasználó oldallátogatási szokásairól, de nem tárolnak személyes információkat. Weboldalunk böngészésével Ön beleegyezik a cookie-k használatába.',
                
                policyButton: true,
                policyText: 'Adatvédelem',
                policyURL: './adatvedelem/',
                fixed: true,
                bottom: true,
                acceptText:'Rendben',
            });
        });  
    }
}