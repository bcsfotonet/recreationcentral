"use strict";

module.exports = {

    init: function(){
        //faq
        $('.js-question').click(function(e){
            $(this).next('.faq__answer').stop().slideToggle();
            $(this).toggleClass('faq__question--open');
        });
    }
}