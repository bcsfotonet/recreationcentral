'use strict';
var moment     = require('moment');
require('moment/locale/hu');

module.exports = {
    
    loading: {
        xhrs: [],
        loadedEventTypes: 0,
        eventTypeNumber: 4,
        errorOccured: false
    },

    init: function() {
        var self = this;

        this.$obj = $('.js-calendar');
        if (!this.$obj.length) {
            return void false;
        }

        this.$calendar = $('#js-calendar__view');
        this.initCalendar();
        this.initFunctionBtns();

    },

    /**
     */
    initCalendar: function() {
        var self = this;
        var eventZIndex = 1;
        if(strBaseLang==''){
            var strLang = 'hu';
        } else {
            var strLang = strBaseLang;
        }

        this.$calendar.fullCalendar({
            locale: strLang,
            theme: true,
            defaultView: 'month',
            events: strXhrUrl+'load.php?l='+strBaseLang,
            height: 500,
            contentHeight: 'auto',
            header: false,
            columnHeader: false,
            nowIndicator: true,

            viewRender: function(view) {
                var title = document.getElementsByClassName('js-calendar__title');
                self.$obj.find('.js-calendar__title').html(view.title);
            },
            
            eventRender: function(event, element, view) {
                element.find('.fc-time')
                .remove('.fc-time')
            },

            eventClick: function(calEvent, jsEvent, view)  {

                var calendar_content = document.querySelector('.events-box');
                var tr = this.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode;

                if (calendar_content == null) {
                    tr.insertAdjacentHTML('afterend',
                    '<div class="events-box"><div class="events-box__item">' +
                        '<div class="events-box__image">' +
                            '<img src="'+ calEvent.imgSource + '" alt="">' +
                        '</div>' +
                        '<div class="events-box__content">' +
                            '<div class="events-box__city">'+ calEvent.city +'</div>' +
                            '<div class="events-box__title">'+ calEvent.title +'</div>'+
                            '<div class="events-box__date">' + moment(calEvent.start).format('YYYY/MM/DD') + (calEvent.end == null ? '' : ' - ' + moment(calEvent.end).format('YYYY/MM/DD')) +' </div>' +
                            '<div class="events-box__lead">' + calEvent.lead + '</div>' +
                        '<a href="'+ calEvent.moreSrc +'" class="events-box__more" target="_blank">'+ calEvent.strMore +'</a>' +
                        '</div></div>' +
                    '</div>');
                } else {
                    calendar_content.remove();
                } 
            }
        });
    },

    /**
     * initFunctionBtns
     */
    initFunctionBtns: function() {
        var self = this;

        this.$obj.find(".js-calendar__next").on('click', function() {
            self.$calendar.fullCalendar('next')
        });

        this.$obj.find(".js-calendar__prev").on('click', function() {
            self.$calendar.fullCalendar('prev')
        });
    }
};
