"use strict";

module.exports = {

    init: function(){
        window.onscroll = function() {scrollFunction()};
        var btn = document.getElementById("goToTop");

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                btn.style.display = "block";
            } else {
                btn.style.display = "none";
            }
        }

        $(window).on('scroll', function() {
            var scrollTop = $(window).scrollTop();
        });

        btn.addEventListener('click', function(e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
}