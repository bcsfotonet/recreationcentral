"use strict";

module.exports = {
    
    init: function(){
        this.pcnav();
        this.mobilnav();
    },

        pcnav: function(){
            // pc nav
        $('.js-sub-nav').click(function(e){
            $('.nav__sub-nav').not($(this).children()).slideUp(300);
            $('.nav__parent-nav').not($(this)).removeClass('nav__parent-nav-opened');
            $(this).children('.js-sub-list').stop().slideToggle(300);
            $(this).toggleClass('nav__parent-nav-opened');
        }); 
        
        $(window).on('click', function(e) {
            var menu = $('.nav__li');
            if (!menu.is(e.target) // if the target of the click isn't the container...
                && menu.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $('.nav__sub-nav').slideUp(300);
                $('.nav__parent-nav').removeClass('nav__parent-nav-opened');
            }    
        });
        
        $('.js-sub-lvl-2-nav').click(function(e){
            e.stopPropagation();
            $(this).toggleClass('nav__parent-nav-opened');
            $(this).children('.js-sub-list-lvl-2').stop().slideToggle(300);
        });
    
    },

    mobilnav: function(){
        // mobil nav
        $('.mobil-nav').click(function(e){
            $('.nav__list').stop().fadeToggle({direction:"left"}, 1500);
        });
    }
}