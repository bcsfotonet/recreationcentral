"use strict";

module.exports = {

    init: function () {
        var timeout;
        var receiveMode = $('input[name="receive_mode"]');
        var postDifferent = $('input[name="post_different"]');
        
        receiveMode.on("change", function () {
            var checkedReceiveModeValue = $(this).val();
            var customer_id = $(this).attr('data-cid');
            var action = 'update_total';
            showHideReceiveMode(checkedReceiveModeValue);
            updateTotal(customer_id,action,checkedReceiveModeValue);
        });

        // ha az input checked es ujratolt az oldal akkor is mukodjon
        receiveMode.each(function() {
            var checkedReceiveModeValue = $(this).val();
            if($(this).prop('checked')){
                showHideReceiveMode(checkedReceiveModeValue);
            }
        });

        postDifferent.on("change", function () {
            showHidePostDiff($(this));
        });
        
        // ha az input checked es ujratolt az oldal akkor is mukodjon
        showHidePostDiff(postDifferent);
        
        // add to basket
        $('.add-to-basket').click(function(e){
            var newLocation = $(this).attr('data-href');
            var magazine_id = $(this).attr('data-mid');;
            var num_magazine = $(this).attr('data-num');
            var customer_id = $(this).attr('data-cid');
            var action = 'add';

            $.ajax({type:'POST',
                url: strXhrUrl+'basket.php?l='+strBaseLang,
                dataType:'json',
                data: {magazine_id: magazine_id, num_magazine:num_magazine, customer_id:customer_id, action:action},
                success: function(response){
//                    alert(response);
//                    if(response.status == 'ok'){
//                        alert(response.order_id);
                        window.location = newLocation;
//                    }
                }
            });
        });
        
        // remove from basket
        $('.remove-from-basket').click(function(e){
            var newLocation = $(this).attr('data-href');
            var magazine_id = $(this).attr('data-mid');;
            var num_magazine = $(this).attr('data-num');
            var customer_id = $(this).attr('data-cid');
            var action = 'remove';

            $.ajax({type:'POST',
                url: strXhrUrl+'basket.php?l='+strBaseLang,
                dataType:'json',
                data: {magazine_id: magazine_id, num_magazine:num_magazine, customer_id:customer_id, action:action},
                success: function(response){
                    window.location = newLocation;
                }
            });
        });
        
        $(document).on("keyup", ".basket-numbox", function(event){
            var magazine_id = $(this).attr('data-mid');;
            var num_magazine = $(this).val();
            var customer_id = $(this).attr('data-cid');
            var action = 'change';
            if(timeout){ clearTimeout(timeout);}
            if (event.keyCode != 46 && event.keyCode != 8){
                timeout = setTimeout(function() {
                    basketNumbox(magazine_id,num_magazine,customer_id,action);
                },500);
            }
        });
        
//        $('.basket-numbox').change(function(e){
//            var magazine_id = $(this).attr('data-mid');;
//            var num_magazine = $(this).val();
//            var customer_id = $(this).attr('data-cid');
//            var action = 'change';
//            
//            basketNumbox(magazine_id,num_magazine,customer_id,action);
//        });
        
        function basketNumbox(magazine_id,num_magazine,customer_id,action){
            $.ajax({type:'POST',
                url: strXhrUrl+'basket.php?l='+strBaseLang,
                dataType:'json',
                data: {magazine_id: magazine_id, num_magazine:num_magazine, customer_id:customer_id, action:action},
                success: function(response){
                    if(response.status == 'ok'){
                        window.location.reload();
                    }                 
                }
            });
        }
        
        function updateTotal(customer_id, action, receive_mode){
            $.ajax({type:'POST',
                url: strXhrUrl+'basket.php?l='+strBaseLang,
                dataType:'json',
                data: {customer_id:customer_id, action:action, receive_mode:receive_mode},
                success: function(response){
                    if(response.status == 'ok'){
                        window.location.reload();  
                    }                 
                }
            });
        }

        function showHideReceiveMode(receiveModeValue){
            var postDifferent = $('input[name="post_different"]');
            if (receiveModeValue == 0) {
                $('#differentAddress').css("display", "block");
                $('#deliveryCost').css("display", "flex");
                $('#personalReceipt').css("display", "none");
            } else if(receiveModeValue == 1) {
                postDifferent.prop("checked",false);
                showHidePostDiff(postDifferent);
                $('#differentAddress').css("display", "none");
                $('#deliveryCost').css("display", "none");
                $('#personalReceipt').css("display", "block");
            }
        }

        function showHidePostDiff(postDifferent){
            if (postDifferent.prop("checked")){
                $('#addressData').css("display", "block");
            } else {
                $('#addressData').css("display", "none");
                $('#addressData').find('input').val('');
            }
        }

    }
};

