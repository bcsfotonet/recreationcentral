"use strict";

module.exports = {

    init: function () {
        var personRadio = document.getElementById("personRadio");
        var companyRadio = document.getElementById("companyRadio");

        if (personRadio == null) {
            return void 0;
        }

        if (personRadio.checked) {
            companyRadio.checked = false;
            personRadio.checked = true;
            taxNumber.disabled = true;
            formRegistrationFirstName.disabled = false;
            formRegistrationLastName.disabled = false;
            (taxNumber.parentNode.parentNode).style.display = "none";
            (formRegistrationFirstName.parentNode.parentNode).style.display = "inline-block";
            (formRegistrationLastName.parentNode.parentNode).style.display = "inline-block";
            formRegistrationCompany.disabled = true;
            (formRegistrationCompany.parentNode.parentNode).style.display = "none";
        }

        personRadio.addEventListener("click", function () {
            companyRadio.checked = false;
            personRadio.checked = true;
            taxNumber.disabled = true;
            formRegistrationFirstName.disabled = false;
            formRegistrationLastName.disabled = false;
            (taxNumber.parentNode.parentNode).style.display = "none";
            (formRegistrationFirstName.parentNode.parentNode).style.display = "inline-block";
            (formRegistrationLastName.parentNode.parentNode).style.display = "inline-block";
            formRegistrationCompany.disabled = true;
            (formRegistrationCompany.parentNode.parentNode).style.display = "none";
        });

        if (companyRadio == null) {
            return void 0;
        }

        if (companyRadio.checked) {
            companyRadio.checked = true;
            personRadio.checked = false;
            taxNumber.disabled = false;
            (taxNumber.parentNode.parentNode).style.display = "inline-block";
            formRegistrationFirstName.disabled = true;
            formRegistrationLastName.disabled = true;
            (formRegistrationFirstName.parentNode.parentNode).style.display = "none";
            (formRegistrationLastName.parentNode.parentNode).style.display = "none";
            formRegistrationCompany.disabled = false;
            (formRegistrationCompany.parentNode.parentNode).style.display = "inline-block";
        }

        companyRadio.addEventListener("click", function () {
            companyRadio.checked = true;
            personRadio.checked = false;
            taxNumber.disabled = false;
            (taxNumber.parentNode.parentNode).style.display = "inline-block";
            formRegistrationFirstName.disabled = true;
            formRegistrationLastName.disabled = true;
            (formRegistrationFirstName.parentNode.parentNode).style.display = "none";
            (formRegistrationLastName.parentNode.parentNode).style.display = "none";
            formRegistrationCompany.disabled = false;
            (formRegistrationCompany.parentNode.parentNode).style.display = "inline-block";
        });
    }
};

