"use strict";

module.exports = {

    init: function(){

        //var strWebUrl = null;
        
            $('.partners__wrapper').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            centerMode: true,
            variableWidth: true,
            autoplay: true,
            autoplaySpeed: 2000,
            prevArrow: '<button class="partners__prev slick-prev" aria-label="Előző" type="button"><img src="' + strWebUrl +'images/prev.svg" alt=""></button>',
            nextArrow: '<button class="partners__next slick-next" aria-label="Következő" type="button"><img src="' + strWebUrl +'images/next.svg" alt=""></button>'
        });
    }
}