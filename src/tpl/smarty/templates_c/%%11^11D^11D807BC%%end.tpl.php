<?php /* Smarty version 2.6.31, created on 2019-02-11 01:26:15
         compiled from content/end.tpl */ ?>
			
            <footer class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-4 pb20">
                            <div class="footer__title"><?php echo $this->_tpl_vars['rowLabel']['elerhetosegi_adatok']; ?>
:</div>
                            <div class="footer__text">
                                <p>Közép-Kelet-Európai Rekreációs Társaság</p>
                                <p><?php echo $this->_tpl_vars['rowLabel']['roviditett_nev']; ?>
: KERT</p>
                                <p><?php echo $this->_tpl_vars['rowLabel']['szekhely']; ?>
: 6723 Szeged, Csaba utca 48/A</p>
                                <p><?php echo $this->_tpl_vars['rowLabel']['levelezesi_cim']; ?>
: 6723 Szeged, Csaba utca 48/A</p>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-md-5 pb20">
                            <div class="footer__title"><?php echo $this->_tpl_vars['rowLabel']['tovabbi_tartalom']; ?>
</div>
                            <?php if (! empty ( $this->_tpl_vars['tblFooterMenu'] )): ?>
                                <ul class="footer__list">
                                    <?php $_from = $this->_tpl_vars['tblFooterMenu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowFooterMenu']):
?>
                                        <li class="footer__li"><a <?php if (! empty ( $this->_tpl_vars['rowFooterMenu']['url'] )): ?>href="<?php echo $this->_tpl_vars['rowFooterMenu']['url']; ?>
" target="<?php if (! empty ( $this->_tpl_vars['rowFooterMenu']['target_value'] )): ?><?php echo $this->_tpl_vars['rowFooterMenu']['target_value']; ?>
<?php else: ?>_self<?php endif; ?>"<?php endif; ?>><?php echo $this->_tpl_vars['rowFooterMenu']['name']; ?>
</a></li>
                                    <?php endforeach; endif; unset($_from); ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                        <div class="col-12 col-md-3 pb20">
                            <div class="footer__title"><?php echo $this->_tpl_vars['rowLabel']['kozossegi_media']; ?>
</div>
                            <a href="https://www.youtube.com/channel/UCghtAX1H_Wsyd7hEqxJ_yQg?feature=watch" target="_blank" class="footer__icon">
                                <img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/youtube.svg" alt="Youtube" >
                            </a>
                            <a href="https://www.facebook.com/rekreacio" target="_blank" class="footer__icon">
                                <img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/facebook.svg" alt="Facebook" >
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 footer__made-by">
                            <a href="https://bcsfotonet.hu" target="_blank">
                                <img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/bcs-logo.svg" alt="BCs FotoNet">
                            </a>
                        </div>
                    </div>
                </div>
            </footer>

        </div>
        <div id="goToTop" class="goToTop"></div>
        <?php echo '
            <script>
                var strBaseUrl = \''; ?>
<?php echo $this->_tpl_vars['CONF']['base_url']; ?>
<?php echo '\';
            	var strWebUrl = \''; ?>
<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
<?php echo '\';
                var strXhrUrl = \''; ?>
<?php echo $this->_tpl_vars['CONF']['base_url']; ?>
xhr/<?php echo '\';
            </script>
        '; ?>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
vendor/vendor.js"></script>
        <script src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
js/main.js"></script>
    </body>
</html>