<?php /* Smarty version 2.6.31, created on 2018-11-01 22:34:37
         compiled from page%5CRolunk_irtak.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Rolunk_irtak.tpl', 2, false),array('insert', 'header', 'page\\Rolunk_irtak.tpl', 3, false),array('insert', 'nav', 'page\\Rolunk_irtak.tpl', 4, false),array('insert', 'headline', 'page\\Rolunk_irtak.tpl', 7, false),array('insert', 'end', 'page\\Rolunk_irtak.tpl', 67, false),array('modifier', 'date_format', 'page\\Rolunk_irtak.tpl', 27, false),)), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>

                        
        <div class="content">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => "Rólunk írták", 'firstli' => "Nyitóoldal", 'firstliurl' => $this->_tpl_vars['CONF']['base_url'], 'secondli' => "Média", 'secondliurl' => "", 'selected' => "Rólunk írták")), $this); ?>

            <div class="container">
                <div class="row">
                    <?php if (! empty ( $this->_tpl_vars['tblData'] )): ?>
                        <?php $_from = $this->_tpl_vars['tblData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowData']):
?>
                            <div class="col-12 col-sm-6 col-lg-4">
                                <div class="news-item">
                                    <img class="news-item__image" src="<?php echo $this->_tpl_vars['rowData']['image']; ?>
" alt="<?php echo $this->_tpl_vars['rowData']['title']; ?>
">
                                    <span class="news-item__title"><?php echo $this->_tpl_vars['rowData']['title']; ?>
</span>
                                                                        <?php if (! empty ( $this->_tpl_vars['rowData']['start_date'] )): ?>
                                        <span class="news-item__date"><?php echo ((is_array($_tmp=$this->_tpl_vars['rowData']['start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y. %m. %d %H:%M") : smarty_modifier_date_format($_tmp, "%Y. %m. %d %H:%M")); ?>
<?php if (! empty ( $this->_tpl_vars['rowData']['end_date'] )): ?> - <?php echo ((is_array($_tmp=$this->_tpl_vars['rowData']['end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y. %m. %d %H:%M") : smarty_modifier_date_format($_tmp, "%Y. %m. %d %H:%M")); ?>
<?php endif; ?></span>
                                    <?php endif; ?>
                                    <?php if (! empty ( $this->_tpl_vars['rowData']['lead'] )): ?>
                                        <p class="news-item__lead">
                                            <?php echo $this->_tpl_vars['rowData']['lead']; ?>

                                        </p>
                                    <?php endif; ?>
                                    <?php if (! empty ( $this->_tpl_vars['rowData']['url'] )): ?>
                                        <a href="<?php echo $this->_tpl_vars['CONF']['base_url']; ?>
rolunk_irtak/<?php echo $this->_tpl_vars['rowData']['url']; ?>
" class="news-item__more">Részletek</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; endif; unset($_from); ?>
                    <?php endif; ?>
                </div>
            </div>
                    </div>
        
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>