<?php /* Smarty version 2.6.30, created on 2018-10-28 02:53:09
         compiled from page%5CGyik.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Gyik.tpl', 2, false),array('insert', 'header', 'page\\Gyik.tpl', 3, false),array('insert', 'nav', 'page\\Gyik.tpl', 4, false),array('insert', 'headline', 'page\\Gyik.tpl', 7, false),array('insert', 'end', 'page\\Gyik.tpl', 38, false),)), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>

                        
        <div class="content">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => ($this->_tpl_vars['rowStaticData']['name']), 'firstli' => "Nyitóoldal", 'firstliurl' => $this->_tpl_vars['CONF']['base_url'], 'secondli' => "", 'secondliurl' => "", 'selected' => ($this->_tpl_vars['rowStaticData']['name']))), $this); ?>

            <?php if (! empty ( $this->_tpl_vars['tblData'] )): ?>
                <div class="container">
                    <div class="row">
                        <?php if (! empty ( $this->_tpl_vars['rowStaticData']['static_description'] )): ?>
                            <div class="col-12 col-md-5 stat-text">
                                <?php echo $this->_tpl_vars['rowStaticData']['static_description']; ?>

                            </div>
                        <?php endif; ?>
                        <div class="col-12 col-md-7 faq">
                            <?php $_from = $this->_tpl_vars['tblData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowData']):
?>
                                <div class="faq__question js-question">
                                    <?php echo $this->_tpl_vars['rowData']['question']; ?>

                                </div>
                                <div class="faq__answer">
                                    <?php echo $this->_tpl_vars['rowData']['answer']; ?>

                                </div>
                            <?php endforeach; endif; unset($_from); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>