<?php /* Smarty version 2.6.31, created on 2019-02-11 01:26:15
         compiled from content/headline.tpl */ ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="headline">
                <div class="headline__title"><?php echo $this->_tpl_vars['strTitle']; ?>
</div>
                <ul class="breadcrumb">
                	<?php if (! empty ( $this->_tpl_vars['strFirstLi'] )): ?>
                    <li class="breadcrumb__li">
                        <a <?php if (! empty ( $this->_tpl_vars['strFirstLiUrl'] )): ?>href="<?php echo $this->_tpl_vars['strFirstLiUrl']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['strFirstLi']; ?>
</a>
                    </li>
                    <?php endif; ?>
                    <?php if (! empty ( $this->_tpl_vars['strSecondLi'] )): ?>
                    <li class="breadcrumb__li">
                        <a <?php if (! empty ( $this->_tpl_vars['strSecondLiUrl'] )): ?>href="<?php echo $this->_tpl_vars['strSecondLiUrl']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['strSecondLi']; ?>
</a>
                    </li>
                    <?php endif; ?>
                    <?php if (! empty ( $this->_tpl_vars['strThirdLi'] )): ?>
                    <li class="breadcrumb__li">
                        <a <?php if (! empty ( $this->_tpl_vars['strThirdLiUrl'] )): ?>href="<?php echo $this->_tpl_vars['strThirdLiUrl']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['strThirdLi']; ?>
</a>
                    </li>
                    <?php endif; ?>
                    <li class="breadcrumb__li breadcrumb__li--selected"><?php echo $this->_tpl_vars['strSelected']; ?>
</li>
                </ul>
            </div>
        </div>
    </div>
</div>