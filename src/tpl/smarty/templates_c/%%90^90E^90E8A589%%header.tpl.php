<?php /* Smarty version 2.6.31, created on 2019-02-11 01:26:14
         compiled from content/header.tpl */ ?>
<header class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-4">
                <a href="<?php echo $this->_tpl_vars['CONF']['base_url_lang']; ?>
">
                    <img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/logo.png" alt="Közép-Kelet-Európai Rekreációs Társaság" class="header__logo">
                </a>
                <div class="header__slogen">"Az élmény önmagát írja; az ötletet Te írod" – Márai Sándor</div>
                <div class="mobil-nav">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="col-md-8">
                                <div class="row mt10 mb10 justify-content-center justify-content-md-end"> 
                    <?php if (empty ( $this->_tpl_vars['strLanguage'] )): ?>
                        <a href="<?php if (! empty ( $this->_tpl_vars['loginLink'] )): ?> <?php echo $this->_tpl_vars['loginLink']; ?>
 <?php endif; ?>" class="header__link"> 
                            <img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/login.svg" alt="<?php if (! empty ( $this->_tpl_vars['loginText'] )): ?><?php echo $this->_tpl_vars['loginText']; ?>
<?php else: ?>Bejelentkezés<?php endif; ?>" class="header__icon">
                            <?php if (! empty ( $this->_tpl_vars['loginText'] )): ?><?php echo $this->_tpl_vars['loginText']; ?>
<?php else: ?><?php echo $this->_tpl_vars['rowLabel']['bejelentkezes']; ?>
<?php endif; ?>
                        </a>
                        <a href="<?php if (! empty ( $this->_tpl_vars['registerLink'] )): ?> <?php echo $this->_tpl_vars['registerLink']; ?>
 <?php endif; ?>" class="header__link">
                            <img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/reg.svg" alt="<?php if (! empty ( $this->_tpl_vars['registerText'] )): ?><?php echo $this->_tpl_vars['registerText']; ?>
<?php else: ?>Regisztráció<?php endif; ?>">
                            <?php if (! empty ( $this->_tpl_vars['registerText'] )): ?><?php echo $this->_tpl_vars['registerText']; ?>
<?php else: ?><?php echo $this->_tpl_vars['rowLabel']['regisztracio']; ?>
<?php endif; ?>
                        </a>
                    <?php endif; ?>
                    <div class="language-selector">
                        <a href="<?php echo $this->_tpl_vars['CONF']['base_url']; ?>
"><img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/hungary.svg" alt="Magyar nyelv"></a>
                        <a href="<?php echo $this->_tpl_vars['CONF']['base_url']; ?>
en"><img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/angol.svg" alt="Angol nyelv"></a>
                        <a href="<?php echo $this->_tpl_vars['CONF']['base_url']; ?>
ro"><img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/roman.svg" alt="Román nyelv"></a>
                    </div>
                </div>      
                <div class="row justify-content-center justify-content-md-end">
                    <div class="form__item">
                        <form class="form__group" method="get" action="<?php if (! empty ( $this->_tpl_vars['searchLink'] )): ?> <?php echo $this->_tpl_vars['searchLink']; ?>
 <?php endif; ?>">
                            <input type="text" name="<?php echo $this->_tpl_vars['rowLabel']['keresett_szoveg']; ?>
" placeholder="<?php echo $this->_tpl_vars['rowLabel']['kereses']; ?>
..." />
                        </form>
                    </div>
                                    </div>
            </div>
        </div>
    </div>
</header>