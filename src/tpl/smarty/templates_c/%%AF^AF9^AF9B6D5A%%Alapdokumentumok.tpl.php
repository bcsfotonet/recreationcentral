<?php /* Smarty version 2.6.31, created on 2018-11-17 21:47:58
         compiled from page%5CAlapdokumentumok.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Alapdokumentumok.tpl', 2, false),array('insert', 'header', 'page\\Alapdokumentumok.tpl', 3, false),array('insert', 'nav', 'page\\Alapdokumentumok.tpl', 4, false),array('insert', 'headline', 'page\\Alapdokumentumok.tpl', 7, false),array('insert', 'end', 'page\\Alapdokumentumok.tpl', 39, false),)), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>

                        
        <div class="content">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => 'Alapdokumentumok', 'firstli' => "Nyitóoldal", 'firstliurl' => $this->_tpl_vars['CONF']['base_url'], 'secondli' => "Társaság", 'secondliurl' => "", 'selected' => 'Alapdokumentumok')), $this); ?>

            <div class="container">
                <div class="row">
                    <?php if (! empty ( $this->_tpl_vars['tblData'] )): ?>
                        <?php $_from = $this->_tpl_vars['tblData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowData']):
?>
                            <div class="col-12 col-md-6">
                                <div class="document">
                                    <?php if (! empty ( $this->_tpl_vars['rowData']['title'] )): ?>
                                        <span class="document__title"><?php echo $this->_tpl_vars['rowData']['title']; ?>
</span>
                                    <?php endif; ?>
                                    <?php if (! empty ( $this->_tpl_vars['rowData']['lead'] )): ?>
                                        <p class="document__desc">
                                            <?php echo $this->_tpl_vars['rowData']['lead']; ?>

                                        </p>
                                    <?php endif; ?>
                                    <?php if (! empty ( $this->_tpl_vars['rowData']['filename'] )): ?>
                                        <a class="document__download" href="<?php echo $this->_tpl_vars['rowData']['filename']; ?>
">Letöltés</a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; endif; unset($_from); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>