<?php /* Smarty version 2.6.31, created on 2019-02-11 01:23:06
         compiled from content/partners.tpl */ ?>
<hr>
<?php if (! empty ( $this->_tpl_vars['tblPartnerData'] )): ?>
    <div class="container">
        <div class="partners">
            <div class="row">               
                <div class="col-12 col-md-3">
                    <h3 class="partners__title"><?php echo $this->_tpl_vars['rowLabel']['partnereink']; ?>
</h3>
                </div>
                            </div>
            <div class="row justify-content-center align-items-center mt40">
                <div class="col-12">
                    <div class="partners__wrapper">
                        <?php $_from = $this->_tpl_vars['tblPartnerData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowPartnerData']):
?>
                            <a <?php if (! empty ( $this->_tpl_vars['rowPartnerData']['ext_url'] )): ?> href="<?php echo $this->_tpl_vars['rowPartnerData']['ext_url']; ?>
" target="<?php if (! empty ( $this->_tpl_vars['rowPartnerData']['target_value'] )): ?><?php echo $this->_tpl_vars['rowPartnerData']['target_value']; ?>
<?php else: ?>_blank<?php endif; ?>" <?php endif; ?> class="partners__item" title="<?php echo $this->_tpl_vars['rowPartnerData']['name']; ?>
">
                                <img src="<?php echo $this->_tpl_vars['rowPartnerData']['image']; ?>
" alt="<?php echo $this->_tpl_vars['rowPartnerData']['name']; ?>
">
                            </a> 
                        <?php endforeach; endif; unset($_from); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>                
<?php endif; ?>

