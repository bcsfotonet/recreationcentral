<?php /* Smarty version 2.6.31, created on 2019-02-09 01:21:36
         compiled from page%5CIsmeretlen_oldal.tpl */ ?>
<!DOCTYPE html>
<html lang="hu">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
vendor/vendor.css">
        <link rel="stylesheet" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
css/main.css">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/site.webmanifest">
        <link rel="mask-icon" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">
        <meta name="description" content="<?php echo $this->_tpl_vars['strDescription']; ?>
">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/default-skin/default-skin.min.css">
        <title><?php echo $this->_tpl_vars['strTitle']; ?>
</title>
    </head>
    <body>
                
        <div class="container-scroller">
            <div class="container-fluid page-body-wrapper full-page-wrapper">
                <div class="content-wrapper d-flex align-items-center text-center error-page">
                    <div class="row flex-grow w-100">
                        <div class="col-lg-12 mx-auto text-white">
                            <div class="row align-items-center d-flex">
                                <div class="col-lg-6 text-lg-right pr-lg-4">
                                    <h1 class="display-1 mb-0">404</h1>
                                </div>
                                <div class="col-lg-6 error-page-divider text-lg-left pl-lg-4">
                                    <h2>SORRY!</h2>
                                    <h3 class="font-weight-light">A keresett oldal nem található.</h3>
                                </div>
                            </div>
                            <div class="row mt-5">
                                <div class="col-12 text-center mt-xl-2">
                                    <a class="text-white font-weight-medium" href="<?php echo $this->_tpl_vars['CONF']['base_url_lang']; ?>
">Vissza a főoldalra</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content-wrapper ends -->
            </div>
            <!-- page-body-wrapper ends -->
        </div>
    </body>
</html>