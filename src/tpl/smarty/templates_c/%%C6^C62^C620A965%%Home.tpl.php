<?php /* Smarty version 2.6.31, created on 2019-02-11 01:23:05
         compiled from page%5CHome.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Home.tpl', 2, false),array('insert', 'header', 'page\\Home.tpl', 3, false),array('insert', 'nav', 'page\\Home.tpl', 4, false),array('insert', 'headline', 'page\\Home.tpl', 7, false),array('insert', 'sidebar', 'page\\Home.tpl', 33, false),array('insert', 'partners', 'page\\Home.tpl', 35, false),array('insert', 'kert', 'page\\Home.tpl', 37, false),array('insert', 'end', 'page\\Home.tpl', 38, false),array('modifier', 'date_format', 'page\\Home.tpl', 18, false),)), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>
                       
        
    <div class="content">
        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => ($this->_tpl_vars['rowLabel']['kiemelt_hirek']))), $this); ?>

        <div class="two-part">
            <div class="two-part__main">
                <?php if (! empty ( $this->_tpl_vars['tblNewsData'] )): ?>
                    <?php $_from = $this->_tpl_vars['tblNewsData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowNewsData']):
?>
                        <div class="col-12 col-sm-6">
                             <div class="news-item">
                                <img class="news-item__image" src="<?php echo $this->_tpl_vars['rowNewsData']['image']; ?>
" alt="<?php echo $this->_tpl_vars['rowNewsData']['title']; ?>
">
                                <span class="news-item__title"><?php echo $this->_tpl_vars['rowNewsData']['title']; ?>
</span>
                                <?php if (! empty ( $this->_tpl_vars['rowNewsData']['start_date'] )): ?>
                                    <span class="news-item__date"><?php echo ((is_array($_tmp=$this->_tpl_vars['rowNewsData']['start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y/%m/%d %H:%M") : smarty_modifier_date_format($_tmp, "%Y/%m/%d %H:%M")); ?>
<?php if (! empty ( $this->_tpl_vars['rowNewsData']['end_date'] )): ?> - <?php echo ((is_array($_tmp=$this->_tpl_vars['rowNewsData']['end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y/%m/%d %H:%M") : smarty_modifier_date_format($_tmp, "%Y/%m/%d %H:%M")); ?>
<?php endif; ?></span>
                                <?php endif; ?>
                                <?php if (! empty ( $this->_tpl_vars['rowNewsData']['lead'] )): ?>
                                    <p class="news-item__lead">
                                        <?php echo $this->_tpl_vars['rowNewsData']['lead']; ?>

                                    </p>
                                <?php endif; ?>
                                <?php if (! empty ( $this->_tpl_vars['rowNewsData']['url'] )): ?>
                                    <a href="<?php echo $this->_tpl_vars['CONF']['base_url_lang']; ?>
<?php echo $this->_tpl_vars['rowNewsData']['type_url']; ?>
/<?php echo $this->_tpl_vars['rowNewsData']['url']; ?>
" class="news-item__more"><?php echo $this->_tpl_vars['rowLabel']['reszletek']; ?>
</a>
                                <?php endif; ?>
                             </div>
                        </div>
                    <?php endforeach; endif; unset($_from); ?>
                <?php endif; ?>
            </div>
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'sidebar')), $this); ?>

        </div>
        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'partners')), $this); ?>

    </div>
        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'kert')), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>