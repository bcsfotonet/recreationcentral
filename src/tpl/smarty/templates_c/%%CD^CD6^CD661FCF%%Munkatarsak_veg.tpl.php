<?php /* Smarty version 2.6.31, created on 2018-11-17 22:12:22
         compiled from page%5CMunkatarsak_veg.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Munkatarsak_veg.tpl', 2, false),array('insert', 'header', 'page\\Munkatarsak_veg.tpl', 3, false),array('insert', 'nav', 'page\\Munkatarsak_veg.tpl', 4, false),array('insert', 'headline', 'page\\Munkatarsak_veg.tpl', 12, false),array('insert', 'end', 'page\\Munkatarsak_veg.tpl', 55, false),)), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>

                        
    <div class="content">
        <?php if (! empty ( $this->_tpl_vars['tblData']['name'] )): ?>
            <?php $this->assign('selected_name', $this->_tpl_vars['tblData']['name']); ?>
        <?php else: ?>
            <?php $this->assign('selected_name', "Munkatárs"); ?>
        <?php endif; ?>
        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => "Munkatársak", 'firstli' => "Nyitóoldal", 'firstliurl' => $this->_tpl_vars['CONF']['base_url'], 'secondli' => "Társaság", 'secondliurl' => "", 'thirdli' => "Munkatársak", 'thirdliurl' => ($this->_tpl_vars['CONF']['base_url'])."munkatarsak", 'selected' => $this->_tpl_vars['selected_name'])), $this); ?>

        <div class="container">
            <?php if (! empty ( $this->_tpl_vars['tblData'] )): ?>
                <div class="row justify-content-sm-center">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                        <div class="staff">
                            <?php if (! empty ( $this->_tpl_vars['tblData']['image'] )): ?>
                                <a class="staff__img">
                                    <img src="<?php echo $this->_tpl_vars['tblData']['image']; ?>
" alt="<?php echo $this->_tpl_vars['tblData']['name']; ?>
">
                                </a>
                            <?php endif; ?>
                            <span class="staff__name"><?php echo $this->_tpl_vars['tblData']['name']; ?>
</span>
                            <?php if (! empty ( $this->_tpl_vars['tblData']['title'] )): ?>
                                <span class="staff__titulus"><?php echo $this->_tpl_vars['tblData']['title']; ?>
</span>
                            <?php endif; ?>
                            <?php if (! empty ( $this->_tpl_vars['tblData']['email'] )): ?>
                                <span class="staff__mail">
                                    <a href="mailto:<?php echo $this->_tpl_vars['tblData']['email']; ?>
"><?php echo $this->_tpl_vars['tblData']['email']; ?>
</a>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php if (! empty ( $this->_tpl_vars['tblData']['description'] )): ?>
                        <div class="col-12 col-md-8 col-lg-9">
                            <div class="editor-text stat-text">
                                <?php echo $this->_tpl_vars['tblData']['description']; ?>

                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
        
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>