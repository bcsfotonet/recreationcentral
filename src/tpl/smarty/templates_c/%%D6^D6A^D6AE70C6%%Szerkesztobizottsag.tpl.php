<?php /* Smarty version 2.6.31, created on 2018-11-01 22:34:40
         compiled from page%5CSzerkesztobizottsag.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Szerkesztobizottsag.tpl', 2, false),array('insert', 'header', 'page\\Szerkesztobizottsag.tpl', 3, false),array('insert', 'nav', 'page\\Szerkesztobizottsag.tpl', 4, false),array('insert', 'headline', 'page\\Szerkesztobizottsag.tpl', 7, false),array('insert', 'end', 'page\\Szerkesztobizottsag.tpl', 42, false),)), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>

                        
        <div class="content">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => "Szerkesztőbizottság", 'firstli' => "Nyitóoldal", 'firstliurl' => $this->_tpl_vars['CONF']['base_url'], 'secondli' => "Társaság", 'secondliurl' => "", 'selected' => "Szerkesztőbizottság")), $this); ?>

            <div class="container">
                <div class="row">
                    <?php if (! empty ( $this->_tpl_vars['tblData'] )): ?>
                        <?php $_from = $this->_tpl_vars['tblData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowData']):
?>
                            <div class="col-12 col-sm-6 col-md-4 col-lg-3">
                                <div class="staff">
                                    <a class="staff__img" style="cursor:default;">
                                                                                <img src="<?php echo $this->_tpl_vars['rowData']['image']; ?>
" alt="<?php echo $this->_tpl_vars['rowData']['name']; ?>
">
                                    </a>
                                    <span class="staff__name"><?php echo $this->_tpl_vars['rowData']['name']; ?>
</span>
                                    <span class="staff__titulus"><?php echo $this->_tpl_vars['rowData']['title']; ?>
</span>
                                    <span class="staff__mail">
                                        <a href="mailto:<?php echo $this->_tpl_vars['rowData']['email']; ?>
"><?php echo $this->_tpl_vars['rowData']['email']; ?>
</a>
                                    </span>
                                                                                                        </div>
                            </div>
                        <?php endforeach; endif; unset($_from); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>