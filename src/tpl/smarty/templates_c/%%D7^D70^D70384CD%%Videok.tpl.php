<?php /* Smarty version 2.6.31, created on 2019-02-11 01:26:14
         compiled from page%5CVideok.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Videok.tpl', 2, false),array('insert', 'header', 'page\\Videok.tpl', 3, false),array('insert', 'nav', 'page\\Videok.tpl', 4, false),array('insert', 'headline', 'page\\Videok.tpl', 7, false),array('insert', 'end', 'page\\Videok.tpl', 39, false),)), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>

                        
        <div class="content">
            <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => "Videók", 'firstli' => ($this->_tpl_vars['rowLabel']['nyitooldal']), 'firstliurl' => $this->_tpl_vars['CONF']['base_url_lang'], 'secondli' => "Média", 'secondliurl' => "", 'selected' => "Videók")), $this); ?>

            <div class="container">
                <div class="row">
                    <?php if (! empty ( $this->_tpl_vars['tblData'] )): ?>
                        <?php $_from = $this->_tpl_vars['tblData']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowData']):
?>
                            <div class="col-12 col-sm-6 col-md-4">
                                <div class="video">
                                    <?php if (! empty ( $this->_tpl_vars['rowData']['code'] )): ?>
                                        <div class="video__frame">
                                                <iframe width="100%" height="200" src="https://www.youtube.com/embed/<?php echo $this->_tpl_vars['rowData']['code']; ?>
" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (! empty ( $this->_tpl_vars['rowData']['title'] )): ?>
                                        <div class="video__title"><?php echo $this->_tpl_vars['rowData']['title']; ?>
</div>
                                    <?php endif; ?>
                                    <?php if (! empty ( $this->_tpl_vars['rowData']['description'] )): ?>
                                        <div class="video__description"><?php echo $this->_tpl_vars['rowData']['description']; ?>
</div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endforeach; endif; unset($_from); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>