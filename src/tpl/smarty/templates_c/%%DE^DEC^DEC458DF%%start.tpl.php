<?php /* Smarty version 2.6.31, created on 2019-02-11 01:26:14
         compiled from content/start.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'hotjar', 'content/start.tpl', 20, false),)), $this); ?>
<!DOCTYPE html>
<html lang="hu">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
vendor/vendor.css">
        <link rel="stylesheet" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
css/main.css">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/site.webmanifest">
        <link rel="mask-icon" href="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="theme-color" content="#ffffff">
        <meta name="description" content="<?php echo $this->_tpl_vars['strDescription']; ?>
">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/4.1.2/default-skin/default-skin.min.css">
        <title><?php echo $this->_tpl_vars['strTitle']; ?>
</title>
        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'hotjar')), $this); ?>

    </head>
    <body>
        <div class="wrapper">
    