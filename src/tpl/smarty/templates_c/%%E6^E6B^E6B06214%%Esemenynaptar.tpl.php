<?php /* Smarty version 2.6.31, created on 2019-02-11 01:23:01
         compiled from page%5CEsemenynaptar.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Esemenynaptar.tpl', 2, false),array('insert', 'header', 'page\\Esemenynaptar.tpl', 3, false),array('insert', 'nav', 'page\\Esemenynaptar.tpl', 4, false),array('insert', 'headline', 'page\\Esemenynaptar.tpl', 7, false),array('insert', 'end', 'page\\Esemenynaptar.tpl', 34, false),)), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>


    <div class="content">
        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => "Eseménynaptár", 'firstli' => ($this->_tpl_vars['rowLabel']['nyitooldal']), 'firstliurl' => $this->_tpl_vars['CONF']['base_url_lang'], 'secondli' => "", 'secondliurl' => "", 'selected' => "Eseménynaptár")), $this); ?>

        <div class="container">
            <div class="row">
                <div class="col-12 calendar js-calendar">
                    <div class="calendar__header">
                        <div class="calendar__prev js-calendar__prev">
                            <img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/prev.svg" alt="Előző">
                        </div>
                        <div class="calendar__next js-calendar__next">
                            <img src="<?php echo $this->_tpl_vars['CONF']['web_url']; ?>
images/next.svg" alt="Következő">
                        </div>
                        <div class="calendar__month js-calendar__title">&nbsp;</div>
                    </div>
                    <div id="js-calendar__view">
                        &nbsp;
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>