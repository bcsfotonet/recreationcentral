<?php /* Smarty version 2.6.31, created on 2019-02-09 01:21:39
         compiled from page%5CHirek_veg.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('insert', 'start', 'page\\Hirek_veg.tpl', 1, false),array('insert', 'header', 'page\\Hirek_veg.tpl', 2, false),array('insert', 'nav', 'page\\Hirek_veg.tpl', 3, false),array('insert', 'photoswipe', 'page\\Hirek_veg.tpl', 5, false),array('insert', 'headline', 'page\\Hirek_veg.tpl', 14, false),array('insert', 'attachments', 'page\\Hirek_veg.tpl', 49, false),array('insert', 'end', 'page\\Hirek_veg.tpl', 52, false),array('modifier', 'truncate', 'page\\Hirek_veg.tpl', 9, false),array('modifier', 'date_format', 'page\\Hirek_veg.tpl', 34, false),)), $this); ?>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'start', 'title' => "Közép-Kelet-Európai Rekreációs Társaság", 'description' => "Lorem ipsum dolor sit amet, consectetur adipisicing elit.")), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'header')), $this); ?>

<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'nav')), $this); ?>

<?php if (! empty ( $this->_tpl_vars['tblData']['gallery_id'] )): ?>
    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'photoswipe')), $this); ?>

<?php endif; ?>
<div class="content">
    <?php if (! empty ( $this->_tpl_vars['tblData']['title'] )): ?>
        <?php $this->assign('selected_title', ((is_array($_tmp=$this->_tpl_vars['tblData']['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30) : smarty_modifier_truncate($_tmp, 30))); ?>        
    <?php else: ?>
        <?php $this->assign('selected_title', "Hír"); ?>
    <?php endif; ?>    

    <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'headline', 'title' => ($this->_tpl_vars['rowType']['name']), 'firstli' => ($this->_tpl_vars['rowLabel']['nyitooldal']), 'firstliurl' => $this->_tpl_vars['CONF']['base_url_lang'], 'secondli' => "Média", 'secondliurl' => "", 'thirdli' => ($this->_tpl_vars['rowType']['name']), 'thirdliurl' => ($this->_tpl_vars['CONF']['base_url_lang']).($this->_tpl_vars['rowType']['url']), 'selected' => $this->_tpl_vars['selected_title'])), $this); ?>
   
    <?php if (! empty ( $this->_tpl_vars['tblData'] )): ?>
        <div class="container">         
            <div class="row">
                <?php if (! empty ( $this->_tpl_vars['tblData']['image'] )): ?>
                    <div class="col-12 col-lg-4 text-center">
                        <img class="news-item__image" src="<?php echo $this->_tpl_vars['tblData']['image']; ?>
" alt="<?php echo $this->_tpl_vars['tblData']['title']; ?>
">
                    </div>
                <?php endif; ?>
                <div class="col-12 col-lg-8">
                    <span class="news-item__title"><?php echo $this->_tpl_vars['tblData']['title']; ?>
</span>
                    <?php if (! empty ( $this->_tpl_vars['tblData']['start_date'] )): ?>
                        <span class="news-item__date"><?php echo ((is_array($_tmp=$this->_tpl_vars['tblData']['start_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y/%m/%d %H:%M") : smarty_modifier_date_format($_tmp, "%Y/%m/%d %H:%M")); ?>
<?php if (! empty ( $this->_tpl_vars['tblData']['end_date'] )): ?> - <?php echo ((is_array($_tmp=$this->_tpl_vars['tblData']['end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y/%m/%d %H:%M") : smarty_modifier_date_format($_tmp, "%Y/%m/%d %H:%M")); ?>
<?php endif; ?></span>
                    <?php endif; ?>
                    <?php if (! empty ( $this->_tpl_vars['tblData']['lead'] )): ?>
                        <p class="news-item__lead">
                            <?php echo $this->_tpl_vars['tblData']['lead']; ?>

                        </p>
                    <?php endif; ?>
                    <?php if (! empty ( $this->_tpl_vars['tblData']['description'] )): ?>
                        <div class="editor-text stat-text">
                            <?php echo $this->_tpl_vars['tblData']['description']; ?>

                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'attachments')), $this); ?>

    <?php endif; ?>       
</div>
<?php require_once(SMARTY_CORE_DIR . 'core.run_insert_handler.php');
echo smarty_core_run_insert_handler(array('args' => array('name' => 'end')), $this); ?>