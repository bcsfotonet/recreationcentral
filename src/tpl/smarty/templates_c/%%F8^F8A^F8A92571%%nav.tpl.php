<?php /* Smarty version 2.6.31, created on 2019-02-11 01:26:14
         compiled from content/nav.tpl */ ?>
<div class="nav">
    <?php if (! empty ( $this->_tpl_vars['tblMainMenu'] )): ?>
        <ul class="nav__list">
            <?php $_from = $this->_tpl_vars['tblMainMenu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowMainMenu']):
?>
                <?php if (! empty ( $this->_tpl_vars['rowMainMenu']['name'] )): ?>
                    <li class="nav__li <?php if (! empty ( $this->_tpl_vars['rowMainMenu']['child'] )): ?>nav__parent-nav js-sub-nav<?php endif; ?>">
                        <a <?php if (! empty ( $this->_tpl_vars['rowMainMenu']['url'] )): ?>href="<?php echo $this->_tpl_vars['rowMainMenu']['url']; ?>
" target="<?php if (! empty ( $this->_tpl_vars['rowMainMenu']['target_value'] )): ?><?php echo $this->_tpl_vars['rowMainMenu']['target_value']; ?>
<?php else: ?>_self<?php endif; ?>"<?php endif; ?>><?php echo $this->_tpl_vars['rowMainMenu']['name']; ?>
</a>
                        <?php if (! empty ( $this->_tpl_vars['rowMainMenu']['child'] )): ?>
                            <ul class="nav__sub-nav js-sub-list">
                                <?php $_from = $this->_tpl_vars['rowMainMenu']['child']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowChildMenu']):
?>
                                    <?php if (! empty ( $this->_tpl_vars['rowChildMenu']['name'] )): ?>
                                        <li class="nav__li">
                                            <a <?php if (! empty ( $this->_tpl_vars['rowChildMenu']['url'] )): ?>href="<?php echo $this->_tpl_vars['rowChildMenu']['url']; ?>
" target="<?php if (! empty ( $this->_tpl_vars['rowChildMenu']['target_value'] )): ?><?php echo $this->_tpl_vars['rowChildMenu']['target_value']; ?>
<?php else: ?>_self<?php endif; ?>"<?php endif; ?>><?php echo $this->_tpl_vars['rowChildMenu']['name']; ?>
</a>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                <?php endif; ?>
            <?php endforeach; endif; unset($_from); ?>
        </ul>
    <?php endif; ?>
</div>