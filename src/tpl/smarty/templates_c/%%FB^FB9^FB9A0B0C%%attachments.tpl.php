<?php /* Smarty version 2.6.31, created on 2019-02-09 01:21:39
         compiled from content/attachments.tpl */ ?>
<div class="container">
    <div class="row attachment justify-content-center">
        <?php if (! empty ( $this->_tpl_vars['tblGallery'] )): ?>
            <div class="col-12 col-md-6">
                <div class="attachment__title">Galéria</div>
                <div class="attachment__gallery gallery" itemscope itemtype="https://schema.org/ImageGallery">
                    <?php $_from = $this->_tpl_vars['tblGallery']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['numIdx'] => $this->_tpl_vars['rowGallery']):
?>
                        <figure itemprop="associatedMedia" itemscope class="gallery__item" itemtype="https://schema.org/ImageObject">
                            <a href="<?php echo $this->_tpl_vars['rowGallery']['original_image']; ?>
" itemprop="contentUrl" id="gallery-<?php echo $this->_tpl_vars['rowGallery']['gallery_id']; ?>
" data-size="<?php echo $this->_tpl_vars['rowGallery']['original_size']; ?>
">
                                <img src="<?php echo $this->_tpl_vars['rowGallery']['big_image']; ?>
" itemprop="thumbnail" alt="" />
                            </a>
                        </figure>
                    <?php endforeach; endif; unset($_from); ?>
                </div>
            </div>
        <?php endif; ?>
                <?php if (! empty ( $this->_tpl_vars['tblVideo']['code'] )): ?>
            <div class="col-12 col-md-6">
                <div class="attachment__title">Videó</div>
                <div class="attachment__video">
                    <iframe src="https://www.youtube.com/embed/<?php echo $this->_tpl_vars['tblVideo']['code']; ?>
" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    </div>
            </div>
        <?php endif; ?>
        <?php if (! empty ( $this->_tpl_vars['tblDocument'] )): ?>
            <div class="col-12">
                <div class="attachment__title">Dokumentumok</div>
                <?php $_from = $this->_tpl_vars['tblDocument']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rowDocument']):
?>
                    <div class="document">
                        <?php if (! empty ( $this->_tpl_vars['rowDocument']['title'] )): ?>
                            <span class="document__title"><?php echo $this->_tpl_vars['rowDocument']['title']; ?>
</span>
                        <?php endif; ?>
                        <?php if (! empty ( $this->_tpl_vars['rowDocument']['lead'] )): ?>
                            <p class="document__desc">
                                <?php echo $this->_tpl_vars['rowDocument']['lead']; ?>

                            </p>
                        <?php endif; ?>
                        <a class="document__download" href="<?php echo $this->_tpl_vars['rowDocument']['filename']; ?>
">Letöltés</a>
                    </div>
                <?php endforeach; endif; unset($_from); ?>
            </div>
        <?php endif; ?>
    </div>
</div>