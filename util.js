'use strict';
var argv = require('yargs').argv;
var path = require('path');

module.exports = {
    env:           argv.env || 'dev',
    availableEnvs: ['dev', 'prod'],

    // Front elérési útvonalok
    srcPath:            './src/front-sb',
    destPath:           './app/front-sb',
    jsDestDir:          './app/web/js',
    cssDestDir:         './app/web/css',
    vendorDestDir:      './app/vendor',

    // Admin elérési útvonalak
    adminSrcPath:       './src/admin-sb',
    adminDestPath:      './app/admin-sb',
    adminJsDestDir:     './app/admin/web/js',
    adminCssDestDir:    './app/web/admin/css',
    adminVendorDestDir: './app/admin/vendor',

    src: function(path) {
        return this.srcPath + '/' + path;
    },

    adminSrc: function(path) {
        return this.adminSrcPath + '/' + path;
    },

    dest: function(path) {
        return this.destPath + '/' + path;
    },

    adminDest: function(path) {
        return this.adminDestPath + '/' + path;
    },

    isEnv: function(env) {
        return this.env === env;
    },

    checkIfEnvIsValid: function() {
        if (this.availableEnvs.indexOf(this.env) === -1) {
            throw 'The "' + this.env + '" environment is not supported in this gulpfile.';
        }
    },

    handleError: function(error) {
        // require('node-notifier').notify({
        //     'title': 'Compile Error',
        //     'message': path.basename(error.fileName) + ':' + error.lineNumber + ' "' + error.message + '"'
        // });

        console.log(error);

        this.emit('end'); // Keep gulp from hanging on this task
    }
};
